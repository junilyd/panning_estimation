The panning parameters discussed in this report is a product of the mixing process applied in sound studios. To enhance the sound quality and to ease the virtual perceived separation of sound sources in a stereo mixture the sound engineer can apply various effects, such as amplitude and delay panning. Other effects such as reverb, equalization and dynamic effects are usually also applied, but are of no interest in the remaining report. Amplitude and delay panning is exactly the two parameters that we estimate in this report, since they carry spatial information that is equivalent to direction and positioning in a real geometric setup. 
%
\subsection{Delay Panning} % (fold)
\label{sub:delay_panning}
% subsection delay_panning (end)
The delay panning parameter is directly related to the time delay that humans experience when a sound signal is propagated through the air from some source and received at each ear at separate time instances. Such a delay changes the perceived direction of the sound source~\cite{blauert}. It has been shown that a constant time delay to one of the speakers is frequency dependent in terms of virtual source positioning~\cite{cooper1987problems}. Though amplitude panning is the traditional post processing ``way to go'' for sound engineers, delays are being added as part of post processing mixing procedure both to correlate phases of microphones and to change directivity of sources. Lastly, delays longer than 1 ms can be applied to achieve depth and dimension, by virtually placing the sound source mostly in the channel where the signal arrives first~\cite{katz}. 
%
\subsection{Amplitude Panning} % (fold)
\label{sec:amplitude_panning}
Amplitude panning is the general method for altering the perceived direction of a sound source in a sound field between two or more loud speakers. Amplitude panning is an approximation of source localisation and its application ranges from stereophonic amplifiers to multichannel speaker setup and professional multi-channel mixing desks, DAW's etc. Most often the user/engineer of a mixing desk can configure the the perceived direction of each individual sound source in the mix by turning one knob, attached to a trim pot that controls the signal voltage level to each speaker output. If the desk is digital, the user has a similar digital knob or slider interface.%
%
\tikzexternaldisable
 \input img/Speaker_Setup.tex
\tikzexternalenable
%
Amplitude panning can be applied to multi-speaker setups, while the most common speaker configuration is a stereo setup, consisting of a left and a right speaker, with two audio channels being played back (one for each loudspeaker), whether it is a home audio hi-fi system, PA (Public Adress) system, headphone system etc. The stereophonic configuration is shown in Figure~\ref{fig:Speaker_Setup}, where the listener is placed in orego, fronting the $x$-axis in ($x=y=0$). Amplitude panning in the sterephonic configuration is explained in the following sub section.
%
%   
%%
\subsection{Stereophonic Amplitude Panning} % (fold)
\label{sec:stereophonic_amplitude_panning}
Figure~\ref{fig:Speaker_Setup} shows the stereophonic sound configuration patented by Blumlein\cite{blumlein}. The listener is situated equidistant to each speaker in orego. The listener perceives an illusion of an auditory event, that is placed in a specific point on a two dimensional arc between the two speakers. The auditory event is moved by changing the signal amplitudes of the signal in the left and right channel. Amplitude panning is described by Ville Pulkki~\cite{pulkki1} in a vector based framework that allows two- and three dimensional speaker setups. Amplitude panning can be formulated at time $t$, by applying a signal $x(t)$ to both loudspeakers with different amplitudes, and gain factors for left and right channel respectively. In general the signal $x_i(t)$ is then  
\begin{equation}
    	x_i(t) = g_i x(t), \quad\quad  i=1,2,\cdots,N
 \end{equation}    
 where $x_i(t)$ is the signal applied to the $i^\textup{th}$ loudspeaker and $g_i$ is the gain factor of the corresponding channel and $N=2$ is the number of speakers in stereo configuration. While the virtual source is moving along the arc, the distance to the the listener should be constant. For the stereophonic configuration the vectorial distance of the gain factors $g_1$ and $g_2$ equals a constant $C$ 
 \begin{equation}
  	g_1^2+g_2^2 = C	
  \end{equation} 
  The relation between the gain factors and the perceived virtual source direction has been derived for panning in the stereophonic configuration by Bauer~\cite{bauer} as the ``stereophonic law of sines'', where the acoustic shadow of the head is not taken in to account and the sine law is assumed valid at all frequencies. For the sine law, the listener is situated symmetrically between the speakers in orego, facing along the $x$-axis in Figure~\ref{fig:Speaker_Setup}. The stereophonic sine law is described by the ratio of the difference and sum of the gain factors as,
 \begin{equation}
     	\frac{\sin{\phi}}{\sin{\phi_0}} = \frac{g_1-g_2}{g_1+g_2}
\end{equation} 
where $\phi$ is the perceived angle and $\phi_0$ is the speaker base angle. It is required that $0^\circ<\phi_0<90^\circ$, $-\phi_0\leq\phi\leq\phi_0$ and $g_1, g_2 \in [0,1]$. An extension of the sine law is the tangent law, originally proposed by Bernfeld~\cite{bernfeld1973attempts} as 
\begin{equation}
 	     	\frac{\tan{\phi}}{\tan{\phi_0}} = \frac{g_1-g_2}{g_1+g_2}
 \end{equation} 
 The tangent law behaves similar to the sine law with very small difference, taking some of the listeners head complexity into account. Ville Pulkki~\cite{pulkki1} formulates the vector based approach as a reformulation of the tangent law, called the vector based amplitude panning (VBAP). Figure~\ref{fig:Speaker_Setup_Vectors} visualizes the vector based framework of the stereo configuration, that is used in the remaining of this report to describe the estimated amplitude panning angle, shown in results and in figures.
%
%
\subsubsection{Gain Vector Relation to Virtual Sound Source Positioning} % (fold)
 \label{sec:virtual_sound_source_positioning_using_vector_base}
\tikzexternaldisable
\input img/Speaker_Setup_Vectors.tex
\tikzexternalenable
To ease the understanding of the amplitude panning parameter it is convenient for the human reader to consider the parameter as a perceived angle in a carthesian coordinate system, since a music listener is normally placed in front of two speakers as mentioned in Section~\ref{sec:stereophonic_amplitude_panning}. To present the gain ratios as angles we apply the stereo vector base virtual sound source positioning~\cite{pulkki1}. A backwards amplitude panning algorithm serves the purpose of estimating the gain parameters. As visualized in Figure\ref{fig:Speaker_Setup}, each loudspeaker has a base angle $\phi_0=\pm45^\circ$ to the $x$-axis direction that the listener is facing towards; the listener is situated equidistant to each speaker in ($x=y$). The angle $\phi$ describes the virtual source position respective to the $x$-axis. The trigonometric functions are used for the panning gain since they fit the unit circle, thus they retain unity power along an arc as $1 = \cos^2+\sin^2$. The gains are then
\begin{equation}	
	g_x = \cos\theta
\end{equation}\label{eq:gx}

\begin{equation}
	g_y = \sin\theta
\end{equation}\label{eq:gy}
where $\theta = \phi+\phi_0$. 
If we define a loudspeaker base matrix $\textbf{L}$
\begin{equation}
 	\textbf{L} = [\textbf{l}_1 \, \textbf{l}_2]^T
 \end{equation} 
 consisting of two unit length loudspeaker vectors $\textbf{l}_1 = [l_{11} l_{12}]^T$ and $\textbf{l}_2 = [l_{21} l_{22}]^T $ pointing toward each speaker. In Figure~\ref{fig:Speaker_Setup_Vectors}, the unit vector $\textbf{p}$ points towards the virtual source as a linear combination of the gained loudspeaker vectors
 \begin{equation}
 	\textbf{p}^T = \textbf{g}\textbf{L}
 \end{equation}
 This equation can be solved for the gain vector, by applying the inverse loudspeaker base matrix
 \begin{equation}
 	\textbf{g} = \textbf{p}^T \textbf{L}^{-1}  
 \end{equation}
 The loudspeaker base matrix $\textbf{L}$ is unitary and $\textbf{L}^{-1}$ exists under the conditions $0^\circ<\phi_0<90^\circ$, $-\phi_0\leq\phi\leq\phi_0$ and $g_1, g_2 \in [\ 0,1]\ $. Finally, we can estimate the panning angle $\hat{\theta}$ as 
 \begin{equation}
 	\hat{\theta} = \arctan{\frac{p(1)}{p(2)}}
 \end{equation}
 The amplitude panning angle applied to sources in a stereo mixture, can be estimated as shown from the obtained gain factors. The trigonometric functions used in this computation, estimates within the domain of the loudspeaker base matrix $\textbf{L}$ with a span of $90^\circ$. In professional studios the aperture of loudspeakers is typically $60^\circ$. However, this relation between to the tangent law is linear and is simply solved by normalization to a wider domain by multiplication.
 This was a description of the panning parameters that is the subject of estimation in the following thesis. The panning parameters are used for source identification, which means that we estimate the number of sources in the stereophonic mixture, using these. 
 In the following sections the signal model and the associated assumptions are defined.
 %