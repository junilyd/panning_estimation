\section{Signal Model} % (fold)
\label{sec:signal_model}
In this section, the signal model and assumptions are introduced. Consider an M-channel music mixture consisting of $K$ unknown sources corrupted by white Gaussian noise at time instant $n$. The data in the $m^{\textup{th}}$ channel is represented as $\mathbf{x}_m(n) \in {\rm I\!R}^N$,  
\begin{equation}\label{eq:mixture1}
	\mathbf{x}_m(n) = [x_m(n)\quad x_m(n+1)\quad \dots \quad x_m(n+N-1)]^T
\end{equation}
for $m=1,\dots,M$. The signals captured by channel $m$, relating to the $k^{\textup{th}}$ source are attenuated by gain coefficient $g_{m,k}$ and delayed by $\tau_{m,k}$ depending on their perceptional virtual positioning, given by the panning parameters. The signal mixture is modelled as a linear superposition of $K$ attenuated and delayed sources embedded in noise $\mathbf{e}_{m,k}(n)$,
 \begin{equation}
 	\mathbf{x}_m(n) = \sum_{k=1}^K g_{m,k} \mathbf{s}_k(n-f_s\tau_{m,k}) + \mathbf{e}_{m,k}(n) 
 \end{equation}\label{eq:mixture}
where $g_{m,k}$ and $\tau_{m,k}$ are the attenuation and delay applied to the source $\mathbf{s}_k(n)$, respectively and $f_s$ is the sampling frequency.
Considering stereophonic mixtures with $M=2$ for a stereo loudspeaker setup, amplitude panning is the traditional procedure~\cite{bauer,pulkki1} for virtual source positioning. In the post-processing of a every music production, delays can be added to enhance the spatial perception~\cite{blauert}. The trigonometric functions are often used for the panning attenuation because they induce a constant perceived distance between listener and the virtual source, described by $1 = \cos^2+\sin^2$. The gains for channel $m$ are expressed as~\cite{pulkki2},
\begin{equation}
	g_m = 
	\begin{cases}
    	\cos\theta_k,    & \text{for } m=1\\
    	\sin\theta_k,    & \text{for } m=2
	\end{cases}
\end{equation}
where $\theta = \phi+\phi_0$ is a sum of the perceived angle $\phi$ and the speaker base angles $\pm\phi_0 = 45^\circ$. Under the conditions $0^\circ<\phi_0<90^\circ$, $-\phi_0\leq\phi\leq\phi_0$ and $g_1, g_2 \in [\ 0,1]\ $ the gains can be expressed as,
\begin{equation}
     \mathbf{g}_k = \mathbf{p}_k \mathbf{L}^{-1}  
\end{equation}
where the unit-vector $\mathbf{p}$ points towards the virtual source with $\mathbf{L}$ as a unitary loudspeaker base matrix. 
% The amplitude panning angle $\hat{\theta}_k$ of the $k\text{th}$ source is found as, 
% \begin{equation}
%  	\hat{\theta}_k = \arctan{\frac{p_k(1)}{p_k(2)}}
% \end{equation} 
%
%Since all mixing parameters are applied as post-processing of the mixture, there is no sound radiation in this model. 
For the stereophonic mixture ($M=2$), we simplify notation by modelling attenuation and delay parameters as ratios between the frequency representations of active sources in the two channels. 
\subsection{Estimating the Panning Parameters} % (fold)
\label{sub:estimating_the_panning_parameters}
% subsection estimating_the_panning_parameters (end)
When only source $k$ is active, the frequency representation in each stereophonic channel is, 
% OLD x. full DFT
%\begin{equation}
%  	S_{1,k}(\omega) = \sum_{n=1}^N \mathbf{s}_k(n)e^{-j\omega l n}, 
%  \end{equation}
% \begin{equation}
%  	S_{2,k}(\omega) = \sum_{n=1}^N \gamma_k \mathbf{s}_k(n)e^{-j\omega l n \delta_k}, 
%  \end{equation} for $l = {0,\dots,N-1}$, 
\begin{equation}
 	S_{1,k}(\omega) = \sum_{n=1}^N s_k(n)e^{-j\omega n}, 
 \end{equation}
\begin{equation}
 	S_{2,k}(\omega) = \sum_{n=1}^N \gamma_k s_k(n)e^{-j\omega n \delta_k}, 
 \end{equation}
$\omega$ is the frequency grid, $\delta_{k}=f_s\tau_{k}$ is the relative delay of source $k$ between the channels and $\gamma_{k}$ is the relative attenuation factor corresponding to the ratio of attenuation of source $k$ between the channels. 
The panning parameters ${\gamma}_k $ and ${\delta}_k$, that are associated with active sources in each frequency point can be computed as,
\begin{equation}
 	(\gamma_k, {{\delta_k}}) = \left(\left|\frac{S_{2,k}(\omega)}{S_{1,k}(\omega)}\right|, \frac{1}{\omega} \angle \frac{S_{1,k}(\omega)}{S_{2,k}(\omega)}\right) 
 \end{equation} 
where we must ensure that,
\begin{equation}\label{eq:narrowband}
	|\omega_{\textup{max}}\delta_{\textup{max}}|<\pi  	
\end{equation}  
to avoid phase ambiguity. Our aim is to estimate the panning parameters $(\gamma, {{\delta}})$ for all $K$ sources, along with an optimal segment length $N$, given only the stereophonic mixture in (\ref{eq:mixture1}). The $k\text{th}$ panning parameter is associated with only the $k\text{th}$ source component, under the assumption that only one source is dominant at each frequency point. This is described by the approximate disjoint orthogonality expressed as~\cite{rickardwdisjoint},
\begin{equation}
	S_{1,k}(\omega)S_{1,i}(\omega) \approx0 \quad \forall \omega, k\neq i
\end{equation}\label{eq:orth}
Subject to this assumption, we apply a segmentation of the signal $\mathbf{x}_m(n)$ into segments of size $N$, that provides an improved separation of the clusters in (\ref{eq:clusters}). The optimal segmentation scheme is described in Section~\ref{sec:segmentation}. The estimated amplitude and delay ratios, which often is refered to as the measurement vectors, are described from the spectral content of each channel in the stereophonic mixture as,
\begin{equation}\label{eq:clusters}
 	 (\hat{\symvec{\gamma}}, {\symvec{\hat{\delta}}}) = \left(\left|\frac{X_{2}(\omega)}{X_{1}(\omega)}\right|, \frac{1}{\omega} \angle \frac{X_{1}(\omega)}{X_{2}(\omega)}\right) 
 \end{equation}
where $X_m(\omega)$ is the discrete Fourier transform of $\mathbf{x}_m(n)$. In this domain $K$ is unknown and we can expect the parameters to cluster in some form. Music mixtures often have a long duration of several minutes and we assume that such mixtures have stationary panning parameters throughout the full mixture i.e. a 3 minute song. We will collect measurement vectors and perform segmentation to select parts of the signal that carries relevant information of the measurement vectors. A great part of the noisefloor in the spectrum is removed which also lowers computional complexity. We define an indicator function $b(\omega)$ as,
\begin{equation}
	b(\omega)  	
	\begin{cases}
    	1,    & |X_1(\omega)||X_2(\omega)|>|\mathbf{X}_1|^T|\mathbf{X}_2|/N)\\
    	0,    & \text{otherwise}
	\end{cases}	
\end{equation}\label{eq:threshold}  
where $X_m(\omega)$ is the pre-whitened DFT of $\mathbf{x}_m(n)$. It is possible to pick a specific number of measurement vectors by increasing the threshhold on the indicator function and improve on computational complexity. 
\forceindent This was a description of the signal model and the main assumptions in this contex. We will end this chapter by introducing a time-panning domain visualization, which we call the panogram. The next chapter will explain the clustering of measurement vectors.
%
%
\subsection{Visualizing the Amplitude Angle as a Panogram} % (fold)
 \label{sub:visualizing_the_panogram}
 A visual output of the panning angle can be used to identify the various sources in a stereo mix based on their panning coefficient. This can be acomplished via the amplitude panning ratio in (\ref{eq:clusters}). The computation is very fast and the output is shown in Figure~\ref{fig:panogram_trumpet_horn} for a multi-pitch mixture of two instruments, trumpet and horn, playing the notes C4 (262 Hz) and F\#4 (370 Hz), respectively. This specific mixture is also used in an experiment in~\cite{DBLP:conf/eusipco/HansenJC16}. 
 \begin{figure}[htbp]
 	\centering
 	\includegraphics[width=0.65\textwidth]{img/trumpet_horn_mixture.pdf}
 	\caption{Panogram of a multi-pitch mixture of two instruments, trumpet and horn.}
 	\label{fig:panogram_trumpet_horn}
 \end{figure}
 The algorithm for the panogram in Figure~\ref{fig:panogram_trumpet_horn} is based on searching through the power ratio of the absolute discrete Fourier transform of the two stereo channels. 
 % By realizing that,
 % \begin{equation}
 % 	\frac{g_x}{g_y} = \tan{\theta}
 % \end{equation}
 % where $g_x$ and $g_y$ are defined in (\ref{eq:gx}) and (\ref{eq:gy}). The ratio expressed as $\tan{\theta}$ can be considered as a search space given by the aperture of the loudspeakers as a function of $\theta$. 
 Consider a stereo input signal $\symvec{x}(n)$ consisting of both $x_1(n)$ and $x_2(n))$ at time instant $n$. At each time instant we compute the amplitude ratio $\gamma_n(\omega)$ using (\ref{eq:clusters}) and lastly marginalize by summing over all delays and at each time instant $n$, the panogram $p(\theta)$ is a vector function of $\theta$ and can be expressed the power at each panning angle. More Panograms can be found in Appendix~\ref{sec:estimation_the_amplitude_panning_angle}. The visual inspection and manual peak finding in an objective function or histogram created from time-frequency domain ratios, is used within the research area of blind source separation~\cite{rickardwdisjoint,rickardBSS1,rickardBSS2} and also used for channel upmix texhniques~\cite{avendano}, however the aim in this thesis is to automatically estimate the panning parameters. Hence, we do not consider the approach of BSS and resynthesis of source signals.\\
 In this chapter the signal model and assumptions has been defined. The measurement space has been described as consisting of stereophonic panning parameters that we could expect to cluster in some form. Therefore, we continue in the next chapter with the definition of the approach that is utilized for clustering algorithm, which is based on the Bayesian posterior modelling. Therefore, we start by defining the clustering problem as a likelihood description.

 % subsection visualizing_the_panogram (end)