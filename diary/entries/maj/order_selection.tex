One advantage of the mixture model approach to clustering is that it allows the use of approximate Bayes factors to compare models. A thorough comparison of Bayes factors can be read in~\cite{bayes_factors}.
The model order selection and the segmentation can be done with a \textit{maximum} a posteriori (MAP) criterion. The MAP estimator is,
\begin{equation}\label{eq:MAP-1}
  	\hat{\symvec{\theta}}_\text{MAP} = \argmax{\symvec{\theta}}\{\ln p(\x|\symvec{\theta})\ + \ln p(\symvec{\theta})\}
\end{equation}
where $p(\theta)$ is the prior on the parameters and $\x$ is the observed data. We will introduce the MAP criterion in the following.

There exists several approaches for finding a solution to the model order estimate. Two of these are very often used~\cite{Multipitch,DBLP:journals/tsp/Djuric98}, namely the AIC and the MDL, the latter of which formally coincides with the BIC~\cite{fraleyR98}. In the following we will describe the MDL as a special case MAP criterion in the following section. The AIC is given as~\cite{DBLP:journals/tsp/Djuric98}, 
\begin{equation}
	\M_s = \argmin{\M_k} \{- \ln p(\x|\hat{\symvec{\theta}},\M_k) + N_p\}
\end{equation}
The MDL is,
\begin{equation}
		\M_s = \argmin{\M_k} \bigg\{- \ln p(\x|\hat{\symvec{\theta}},\M_k) + \frac{N_p}{2}\ln N\bigg\}
\end{equation}
%
where $\M_s$ is the selected model, $\x$ is the observed measurement vector, $p(\x|\hat{\symvec{\theta}},\M_k)$ is the probability density function of the data given the model parameters and the model, $\symvec{\theta}$ is the parameter vector and $\hat{\symvec{\theta}}$ is the maximum likelihood of $\symvec{\theta}$ and $N_p$ is the dimension of $\symvec{\theta}$. 
%
%
\subsection{The Asymptotic MAP criterion} % (fold)
\label{sec:clustering_and_panning_estimation}
%Maybe USE Stoica Model Order Selection, A Review of information criterion rules
%Maybe USE Jesper Kjær et al. paper i dropbox
% \subsection{Maximum A Posteriori (MAP)}
% \label{sub:maximum_a_posteriori_map_}% (fold)
% subsection maximum_a_posteriori_map_ (end)
The principle of the MAP is choosing the model $\M$ that maximizes the posterior probability given the observed data $\x$,
\begin{equation}\label{eq:map1}
	\Mest=\argmax{\M}p(\M|\x)
\end{equation}
%
expressed by using Bayes method,
%
\begin{equation}\label{eq:map2}	
	\Mest=\argmax{\M}\frac{p(\x|\M)p(\M)}{p(\x)}
\end{equation}
%
Choosing a uniform prior $p(\M)$ to not favour any model beforeh and noting that once $\x$ is observed $p(\x)$ is constant and the MAP model reduces to the likelihood of the observed data given the model,
%
\begin{equation}\label{eq:map3}
	\Mest=\argmax{\M}p(\x|\M)
\end{equation}
%
where the likelihood is dependent on the parameters, $\boldsymbol{\theta}$. In the Bayesian framework we obtain the marginal density of the measurents given the model, by integrating the parameters out~\cite{Multipitch},
%  
\begin{equation}\label{eq:map4}
	p(\x|\M)=\int_{\Theta} p(\x|\boldsymbol{\theta},\M)p(\boldsymbol{\theta}|\M)d\boldsymbol{\theta}
\end{equation}
%
The asymptotic approximation to this integral is found by assuming high amounts of data, when the most significant peaks occur in the likelihood function around the maximum likelihood estimates $\hat{\boldsymbol{\theta}}$.~\eqref{eq:map4} becomes equal to~\cite{DBLP:journals/tsp/Djuric98},
\begin{equation}\label{eq:map5}
	p(\x|\M)=(2\pi)^{N_p/2}\det\left(\widehat{H}\right)^{-1/2}p(\x|\hat{\boldsymbol{\theta}},\M)p(\hat{\boldsymbol{\theta}}|\M)
\end{equation}
%
where $\widehat{H}$ is the Hessian of the log-likelihood function when evaluated at the $\hat{\boldsymbol{\theta}}$,
\begin{equation}\label{eq:map6}
	\widehat{\mathbf{\mathcal{H}}}=-\frac{\partial^{2}\ln(p(\x|\boldsymbol{\theta},\M)}{\partial\boldsymbol{\theta}\partial\boldsymbol{\theta}^{T}}\Bigg|_{\boldsymbol{\theta}=\hat{\boldsymbol{\theta}}}
\end{equation}
%
By neglecting terms of order $\mathcal{O}(1)$, the assymptotic MAP expression is found by taking the negative logarithm of~\eqref{eq:map5}, where the term $2\pi^{N_p/2}$ can be assumed constant for asymptotic signal length $N$, while a weak prior on $p(\symvec{\theta}|\M)$ has been used~\cite{DBLP:journals/tsp/Djuric98} to obtain the MAP expression~\cite{Multipitch},
\begin{equation}\label{eq:map7}
	\Mest=\argmin{\M} \bigg\{-\ln p(\x|\hat{\boldsymbol{\theta}},\M)+\frac{1}{2}\textrm{ln det}(\widehat{\mathbf{\mathcal{H}}})\bigg\}
\end{equation}
%
where the first term is the log-likelihood and the last term is the penalty added. The first term of the criterion decreases when the complexity of the model increases, and by contrast, the second term increases and acts as a penalty for using additional parameters to model the data. The penalty term is found by noting that the Hessian in~\eqref{eq:map6} can be replaced by the Fisher information matrix since the error it introduces is smaller than the neglected terms of order $\mathcal{O}(1)$~\cite{Multipitch,DBLP:journals/tsp/Djuric98}.
The Hessian is then,
\begin{equation}\label{eq:map8}
	\widehat{\mathbf{\mathcal{H}}}\approx-\mathbf{\textrm{E}}\left\{ \frac{\partial^{2}\ln(p(\x|\boldsymbol{\theta})}{\partial\boldsymbol{\theta} \partial\boldsymbol{\theta}^{T}}\right\}\Bigg|_{\boldsymbol{\theta}=\hat{\boldsymbol{\theta}}}
\end{equation}
%
Under the assumptions of the observed measurement being real, independent and identically distributed, we can write,
\begin{equation}
	\det{\widehat{\mathbf{\mathcal{H}}}} = \mathcal{O}(N^{\frac{N_p}{2}})
\end{equation}
The interested reader can find specific details on this assumption in~\cite{DBLP:journals/tsp/Djuric98}. 
The expression in~\eqref{eq:map7} then reduces to,
\begin{equation}\label{eq:MAP_reduced}
	\Mest=\argmin{\M} -\ln p(\x|\hat{\boldsymbol{\theta}},\M)+\frac{N_p}{2} \ln N
\end{equation}
which is the MDL that formally coincides with the BIC. For the case of the multivariate Gaussian distribution with arbitrary covariance $N_p=d+d(d+1)/2$, where $d$ is the dimensionality of the feature space. The expression in~\eqref{eq:MAP_reduced} is not valid for all signal processing families of models. 
In fact, for the Gaussian mixture model this rule will not be directly appropriate for model order selection without applying priors to the parameters as decribed in Section~\ref{sec:the_dirichlet_prior_on_the_mixing_probabilities}. Figure~\ref{fig:map_aic_curve} and~\ref{fig:map_bic_curve} shows the AIC  and the asymptotic MAP, refered to as the BIC under the described assumptions.
%
\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.65\textwidth]{img/map_test_aic_curve}
	\caption{AIC, as a function of model order for fitting a mixture of seven sources with the Gaussian mixture model.}
	\label{fig:map_aic_curve}
\end{figure}
\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.65\textwidth]{img/map_test_bic_curve}
	\caption{BIC, as a function of model order, for fitting a mixture of seven sources with the Gaussian mixture model.}
	\label{fig:map_bic_curve}
\end{figure}%
Both figures show the criterion applied to a mixture of seven sources from the SQAM database, using the MAP criterion implemented by the EM-algorithm. From both of the figures it is clear that the criterion results in a monotonically decreasing function of the model order. This tendency to overfitting is due to the fact that the measurement vectors does not have equal weight in each parameter estimate in the Gaussian mixture model. The penalty term that is dependent on $N$ can be altered to become appropriate for a Gaussian mixture model, which will be described in the following section.
%
\subsection{Suitable Prior on the Mixing Probabilities} % (fold)
\label{sec:the_dirichlet_prior_on_the_mixing_probabilities}
With suitable priors on the parameters, the MAP estimator can be used for model selection. In particular,~\cite{DBLP:journals/tnn/OrmoneitT98} and~\cite{zivkovic} put the Dirichlet prior on the mixing probabilities, of the components in the Gaussian mixture model, and~\cite{DBLP:journals/neco/Brand99} applied the ``entropic prior'' on the same parameters to favor models with small entropy. All of these have in common that they used the MAP estimator to drive the mixing probabilities associated with unnecessary components toward extinction. Based on an improper Dirichlet prior, ~\cite{Figueiredo00unsupervisedlearning} suggested to use minimum message length criterion to determine the number of the components, and further proposed an efficient algorithm for learning a finite mixture from multivariate data which we have adopted for source estimation based on panning parameters. It is the model called mixture-MDL (MMDL), which is described in the following section. 

%
\section{Mixture MDL (MMDL)} % (fold)
\label{sec:mixture_mdl}
% section mixture_mdl (end)
If we recall the parameter vector of the Gaussian mixture model as, 
$$\symmat{\Theta} = \{ \boldsymbol{\theta}_1,\boldsymbol{\theta}_2,\dots, \boldsymbol{\theta}_k, \alpha_1, \alpha_2,\dots, \alpha_k \}$$
Once we have estimated one source parameter i.e. $\hat{\symmat{\theta}_k}$ the sample size ``seen'' by this parameter is $N\alpha_k$ due to the mixing probability weighting~\cite{Figueiredo00unsupervisedlearning}. 
The penalty term becomes dependent on not only the number of measurement vectors $N$, but also on the mixing probabilities $\alpha$. The full derivation of the MMDL criteria is derived in~\cite{Figueiredo00unsupervisedlearning}, where it is described how the Fisher information for $\symmat{\theta}_k$ for one observation from component $k$ becomes $N\alpha_k \mathcal{I}(\symmat{\theta}_k)$. The prior on the parameters of the asymptotic MAP expression for mixtures is,
\begin{equation}
	p(\symvec{\theta}_k) = \frac{k(N_p+1)}{2}\ln N + \frac{N_p}{2} \sum_k \ln (n \alpha_k)
\end{equation}
We will adopt this criterion from~\cite{Figueiredo00unsupervisedlearning}. The mixture-MDL is,
\begin{equation}
	  	\hat{\symvec{\theta}_k}_\text{MMDL} = \argmin{\symvec{\theta}_k}\{-\ln p(\x|\symvec{\theta}_k)\ + p(\symvec{\theta}_k)\}
\end{equation}
The key observation of the MMDL is that the prior $p(\symvec{\theta}_k)$ is not only a function of $k$ and for a fixed $k$ it is not a ML estimate. For fixed $k$, MMDL has a simple Bayesian interpretation~\cite{Figueiredo00unsupervisedlearning}:
\begin{equation}
	p(\{\alpha_1,\dots,\alpha_k\}) \propto \exp\bigg\{ -\frac{N_p}{2} \sum_{k=1}^{K} (\alpha_k)^\frac{N_p}{2}\bigg\}
\end{equation}
Which is a Dirichlet-type improper prior, which can be used on the mixing probability in the maximum a posteriori (MAP) estimator for model selection. 

The procedure is then as follows: We start with a large number of randomly initialized components and search for the MAP solution using the iterative procedure of the EM algorithm. The prior drives the irrelevant components to extinction. In this way, while searching for the MAP solution, the number of components is reduced until convergence is achieved. See~\cite{Gelman03} for details on Dirichlet type prior relation to the standard MDL. 
The MMDL minimization criteria and the complete algorithm that is implemented is described in~\cite{Figueiredo00unsupervisedlearning},
%
\begin{equation}\label{eq:MMDL}
	\hat{\symvec{\theta}} = \argmin{\symvec{\theta}} \mathcal{L}(\symvec{\theta}|\mathcal{X})
\end{equation}
with
\begin{multline}\label{eq:MMDL2}
	\mathcal{L}(\symvec{\theta}|\X)  = \argmin{\symvec{\theta}} \bigg\{ -\ln p(\X|\symvec{\theta}) \\
	+ \frac{N_p}{2} \sum_{k=1}^{K} \ln \frac{N \alpha_k}{12} + \frac{k}{2} + \ln \frac{N}{12} + \frac{k(N_p+1)}{2} \bigg\} 
\end{multline}
where $\alpha_k>0$ and $N_p$ is the number of parameters specifying each component. The MMDL mixture model is including the component-wise EM algorithm $\text{CEM}^2$~\cite{cem2}. The expected number of measurement vectors generated by the $k$th component of the mixture is $N\alpha_k$, which is the sample size seen by the $\symvec{\theta}_k$, thus the optimal (in the MDL sense) for each $\symvec{\theta}_k$ is $N_p/2\log (N\alpha_k)$~\cite{Figueiredo00unsupervisedlearning}. The MMDL promotes sparseness in the sense that it is intialized with much higher $k$ than expected, and the EM-MMDL will then set some $a_k=0$ by killing the weakest component and then restart the $\text{CEM}^2$ algorithm~\cite{cem2}.
% section the_dirichlet_prior_on_the_mixing_probabilities (end)