\section{Experiments on Proposed Methods} % (fold)
\label{sec:experiments}
In the following the different proposed methods are tested through simulations on synthetic signals and real audio from the SQAM database~\cite{sqam}. To represent real music the synthetic signal are based on guitar recordings from which the amplitudes and phases have been extracted, by using an inharmonic approximate non-linear least square (ANLS) pitch estimator~\cite{DBLP:series/synthesis/2009Christensen}. By testing the segmentation with synthetic signals, we can create a ground truth to when each source is active. The following proposed methods are being tested:
\begin{itemize}
	\item Signal segmentation using the MAP log-likelihood criteria.
	\begin{itemize}
		\item Segmentation of known synthetic guitar mixture of 2 sources.
		\item Segmentation of real audio containing 2 sources from the SQAM database.
	\end{itemize}
	\item Source Parameter Estimation for uniform segmentation with model pruning.
	\begin{itemize}
		\item Precision measure on synthetic guitar signals of various duration.
	\end{itemize}
	\item Source Parameter Estimation for applied optimal segmentation.
    \begin{itemize}
		\item Precision measure on synthetic guitar signals of various duration, using the MAP log-likelihood criteria.
		\item Precision measure on synthetic guitar signals of various duration, using the Calinski Harabasz criteria.
		\item Segmentation criteria performance in 50 iterations on synthetic guitar signals of various duration, using the MAP log-likelihood criteria.
		\item Segmentation criteria performance in 50 iterations on synthetic guitar signals of various duration, using the Calinski Harabasz criteria.
	\end{itemize}
\end{itemize}
%
All synthetic signals were generated with 20 harmonic amplitudes and phases. The fundamental frequencies are representing notes that can be played on a guitar in the range $f_0\in \left[ 80, 1700 \right] $Hz, randomly applied. $f_s$ was set to 44100 Hz. 
%
\subsection{Segmentation of Known Segments on Synthetic Guitar} % (fold)
\label{sec:segmentation}
The segmentation is tested on a synthetic signal with a source ground truth. The synthetic signal has a duration of 15 seconds and white Gaussian noise has been applied to the signal with an SNR of 50 dB. The synthetic signal is consisting of two sources with a minimum active signal duration of 300 ms and note duaration as multiples of 300 ms. The signal is segmented according to the MAP MMDL criteria of~(\ref{eq:MMDL}), where the minimum segment length $N_\text{Min}=150$ ms and the maximum number of blocks $K_\text{Max}=20$ meaning that the maximum length of a segment is 3 s. A representative example of the chosen segment length as a function of time is shown in Figure~\ref{fig:segment} with white vertical lines. 
%
\begin{figure}[h!]
  \centering
  \centerline{\includegraphics[width=0.8\columnwidth]{img/teston2synthetic}}
  \caption{Optimal segmentation on known synthetic guitar signal, using the MAP log-likelihood criteria.}
\end{figure}\label{fig:segment}
%
In the top the two active sources are shown time domain along with a black horizontal line indicating which source is active at which time (the input segment ground truth). In the background the signal frequency content is shown to give a detailed view of the signal content. Generally the chosen segments are long if the content is not changing. Each segment contains some valuable information and seperates active input segments of the two sources. The four notes played from 0 to 4 sec (with same $f_0$) will consistenlty produce two underlying clusters, and we would expect the segments to be long but random. When the silent period starts a shorter segment length is chosen in all three silent periods starting at [3.6, 5.4, 8] sec. The note at 5 s. is clearly chosen, and the next three notes has an overlap that is segmented in to two parts, where only the second part has two active sources. The following notes after the silence at 8 s is chosen precisely in 300 ms segments each. 
%
\begin{figure}[h!]
  \centering
  \centerline{\includegraphics[width=0.8\columnwidth]{img/spectrogram_real_41}}
  \caption{This is a visual example of signal segmentation on two sources from the SQAM database. This figure is left for the reader to analyse visually (a bit like interpretation of art). There is more segmentation figures like this in Appendix~\ref{app:segmentation}}
\end{figure}  \label{fig:spectrogram1}
% \begin{figure}[h!]
%   \centering
%   \centerline{\includegraphics[width=0.8\columnwidth]{img/spectrogram_real_45}}
%   \caption{Signal segmentation on two sources from the SQAM database.}
% \end{figure}  \label{fig:spectrogram2}
\begin{figure}[h!]
  \centering
  \centerline{\includegraphics[width=0.8\columnwidth]{img/spectrogram_real_48}}
  \caption{Signal segmentation on two sources from the SQAM database. There is more segmentation figures like this in Appendix~\ref{app:segmentation}}
\end{figure}  \label{fig:spectrogram3}
%
Lastly, the long note from 12-15 s. is chosen in longer segments of 600 ms, but in order to separate the underlying clusters, the two overlapping notes in the end is chosen in segments in their respective note duration, even though they both overlap with the longer note. This indicates that the panning model, describes the signal in a precise way considering the source panning parameters, independently of the pitch information. The resulting distribution of parameter estimates can be seen in Figure~\ref{fig:parameterspace2}. It is clear that $k=2$, after segmentation and thresholding.       
\begin{figure}[h!]
  \centering
  \centerline{\includegraphics[width=0.65\columnwidth]{img/xComparison}}
  \caption{Parameter space with and without optimal segmentation and thresholding for the synthetic signal in Figure~\ref{fig:segment}}
\end{figure}\label{fig:parameterspace2}
%
%
\subsection{Parameter Estimation Performance on Synthetic Guitar} % (fold)
\label{sec:parameter_estimates_on_synthetic_guitar}
%
The following test is testing the performance of the proposed estimator and precision of the estimates for both uniform segmentation and optimal segmentation for the MAP log-likelihood criteria and in this test we compare to the Calinski-Harabasz criteria as well. The uniform segmentation has a segment size of 300 ms and the optimal segmentation scheme can choose segment sizes in the range of 100 ms to 2000 ms in 100 ms intervals.
%
%
The performance measures in this test are:
\begin{itemize}
	\item Correctly estimated sources. A correct cluster is defined as an error below one half degree amplitude panning angle.
	\item RMSE of the amplitude angle estimates.
	\item RMSE of delay estimates.
	\item Estimated model order.
\end{itemize}
%
The test is based on 50 iteration for various durations as seen form the Figures~\ref{fig:uniform_model_pruning}-\ref{fig:performance6}. 
The synthetic signals used in the test have a segmentation ranging from 300 ms to 3000 ms in steps of 300 ms. They all contain 30\% silence divided in to the same the segment durations. They are all consisting of a 2-5 source mixture. 
%
\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.65\textwidth]{img/test_performance_graph_uniform}
	\caption{Correct cluster estimates on synthetic guitar signals as a function of signal duration. This is the proposed estimator with model order selection by model pruning and uniform segmentation.}
	\label{fig:uniform_model_pruning}
\end{figure}
%
\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.65\textwidth]{img/test_performance_graph_three}
	\caption{Correct cluster estimates on synthetic guitar signals as a function of signal duration. This is the proposed estimator with model order selection by model pruning with the applied optimal segmentation, using two different criterias.}
	\label{fig:performance2}
\end{figure}
%
%
\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.65\textwidth]{img/test_performance_graph_three_A}
	\caption{RMSE of the amplitude angle estimates on synthetic guitar signals as a function of signal duration. This is the proposed estimator with model order selection by model pruning with the applied optimal segmentation, using two different criterias.}
	\label{fig:performance3}
\end{figure}
%
%
\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.65\textwidth]{img/test_performance_graph_three_D}
	\caption{RMSE of the delay estimates on synthetic guitar signals as a function of signal duration. This is the proposed estimator with model order selection by model pruning with the applied optimal segmentation, using two different criterias.}
	\label{fig:performance4}
\end{figure}
%
%
\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.65\textwidth]{img/test_performance_graph_three_model_order}
	\caption{Model order estimates on synthetic guitar signals as a function of signal duration. This is the proposed estimator with model order selection by model pruning with the applied optimal segmentation, using two different criterias.}
	\label{fig:performance5}
\end{figure}
%
%
\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.65\textwidth]{img/test_performance_orderestimates_2sec2}
	\caption{Model order estimates on synthetic guitar signals for all 50 iterations. More of these examples can be found in Appendix A.}
	\label{fig:performance6}
\end{figure}
%
From Figure~\ref{fig:uniform_model_pruning} it can be seen that the correct amount of cluster estimates is directly proportional to the duration of signal under test. Which means that the more data we gather, the more correctly we can estimate the sources in the mixture. From the Figure~\ref{fig:uniform_model_pruning}, it seems that in the duration range of 2-10 seconds, the performance is getting exponentially better, however this should be always be seen relative to the synthetic signal under test. From these specific 50 iterations it seems that the performance peaks at 99\% correctly estimated clusters.

\forceindent Figure~\ref{fig:performance5} shows the performance comparison of the uniform segmentation versus the optimal segmentation for shorter duration on the range 2-10 seconds. 
From this specific comparison it seems that by applying the optimal segmentation scheme, it is possible to achieve an improvement in the number of correct cluster estimates, especially for the shorter durations. The case of optimal segmentation based on MAP log-likelihood criteria, the improvement is approximately 5-10\% correctly estimated clusters for the shorter durations. Similarly, by using the the Calinski-Harabasz criteria, an improvement is also possible in the same duration range.

\forceindent It is implied from the test that both of the criterias under test offers improvement and it seems as if the MAP log-likelihood has the best performance for the shorter durations in terms of correct cluster estimates. However, when considering the precision results shown in Figure~\ref{fig:performance3} and Figure~\ref{fig:performance4} the two criterias under test, differs from this result. For this specific measure the Calinski-Harabasz criteria performs best for all durations in the short term range from 2-10 seconds. Where we have concluded that the performance in terms of correct number of cluster estimates was best for the MAP log-likelihood criteria, in this case it performs the worst when considering the precision of the correct estimates. It it even more unprecise than the uniform segmentation. It should be noted here, that because the global optimality is reached using the optimal segmentation, based on a given model, the model can still be inappropriate. 

\forceindent Lastly, Figure~\ref{fig:performance5} shows that the model order estimates improves with signal duration. It is again noticeable that for the shorter duration range, the MAP log-likelihood performs well, better than the Calinski-Harabasz and the uniform segmentation. However, non of the optimal segmentation schemes performs as well one could expect for this measure. The uniform worst case model order estimates would be for the uniform segmentation of 2  seconds signal duration. This case is shown in Figure~\ref{fig:performance6} where it can be seen that the estimated model order is this specific case always underdetermined, meaning that the estimates are below the true value, but only with an error of one or two number of clusters, which is way better than what we saw from the MAP directly without model pruning as described in in section~\ref{sec:component_annihilation}. More of these model order plots can be found in Appendix A.
%
%
%
\subsection{Parameter Estimation Performance on real audio} % (fold)
\label{sec:clustermodel_selection}
The estimation of source parameters are tested on the SQAM-CD signals in 100 iterations. In this part, the estimation of panning parameters are tested for optimal segments and fixed segments. Panning Parameters implicitly has the model order, as dimensionality. For each iteration, a mixture consists of minimum 2 and maximum 5 randomly picked source components, mixed according to~\eqref{eq:mixture}. Each source signal is normalized to have an absolute maximum amplitude of 1. 
\begin{table}
\centering
	\begin{tabular}{l*{4}{c} r}
		\hline
		Estimates         			   & MAP opt. seg.   & uniform seg.  \\
		\hline
		Correct Parameters (err. $\angle <0.5^\circ$) & $94.6\%$ &  $84.5\%$      \\
		Correct Model Order    & $89.1\%$             & 58.4\%  \\
		Amplitude Angle (RMSE) & $0.1^\circ$          & 0.07 \\
		Delay (RMSE)    	   &  0.33 samples        &  0.03      \\
		\hline
		\hline
	\end{tabular}
\caption{\label{tbl:experiments}Source parameter test results for real audio from the SQAM database in 100 iterations. The true number of clusters is in the range 2-5.}
\end{table}
The duration of each mixture is varying, and is defined from the audio signal in the mixture with the shortest duration, which is minimum 16 seconds for files on the SQAM-CD. The files containing pink noise has been removed from the test set. The fixed segment size is set to $600$ ms. and all mixtures are passed through the thresholding of (\ref{eq:threshold}). All applied panning parameters to sources are stereo simulations, which means that every pair of consecutive sources, will be panned equally to each side. Other than that all panning parameters applied are random. The applied optimal segmentation scheme in this test is only the MAP log-likelihood. 
\forceindent The results are shown in Table~\ref{tbl:experiments}. We measure the estimated model order, number of correct parameter estimates, and the root mean square error for both source parameters. Clearly, there is an improvement by applying the optimal segmentation scheme, with a correct number of parameter estimates of $94.6\%$ compared to $84.5\%$ with uniform segmentation. The model order i.e. the correct estimate of the number of sources is also clearly improved by the time-segmentation scheme. It seems that by applying the segmentation scheme the estimator loses some of the precision in the parameter estimates, both for the amplitude and delay estimates, which was exactly the case for the test on synthetic signals with the segmentation scheme based on the MAP log-likelihoos criteria. \\
\\
\forceindent In this chapter we have tested the proposed sterophonic source parameter estimator. The tests have shown promising results for the unsupervised learning algorithm and by modelling the measurement space as a Gaussian mixture the estimation of source parameters are very precise when the proposed MAP estimator with uniform segmentation and it is implied that for relative short signals an improvement in cluster estimates nad model order is possible by applying the proposed time segmentation scheme.
% section clustermodel_selection (end){}
%
%
%
%\end{center}
% section parameter_estimates_on_synthetic_guitar (end)