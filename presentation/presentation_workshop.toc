\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\beamer@endinputifotherversion {3.36pt}
\defcounter {refsection}{0}\relax 
\select@language {english}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Signal Model}{6}{0}{2}
\beamer@sectionintoc {3}{Proposed Method}{16}{0}{3}
\beamer@sectionintoc {4}{Experimental Results}{32}{0}{4}
\beamer@sectionintoc {5}{Conclusion}{34}{0}{5}
