function [x,smplNdx, fs] = smc_apply_panning_to_sqam_max_duration(theta, delaySec, numSources, fs)
sti = '/home/jacob/audio/sqam/';
files={strcat(sti,'14.flac'),strcat(sti,'26.flac'),strcat(sti,'9.flac'),strcat(sti,'22.flac'),strcat(sti,'8.flac'),strcat(sti,'12.flac'),strcat(sti,'4.flac'),strcat(sti,'17.flac'),strcat(sti,'16.flac')};
% 
smplNdx = randsample(63,numSources);

    for m=1:numSources,
     %x    strcat(sti,sprintf('%1.0f',smplNdx(m)),'.flac')
    %[source{m},fs]=audioread(files{m}, [startSec*fs (startSec+durationSec)*fs]);
     [tmp,fs]=audioread(strcat(sti,sprintf('%1.0f',smplNdx(m)),'.flac'));%, [startSec*fs (startSec+durationSec)*fs]);
     source{m} = tmp(fs:end,1);
        len(m)=length(source{m});
    end
    len=min(len);
    x=zeros(len,2);
    
    delay = floor(abs(delaySec*fs));

    for m=1:numSources
        source{m}=source{m}(:,1)/max(abs( source{m}(:,1) ));    
        source{m}=source{m}(1:len);
        source{m}=source{m}/max(abs(source{m}));%/var(source{m});    
    end

    for m=1:numSources
      g(1,m)=sin(theta(m));
      g(2,m)=cos(theta(m));
      if delaySec(m) > 0 
        x(:,1)=x(:,1)+g(1,m)*source{m};
        x(:,2)=x(:,2)+g(2,m)*[zeros(delay(m),1); source{m}(1:end-delay(m))];
      else
        x(:,2)=x(:,2)+g(2,m)*source{m};
        x(:,1)=x(:,1)+g(1,m)*[zeros(delay(m),1); source{m}(1:end-delay(m))];
      end
    end
    x=x./max(max(abs(x)));
end