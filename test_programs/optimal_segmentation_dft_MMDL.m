%%  Clustering of almplitude and delay ratios, using optimal sepgmentaion 
%   in time-frequency domain.
%
%   Apply panning parameters to music recordings
%   Compute short time fourier transform   
%   Apply optimal segmentation in tf-domain
%       - set min and max segment sizes
%       - minimize cost based on minimizing the BIC of GMM fit
%         in estimated clusters.
%         (MAKE SURE THAT THESE ARE SCALED TO BOTH DIRECTIONS).
%
clear all; close all;
% OSX: sti = '/Users/home/Documents/P9/panning_project/test_programs/SQAM_FLAC/';

%% Apply panning parameters to M audio recordings
maxNumSources=4; fs = 44100;
maxDelaySamples=6;
 sti = '/home/jacob/audio/sqam/'
 files={strcat(sti,'14.flac'),strcat(sti,'26.flac'),strcat(sti,'09.flac'),strcat(sti,'22.flac'),strcat(sti,'08.flac'),strcat(sti,'03.flac'),strcat(sti,'04.flac'),strcat(sti,'17.flac'),strcat(sti,'16.flac')};
% theta=(    [-20 20]+45)/180*pi;
% delaySec = [150e-6 -150e-6]/1.4;
% truePoints = [theta(:)/pi*180-45 delaySec(:)*fs;];
numSources = randi(maxNumSources-1)+1;
[theta, delaySec, truePoints] = smc_random_panning_parameters(numSources,fs,maxDelaySamples);

fs=44100;
%numSources = size(truePoints,1);
startSec = 2
durationSec = 18;
%x = smc_apply_panning_to_sqam_(theta, delaySec, numSources, durationSec, fs);
x = smc_apply_panning_to_sqam_max_duration(theta, delaySec, numSources, fs);% 
% for m=1:numSources,
%     [source{m},fs]=audioread(files{m}, [startSec*fs (startSec+durationSec)*fs]);
%     len(m)=length(source{m});
% end
% len=min(len);
% x=zeros(len,2);
% delay = floor(abs(delaySec*fs));
% 
% for m=1:numSources,
%     source{m}=source{m}(:,1);    
%     source{m}=source{m}(1:len);
%     source{m}=source{m}/var(source{m});    
% end
% 
% for m=1:numSources;
%   g(1,m)=sin(theta(m));
%   g(2,m)=cos(theta(m));
%   if delaySec(m) > 0 
%     x(:,1)=x(:,1)+g(1,m)*source{m};
%     x(:,2)=x(:,2)+g(2,m)*[zeros(delay(m),1); source{m}(1:end-delay(m))];
%   else
%     x(:,2)=x(:,2)+g(2,m)*source{m};
%     x(:,1)=x(:,1)+g(1,m)*[zeros(delay(m),1); source{m}(1:end-delay(m))];
%   end
% end
    
%%

% implement the fourier transform
wlen     = floor(600e-3*fs);
NMin = floor(wlen/4);
w = hann(wlen);
% Xl = smc_stft_complex(x(:,1), w, NMin, NFFT);
% Xr = smc_stft_complex(x(:,2), w, NMin, NFFT);

%%   Apply optimal segmentation in tf-domain
%       - set min and max segment sizes
KMax = 8;
NMax = NMin * KMax;
M = floor(size(x,1)/NMin) ;%size(Xl,2);
% resize x to be multiple of Nmin samples.
x=x(1:M*NMin,:);
NFFT  = 2^nextpow2(NMin*KMax);

k_opt=[];
numClusters=[];
mu1=[];
mu2=[];
m=1;
while (m*NMin) <= (size(x,1))
    cost=[];
    J=[];
    K = min(m, KMax);
    for k=1:K
        N  = ((m-k)*NMin+1:m*NMin); %sN=size(N)
        N2 = (1:(m-k)*NMin); %sN2 = size(N2)  
        [J, numClusters(m)] = smc_GMM_MMDL_dft( x(N, 1), x(N, 2), fs, NFFT,maxDelaySamples);
        if (m-k) > 0
            cost(k) = J + smc_GMM_MMDL_dft( x(N2, 1), x(N2, 2), fs, NFFT,maxDelaySamples); 
        else
            cost(k) = J;
        end
        % k
    end
    [~, k_opt(m)] = min(cost);
    m=m+1;
end
m = M; % is defined above the loop.
bb=1;
while (m > 0)
    numBlocksInSegment(bb) = k_opt(m);
    numClustersInSegment(bb) = numClusters(m);
    m=m-k_opt(m);
    bb=bb+1;
end
numBlocksInSegment = numBlocksInSegment(end:-1:1);
numClustersInSegment = numClustersInSegment(end:-1:1);

%% Evaluate the given optimal segments and compare to uniform segmentation
% Evaluate on optimal segmetns and MMDL
[X] = smc_estimate_panning_space_from_optimal_segments(x, numBlocksInSegment*wlen, fs, truePoints, NFFT);

%[~,~,~,~,finalMu.MMDL] = smc_GMM_MMDL(X, fs);
[mindl, bestk, bestmu, bestcov,finalMu,safemu, safemuMean, safemuSelected, bestmuCovRatio, meanthr,X2] = smc_GMM_MMDL_normdetC(X, fs,size(true.Points,1));
% Evaluate on optimal segments, but new DFTs for each segment 
[~,~,finalmuBIC] = smc_GMM_BIC(x,numBlocksInSegment*wlen,fs,truePoints,NFFT,maxDelaySamples);
[~, ~, finalMu.kMeans] = smc_kmeans(x,numBlocksInSegment*wlen,fs,truePoints,NFFT); title('K-Cluster Assignments and Centroids (optimal new DFTs)');
% Evaluate on uniform segments
%smc_GMM_BIC(x,ones(1000,1)*wlen     , fs, truePoints, NFFT); title('GMM-Cluster Assignments and Centroids (uniform DFTs)');
%smc_kmeans(x,ones(1000,1)*wlen       , fs, truePoints, NFFT); title('K-Cluster Assignments and Centroids (uniform DFTs)');

% implement the short time fourier transform
%wlen     = floor(200e-3*fs);
NMin = floor(wlen/4);
NFFT  = 2^nextpow2(wlen);%2^14;
w = hann(wlen);
Xl = smc_stft_complex(x(:,1), w, NMin, NFFT);
Xr = smc_stft_complex(x(:,2), w, NMin, NFFT);
% plot segementations

figure;
smc_plot(x,fs);
tmp=0;
for ii=1:length(numBlocksInSegment) 
    subplot(211); hold on; plot([tmp+numBlocksInSegment(ii)*NMin/fs tmp+numBlocksInSegment(ii)*NMin/fs],[1.5*min(x(:,1)) 1.5*max(x(:,1))],'k')
    tmp=tmp+numBlocksInSegment(ii)*NMin/fs;
end
figure;
    imagesc([1:(size(Xl,2))]*NMin/fs-NMin/fs/2,linspace(0,1,(NFFT/2-1))*fs/2 ,20*log10(abs(Xl(1:NFFT/2-1,:))));
    axis xy;
    caxis(max(caxis)+[-60 0]);
    xlabel('Time [sec]'); ylabel('Frequency [Hz]');
    title(sprintf('Spectrogram of track'));
    ylim([0 fs/2]);
    
tmp=0; % to fit the above
for ii=1:length(numBlocksInSegment) 
    hold on; plot([tmp+numBlocksInSegment(ii)*NMin/fs tmp+numBlocksInSegment(ii)*NMin/fs],[0 fs/2],'w')
    tmp=tmp+numBlocksInSegment(ii)*NMin/fs;
end
