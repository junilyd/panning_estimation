%% search through part of the space
clear all; clf;
% ITD ILD Dan ellis 
duration = 0.1;
N = 800;
fs  = 8e3;
duration = N/fs;
f0  = [200 333 659 173 234 470 150 280] ;
pan = [-40 -30 -20 -10 0 15 25 35]';
panAngle = pan;
% Make a script to produce panned
%sinusoids with phases also.
[P,Pc] = smc_panned_sinusoids(f0, pan, duration, fs);
soundsc(P,fs)
N = length(Pc);
%Z = smc_Z_dft(Pc,N);
%X = Z*Pc;
w = smc_get_window(length(Pc), 'blackman');
[X(:,1),pX(:,1),~] = smc_dft(Pc(:,1), w, 2^11, fs);
[X(:,2),pX(:,2),f] = smc_dft(Pc(:,2), w, 2^11, fs);
X = 10.^(X./20);

ratios = abs(X(:,1))./abs(X(:,2));
    
panSpace = [-45:0.5:45]';
    panAngle = panSpace;%(ii);
    % 1) pandir (theta) to gain ratios
    baseAngle = 45;
    phi0 = baseAngle * pi/180; 
    phi  = panAngle  * pi/180;
    theta = phi + phi0;
    g = [cos(theta) sin(theta)];
    ratiosToLookFor = g(:,1)./g(:,2);

for ii = 1:length(ratiosToLookFor)
    tmp = ratios./ratiosToLookFor(ii);
    tmp = (tmp-1);
    tmp = tmp.*(-1);
    tmp = abs(tmp).*(-1);
    tmp = tmp + 1;
    tmp(tmp<0.9) = 0; 
    ratioFunction(:,ii) = tmp;
end
costFunction = sum(ratioFunction(1:N,:));
% plot with imagesc
    figure(1)
    subplot(211)
        imagesc(panSpace,f,ratioFunction);
            axis xy
            xlabel('Estimated Pan Angle [deg.]'); ylabel('Frequency [Hz]');
            titleString = 'True \Theta = ';
            for ii = 1:length(pan)
                titleString =  [titleString sprintf('%1.0f ',pan(ii))];
            end
            title(titleString)    
    subplot(212)
        %plot(panSpace, sum(ratioFunction(floor(N/4):floor(N/2),:)));
        plot(panSpace, sum(ratioFunction(1:N,:)));
            grid on;
            xlim([panSpace(1) panSpace(end)])
            title('Integration over the frequency range');
            xlabel('Estimated Pan Angle [deg.]');    