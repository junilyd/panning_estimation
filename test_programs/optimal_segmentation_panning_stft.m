function [numBlocksInSegment, numClustersInSegment] = optimal_segmentation_panning_stft(Xl,Xr,NMin,fs, NFFT)
    %%   Apply optimal segmentation in tf-domain
    %       - set min and max segment sizes
    KMax = 8;
    NMax = NMin * KMax;
    M = size(Xl,2);
    %       - minimize cost based on minimizing the standard deviation 
    %         in estimated clusters.
    k_opt=[];
    numClusters=[];
    mu1=[];
    mu2=[];
    m=1;
    while (m <= M)
        cost=[];
        J=[];
        K = min(m, KMax);
        for k=1:K
            XlBlock = Xl( :,(m-k+1):m );
            XrBlock = Xr( :,(m-k+1):m );
            [J, numClusters(m)] = smc_GMM_MMDL_stft(XlBlock, XrBlock, fs, NFFT);
            if (m-k) > 0
                cost(k) = J + smc_GMM_MMDL_stft(Xl( :,(1:m-k) ), Xr( :,(1:m-k) ), fs, NFFT); 
            else
                cost(k) = J;
            end
            % k
        end
        [~, k_opt(m)] = min(cost);
        m=m+1;
    end
    m = M; % is defined above the loop.
    bb=1;
    while (m > 0)
        numBlocksInSegment(bb) = k_opt(m);
        numClustersInSegment(bb) = numClusters(m);
        m=m-k_opt(m);
        bb=bb+1;
    end
    numBlocksInSegment = numBlocksInSegment(end:-1:1);
    numClustersInSegment = numClustersInSegment(end:-1:1);

end
