clear all; clf;
% ITD ILD Dan ellis 
duration = 0.5;
fs  = 8e3;
f0  = [63 140 216] ;
pan = [-40 35 10]';
panAngle = pan;
% Make a script to produce panned
%sinusoids with phases also.
[P,Pc] = smc_panned_sinusoids(f0, pan, duration, fs);
P(:,1)=P(:,1).*hann(length(P(:,1)));
P(:,2)=P(:,2).*hann(length(P(:,2)));
soundsc(P,fs)

% f versus theta
% integrate over all frequencies for all angles
% angles equals ratio of energy
% 1) pandir (theta) to gain ratios
baseAngle = 45;
%panAngle  = 30;
phi0 = baseAngle * pi/180; 
phi  = panAngle  * pi/180;
theta = phi + phi0;
g = [cos(theta) sin(theta)]';
%g=g/sum(g.^2);
L = [ [-cos(phi0) sin(phi0)]' ...
      [ cos(phi0) sin(phi0)]' ...
      ];
  
p = (g'*L)';

% 2) gain (energy) ratios to pandir (theta)
g2 = inv(L)'*p;
g2 =g2/sum(g2.^2);
theta2 = [acos(g2(1))];
phi2 = (theta-phi0);
panAngle2 = phi/pi*180;

% searchspace theta
    N = length(Pc);
    Z = smc_Z_dft(Pc,N);
    X = Z*Pc;

    for ii=1:N
    ratios(ii) = abs(X(ii,1))/abs(X(ii,2));
    end
    
    ratiosToLookFor = g(1,:)./g(2,:)
    
    for ii = 1:length(ratiosToLookFor)
        % if ratios(n) is above 1 it is left side
        searchValue = ratiosToLookFor(ii);
        if searchValue < 1
            searchValue = 1/searchValue
            subtractRatio = 1./ratios;
            side = 'right';
        else
            searchValue = searchValue
            subtractRatio = ratios;
            side = 'left';
        end
        scaleFactor = 1;
        % fit the ratio function to have max at closest to theta-related value       
        ratiofunction(:,ii) = abs(searchValue - subtractRatio);
        ratiofunction(:,ii) = (ratiofunction(:,ii) - max(ratiofunction(:,ii)))./searchValue.*(-scaleFactor);
        ratiofunction(:,ii) = ratiofunction(:,ii)-max(ratiofunction(:,ii))+1;
        %ratiofunction(:,ii) = (ratiofunction(:,ii)./searchValue)*10
        
    end
    figure(1)
        subplot(611)
        plot([1:N],abs(X(:,1)),[1:N],abs(X(:,2))); xlim([0 N/2]);
        subplot(612)
        plot([1:N],ratios); xlim([0 N/2]);%legend('values above 1 is the left side');      
        subplot(613)
        plot([1:N],1./ratios); xlim([0 N/2]); % %legend('values above 1 is the right side');
        subplot(614)
        plot([1:N], ratiofunction(:,1)); axis([0 N/2 -scaleFactor scaleFactor]); % legend('ratiofunction for specific theta');
        subplot(615)
        plot([1:N],ratiofunction(:,2)); axis([0 N/2 -scaleFactor scaleFactor]); % legend('ratiofunction for specific theta');
        subplot(616)
        plot([1:N],ratiofunction(:,3)); axis([0 N/2 -scaleFactor scaleFactor]); % legend('ratiofunction for specific theta');

    
    
%% search through part of the space
clear all; clf;
% ITD ILD Dan ellis 
duration = 0.5;
fs  = 8e3;
f0  = [350] ;
pan = [5]';
panAngle = pan;
% Make a script to produce panned
%sinusoids with phases also.
[P,Pc] = smc_panned_sinusoids(f0, pan, duration, fs);
P(:,1)=P(:,1).*hann(length(P(:,1)));
P(:,2)=P(:,2).*hann(length(P(:,2)));
soundsc(P,fs)

    N = length(Pc);
    Z = smc_Z_dft(Pc,N);
    X = Z*Pc;

    for ii=1:N
    ratios(ii) = abs(X(ii,1))/abs(X(ii,2));
    end
    
panSpace = [-42:0.5:42]';
for ii = 1:length(panSpace)
    panAngle = panSpace(ii);
    % 1) pandir (theta) to gain ratios
    baseAngle = 45;
    %panAngle  = 30;
    phi0 = baseAngle * pi/180; 
    phi  = panAngle  * pi/180;
    theta = phi + phi0;
    g = [cos(theta) sin(theta)]';
    %g=g/sum(g.^2);
    % L = [ [-cos(phi0) sin(phi0)]' ...
    %       [ cos(phi0) sin(phi0)]' ...
    %       ];
    % p = (g'*L)';
    ratiosToLookFor(ii) = g(1)./g(2)
end

    for ii = 1:length(ratiosToLookFor)
        % if ratios(n) is above 1 it is left side
        searchValue = ratiosToLookFor(ii);
        if searchValue < 1
            searchValue = 1/searchValue
            subtractRatio = 1./ratios;
            side = 'right';
        else
            searchValue = searchValue
            subtractRatio = ratios;
            side = 'left';
        end
        scaleFactor = 300;
        % fit the ratio function to have max at closest to theta-related value       
          ratiofunction(:,ii) = subtractRatio;
        ratiofunction(:,ii) = abs(searchValue - subtractRatio);
        ratiofunction(:,ii) = (ratiofunction(:,ii) - max(ratiofunction(:,ii)))./searchValue.*(-scaleFactor);
        ratiofunction(:,ii) = ratiofunction(:,ii)-max(ratiofunction(:,ii))+1;
        
    end
% plot with imagesc
    figure(1)
    subplot(211)
        imagesc(panSpace,[1:N],ratiofunction);
        axis xy;
        caxis([-scaleFactor/4 1]);%max(caxis) + [-0.2 0]);
        xlabel('Pan Angle [deg.]'); ylabel('Frequency [samples]');
        %xlim([t(1) t(end)]); %set(gca,'Yscale','log');
        ylim([0 N-1]);
        titleString = 'True \Theta = ';
        for ii = 1:length(pan)
            titleString =  [titleString sprintf('%1.0f ',pan(ii))];
        end
        title(titleString)    
    subplot(212)
        plot(panSpace, sum(ratiofunction(1:N/2,:)));
        grid on;
       
    % wrong normalization in ll. 140.
    
    figure
    for ii=1:length(ratiosToLookFor)
       plot(ratiofunction(:,ii)); title(sprintf('%1.0f',panSpace(ii)));pause
    end
    
    
    
    
    
    
    
    
    
    
    
    