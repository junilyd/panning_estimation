% evaluate for duration 18 sec.
clear all
cd /home/jacob/panning_project/test_programs/
% GOD --> load test_MMDL_40iter_4April_dur_18sec.mat
load test_MMDL_40iter_findfejl_27March_dur_18sec.mat
for ii=1:4
    X = savedX{ii};
    %X=X(sign(X(:,1))~=sign(X(:,2)),:);
    ii
    [~, ~, ~, ~,~,~, ~, ~, safemu(ii).covRatio, tmp] = smc_GMM_MMDL_normdetC(X, fs, size(true(ii).Points,1));
    meanVal(ii,1) = tmp(1);
    meanVal(ii,2) = tmp(2);
    minmax(ii,1) = tmp(3);
    minmax(ii,2) = tmp(4);
    minmax(ii,3) = tmp(5);
    minmax(ii,4) = tmp(6);
    %meanratio(ii) = tmp(7)
    true(ii).Points
    %pause
    close all
end
iter=ii;
evaluate_iterations_covariance_function(safemu, true, iter, maxNumSources);
