%%  Clustering of almplitude and delay ratios, using optimal sepgmentaion 
%   in time-frequency domain.
%
%   Apply panning parameters to music recordings
%   Compute short time fourier transform   
%   Apply optimal segmentation in tf-domain
%       - set min and max segment sizes
%       - minimize cost based on minimizing the stadard deviation
%         in estimated clusters.
%         (MAKE SURE THAT THESE ARE SCALED TO BOTH DIRECTIONS).
%
clear all; close all;
%% Apply panning parameters to M audio recordings
M=7; fs = 44100;
sti = '/home/jacob/audio/sqam/'
%sti = '/Users/home/Documents/P9/panning_project/test_programs/SQAM_FLAC/';
files={strcat(sti,'14.flac'),strcat(sti,'26.flac'),strcat(sti,'09.flac'),strcat(sti,'22.flac'),strcat(sti,'08.flac'),strcat(sti,'03.flac'),strcat(sti,'04.flac'),strcat(sti,'17.flac'),strcat(sti,'16.flac')};
theta=(    [-30    -20    -10    0 10      20      30    ]+45)/180*pi;
delaySec = [200e-6 150e-6 100e-6 0 -100e-6 -150e-6 -200e-6]/1.4;
truePoints = [theta(:)/pi*180-45 delaySec(:)*fs;];
fs=44100;
for m=1:M,
    [source{m},fs]=audioread(files{m}, [2*fs 20*fs]);
    len(m)=length(source{m});
end
len=min(len);
x=zeros(len,2);
delay = floor(abs(delaySec*fs));

for m=1:M,
    source{m}=source{m}(:,1);    
    source{m}=source{m}(1:len);
    source{m}=source{m}/var(source{m});    
end

for m=1:M;
  g(1,m)=sin(theta(m));
  g(2,m)=cos(theta(m));
  if delaySec(m) > 0 
    x(:,1)=x(:,1)+g(1,m)*source{m};
    x(:,2)=x(:,2)+g(2,m)*[zeros(delay(m),1); source{m}(1:end-delay(m))];
  else
    x(:,2)=x(:,2)+g(2,m)*source{m};
    x(:,1)=x(:,1)+g(1,m)*[zeros(delay(m),1); source{m}(1:end-delay(m))];
  end
end
    
%%

% implement the short time fourier transform
wlen     = floor(200e-3*fs);
NMin = floor(wlen/4);
NFFT  = 2^nextpow2(wlen);%2^14;
w = hann(wlen);
Xl = smc_stft_complex(x(:,1), w, NMin, NFFT);
Xr = smc_stft_complex(x(:,2), w, NMin, NFFT);

%%   Apply optimal segmentation in tf-domain
%       - set min and max segment sizes
KMax = 8;
NMax = NMin * KMax;
M = size(Xl,2);
%       - minimize cost based on minimizing the standard deviation 
%         in estimated clusters.
k_opt=[];
numClusters=[];
mu1=[];
mu2=[];
m=1;
while (m) <= (size(Xl,2))
    cost=[];
    J=[];
    K = min(m, KMax);
    for k=1:K
        XlBlock = Xl( :,(m-k+1):m );
        XrBlock = Xr( :,(m-k+1):m );
        [J, numClusters(m)] = smc_kmeans_std_stft(XlBlock, XrBlock, fs, NFFT);
        if (m-k) > 0
            cost(k) = J + smc_kmeans_std_stft(Xl( :,(1:m-k) ), Xr( :,(1:m-k) ), fs, NFFT); 
        else
            cost(k) = J;
        end
    end
    [~, k_opt(m)] = min(cost);
    m=m+1;
end
m = M; % is defined above the loop.
bb=1;
while (m > 0)
    numBlocksInSegment(bb) = k_opt(m);
    numClustersInSegment(bb) = numClusters(m);
    m=m-k_opt(m);
    bb=bb+1;
end
numBlocksInSegment = numBlocksInSegment(end:-1:1);
numClustersInSegment = numClustersInSegment(end:-1:1);

%% Evaluate the given optimal segments and compare to uniform segmentation
% Evaluate on optimal segments already computed STFT
[stdOutput, K, mu1] = smc_kmeans_stft(Xl,Xr, numBlocksInSegment, fs, NFFT, truePoints); title('K-Cluster Assignments and Centroids (optimal STFT)');
% Evaluate on optimal segments, but new DFTs for each segment 
smc_kmeans(x, numBlocksInSegment*wlen, fs, truePoints, NFFT); title('K-Cluster Assignments and Centroids (optimal new DFTs)');
% Evaluate on uniform segments
smc_kmeans(x,ones(1000,1)*wlen       , fs, truePoints, NFFT); title('K-Cluster Assignments and Centroids (uniform DFTs)');
% plot segementations
figure;
smc_plot(x,fs);
tmp=0;
for ii=1:length(numBlocksInSegment) 
    subplot(211); hold on; plot([tmp+numBlocksInSegment(ii)*NMin/fs tmp+numBlocksInSegment(ii)*NMin/fs],[1.5*min(x(:,1)) 1.5*max(x(:,1))],'k')
    tmp=tmp+numBlocksInSegment(ii)*NMin/fs;
end
figure;
    imagesc([1:(size(Xl,2))]*NMin/fs-NMin/fs/2,linspace(0,1,(NFFT/2-1))*fs/2 ,20*log10(abs(Xl(1:NFFT/2-1,:))));
    axis xy;
    caxis(max(caxis)+[-60 0]);
    xlabel('Time [sec]'); ylabel('Frequency [Hz]');
    title(sprintf('Spectrogram of track'));
    ylim([0 fs/2]);
    
tmp=0; % to fit the above
for ii=1:length(numBlocksInSegment) 
    hold on; plot([tmp+numBlocksInSegment(ii)*NMin/fs tmp+numBlocksInSegment(ii)*NMin/fs],[0 fs/2],'w')
    tmp=tmp+numBlocksInSegment(ii)*NMin/fs;
end
