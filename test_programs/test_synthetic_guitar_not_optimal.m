clear all;
close all;
dur = [300e-3 600e-3 900e-3 1200e-3 1500e-3 1800e-3 3000e-3];
cd /home/jacob/panning_project/test_programs/
load mats/synthetic_guitar_clustering_and_pruning.mat;

startIter = size(savedx,1)-1;

for iter=startIter:100
cd /home/jacob/Documents/MATLAB/code/test_programs/RMSE;
fs=44100;
L=30;
SNR=80;
fig=0;

duration = 120;

numSources = randi(4)+1;
sec30=duration*fs;

sig=[];
for nn=1:numSources
    x=[];
    while length(x) < sec30+3*fs
        if rand>0.7
            x = [x; zeros(randi(6),1);];
        else
            x = [x; smc_synth(fs,dur(randi(7)),L,SNR,fig);];
        end
    end
    sig(:,nn) = x(1:sec30);
end


cd /home/jacob/panning_project/test_programs/

    maxDelaySec= 150e-6;
    maxDelaySamples = floor(maxDelaySec*fs);

    [theta, delaySec, true(iter).Points] = smc_random_panning_parameters(numSources,fs, maxDelaySec);

    [x] = smc_apply_panning_to_synthetic(sig,theta, delaySec, numSources, fs);
    %soundsc(x,fs)
    % parameters for the fourier transform
    %% Segmentation Part
    wlen     = floor(600e-3*fs);
    NMin = floor(wlen/2);
    w = hann(wlen);
    NFFT  = 2^nextpow2(wlen*2);%2^14;

    %% Evaluate the given optimal segments 
    [X] = smc_estimate_panning_space_from_optimal_segments(x, ones(40000,1)*wlen*2, fs, true(iter).Points, NFFT, maxDelaySamples);
    
    savedX{iter,1} = X;
    %savedx{iter,1} = x;
    %savedf0{iter,1} = f0;

    
    %[~, bestk(iter),~,~,finalMu(iter).MMDL,safemu(iter).first,safemu(iter).Mean, safemu(iter).Selected, safemu(iter).covRatio] = smc_GMM_MMDL_normdetC(X, fs, size(true(iter).Points,1));
    [~, bestk(iter),~,~,~,~,~, ~, finalMu(iter).MMDL, ~,~,~] = smc_GMM_MMDL_normdetC(X, fs,numSources);
     
    true(iter).Points
    finalMu(iter).MMDL 
    %safemu(iter).covRatio
    %%
    close all;
end