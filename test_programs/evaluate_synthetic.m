function [errorAngle, errorDelay,trueNumClusters,NumClustersMMDL,errorRateModelOrder] = evaluate_synthetic(finalMu, true, iter, maxNumClusters)
for hh=1:iter
%     mean_ = mean(abs(safemu(ii).covRatio))
%     if mean_(1)<1
%         safemu(ii).covRatio = safemu(ii).covRatio.*[45 200e-6*44100];
%     end
    
    m=[];
    t = true(hh).Points;
    m = finalMu(hh).MMDL;
    
    figure(length(true(hh).Points));
    hold on
%     if ~isempty(m)
        for qq=1:size(m,1)
            text(m(qq,1), m(qq,2), sprintf('%1.0f',hh),'Color','blue','horizontalalignment','right');
        end
        plot(t(:,1), t(:,2),'k+','markersize',12);
        for qq=1:length(t)
            text(t(qq,1), t(qq,2), sprintf('%1.0f',hh));
        end
        plot(m(:,1), m(:,2), 'bx','markersize',18);

    
    title(sprintf('Estimates of panning parameters (%1.0f clusters)',size(true(hh).Points,1)))
    xlabel('Pan Angle Estimate');
    ylabel('Delay Estimate');

end
% for ii=2:maxNumClusters
%     figure(ii)
% %     legend('True Mean','MMDL covRatio','MMDL smoothed','MMDL Picked');
% end

for hh=1:iter
    fprintf('ii: %1.0f, true: %1.0f, MMDL: %1.0f\n',hh, length(true(hh).Points),length(finalMu(hh).MMDL))
    trueNumClusters(hh) = size(true(hh).Points,1);
    NumClustersMMDL(hh) = size(finalMu(hh).MMDL,1);
end
trueNumClusters
NumClustersMMDL
errorRateModelOrder = 100-100*(sum(NumClustersMMDL==trueNumClusters)/iter)

errorAngle=[];
errorDelay=[];
for hh=1:iter
    tp = true(hh).Points;
    [~,ndx] = sort(tp(:,1));
    tp = tp(ndx,:);
    
    ep = finalMu(hh).MMDL;
    [~,ndx] = sort(ep(:,1));
    ep = ep(ndx,:);
    
    %ep-tp
    %ii
    tp ;
    ep;
    min(length(tp),size(ep,1));
    for qq=1:min(length(tp),size(ep,1))
        if length(ep)>=length(tp)
            qq;
            [tmpErrorAngle(qq),minndx] = min(abs(tp(qq,1)-ep(:,1)));
            tmpErrorDelay(qq) = abs(ep(minndx,2)-tp(qq,2));
        else
            qq;
            [tmpErrorAngle(qq),minndx] = min(abs(tp(:,1)-ep(qq,1)));        
            tmpErrorDelay(qq) = abs(ep(qq,2)-tp(minndx,2));
        end
%                 if tmpErrorAngle(qq)>5
%                     hh
%                     pause
%                 end
    end

    errorAngle=[errorAngle tmpErrorAngle];
    errorDelay=[errorDelay tmpErrorDelay];
    tmpErrorAngle=[];
    tmpErrorDelay=[];
end


