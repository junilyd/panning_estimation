clear all; clf;
% ITD ILD Dan ellis 
duration = 1;
fs  = 8e3;
f0  = [130 195] ;
pan = [-28 40]';
panAngle = pan;
% Make a script to produce panned
%sinusoids with phases also.
[P,Pc] = smc_panned_sinusoids(f0, pan, duration, fs);
P(:,1)=P(:,1).*hann(length(P(:,1)));
P(:,2)=P(:,2).*hann(length(P(:,2)));
soundsc(P,fs)

% f versus theta
% integrate over all frequencies for all angles
% angles equals ratio of energy
% 1) pandir (theta) to gain ratios
baseAngle = 45;
%panAngle  = 30;
phi0 = baseAngle * pi/180; 
phi  = panAngle  * pi/180;
theta = phi + phi0;
g = [cos(theta) sin(theta)]';
%g=g/sum(g.^2);
L = [ [-cos(phi0) sin(phi0)]' ...
      [ cos(phi0) sin(phi0)]' ...
      ];
  
p = (g'*L)';

% 2) gain (energy) ratios to pandir (theta)
g2 = inv(L)'*p;
g2 =g2/sum(g2.^2);
theta2 = [acos(g2(1))];
phi2 = (theta-phi0);
panAngle2 = phi/pi*180;

% searchspace theta
% theta = ratio of energy between both sides.    
    N = length(Pc);
    Z = smc_Z_dft(Pc,N);
    X = Z*Pc;

    for ii=1:N
    ratios(ii) = abs(X(ii,1))/abs(X(ii,2));
    end
    
    ratiosToLookFor = g(1,:)./g(2,:)

    figure(1)
        subplot(311)
        plot([1:N],abs(X(:,1)),[1:N],abs(X(:,2)));% xlim([20 max(f0)*5]);
        subplot(312)
        plot([1:N],ratios); ylim([-0.2 4]); legend('values above 1 is the left side');      
        subplot(313)
        plot([1:N],1./ratios);legend('values above 1 is the right side');
    % check that Z is correct as a dft also sign of j-
    % fix theta from -45 to 45 (make a vector for that)
    %   should output a ratio to look for
    % compare energy for each frequency bin (left and right channel)
    %   are there any close to the fixed theta ? 
    % plot with imagesc


