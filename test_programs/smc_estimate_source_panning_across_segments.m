function [muEstimates,muDegSampl,alphaEstimates, CEstimates] = smc_estimate_source_panning_across_segments(xFullband,fs)
%
% This function estimates the source panning paramters for sources in a
% stereophonic mixture, across segments. The generalized variance and degree of membership 
% of the Gaussian components across segments is used as a basis 
% for the selection of clusters amongst candidates.
%
% INPUT: 
%       xFullband:      Input stereo audio data (preferably long duration) 
%       fs       :      sampling rate of xFullband
%
% OUTPUT:
%       muEstimates:    Panning parameter estimates.
%       muDegSampl :    Panning parameter estimates in amplitude angle and
%       samples.
%       alphaEstimates: mixing parametere estimates.
%       CEstimates:     Covariance matrix estimate.
%
%
% This method is proposed in the paper by J. M. Hjerrild and M. G. Christensen 
% "ESTIMATION OF SOURCE PANNING PARAMETERS AND SEGMENTATION OF STEREOPHONIC MIXTURES."
%
% Usage:
% [muEstimates,muDegSampl,alphaEstimates, CEstimates] = smc_estimate_source_panning_across_segments(xFullband,fs)
%
% Implemented by Jacob Møller Hjerrild at the Audio Analysis Lab, Aalborg
% University.
%------------------------------------------------------------

% check if these exists outside function first
if ~exist('maxNumSources') & ~exist('maxDelaySec')'
    maxNumSources=5; 
    maxDelaySec= 150e-6;
    maxDelaySamples = floor(maxDelaySec*fs);
    maxPanAngle=44;
end
   
% filter to be compliant with downsampled signals to fs = 8 kHz.
fc = 4e3; % cutoff freq.
[b,a] = butter(6,fc/(fs/2));
x = filter(b,a,xFullband);

%% estimate spatial distribution space
wlen     = floor(600e-3*fs);
NMin = floor(wlen/4); KMax = 10; % only used, if optimal segmentation is applied
NFFT  = 2^nextpow2(NMin*KMax);
[X] = smc_estimate_panning_space_from_optimal_segments(x, ones(1000,1)*wlen, fs, 0, NFFT, maxDelaySamples, maxPanAngle);

%% Estimate overfitted statististics
% PARAMETERS FOR CLUSTERING
kmax=35;
regVal  = 0.5e-4;
covType = 1;
plotFlag= 0;
thrStop = 1e-3; 
thrForCorrect=.5;

[k,alpha,mu,C] = gmm_mmdl_overfitted(X',kmax,kmax,regVal,thrStop,covType,plotFlag);


%%  sort cluster candidates statistics
%   according to the metric of Generalized Variance (detC).
for kk=1:size(C,3), detC(kk)=det(C(:,:,kk)); end
GMM_MODEL = gmdistribution(mu',C,alpha); % generate a GMM
degree_of_membership = posterior(GMM_MODEL,X); % calculate the posterior probability of observations

metric = detC;
metric=metric/sum(metric);
[~,sortNdx]=sort(metric);
mu=mu(:,sortNdx);C=C(:,:,sortNdx); detC=detC(sortNdx);  alpha=alpha(sortNdx);

% remove redundant clusters based on proximity (panAngle/Delay<0.5 is a cluster)
testMu=(mu.*[maxPanAngle maxDelaySamples]')';
[ removeNdx ] = removeRedundantMeans(testMu, 0.5);
detC(removeNdx)=[];alpha(removeNdx)=[];mu(:,removeNdx)=[];degree_of_membership(:,removeNdx)=[];C(:,:,removeNdx)=[];
% compute new statistics
GMM_MODEL = gmdistribution(mu',C,alpha);
degree_of_membership = posterior(GMM_MODEL,X); %posterior mixing probability
N=length(X);

kk=1;
percentageOverlap=0.001; % overlap threshold very small
while sum((degree_of_membership(:,1:kk)>percentageOverlap),2)<2 
    kk=kk+1;
end
% select K clusters
alphaEstimates = alpha(1:kk-1);
CEstimates = C(:,:,1:kk-1);
muEstimates = mu(:,1:kk-1);
muDegSampl = muEstimates.*[maxPanAngle maxDelaySamples]';

end
