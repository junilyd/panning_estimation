function [ repeatedValues, indexToRepeatedValue,numberOfAppearancesOfRepeatedValues ] = repetitions( input )
    uniqueInput = unique(input)
    countOfinput = hist(input,uniqueInput);
    indexToRepeatedValue = (countOfinput~=1);
    repeatedValues = uniqueInput(indexToRepeatedValue);
    numberOfAppearancesOfRepeatedValues = countOfinput(indexToRepeatedValue);
end

