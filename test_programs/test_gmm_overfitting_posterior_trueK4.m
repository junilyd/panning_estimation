clear all; clf;
kTrue=4;
for iter=1:5000
%%
elipses=[];
elipsesPruned=[];
% PARAMETERS FOR CLUSTERING
regVal  = 0.5e-4;
covType = 1;
plotFlag= 0;
thrStop = 1e-3; 
kmax=35;
thrForCorrect=.5;
%% create random sources from IOWA
fs = 44100;

maxNumSources=5; 
maxDelaySec= 150e-6;
maxDelaySamples = floor(maxDelaySec*fs);
maxPanAngle=44;
trueNumSources = kTrue;% randi(maxNumSources-1)+1;

[theta, delaySec, trueParams] = smc_random_panning_parameters(trueNumSources,fs, maxDelaySec, maxPanAngle);
[xFullband,fileNumber] = smc_apply_panning_to_iowa_max_duration(theta, delaySec, trueNumSources, fs);
% filter
fc = 4e3; % cutoff freq.
[b,a] = butter(6,fc/(fs/2));
x = filter(b,a,xFullband);

% parameters for the fourier transform
%% estimate spatial distribution space
wlen     = floor(600e-3*fs);
NMin = floor(wlen/4); KMax = 10; % only if optimal segmentation is applied
NFFT  = 2^nextpow2(NMin*KMax);

[X] = smc_estimate_panning_space_from_optimal_segments(x, ones(1000,1)*wlen, fs, trueNumSources, NFFT, maxDelaySamples, maxPanAngle);
%% estimate overfitted statististics
[k,alpha,mu,C] = gmm_overfitted(X',kmax,kmax,regVal,thrStop,covType,plotFlag);
%end
%%
GMM_MODEL = gmdistribution(mu',C,alpha); % generate a GMM
degree_of_membership = posterior(GMM_MODEL,X); % calculate the posterior probability of observations
N=length(X);
 
%% Evaluation of the clustering
ep = 0.01*eps; % add a very small number in order to prevent division by 0
degree_of_membership = (ep+degree_of_membership)/(1+ep*(size(degree_of_membership,2)));
% fm = (degree_of_membership).^2;
% PC = 1/N*sum(sum(fm)); % Partition Coefficient
% fm = (degree_of_membership).*log10(degree_of_membership);
% PE = -1/N*sum(sum(fm)); % Partition Entropy

%% compute the weighted density measure

% initialize muWeighted to be able to contain various selection lengths
muWeighted=zeros(KMax,21);
% sort by prior probabilities (alpha)
[prior2,ndx] = sort(alpha,'descend');
% locate redundant entries
testMu = [maxPanAngle maxDelaySamples].*mu(:,ndx)';
redundantNdx = removeRedundantMeans(testMu, thrForCorrect);
%remove redunancies
testMu(redundantNdx,:)=[]; prior2(redundantNdx)=[]; ndx(redundantNdx)=[];
muWeighted(1:length(ndx),1) = ndx;
muWeighted(1:size(testMu,1),2:3) = testMu;
muWeighted(1:length(prior2),4) = prior2;

% count fraction of population inside '1'sigma region
stdFactor=1;
[insideIndicator, elipses, detC] = compute_inside_indicator(X,mu,C,stdFactor);
p = sum(insideIndicator.*degree_of_membership)./(detC/sum(detC)); 
[pSorted,ndx] = sort(p/(sum(p)),'descend');

% locate redundant entries
testMu = [maxPanAngle maxDelaySamples].*mu(:,ndx)';
redundantNdx = removeRedundantMeans(testMu, thrForCorrect);
%remove redunancies
testMu(redundantNdx,:)=[]; pSorted(redundantNdx)=[]; ndx(redundantNdx)=[];

%    hold on; plot(pSorted)
muWeighted(1:length(ndx),6) = ndx;
%muWeighted(:,7:8) = [maxPanAngle maxDelaySamples].*mu(:,ndx)';
muWeighted(1:size(testMu,1),7:8) = testMu;
%muWeighted(:,9) = prior2(1:10);
muWeighted(1:length(pSorted),10) = pSorted;

d=2; % 2-dim measurements.
% class dependent metrics
for k=1:size(degree_of_membership,2)
    dm = degree_of_membership(:,k);
    I(k)= sum(dm); % for uplifting to detC^(N/2)
    Nk(k) = N*alpha(k);
    ML(k) = (2*pi)^(-Nk(k)*d/2)*(detC(k)/sum(detC))^(-Nk(k)/2)*exp(-Nk(k)*d/2);
    errorVector(:,:,k) = (mu(:,k)'-X).*dm;
    ICD(k) = sqrt( sum( sum(errorVector(:,:,k)'*errorVector(:,:,k)) ) )/N;%(Nk(k)) ; % euclidean distance. 
    %Gamma(k)=-1/2*(log(det(C(:,:,1)))-log(det(C(:,:,k)))) ...
    %    +errorVector(:,:,1)*inv(C(:,:,1))*errorVector(:,:,1)'-errorVector(:,:,k)'*inv(C(:,:,k))*errorVector(:,:,k))
end

%% sort by ICD
[ICDSorted,ndx] = sort(ICD);

% locate redundant entries
testMu = [maxPanAngle maxDelaySamples].*mu(:,ndx)';
redundantNdx = removeRedundantMeans(testMu, thrForCorrect);
%remove redunancies
testMu(redundantNdx,:)=[]; ICDSorted(redundantNdx)=[]; ndx(redundantNdx)=[];

muWeighted(1:length(ndx),12) = ndx;
muWeighted(1:size(testMu,1),13:14) = testMu ;%[maxPanAngle maxDelaySamples].*mu(:,ndx)';
muWeighted(1:length(ICDSorted),16) = ICDSorted;

%% beta method (detC/alpha)
% count fraction of population inside '1'sigma region
%stdFactor=1;
%[insideIndicator, elipses, detC] = compute_inside_indicator(X,mu,C,stdFactor);
beta = (detC/sum(detC))./alpha; 
[betaSorted,ndx] = sort(beta,'ascend');

% locate redundant entries
testMu = [maxPanAngle maxDelaySamples].*mu(:,ndx)';
redundantNdx = removeRedundantMeans(testMu, thrForCorrect);
%remove redunancies
testMu(redundantNdx,:)=[]; betaSorted(redundantNdx)=[]; ndx(redundantNdx)=[];

%    hold on; plot(pSorted)
muWeighted(1:length(ndx),18) = ndx;
%muWeighted(:,7:8) = [maxPanAngle maxDelaySamples].*mu(:,ndx)';
muWeighted(1:size(testMu,1),19:20) = testMu;
%muWeighted(:,9) = prior2(1:10);
muWeighted(1:length(betaSorted),21) = betaSorted;

%% gamma method (detC)
% count fraction of population inside '1'sigma region
%stdFactor=1;
%[insideIndicator, elipses, detC] = compute_inside_indicator(X,mu,C,stdFactor);
gamma = (detC/sum(detC));
[gammaSorted,ndx] = sort(gamma,'ascend');

% locate redundant entries
testMu = [maxPanAngle maxDelaySamples].*mu(:,ndx)';
redundantNdx = removeRedundantMeans(testMu, thrForCorrect);
%remove redunancies
testMu(redundantNdx,:)=[]; gammaSorted(redundantNdx)=[]; ndx(redundantNdx)=[];

%    hold on; plot(pSorted)
muWeighted(1:length(ndx),22) = ndx;
%muWeighted(:,7:8) = [maxPanAngle maxDelaySamples].*mu(:,ndx)';
muWeighted(1:size(testMu,1),23:24) = testMu;
%muWeighted(:,9) = prior2(1:10);
muWeighted(1:length(gammaSorted),25) = gammaSorted;

%% Delta method (detC)^(I/2)
% count fraction of population inside '1'sigma region
%stdFactor=1;
%[insideIndicator, elipses, detC] = compute_inside_indicator(X,mu,C,stdFactor);
Delta = (detC./sum(detC)).^(I/sum(I)/2);
Delta/sum(Delta);
[DeltaSorted,ndx] = sort(Delta,'ascend');

% locate redundant entries
testMu = [maxPanAngle maxDelaySamples].*mu(:,ndx)';
redundantNdx = removeRedundantMeans(testMu, thrForCorrect);
%remove redunancies
testMu(redundantNdx,:)=[]; DeltaSorted(redundantNdx)=[]; ndx(redundantNdx)=[];

%    hold on; plot(pSorted)
muWeighted(1:length(ndx),26) = ndx;
%muWeighted(:,7:8) = [maxPanAngle maxDelaySamples].*mu(:,ndx)';
muWeighted(1:size(testMu,1),27:28) = testMu;
%muWeighted(:,9) = prior2(1:10);
muWeighted(1:length(DeltaSorted),29) = DeltaSorted;

%% GV method (detC)^(alpha)
% count fraction of population inside '1'sigma region
%stdFactor=1;
%[insideIndicator, elipses, detC] = compute_inside_indicator(X,mu,C,stdFactor);
GV = (detC./sum(detC)).^(alpha);
[GVSorted,ndx] = sort(GV,'ascend');

% locate redundant entries
testMu = [maxPanAngle maxDelaySamples].*mu(:,ndx)';
redundantNdx = removeRedundantMeans(testMu, thrForCorrect);
%remove redunancies
testMu(redundantNdx,:)=[]; GVSorted(redundantNdx)=[]; ndx(redundantNdx)=[];

%    hold on; plot(pSorted)
muWeighted(1:length(ndx),30) = ndx;
%muWeighted(:,7:8) = [maxPanAngle maxDelaySamples].*mu(:,ndx)';
muWeighted(1:size(testMu,1),31:32) = testMu;
%muWeighted(:,9) = prior2(1:10);
muWeighted(1:length(GVSorted),33) = GVSorted;

%% GV method (detC)^(alpha)
% count fraction of population inside '1'sigma region
%stdFactor=1;
%[insideIndicator, elipses, detC] = compute_inside_indicator(X,mu,C,stdFactor);
GV = (detC).^(alpha);
[GVSorted,ndx] = sort(GV,'ascend');

% locate redundant entries
testMu = [maxPanAngle maxDelaySamples].*mu(:,ndx)';
redundantNdx = removeRedundantMeans(testMu, thrForCorrect);
%remove redunancies
testMu(redundantNdx,:)=[]; GVSorted(redundantNdx)=[]; ndx(redundantNdx)=[];

%    hold on; plot(pSorted)
muWeighted(1:length(ndx),34) = ndx;
%muWeighted(:,7:8) = [maxPanAngle maxDelaySamples].*mu(:,ndx)';
muWeighted(1:size(testMu,1),35:36) = testMu;
%muWeighted(:,9) = prior2(1:10);
muWeighted(1:length(GVSorted),37) = GVSorted;
%%

muSorted{iter} = muWeighted;
trueParameters{iter} = trueParams;
trueParams
muWeighted
% normalt test by counting samples
% thr = find((percentageInside(:,find(stdFactors==3)))>0.99& ...
%                (percentageInside(:,find(stdFactors==1)))>0.34 & ...
%                (percentageInside(:,end-1))>0);
% muSelected{tt} = (mu(:,thr) .*[maxPanAngle maxDelaySamples]')';

ICDSelected{iter} = ICDSorted;

origC{iter} = C;
origMu{iter}= mu;
origPrior{iter}=alpha;

clear ICD mu alpha C ICDSorted errorVector I
figure(70); 
subplot(121); 
plot(1:length(betaSorted),muWeighted(1:length(betaSorted),21),'-k*'); ylim([1e-9 0.5e-4])
subplot(122); 
plot(2:5,muWeighted(2:5,21),'-k*'); xticks(2:5); grid on; drawnow
muWeightedSaved{iter} = muWeighted;
end
