function [X, out] = smc_estimate_panning_space_from_optimal_segments(y, numSamplesInSegment, fs, truePoints, NFFT, MaxDelaySamples, maxPanAngle)

    D_vec=[];
    G_vec=[];
    ns=numSamplesInSegment;
    lns1=length(numSamplesInSegment);
    
    bb=1;
    win = blackman(numSamplesInSegment(bb));
    pos=1:numSamplesInSegment(bb);
    
    while (pos(end)<=length(y) && length(numSamplesInSegment)>=bb)
        xl=y(pos,1);
        xr=y(pos,2);
        
        Xl=(fft(blackmanharris(length(xl)).*xl,NFFT));
        Xr=(fft(blackmanharris(length(xr)).*xr,NFFT));  
        
        pwl = abs(fft(blackmanharris(length(xl)).*smc_prewhiten(xl),NFFT));
        pwr = abs(fft(blackmanharris(length(xr)).*smc_prewhiten(xr),NFFT));

        wdpiLimit = round(NFFT/2/MaxDelaySamples/2)-1;
        
        Xl(wdpiLimit:end) = [];
        Xr(wdpiLimit:end) = [];
        pwl(wdpiLimit:end) = [];
        pwr(wdpiLimit:end) = [];

        minBin=2;
        Xl(1:minBin-1) = [];
        Xr(1:minBin-1) = [];
        pwl(1:minBin-1) = [];         
        pwr(1:minBin-1) = [];         
                
        thrl = mean(pwl.*pwr);
        fl=find(pwl.*pwr>thrl);
        while length(fl) > 250 % 
            thrl = thrl + thrl*0.08;
            fl=find(pwl.*pwr>thrl);
        end                
        fAxis = fs/NFFT*(minBin:round(wdpiLimit)-1)';
        ratio = Xr(fl)./Xl(fl);
        
        G=acot(abs(ratio));  
        G_vec=[G_vec; G];
        D = -imag(log(ratio))./(2*pi*(fl-1)/NFFT); 
        D_vec=[D_vec; D];
       
        bb=bb+1;
        lns=length(numSamplesInSegment);
        if pos(end)<length(y)
            pos=(pos(end)+1:pos(end)+numSamplesInSegment(bb));
        end
    end
    out = [(D_vec) G_vec];
    % de-range the data
    if nargin < 7
        maxPanAngle = 45;
    end
%     maxDelay = 150e-6;
    GDMask = (abs(D_vec)<MaxDelaySamples) & (G_vec<maxPanAngle*pi/180*2);
       
    if sum(GDMask)>1
        % scale the gain vector to degrees.
        GD(:,1) = (G_vec(GDMask)./pi*180-45)/maxPanAngle;
        GD(:,2) = (D_vec(GDMask))/(MaxDelaySamples);
        X=GD;
    else
        X=[G_vec D_vec];
    end
  
end