clear all;
close all;
% dur = [300e-3 600e-3 900e-3 1200e-3 1500e-3 1800e-3 3000e-3];
% cd /home/jacob/panning_project/test_programs/
% load mats/synthetic_guitar_clustering_and_pruning.mat;
%startIter = size(savedx,1)-1;
%numSources = randi(4)+1;
%cd /home/jacob/Documents/MATLAB/code/test_programs/RMSE;
cd /Users/home/panning_estimation/test_programs
;%/home/jacob/panning_project/test_programs/
load mats/test_on_N/savedx_2min.mat;
fs=44100;
iterationNumber=1; % representing num. of durations
%durations = [60 50 40 30 20 10 8 6 4 2];
numIterations = 20; % representing num. of files to test.
duration = 6;
durSamples=duration*fs;

% previously wlen was 600e-3
windows = floor([200e-3 400e-3 600e-3 800e-3 1 1.2 1.4 1.6 1.8 2]*fs);
%NMin = floor(wlen/2);
% clear some memory
for iter=1:numIterations
    tmp = savedx{iter};
    savedx{iter} = tmp(1:durSamples,:);
end

for wlen = windows
    fs=44100;

    for iter=1:numIterations
     fs=44100;

    tmp = savedx{iter};
    x = tmp(1:durSamples,:);

    cd /home/jacob/panning_project/test_programs/

        maxDelaySec= 150e-6;
        maxDelaySamples = floor(maxDelaySec*fs);

        %[theta, delaySec, true(iter).Points] = smc_random_panning_parameters(numSources,fs, maxDelaySec);

        %[x] = smc_apply_panning_to_synthetic(sig,theta, delaySec, numSources, fs);

        %% Segmentation Part
        %wlen     = floor(600e-3*fs);
        %NMin = floor(wlen/2);
        w = hann(wlen);
        NFFT  = 2^nextpow2(fs*2);%2^14;

        %% Evaluate the given optimal segments 
        [X] = smc_estimate_panning_space_from_optimal_segments(x, ones(floor(length(x)/(wlen)),1)*wlen, fs, true(iter).Points, NFFT, maxDelaySamples);

        savedX{iter,1} = X;

        [~, bestk(iter),~,~,~,~,~, ~, finalMu(iter).MMDL, ~,~,~] = smc_GMM_MMDL_normdetC(X, fs,size(true(iter).Points,1));

        true(iter).Points
        finalMu(iter).MMDL 

        close all;
    end

    [errorAngle, errorDelay,trueNumClusters,NumClustersMMDL,errorRateModelOrder] = evaluate_synthetic(finalMu, true, iter, 5);

    correctAmplitudes = errorAngle(errorAngle<0.51);
    A_RMSE=sqrt(mean(correctAmplitudes.^2))
    numCorrectParameters = length(correctAmplitudes)
    totalParameterEst = sum(trueNumClusters)
    parameterPercentage = numCorrectParameters/totalParameterEst

    correctDelay = errorDelay(errorAngle<0.51);
    D_RMSE=sqrt(mean(correctDelay.^2))

    results_uniform(iterationNumber).errorAngle = errorAngle;
    results_uniform(iterationNumber).errorDelay = errorDelay;
    results_uniform(iterationNumber).trueNumClusters = trueNumClusters;
    results_uniform(iterationNumber).NumClustersMMDL = NumClustersMMDL;
    results_uniform(iterationNumber).errorRateModelorder = errorRateModelOrder;
    results_uniform(iterationNumber).iter = iter;
    results_uniform(iterationNumber).true = true;
    results_uniform(iterationNumber).finalMu = finalMu;
    results_uniform(iterationNumber).D_RMSE = D_RMSE;
    results_uniform(iterationNumber).totalParameterrEst = totalParameterEst;
    results_uniform(iterationNumber).NumCorrectParameters = numCorrectParameters;
    results_uniform(iterationNumber).parameterPercentage = parameterPercentage;
    results_uniform(iterationNumber).A_RMSE = A_RMSE;
    results_uniform(iterationNumber).savedX = savedX;
    results_uniform(iterationNumber).duration = duration;
    results_uniform(iterationNumber).wlen = wlen;
    
    

    save('mats/test_on_N/N_results_function_of_wlen.mat','results_uniform','-v7.3');
    clear savedX finalMu errorAngle errorDelay NumClustersMMDL errorRateModelOrder D_RMSE totalParameterEst numCorrectParameters parameterPercentage A_RMSE;
    iterationNumber = iterationNumber+1;
end

