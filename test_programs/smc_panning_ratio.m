% % ITD ILD Dan ellis 
% % Make a script to produce panned
% %sinusoids with phases also.

function [costFunction, panAngle] = smc_panning_ratio(x,fs)
    N = length(x);
    NFFT = N*10;
    w = smc_get_window(length(x), 'hann');
    [X(:,1),pX(:,1),~] = smc_dft(x(:,1), w, NFFT, fs);
    [X(:,2),pX(:,2),f] = smc_dft(x(:,2), w, NFFT, fs);
    X = 10.^(X./20);

    ratios = abs(X(:,1))./abs(X(:,2));

    panAngle = [-45:.2:45]';
    baseAngle = 45;
    phi0 = baseAngle * pi/180; 
    phi  = panAngle  * pi/180;
    theta = phi + phi0;
    g = [cos(theta) sin(theta)];
    ratiosToLookFor = g(:,1)./g(:,2);

    for ii = 1:length(ratiosToLookFor)
        tmp = ratios./ratiosToLookFor(ii);
        tmp = (tmp-1);
        tmp = tmp.*(-1);
        tmp = abs(tmp).*(-1);
        tmp = tmp + 1;
        tmp(tmp<0.95) = 0; 
        ratioFunction(:,ii) = tmp;
    end

    costFunction = sum(ratioFunction(1:floor(end/4),:));
    
end




