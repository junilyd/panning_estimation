clear all; clf;

for iter=1:200
%%
elipses=[];
elipsesPruned=[];
% PARAMETERS FOR CLUSTERING
regVal  = 0.5e-4;
covType = 1;
plotFlag= 0;
thrStop = 1e-3; 
kmax=35;
thrForCorrect=.5;
%% create random sources from IOWA
fs = 44100;

maxNumSources=5; 
maxDelaySec= 150e-6;
maxDelaySamples = floor(maxDelaySec*fs);
maxPanAngle=44;
trueNumSources = randi(maxNumSources-1)+1;

[theta, delaySec, trueParams] = smc_random_panning_parameters(trueNumSources,fs, maxDelaySec, maxPanAngle);
[xFullband,fileNumber] = smc_apply_panning_to_iowa_max_duration(theta, delaySec, trueNumSources, fs);
% filter
fc = 4e3; % cutoff freq.
[b,a] = butter(6,fc/(fs/2));
x = filter(b,a,xFullband);

% parameters for the fourier transform
%% estimate spatial distribution space
wlen     = floor(600e-3*fs);
NMin = floor(wlen/4); KMax = 10; % only if optimal segmentation is applied
NFFT  = 2^nextpow2(NMin*KMax);

[X] = smc_estimate_panning_space_from_optimal_segments(x, ones(1000,1)*wlen, fs, trueNumSources, NFFT, maxDelaySamples, maxPanAngle);
%% estimate overfitted statististics
[k,alpha,mu,C] = gmm_overfitted(X',kmax,kmax,regVal,thrStop,covType,plotFlag);
%end
%%
GMM_MODEL = gmdistribution(mu',C,alpha); % generate a GMM
degree_of_membership = posterior(GMM_MODEL,X); % calculate the posterior probability of observations
N=length(X);
 
%% Evaluation of the clustering
ep = 0.01*eps; % add a very small number in order to prevent division by 0
degree_of_membership = (ep+degree_of_membership)/(1+ep*(size(degree_of_membership,2)));
% fm = (degree_of_membership).^2;
% PC = 1/N*sum(sum(fm)); % Partition Coefficient
% fm = (degree_of_membership).*log10(degree_of_membership);
% PE = -1/N*sum(sum(fm)); % Partition Entropy

%% compute the weighted density measure

% initialize muWeighted to be able to contain various selection lengths
muWeighted=zeros(KMax,21);
% sort by prior probabilities (alpha)
[prior2,ndx] = sort(alpha,'descend');
% locate redundant entries
testMu = [maxPanAngle maxDelaySamples].*mu(:,ndx)';
redundantNdx = removeRedundantMeans(testMu, thrForCorrect);
%remove redunancies
testMu(redundantNdx,:)=[]; prior2(redundantNdx)=[]; ndx(redundantNdx)=[];
muWeighted(1:length(ndx),1) = ndx;
muWeighted(1:size(testMu,1),2:3) = testMu;
muWeighted(1:length(prior2),4) = prior2;

% count fraction of population inside '1'sigma region
stdFactor=1;
[insideIndicator, elipses, detC] = compute_inside_indicator(X,mu,C,stdFactor);
p = sum(insideIndicator.*degree_of_membership)./(detC/sum(detC)); 
[pSorted,ndx] = sort(p/(sum(p)),'descend');

% locate redundant entries
testMu = [maxPanAngle maxDelaySamples].*mu(:,ndx)';
redundantNdx = removeRedundantMeans(testMu, thrForCorrect);
%remove redunancies
testMu(redundantNdx,:)=[]; pSorted(redundantNdx)=[]; ndx(redundantNdx)=[];

%    hold on; plot(pSorted)
muWeighted(1:length(ndx),6) = ndx;
%muWeighted(:,7:8) = [maxPanAngle maxDelaySamples].*mu(:,ndx)';
muWeighted(1:size(testMu,1),7:8) = testMu;
%muWeighted(:,9) = prior2(1:10);
muWeighted(1:length(pSorted),10) = pSorted;

% interclass density/distance
for k=1:size(degree_of_membership,2)
    dm = sum(degree_of_membership(:,k),2);
    errorVector = (mu(:,k)'-X).*dm;
    ICD(k) = sqrt( sum( sum(errorVector'*errorVector) ) )/size(X,1) ; % euclidean distance. 
end
% sort by ICD
[ICDSorted,ndx] = sort(ICD);

% locate redundant entries
testMu = [maxPanAngle maxDelaySamples].*mu(:,ndx)';
redundantNdx = removeRedundantMeans(testMu, thrForCorrect);
%remove redunancies
testMu(redundantNdx,:)=[]; ICDSorted(redundantNdx)=[]; ndx(redundantNdx)=[];

muWeighted(1:length(ndx),12) = ndx;
muWeighted(1:size(testMu,1),13:14) = testMu ;%[maxPanAngle maxDelaySamples].*mu(:,ndx)';
muWeighted(1:length(ICDSorted),16) = ICDSorted;

%% new method
% count fraction of population inside '1'sigma region
%stdFactor=1;
%[insideIndicator, elipses, detC] = compute_inside_indicator(X,mu,C,stdFactor);
alpha2 = (detC/sum(detC))./alpha; 
[alphaSorted,ndx] = sort(alpha2/(sum(alpha2)),'ascend');

% locate redundant entries
testMu = [maxPanAngle maxDelaySamples].*mu(:,ndx)';
redundantNdx = removeRedundantMeans(testMu, thrForCorrect);
%remove redunancies
testMu(redundantNdx,:)=[]; alphaSorted(redundantNdx)=[]; ndx(redundantNdx)=[];

%    hold on; plot(pSorted)
muWeighted(1:length(ndx),18) = ndx;
%muWeighted(:,7:8) = [maxPanAngle maxDelaySamples].*mu(:,ndx)';
muWeighted(1:size(testMu,1),19:20) = testMu;
%muWeighted(:,9) = prior2(1:10);
muWeighted(1:length(alphaSorted),21) = alphaSorted;

trueParams
muSorted{tmuWeighted

clear ICD
end