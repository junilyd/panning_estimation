function [optimalVal,centers, optimalK] = smc_K_clustering_calinski_harabasz(X,maxNumClusters)
normVal(1) = norm(X(:,1));
normVal(2) = norm(X(:,2));
X=X./normVal;
X(sign(X(:,1))==sign(X(:,2)),:) = [];
% cd /home/jacob/panning_project/test_programs
% load mats/panning_data2.mat;
% X = panning_data;
% Calinski Harabasz Evaluation
myfunc = @(X,maxNumClusters)(kmeans(X,maxNumClusters,'Distance','cityblock','Replicates',12));
eva = evalclusters(X,myfunc,'CalinskiHarabasz','KList',[1:maxNumClusters]);%'CalinskiHarabasz','KList',[1:maxNumClusters]);
optimalK = eva.OptimalK;
critVal = eva.CriterionValues;
optimalVal = critVal(optimalK)/length(X);

%[~,centers] = kmeans(X,optimalK,'Distance','cityblock','Replicates',12);


% remove same sign clusters (pan right is: Gain(+) --> Delay(-) )
%signMask = sign(centers(:,1))~=sign(centers(:,2));      
%centers = centers(signMask,:) 
% remove too close clusters
%[~,ndx] = unique(round(centers.*normVal*2)/2,'rows');
%centers = centers(ndx,:);

[~,centers] = kmeans(X,optimalK,'Distance','cityblock','Replicates',12);

% 
figure(1);
X=X.*normVal.*[45 6];
centers=centers.*normVal.*[45 6];
[~,ndx] = unique(round(centers*2)/2,'rows');
centers = centers(ndx,:);

subplot(211); scatter(X(:,1),X(:,2),'.'); hold on;
              plot(centers(:,1),centers(:,2),'k+','linewidth',2);
              xlabel('Amplitude Angle [deg.]'); xlim([-40 40]);
              ylabel('Delay [samples]'); 
subplot(212); plot(eva);grid on;
% eva

end