%% Run GMM by EM, with k=20 Gaussians.
function Xred = smc_reduce_data_by_gmm_em(X, numGaussians, plotFlag)
	if nargin < 2
		k = 30;
	else
		k = numGaussians;
	end
	if nargin < 3,
		plotFlag=0;
	end

	init = kmeanspp(X',k);
	[labels,model,llh] = EMGMM(X,init);
	if plotFlag,
	    plotClass(X,labels); title(sprintf('%1.0f Gaussians on panning parameter estimates',k))
	    xlabel('Angle Estimate [deg]'); ylabel('Delay Estimate [samples]');% axis([-40 40 -10 10]);
	end
	
	%% Compute variance for each Gaussian Mode
	for cc=1:length(unique(labels))
	    vari(cc) = det(model.Sigma(:,:,cc))/ model.pi_k(cc);
        %p=vari(cc)*
	end

	%% Remove all class estimates with variance. above mean variance. 
	MASKaboveSigmaMean = vari>mean(vari)*0.005;
	model.mu(:,MASKaboveSigmaMean)=[];
	model.Sigma(:,MASKaboveSigmaMean)=[];
	model.pi_k(:,MASKaboveSigmaMean)=[];

	% find all data points lower than the mean var.
	classlabels = find(MASKaboveSigmaMean==0);
	Xred=[];
	for cl=1:length(classlabels)
	    Xred = [Xred; X(find(labels==classlabels(cl)),:)];
	end

end