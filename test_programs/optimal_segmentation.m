%%
clear all;
M=4; fs = 44100;
sti = '/home/jacob/audio/sqam/';
files={strcat(sti,'6.flac'),strcat(sti,'15.flac'),strcat(sti,'26.flac'),strcat(sti,'35.flac'),strcat(sti,'8.flac'),strcat(sti,'21.flac'),strcat(sti,'4.flac'),strcat(sti,'17.flac'),strcat(sti,'16.flac')};
theta=(    [-30  -10     10     30    ]+45)/180*pi;
delaySec = [200e-6 100e-6 -100e-6 -200e-6]/1.4;
truePoints = [theta(:)/pi*180-45 delaySec(:)*fs;];
fs=44100;
for m=1:M,
    [source{m},fs]=audioread(files{m}, [5*fs 6*fs]);
    len(m)=length(source{m});
end
len=min(len);
x=zeros(len,2);
delay = floor(abs(delaySec*fs));

for m=1:M,
    source{m}=source{m}(:,1);    
    source{m}=source{m}(1:len);
    source{m}=source{m}/var(source{m});    
end

for m=1:M;
  g(1,m)=sin(theta(m));
  g(2,m)=cos(theta(m));
  if delaySec(m) > 0 
    x(:,1)=x(:,1)+g(1,m)*source{m};
    x(:,2)=x(:,2)+g(2,m)*[zeros(delay(m),1); source{m}(1:end-delay(m))];
  else
    x(:,2)=x(:,2)+g(2,m)*source{m};
    x(:,1)=x(:,1)+g(1,m)*[zeros(delay(m),1); source{m}(1:end-delay(m))];
  end
end
    
    

%%
%clear all; close all;
% loop over 30-120 ms
% evaluate for each size 
% plot std
%fs=44100;
%[x,fs] = audioread('/Users/home/dropbox/Mixing_Parameter_Estimation/trumpet_horn_mixture.wav');
%[x,fs] = audioread('/Users/home/Music/Logic/sinusoidal_mix/Bounces/cello_mix_panned_28.wav',[3*fs 4*fs]);
%[x,fs] = audioread('/Users/home/Music/iTunes/iTunes Media/Music/Fears Unfolding/Unknown Album/Angel In Red Rough.mp3');
%x=x(fs*1:end,:);

% 
NMin  = floor(30e-3*fs);
KMax = 8;
NMax = NMin * KMax;
M = floor(length(x)/NMin);

k_opt=[];
numClusters=[];
mu1=[];
mu2=[];
m=1;
while (m*NMin) < (length(x))
    cost=[];
    J=[];
    K = min(m, KMax);
    for k=1:K
        xBlock = x(((m-k+1)*NMin:m*NMin),:);
        [J, numClusters(m)] = smc_kmeans_std(xBlock, length(xBlock)-1,fs);
        if (m-k) > 0
            cost(k) = J + smc_kmeans_std(x([1:(m-k)*NMin],:), length(xBlock)-1, fs); 
        else
            cost(k) = J;
        end
    end
    [~, k_opt(m)] = min(cost);
    m=m+1;
end
m = M; % is defined above the loop.
bb=1;
while (m > 0)
    numBlocksInSegment(bb) = k_opt(m);
    numClustersInSegment(bb) = numClusters(m);
    m=m-k_opt(m);
    bb=bb+1;
end
numBlocksInSegment = numBlocksInSegment(end:-1:1);
numClustersInSegment = numClustersInSegment(end:-1:1);

numSamplesInSegment = [numBlocksInSegment*NMin NMin+1]; % NMin+1 is added only to stop the next loop
[stdOutput, K, mu1] = smc_kmeans(x, numSamplesInSegment, fs, truePoints)

smc_kmeans(x,ones(1000,1)*floor(90e-3*fs),fs, truePoints)