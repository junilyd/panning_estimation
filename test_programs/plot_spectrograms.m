%plot specrtograms

cd /Users/home/panning_estimation/test_programs;
load mats/test_on_N/final_experiment.mat
%load  mats/test_on_N/test_MMDL_82iter_18April.mat
%%
ndx = find(trueNumClusters==3);
iter=ndx(end-13)

            X = savedX{iter}; x = savedx{iter}; numBlocksInSegment = savedNumBlocksInSegment{iter};
                plotNMin = floor(wlen/4);
            NFFT  = 2^nextpow2(wlen*2);%2^14;
            w = hann(wlen/4);
            Xr = smc_stft_complex(x(:,1), w, plotNMin, NFFT);
            Xl = smc_stft_complex(x(:,2), w, plotNMin, NFFT);
figure; [figureNumber,t] = smc_plot(x,fs);
clf;
        figure;
        tmp=0;

        hf = subplot(212)
        subplot(212)
            (imagesc([1:(size(Xr,2))]*NMin/fs-NMin/fs/2, linspace(0,1,(NFFT/2-1))*fs/2, 20*log10(abs(Xr(1:NFFT/2-1,:)))));
            axis xy;
            caxis(max(caxis)+[-45 0]);
            xlabel('Time [sec]'); ylabel('Frequency [Hz]');
            %title(sprintf('Spectrogram of track'));
            ylim([0 fs/14]);       
            hold on; 
        ht = subplot(211)
            plot(t,[x(:,1) x(:,2)]);%*700+2500)
%            plot([x(:,2) x(:,1)]);%*700+2500)

            colors={'k','w'};
        for pl=1:2
            tmp=0; % to fit the above
        subplot(2,1,pl); hold on;
            for ii=1:length(numBlocksInSegment) 
                hold on; plot([tmp+numBlocksInSegment(ii)*NMin/fs tmp+numBlocksInSegment(ii)*NMin/fs],[-1 fs/2],colors{pl})
                tmp=tmp+numBlocksInSegment(ii)*NMin/fs;
                xlim([0 15])
            end
        end
        subplot(211)
            ylim([-1 1]);

        
%        (plot(tAXis, s1*2-1.5,'k.'));%(s1-0.9)*24000,'k.'));
%        (plot(tAXis, s2-1.5,'k.'));%((s2)-0.9)*25500,'k.')); 
        set(gca,'xtick',[])
        l=legend('left signal', 'right signal','active source indicator')
        
        l.Orientation = 'horizontal';
%          yyaxis right;
        ylabel('Amplitude');
        axf = get(hf,'Position');
        axf(2)=axf(2)+0; %or wathever
        axf(4)=axf(4)+0.25
        set(hf,'Position',axf);
        
         axt = get(ht,'Position');         
         axt(2)=axt(2)+0.13
         axt(4)=axt(4)-0.12;
         set(ht,'Position',axt);
                

        