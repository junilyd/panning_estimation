function [errorAngle, errorDelay,trueNumClusters,estimatedNumClusters,errorRateModelOrder, totalEstimatedParamsPruned, trueMinusEstimated] = evaluate_overfitting_precision_recall(saved, thr)
iter = size(saved.X,2);

if nargin == 1
    plotFlag = 0;
end

% recently commented out!
% for hh=1:iter   
%     m=[];
%     t = saved.trueParams{hh};
%     m = saved.muPruned{hh}';
% end

for hh=1:iter
    %fprintf('ii: %1.0f, true: %1.0f, MMDL: %1.0f\n',hh, saved.trueNumSource{hh},saved.kPruned{hh});
    trueMinusEstimated(hh) = abs(saved.trueNumSource{hh}-saved.kPruned{hh});
    trueNumClusters(hh) = saved.trueNumSource{hh};
    estimatedNumClusters(hh) = saved.kPruned{hh};
end
errorRateModelOrder = 100-100*(sum(estimatedNumClusters==trueNumClusters)/iter);

errorAngle=[];
errorDelay=[];
for hh=1:iter
    tp = saved.trueParams{hh};
%     [~,ndx] = sort(tp(:,1));
%     tp = tp(ndx,:);
    
    ep = saved.muPruned{hh}';
    totalEstimatedParamsPruned{hh} = ep;
%     [~,ndx] = sort(ep(:,1));
%     ep = ep(ndx,:);

% instead of sorting we should remove clusters closer that are
% closer than threshold used for detecting correct clusters.
% Then recall<=1
% for ii=1:size(ep,1)
%     ndx=find(abs(ep(ii,1)-ep(:,1))<thr & abs(ep(ii,2)-ep(:,2))<thr); 
%     ep(ndx(2:end),:)=0;
% end
% ep(ep(:,1)==0,:)=[];

for qq=1:size(ep,1)
    %if size(ep,1)>=size(tp,1)
    %    qq;
    %    [tmpErrorAngle(qq),minndx] = min(abs(tp(qq,1)-ep(:,1)))
    %    tmpErrorDelay(qq) = abs(ep(minndx,2)-tp(qq,2))
    %    ep
    %    minndx
    %    %pause
    %else
        qq;
        [tmpErrorAngle(qq),minndx] = min(abs(tp(:,1)-ep(qq,1)));
        tmpErrorDelay(qq) = abs(ep(qq,2)-tp(minndx,2));
        %pause
    %end
end    
    
    % detect the clusters that has been found
%     min(length(tp),size(ep,1));
%     for qq=1:min(size(tp,1),size(ep,1))
%         if size(ep,1)>=size(tp,1)
%             qq;
%             [tmpErrorAngle(qq),minndx] = min(abs(tp(qq,1)-ep(:,1)))
%             tmpErrorDelay(qq) = abs(ep(minndx,2)-tp(qq,2))
%             ep
%             minndx
%             %pause
%         else
%             qq;
%             [tmpErrorAngle(qq),minndx] = min(abs(tp(:,1)-ep(qq,1)))
%             tmpErrorDelay(qq) = abs(ep(qq,2)-tp(minndx,2))
%             %pause
%         end
%     end

    errorAngle=[errorAngle tmpErrorAngle];
    errorDelay=[errorDelay tmpErrorDelay];
    tmpErrorAngle=[];
    tmpErrorDelay=[];
end


