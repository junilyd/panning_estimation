clear all;
%close all;
%a = dir('/home/jacob/sms-tools/sounds/*.wav');
numSources = 2;
a = dir('/home/jacob/audio/sqam/*.flac');
fs=44100;
indexes = randi(length(a),numSources,1);

%files={'08.wav','13.wav','21.wav','25.wav'};
%files = {'/Users/home/dropbox/testfiles/firebird2/string1/1.wav', ...
        % '/Users/home/dropbox/testfiles/firebird2/string1/2.wav', ...
        % '/Users/home/dropbox/testfiles/firebird2/string1/3.wav', ...
        % '/Users/home/dropbox/testfiles/firebird2/string1/4.wav', ...
        % '/Users/home/dropbox/testfiles/firebird2/string1/5.wav', ...
        % '/Users/home/dropbox/testfiles/firebird2/string1/6.wav'};
for ii=1:numSources
  files{ii} = strcat('/home/jacob/audio/sqam/',a(indexes(ii)).name);
end

M=length(files);    
theta = (45+[-35 -25 -15 0 10 25 45])/180*pi ;

startPoint = fs*4;
%len=18*fs-startPoint;% 40436;
for m=1:M,
    [source{m},fs]=audioread(files{m});
    len(m)=length(source{m});
end


len=min(len);
y=zeros(len,2);

for m=1:M,
    source{m}=source{m}(:,1);    
    source{m}=source{m}(1:len);
    source{m}=source{m}/var(source{m});
end

for m=1:M;
  g(1,m)=cos(theta(m));
  g(2,m)=sin(theta(m));
  y(:,1)=y(:,1)+g(1,m)*source{m};
  y(:,2)=y(:,2)+g(2,m)*source{m};
end

theta(1:M)

G_vec=[];
thr=2e3;

N=round(0.15*fs);
F=2^19;
w = hann(N);
pos=1:N;
ii=1;
while pos(end)<len
   xl=y(pos,1).*w;
   xr=y(pos,2).*w;
   
    pl=smc_prewhiten(xl);
    pr=smc_prewhiten(xr);
    Pl=abs(fft(pl,F));
    Pr=abs(fft(pr,F));
    O=abs(Pl).*abs(Pr);
    thr=mean(O);

   Xl=abs(fft(xl,F));
   Xr=abs(fft(xr,F));   

   G_tmp1=atan(abs(Xr(O>thr)./Xl(O>thr)));      
   G_tmp2=acot(abs(Xl(O>thr)./Xr(O>thr)));

   G_tmp=[G_tmp1; G_tmp2];
   
   G_vec=[G_vec; G_tmp];
   
   pos=pos+N;
   if sum(G_tmp1>0)
      c(ii,:) = hist(G_tmp,200);
      kd(ii,:) = ksdensity(G_tmp,'Function','pdf');
      ii = ii+1;
   end
end

% plotting
angleRes = 1/0.5 * 90;
figure(1);clf;
    d = hist(G_vec,angleRes);
    plot(linspace(-45,45,angleRes), (d/sum(d)));
figure(2);clf;
    ksdensity(G_vec,'Function','pdf');
figure(3);clf;
subplot(311)
    imagesc(linspace(-45,45,angleRes),[1:ii],c); axis xy; 
        xlabel('Estimated Angle [deg]'); ylabel('Time [sec]');
subplot(312)
    imagesc(linspace(-45,45,angleRes),[1:ii],kd); axis xy; 
        xlabel('Estimated Angle [deg]'); ylabel('Time [sec]');
subplot(313)
    plot(linspace(-45,45,angleRes), (d/sum(d)));