cd /home/jacob/panning_project/test_programs/
%clear all;
load('mats/test_on_N/N_results_optimal_minimize.mat');
%load mats/test_on_N/N_results_uniform_merged.mat;
%load mats/test_on_N/N_results_uniform.mat;

% durations = [60 50 40 30 20 10 8 6 4 2];

%% old results
% uniform segmentation results
% for n=1:size(results_uniform,2)
%     clusterPercentage_u(n) = results_uniform(n).parameterPercentage;
%     A_RMSE_u(n) = results_uniform(n).A_RMSE;
%     D_RMSE_u(n) = results_uniform(n).D_RMSE;
%     errorRateModelOrder_u(n) = results_uniform(n).errorRateModelorder; 
%     trueNumClusters_u{n} = results_uniform(n).trueNumClusters;
%     estimatedNumClusters_u{n} = results_uniform(n).NumClustersMMDL;
% end
% % optimal segmentation results
% for n=1:size(results_optimal,2)
%     clusterPercentage_o(n) = results_optimal(n).parameterPercentage;
%     A_RMSE_o(n) = results_optimal(n).A_RMSE;
%     D_RMSE_o(n) = results_optimal(n).D_RMSE;
%     errorRateModelOrder_o(n) = results_optimal(n).errorRateModelorder; 
%     durations_o(n) = results_optimal(n).duration;
%     estimatedNumClusters_o{n} = results_optimal(n).NumClustersMMDL;
% end

%% New results
elipses=[];
elipsesPruned=[];
% PARAMETERS FOR CLUSTERING
regVal  = 0.5e-4;
covType = 1;
plotFlag= 0;
thrStop = 1e-3; 
kmax=35;
thrForCorrect=.5;
%% create random sources from IOWA
fs = 44100;

for iter2=2:size(results_optimal,2)

for iter=1:size(results_optimal(iter2).savedX,1)

%X=results_uniform(iter2).savedX{iter};
%a=results_uniform(iter2).true(iter);

X=results_optimal(iter2).savedX{iter};
a=results_optimal(iter2).true(iter);

trueParams=a.Points.*[-1 1];
X(:,1)=-X(:,1);
fs = 44100;
X = [X;[2*rand(floor(size(X,1)/2),1)-1, 2*rand(floor(size(X,1)/2),1)-1]];

maxNumSources=5; 
maxDelaySec= 150e-6;
maxDelaySamples = floor(maxDelaySec*fs);
maxPanAngle=45;


[k,alpha,mu,C] = gmm_overfitted(X',kmax,kmax,regVal,thrStop,covType,plotFlag);
for kk=1:size(C,3), detC(kk)=det(C(:,:,kk)); end
GMM_MODEL = gmdistribution(mu',C,alpha);%,'CovType','diagonal'); % generate a GMM
degree_of_membership = posterior(GMM_MODEL,X); % calculate the posterior probability of observations

metric = detC;
metric=metric/sum(metric);
[metric,sortNdx]=sort(metric);
mu=mu(:,sortNdx);C=C(:,:,sortNdx); detC=detC(sortNdx);  alpha=alpha(sortNdx);

testMu=(mu.*[maxPanAngle maxDelaySamples]')';
[ removeNdx ] = removeRedundantMeans(testMu, 0.5);
detC(removeNdx)=[];alpha(removeNdx)=[];mu(:,removeNdx)=[];degree_of_membership(:,removeNdx)=[];C(:,:,removeNdx)=[];

GMM_MODEL = gmdistribution(mu',C,alpha);
degree_of_membership = posterior(GMM_MODEL,X); %posterior mixing probability
N=length(X);

kk=1;
percentageOverlap=0.00001;
while sum((degree_of_membership(:,1:kk)>percentageOverlap),2)<2 
    if size(degree_of_membership,2) == kk 
        break;
    else
        kk=kk+1,
    end
end
trueParams
mu(:,1:kk-1).*[maxPanAngle maxDelaySamples]'
muEstimates{iter} = mu(:,1:kk-1).*[maxPanAngle maxDelaySamples]';  


trueParameters{iter} = trueParams;
origC{iter} = C;
origMu{iter}= mu;
origPrior{iter}=alpha;

%% Partition Entropy
%% Evaluation of the clustering
% ep = 0.01*eps; % add a very small number in order to prevent division by 0
% 
% for kk=1:size(degree_of_membership,2)
%     df=[]; fm=[]; clear GMM_MODEL
%     GMM_MODEL = gmdistribution(mu(:,1:kk)',C(:,:,1:kk),alpha(1:kk));%,'CovType','diagonal'); % generate a GMM
%     df = posterior(GMM_MODEL,X); % calculate the posterior probability of observations
%     df = (ep+df)/(1+ep*(size(df,2)));
%     fm = (df).^2;
%     PCoeff(kk) = 1/N*sum(sum(fm(:,1:kk))); % Partition Coefficient
%     fm = df.*log10(df);
%     PEntropy(kk) = -1/N*sum(sum(fm)); % Partition Entropy
% end
%figure(749)
%subplot(211);plot(1:kk,PCoeff,'-ok'); grid minor; legend('part. coeff'); ylim([0.95 1]); drawnow;
%subplot(212);plot(1:kk,PEntropy,'-ob');grid minor; legend('part. entropy'); ylim([0 0.05]); drawnow;
%PCDiff{iter}=[size(trueParams,1)/100 diff(PCoeff(1:6))]*1e2;
%PEDiff{iter}=[size(trueParams,1)/100 diff(PEntropy(1:6))]*1e2;
%PCDiff{iter}
%PEDiff{iter}
%PCSaved{iter}=PCoeff;
%PESaved{iter}=PEntropy;
clear  mu alpha C degree_of_membership detC GMM_MODEL X %PCoeff PEntropy

end


%% Evaluation
for hh=1:iter
    tp = trueParameters{hh}    
    ep = muEstimates{hh}'
    totalEstimatedParamsPruned{hh} = ep;
    qq=1;
    ndx=1;
    while ~isempty(ndx)
        % locate the cluster estimate in tp
        [ndx] = find( abs(ep(qq,1)-tp(:,1))<thrForCorrect & abs(ep(qq,2)-tp(:,2))<thrForCorrect,2); 
        % ERROR - not recalling all, but found a wrong estimate
        if isempty(ndx), 
            firstErrorNdx(hh)=qq; 
            truePositive(hh)=qq-1; 
            falsePositive(hh)=size(ep,1)-truePositive(hh); 
            break; 
        end 
        % set the recent estimated cluster to 'nan'
        tp(ndx,:)=[nan nan]; 
        % ERROR - all clusters have been found and there are more estimates.
        if isnan(tp), 
            %collectRecallOverestimate(hh)= size(ep,1)-size(tp,1); 
            truePositive(hh)=size(tp,1); 
            falsePositive(hh)=size(ep,1)-size(tp,1); 
        break;
        end 
        
        qq=qq+1;
        % ERROR - there are less estimates than clusters
        if qq>size(ep,1), 
            firstErrorNdx(hh)=size(ep,1)-size(tp,1); 
            truePositive(hh) = qq-1;
            break;
        end
    end    
    
end


%% check difference between true and estimated.
for hh=1:iter
    sizeTp(hh)=size(trueParameters{hh},1);
    sizeEp(hh)=size(muEstimates{hh}',1);
end

 
%fe=firstErrorNdx(hh), 

tp=truePositive; 
fp=falsePositive; 

truePositives=sum(truePositive);
falsePositives=sum(falsePositive);
%oe=collectRecallOverestimate(hh)
    relevantElements = sum(sizeTp);
    % Precision Recall
    truePositives = sum(truePositives);
    %falsePositives = sum(estimatedNumClusters)-truePositives(k);
    precision(10,iter2) =  truePositives/(truePositives+falsePositives)
    recall(10,iter2) = truePositives/relevantElements
end