close all;
load panning_data;
data=panning_data';
%X=data;
[numPoints, dimension] = size(data')

for numClusters = 1:20;
% [means, sigmas, priors,ll] = vl_gmm(data, numClusters) 
% 
% 
% figure ;
% hold on ;
% plot(data(1,:),data(2,:),'r.') ;
% for i=1:numClusters
%     vl_plotframe([means(:,i)' sigmas(1,i) 0 sigmas(2,i)]);
% end

% Run KMeans to pre-cluster the data
[initMeans, assignments] = vl_kmeans(data, numClusters, ...
    'Algorithm','Lloyd', ...
    'MaxNumIterations',12);

initCovariances = zeros(dimension,numClusters);
initPriors = zeros(1,numClusters);

% Find the initial means, covariances and priors
for i=1:numClusters
    data_k = data(:,assignments==i);
    initPriors(i) = size(data_k,2) / numClusters;

    if size(data_k,1) == 0 || size(data_k,2) == 0
        initCovariances(:,i) = diag(cov(data'));
    else
        initCovariances(:,i) = diag(cov(data_k'));
    end
end

% Run EM starting from the given parameters
[means,covariances,priors,ll,posteriors] = vl_gmm(data, numClusters, ...
    'initialization','custom', ...
    'InitMeans',initMeans, ...
    'InitCovariances',initCovariances, ...
    'InitPriors',initPriors, ...
    'NumRepetitions',10, ...
    'CovarianceBound',2e-3);

BIC(numClusters) = -ll + log(dimension)*numPoints + 3/2*log(numPoints)
% BIC = NlogL + log(n)*n + 3/2*log(n);

% Plot
figure; 
    scatter(data(1,:),data(2,:)); hold on;
    for ii=1:numClusters
        plot(means(1,ii),means(2,ii),'k+','linewidth',15);
    end
end

figure; 
    plot(BIC);
    xlabel('$k$','interpreter', 'latex');
    ylabel('BIC');
    