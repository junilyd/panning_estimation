% clear all; close all;
% cd /home/jacob/panning_project/test_programs
load mats/test_MMDL_82iter_18April.mat
% X = savedX{43};
% X=X';

function smc_plotGMM(X, bestmu, bestcov, bestpp)
    res=100;
    regVal=0.5e-4;
    covType=0;
    plotFlag=2;
    fs=44100;
    if nargin < 2
        [bestk,bestpp,bestmu,bestcov,dl,countf,mindl,safemu, safemuMean, safemuSelected] = mixtures4_remove_outliers(X,2,25,regVal,1e-4,covType,plotFlag,fs); % regVal was 1e-4
    end
    
    xx=[];
    yy=[];
    semi_indic=[];
    indic=[];
    ii=1;
    for x=linspace(-1,1,res)
        xx(ii:ii+res-1)=x;
        yy(ii:ii+res-1) = linspace(-1,1,res);
        ii=ii+res;
    end

    for i=1:size(bestcov,3)
        semi_indic(i,:) = multinorm([xx; yy],bestmu(:,i),bestcov(:,:,i));
        indic(i,:) = semi_indic(i,:)*bestpp(i);
    end

    A = sum(indic);
    A = reshape(A,res,res);
    x=linspace(-1,1,res);
    %figure; 
    surfc(x*45,x*150e-6*fs,log(A/sum(sum(A))));%,'FaceColor','interp','EdgeColor','blue','FaceLighting','phong');
    xlabel('Amplitude Panning Angle [deg.]');
    ylabel('Delay [samples]');
    zlabel('log-likelihood');
    %
    % obj = gmdistribution(bestmu',bestcov,bestpp);
    % x = -1:.05:1; y = x; n=length(x); a=zeros(n,n);
    % for i = 1:n, for j = 1:n, a(i,j) = pdf(obj,[x(i) y(j)]); end, end;
    % surfc(x*45,y*150e-6*fs,a+10,'FaceColor','interp','EdgeColor','none','FaceLighting','phong')

end