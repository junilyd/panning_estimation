function [muEstimates,muDegSampl,alphaEstimates, CEstimates] = smc_estimate_source_panning_across_segments(xFullband,fs)
% 
% check if these exists outside function first
if ~exist('maxNumSources') & ~exist('maxDelaySec')'
    maxNumSources=5; 
    maxDelaySec= 150e-6;
    maxDelaySamples = floor(maxDelaySec*fs);
    maxPanAngle=44;
   % trueNumSources =  randi(maxNumSources-1)+1;
end
   
% filter to be compliant with downsampled signals.
fc = 4e3; % cutoff freq.
[b,a] = butter(6,fc/(fs/2));
x = filter(b,a,xFullband);

%% estimate spatial distribution space
wlen     = floor(600e-3*fs);
NMin = floor(wlen/4); KMax = 10; % only used, if optimal segmentation is applied
NFFT  = 2^nextpow2(NMin*KMax);
[X] = smc_estimate_panning_space_from_optimal_segments(x, ones(1000,1)*wlen, fs, 0, NFFT, maxDelaySamples, maxPanAngle);

%% Estimate overfitted statististics
% PARAMETERS FOR CLUSTERING
regVal  = 0.5e-4;
covType = 1; % 1: diagonal
plotFlag= 0;
thrStop = 1e-3; 
kmax=35;
thrForCorrect=.5;
[k,alpha,mu,C] = gmm_overfitted(X',kmax,kmax,regVal,thrStop,covType,plotFlag);

%%  sort cluster candidates statistics
%   according to the metric of Generalized Variance (detC).
for kk=1:size(C,3), detC(kk)=det(C(:,:,kk)); end
GMM_MODEL = gmdistribution(mu',C,alpha);; % generate a GMM
degree_of_membership = posterior(GMM_MODEL,X); % calculate the posterior probability of observations

metric = detC;
metric=metric/sum(metric);
[~,sortNdx]=sort(metric);
mu=mu(:,sortNdx);C=C(:,:,sortNdx); detC=detC(sortNdx);  alpha=alpha(sortNdx);

% remove redundant clusters based on proximity (panAngle/Delay<0.5 is a cluster)
testMu=(mu.*[maxPanAngle maxDelaySamples]')';
[ removeNdx ] = removeRedundantMeans(testMu, 0.5);
detC(removeNdx)=[];alpha(removeNdx)=[];mu(:,removeNdx)=[];degree_of_membership(:,removeNdx)=[];C(:,:,removeNdx)=[];
% compute new statistics
GMM_MODEL = gmdistribution(mu',C,alpha);
degree_of_membership = posterior(GMM_MODEL,X); %posterior mixing probability
N=length(X);

kk=1;
percentageOverlap=0.001; % overlap threshold very small
while sum((degree_of_membership(:,1:kk)>percentageOverlap),2)<2 
    kk=kk+1;
end
% select first K clusters
alphaEstimates = alpha(1:kk-1);
CEstimates = C(:,:,1:kk-1);
muEstimates = mu(:,1:kk-1);
muDegSampl = muEstimates.*[maxPanAngle maxDelaySamples]';

% the partition entropy could be used for selecting clusters as well.
% %% Partition Entropy
% %% Evaluation of the clustering
% ep = 0.01*eps; % add a very small number in order to prevent division by 0
% for kk=1:size(degree_of_membership,2)
%     df=[]; fm=[]; clear GMM_MODEL
%     GMM_MODEL = gmdistribution(mu(:,1:kk)',C(:,:,1:kk),alpha(1:kk));%,'CovType','diagonal'); % generate a GMM
%     df = posterior(GMM_MODEL,X); % calculate the posterior probability of observations
%     df = (ep+df)/(1+ep*(size(df,2)));
%     fm = (df).^2;
%     PCoeff(kk) = 1/N*sum(sum(fm(:,1:kk))); % Partition Coefficient
%     fm = df.*log10(df);
%     PEntropy(kk) = -1/N*sum(sum(fm)); % Partition Entropy
% end
end
