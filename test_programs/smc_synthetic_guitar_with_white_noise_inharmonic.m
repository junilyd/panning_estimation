%% Synthetic Guitar signal with white noise (inharmonics)
%
% v. 1.0 Jacob Møller
% edited v. 1.1 (150323) Anders
%
% INPUT:
%       fs : samplre freq.
%  duration: wanted signal length in [ms]
%         L: number of harmonics to make the signal
%       SNR: Wanted signal to noise ratio in signal
%       fig: 0= NO figures shown, 1=figures shown
%
% OUTPUT
%                    xe: guitar signal incl. white complex noise
%                  sige: guitar signal incl. white noise
%                    f0: fundamental freq. in signal
%  betaCoeffReturnValue: beta coefficent in inharmonc model
%      aHatReturnVector: non-complex amplitudes in signal
%            fBetaCoeff: vector of beta coeffecients
%                  xVar: Variance of noise in signal (for use of CRLB)
%           
%

function [xe, sige, f0, betaCoeffReturnValue, aHatReturnVector, fBetaCoeff, xVar] = smc_synthetic_guitar_with_white_noise_inharmonic(fs,duration,L,SNR,fig)
if L>18, L=18; end;
% if fig=1, plots will be made
if nargin < 5, fig=0; end;
% signal window length
N = floor(fs*duration);
% Stiffness Coefficient and harmonic amplitudes 
load a_hat_file_used_in_test_on_L;
% aHat has dim: (18,6,13) = (L,string,fret)

% Random fundamental frequency in Hz that fits on a guitar
maxFreq = 659.2;
minFreq = 82.4;
f0 = (maxFreq-minFreq)*rand+minFreq;

% make a lookup table for amplitudes in (aHat)
frequencyReference = [82.407 110 146.833 195.998 246.942 329.628]';
stringFretMatrix([1:6],:) = frequencyReference([1:6])*2.^([0:12]/12);
[tmp, fret] = min(min(abs(stringFretMatrix-f0)));
[tmp, string] = min(abs(stringFretMatrix(:,fret)-f0));

% Normalize amplitudes to sum = 1;
for l=1:L,
    aHatAbs(l,string,fret) = abs(aHat(l,string,fret))/sum(abs(aHat(1:L,string,fret)));
end
aHatReturnVector = aHatAbs(:,string,fret);

% inharmonic partials
fBetaCoeff = f0.*[1:L].*sqrt(1+betaCoeff(1,string,fret).*[1:L].^2);
betaCoeffReturnValue = betaCoeff(1,string,fret);


%% ------------------- REAL SIGNAL to be hilbert transformed -------------------
% Produce synthetic guitar signal
sig = smc_sum_of_sines(fBetaCoeff, aHatAbs(:,string,fret), L, duration, fs);
% Apply Guitar Envelope
envelope = smc_envelope_guitar(fs, duration);
sig = smc_envelope_apply(sig, envelope);
sig = smc_fadeout(sig,5,'log');
xHilbert = smc_hilbert(sig);
%% -----------------------------------------------------------------------------
%% ------------------- Complex synthetic signal eq(1.5) in MGC Book ------------
% Produce synthetic guitar signal
x = smc_sum_of_sines_complex(fBetaCoeff, aHat(:,string,fret), L, duration, fs);
% Apply Guitar Envelope
envelope = smc_envelope_guitar(fs, duration);
x = smc_envelope_apply(x, envelope);
x = smc_fadeout(x,5,'log');

if fig == 1,
% plot frequency spectrum
    [f,tmp,XdBReal] = smc_fft(sig,fs);
    [f,tmp2,XdBHilbert] = smc_fft(xHilbert,fs);
    [f,tmp3,XdB] = smc_fft(x,fs);

    figure(1)
    subplot(311); plot(f,XdBReal); xlim([0 10000]); grid; title('Real Synthetic Sinusoids'); axis([0 f0*(L+1) -50 100]);
    subplot(312); plot(f,XdBHilbert); xlim([0 10000]); grid; title('Hilbert Transformation'); axis([0 f0*(L+1) -50 100]);
    subplot(313); plot(f,XdB); xlim([0 10000]); grid; title('Complex Synthetic Sinusoids'); axis([0 f0*(L+1) -50 100]);

    ix = smc_ihilbert(x);
    figure(2)
    subplot(211);plot(sig), title('Real Signal'); xlim([0 5100]);
    subplot(212);plot(ix) , title('Complex Signal');    xlim([0 5100]);
% sound(sig, fs);
% pause;
% sound(ix, fs);
end
% Add Noise
[xe, xVar] =    smc_AWGN_complex(x, SNR);
sige =    smc_AWGN(sig, SNR);
if fig == 1,
MATLABe = awgn(x,SNR,'measured');
% plot frequency spectrum with noise
    [f,X,XedBComplex] = smc_fft(xe,fs);
    [f,X,XedBReal] = smc_fft(sige,fs);
    [f,X,XedBMATLAB] = smc_fft(MATLABe,fs);
    figure(3)
        subplot(311); plot(f,XedBReal); xlim([0 10000]); grid;  title(sprintf('Real AWGN - SNR=%1.0fdB',SNR )); axis([0 10e3 -50 100]);
        subplot(312); plot(f,XedBComplex); xlim([0 10000]); grid;  title(sprintf('Complex AWGN - SNR=%1.0fdB',SNR )); axis([0 10e3 -50 100]);
        subplot(313); plot(f,XedBMATLAB); xlim([0 10000]); grid;  title('Complex MATLAB AWGN'); axis([0 10e3 -50 100]);
end
