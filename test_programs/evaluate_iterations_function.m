function evaluate_iterations_function(safemu, true, iter, maxNumClusters)

for ii=1:iter
    m=[];
    t = true(ii).Points;
    m = safemu(ii).Mean.*[1 8.8/10];
    
    figure(length(true(ii).Points));
    hold on
%     if ~isempty(m)
        for nn=1:size(m,1)
            text(m(nn,1), m(nn,2), sprintf('%1.0f',ii),'Color','blue','horizontalalignment','right');
        end
        plot(t(:,1), t(:,2),'k+','markersize',12);
        for nn=1:length(t)
            text(t(nn,1), t(nn,2), sprintf('%1.0f',ii));
        end
        plot(m(:,1), m(:,2), 'bx','markersize',18)
%     end
    m=[];
    m = safemu(ii).Selected.*[1 9/10];
    plot(m(:,1), m(:,2), 'r*')

    m=[];
    m = safemu(ii).covRatio.*[45 9];
    plot(m(:,1), m(:,2), 'g*')
    
    title(sprintf('Estimates of panning parameters (%1.0f clusters)',length(true(ii).Points)))
    xlabel('Pan Angle Estimate');
    ylabel('Delay Estimate');

end
for ii=2:maxNumClusters
    figure(ii)
%     legend('True Mean','MMDL covRatio','MMDL smoothed','MMDL Picked');
end

for ii=1:iter
    fprintf('true: %1.0f, MMDL: %1.0f\n',length(true(ii).Points),length(safemu(ii).Mean))
    trueNumClusters(ii) = length(true(ii).Points);
    NumClustersMMDL(ii) = length(safemu(ii).Mean);
end
trueNumClusters
NumClustersMMDL
errorRateClusters = 100-100*(sum(NumClustersMMDL==trueNumClusters)/iter)
clear trueNumClusters NumClustersMMDL
%% same with reduced data
% for ii=1:iter
%     t = true(ii).Points;
%     m = safemu(ii).RedMean;
%     figure(length(true(ii).Points)+iter);
%     hold on
%     for nn=1:size(m,1)
%         text(m(nn,1), m(nn,2), sprintf('%1.0f',ii),'Color','blue','horizontalalignment','right');
%     end
%     plot(t(:,1), t(:,2),'k+','markersize',12);
%     for nn=1:length(t)
%         text(t(nn,1), t(nn,2), sprintf('%1.0f',ii));
%     end
%     plot(m(:,1), m(:,2), 'bx','markersize',18)
%     m = safemu(ii).RedSelected
%     plot(m(:,1), m(:,2), 'r.')
% 
%     m = safemu(ii).RedSelected ;
%     plot(m(:,1), m(:,2), 'g.')
%     
%     title(sprintf('Estimates of panning parameters (%1.0f clusters)',length(true(ii).Points)))
%     xlabel('Pan Angle Estimate');
%     ylabel('Delay Estimate');
% 
% end
% for ii=2:maxNumClusters
%     figure(ii+iter)
%     legend('True redMean','MMDL redfirst','MMDL redsmoothed','MMDL redPicked');
% end

% for ii=iter
%     trueNumClusters(ii) = length(true(ii).Points);
%     NumClustersMMDL(ii) = length(safemu(ii).RedMean);
% end
% errorRateClusters = 100*(1-sum(NumClustersMMDL==trueNumClusters)/iter)