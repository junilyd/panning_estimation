% evaluate for duration 18 sec.
clear all;% clf
cd /home/jacob/panning_project/test_programs/
% GOD --> load test_MMDL_40iter_4April_dur_18sec.mat
%load test_MMDL_40iter_findfejl_27March_dur_18sec.mat
%load mats/sep2017/test_SQAM_diagonal_morenoisy_k25.mat
%load mats/sep2017/test_SQAM_diagonal_noisy_k25_250x12.mat
%load mats/sep2017/test_IOWA_diagonal_250samples_k25.mat
%load mats/sep2017/test_SQAM_diagonal_250samples_k35_non-contra.mat
%load mats/sep2017/test_IOWA_diagonal_250samples_k35_non-contra.mat

%%%%%%%%%%%%%%% ONLY For QDC method %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%load('mats/sep2017/test_IOWA_QDCdiagonal_250samples_k35_110iter.mat');

% ms(:,2:3)': prior based, 
% ms(:,7:8)', determinant based
% ms(:,13:14)': ICD based, 
%load('mats/sep2017/test_IOWA_QDCdiagonal_250samples_k35_200iter.mat'); % Lab presentation
%load('mats/sep2017/test_IOWA_QDCdiagonal_250samples_k35_111iter_noRedundant.mat'); % test with 2D pruning
load('mats/sep2017/test_IOWA_QDCdiagonal_250samples_k35_200iter_noRedundant_GMM.mat');
methodNdx = [2;7;19]%19 was 13
for methodNumber = 1:3


% test for true number of clusters
for kTrue = 2:5
thrForCorrect = 0.5; % both for angle(degrees) and samples(delay)

% define true number of clusters
for ii=1:size(muSorted,2)
    trueNumClusters(ii) = size(trueParameters{ii},1);
end
% extract all data corresponding to the true number of clusters
clusterMask = (trueNumClusters==kTrue);
indexOfDataToExtract = find(clusterMask)
for ii=1:sum(clusterMask)
    trueParametersByTrueClusters{ii} = trueParameters{indexOfDataToExtract(ii)};
    %muSelectedByTrueClusters{ii} = muSelected{indexOfDataToExtract(ii)};
    muSortedByTrueClusters{ii} = muSorted{indexOfDataToExtract(ii)};
end


kVec=1:14;
for k=kVec
    saved.trueParams = trueParametersByTrueClusters;
    saved.X=trueParametersByTrueClusters; 
    for hh=1:size(muSortedByTrueClusters,2)
        ms = muSortedByTrueClusters{hh}; 
        saved.muPruned{hh} = ms(1:k,methodNdx(methodNumber):methodNdx(methodNumber)+1)'; % ms(:,13:14)': ICD based, ms(:,2:3)': prior based, ms(:,7:8)', determinant based
        saved.trueNumSource{hh} = size(trueParametersByTrueClusters{hh},1);
        saved.kPruned{hh} = size(saved.muPruned{hh},2);
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % evaluation
    [errorAngle, errorDelay,trueNumClusters,estimatedNumClusters,errorRateModelOrder,totalEstimatedParamsPruned, trueMinusEstimated] ...
        = evaluate_overfitting_precision_recall(saved,thrForCorrect);

    correctMask=(errorAngle<thrForCorrect) & (errorDelay<thrForCorrect);
    correctAmplitudes = errorAngle(correctMask);
    correctDelay = errorDelay(correctMask);

    relevantElements(k) = sum(trueNumClusters)
    %numCorrectParamEstimates = length(correctAmplitudes)
    %parameterPercentage = numCorrectParamEstimates/relevantElements;

    A_RMSE(k,methodNumber, kTrue)=sqrt(mean(correctAmplitudes.^2))
    D_RMSE(k, methodNumber, kTrue)=sqrt(mean(correctDelay.^2))

    % Precision Recall
    truePositives(k) = sum(correctMask);
    falsePositives(k) = sum(estimatedNumClusters)-truePositives(k);
    precision(k) =  truePositives(k)/(truePositives(k)+falsePositives(k))
    recall(k) = truePositives(k)/relevantElements(k)
    %clear estimatedNumClusters correctMask correctAmplitudes correctDelay errorAngle errorDelay ms trueNumClusters trueNumClusters
end
prec(:,methodNumber,kTrue) = precision';
recll(:,methodNumber,kTrue) = recall';
clear precision recall
end
end

% plot precision and recall results
figure(3);
for kTrue=2:5
subplot(2,2,kTrue-1); hold on;
title(sprintf('Performance for true k=%1.0f',kTrue))
        h1=plot(kVec,recll(:,1,kTrue),'b-*',kVec,prec(:,1,kTrue),'b--*', ...
                kVec,recll(:,2,kTrue),'k-o',kVec,prec(:,2,kTrue),'k--o',...
                kVec,recll(:,3,kTrue),'r-d',kVec,prec(:,3,kTrue),'r--d');
        xlabel('k'); ylabel('prob.'); ylim([0.1 1]); grid minor;
columnlegend(3,{'Prior: Recall'      ,'Prior: Precision', ...
                'Determinant: Recall','Determinant: Precision', ...
                'Density: Recall'    ,'Density: Precision'} ...
                ,'location','south');
end

% plot RMSE errors
figure(4);
for kTrue=2:5
subplot(2,2,kTrue-1); hold on;
title(sprintf('RMSE for true k=%1.0f',kTrue))
        h2=plot(kVec,A_RMSE(:,1,kTrue),'b-*',kVec,D_RMSE(:,1,kTrue),'b--*', ...
                kVec,A_RMSE(:,2,kTrue),'k-o',kVec,D_RMSE(:,2,kTrue),'k--o',...
                kVec,A_RMSE(:,3,kTrue),'r-d',kVec,D_RMSE(:,3,kTrue),'r--d');
        xlabel('k'); ylabel('Hz or Samples'); ylim([0 .11]);  grid minor;
columnlegend(3,{'Prior: A_{RMSE}'      ,'Prior: D_{RMSE}', ...
                'Determinant: A_{RMSE}','Determinant: D_{RMSE}', ...
                'Density: A_{RMSE}'    ,'Density: D_{RMSE}'} ...
                ,'location','north');
end

%export_fig -pdf -transparent /home/jacob/panning_project/test_programs/testfigure