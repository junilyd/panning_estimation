function [x] = smc_apply_panning(x, theta, delaySec, numSources, fs)

    for m=1:numSources,
     [tmp]=x(:,m);
     source{m} = tmp(1:end,1);
        len(m)=length(source{m});
    end
    len=min(len);
    x=zeros(len,2);
    
    delay = floor(abs(delaySec*fs));

    for m=1:numSources
        source{m}=source{m}(:,1)/max(abs( source{m}(:,1) ));    
        source{m}=source{m}(1:len);
        source{m}=source{m}/max(abs(source{m}));%/var(source{m});    
    end

    for m=1:numSources
      g(1,m)=sin(theta(m));
      g(2,m)=cos(theta(m));
      if delaySec(m) > 0 
        x(:,1)=x(:,1)+g(1,m)*source{m};
        x(:,2)=x(:,2)+g(2,m)*[zeros(delay(m),1); source{m}(1:end-delay(m))];
      else
        x(:,2)=x(:,2)+g(2,m)*source{m};
        x(:,1)=x(:,1)+g(1,m)*[zeros(delay(m),1); source{m}(1:end-delay(m))];
      end
    end
    x=x./max(max(abs(x)));
end