% clear all;
% %close all;
% 
% 
% %files={'08.wav','13.wav','21.wav','25.wav'};
% files = {'/Users/home/dropbox/testfiles/firebird2/string1/1.wav', ...
%         '/Users/home/dropbox/testfiles/firebird2/string1/2.wav', ...
%         '/Users/home/dropbox/testfiles/firebird2/string1/3.wav', ...
%         '/Users/home/dropbox/testfiles/firebird2/string1/4.wav', ...
%         '/Users/home/dropbox/testfiles/firebird2/string1/5.wav', ...
%         '/Users/home/dropbox/testfiles/firebird2/string1/6.wav'};
% 
% M=length(files);    
% theta = (45+[-35 -25 -15 0 10 25 45])/180*pi ;
% 
% startPoint = 58850;
% len=88200-startPoint;% 40436;
% for m=1:M,
%     [source{m},fs]=audioread(files{m}, [startPoint,88200]);
%     len(m)=length(source{m});
% end
% 
% 
% len=min(len);
% y=zeros(len,2);
% 
% for m=1:M,
%     source{m}=source{m}(:,1);    
%     source{m}=source{m}(1:len);
%     source{m}=source{m}/var(source{m});
% end
% 
% for m=1:M;
%   g(1,m)=cos(theta(m));
%   g(2,m)=sin(theta(m));
%   y(:,1)=y(:,1)+g(1,m)*source{m};
%   y(:,2)=y(:,2)+g(2,m)*source{m};
% end
% 
% theta(1:M)

function [c, G_vec] = panning_estimator(y, frameSize, fs)
if nargin < 2
    frameSize = 0.15;
    fs = 44100;
end
angleRes = 1/0.5 * 90 +1;

len = length(y);
G_vec=[];
thr=0;

N=floor(frameSize*fs);
F=2^nextpow2(N*10);%2^19;
w = hann(N);
pos=1:N;
ii=1;
while pos(end)<len
   xl=y(pos,1).*w;
   xr=y(pos,2).*w;
   
   Xl=abs(fft(xl,F));
   Xr=abs(fft(xr,F));  
   
   Xl(end/2:end) = [];
   Xr(end/2:end) = [];
   Xl = Xl(1:floor(end/4));
   Xr = Xr(1:floor(end/4));
  
   G_tmp1=atan(abs(Xr(Xl>thr)./Xl(Xl>thr)));      
   G_tmp2=acot(abs(Xl(Xr>thr)./Xr(Xr>thr)));

   G_tmp=[G_tmp1; G_tmp2];
   
   G_vec=[G_vec; G_tmp];
   
   pos=pos+N;
   
   c(ii,:) = hist(G_tmp,angleRes);
   d(ii,:) = ksdensity(G_tmp,'Function','pdf');
   ii=ii+1;
end

% plotting
tAxis = [1:ii].*N./fs;
figure(4);clf;
subplot(221)
    imagesc(linspace(-45,45,angleRes),tAxis,c); axis xy; 
        xlabel('Estimated Angle [deg]'); ylabel('Time [sec]');
 subplot(222)
    plot(linspace(-45,45,angleRes), sum(c));
 	('Estimated Angle [deg]'); ylabel('sum of energy');
 subplot(223)
     imagesc(linspace(-45,45,angleRes),tAxis,d); axis xy; 
         xlabel('Estimated Angle [deg]'); ylabel('Time [sec]');
 subplot(224)
    plot(sum(d));%linspace(-45,45,angleRes),sum(d));
 	('Estimated Angle [deg]'); ylabel('sum of energy');
end