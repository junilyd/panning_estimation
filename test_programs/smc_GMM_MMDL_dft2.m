function [mindl, bestk, bestmu] = smc_GMM_MMDL_dft2(x1, x2, fs, NFFT,MaxDelaySamples)

%%
    D_vec=[];
    G_vec=[];
   
    %% I have made a new thr that uses new functions 
    % difference abs ofX1 and X2
    % similarity also
    % so threshold is as usual but also they have to be similar.
    
        X1_white = abs(fft(blackmanharris(length(x1)).*smc_prewhiten(x1),NFFT));
        X2_white = abs(fft(blackmanharris(length(x2)).*smc_prewhiten(x2),NFFT));
        X1 = fft(blackmanharris(length(x1)).*x1,NFFT);
        X2 = fft(blackmanharris(length(x2)).*x2,NFFT);
        
        narrowband_limit = floor(NFFT/2/MaxDelaySamples/2);
        
        X1(narrowband_limit:end) = [];
        X2(narrowband_limit:end) = [];
        X1_white(narrowband_limit:end) = [];
        X2_white(narrowband_limit:end) = [];
        
        minBin=2;
        X1(1:minBin-1) = [];
        X2(1:minBin-1) = [];
        X1_white(1:minBin-1) = [];
        X2_white(1:minBin-1) = [];
        
        oldthrl = min(20*mean(abs(X1)),0.4*max(abs(X1)) );
        
        absX1 = abs(X1);
        absX2 = abs(X2);
        O = absX1.*absX2;
        
        thrl = mean(X1_white.*X2_white);
        
        
        fl=find(X1_white.*X2_white>thrl);
        
        ratio = X2(fl)./X1(fl);
        
        G=acot(abs(ratio));  
        G_vec=[G_vec; G];

        D = -imag(log(ratio))./(2*pi*(fl-1)/NFFT); 
        D_vec=[D_vec; D];
        
    % de-range the data
    maxPanAngle = 45;
    maxDelay = 150e-6;
    GDMask = (abs(D_vec)<maxDelay*fs) & (G_vec<maxPanAngle*pi/180*2);
    if sum(GDMask)>10*2 % not sure about this threshhold
        % scale the gain vector to degrees.
        GD(:,1) = G_vec(GDMask)./pi*180-45;
        GD(:,2) = D_vec(GDMask);
    else
        mindl =0;
        bestk = 0;
        bestmu = 0;
        return;
    end
    
    % Normalizing the range
    X = GD.*[1/45 1/6];

    %% Clustering Using Gaussian Mixture Models
covType = 0;
%if (covType==1 || covType == 2)
    regVal = 0.5e-4; % this was 1e-2 before.
% else
%     regVal = 0;
% end
    if size(X,1)>10*2 % not sure about this threshhold
        % scale the gain vector to degrees.
        [mindl, bestk, bestmu, bestcov,safemu, safemuMean, safemuSelected, bestmuCovRatio,X2,bestpp] = smc_GMM_MMDL_normdetC_analysis(X, fs, 2);
        %[bestk,~,bestmu,bestcov,dl,countf,mindl] = mixtures4(X',2,2,regVal,1e-4,covType,0); 
        mindl
        [mindl, bestk, bestmu, bestcov,safemu, safemuMean, safemuSelected, bestmuCovRatio,X2,bestpp] = smc_GMM_MMDL_normdetC_analysis(X2, fs, 2);
        mindl
    else
        mindl = -1e6;
        bestk = 0;
        bestmu = 0;
        return;
    end
