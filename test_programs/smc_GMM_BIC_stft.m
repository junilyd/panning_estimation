function [bestBICVal, kGMM, mu1] = smc_GMM_BIC_stft(Xl, Xr, numBlocksInSegment, fs, NFFT, truePoints)

%%
    D_vec=[];
    G_vec=[];
   
    bb=1;
    pos=1:numBlocksInSegment(bb);
    while pos(end)<size(Xl,2) %len,
        for ii=1:length(pos)
            X1 = Xl(1:NFFT/2-1,pos(ii));
            X2 = Xr(1:NFFT/2-1,pos(ii));
%pos(ii)
            thrl = min(20*mean(abs(Xl(:,pos(ii)))),0.4*max(abs(Xl(:,pos(ii)))) );       

            fl=find(abs(X1)>thrl); % thr

            ratio = X2(fl)./X1(fl);

            G=acot(abs(ratio));  
            G_vec=[G_vec; G];

            D = -imag(log(ratio))./(2*pi*(fl-1)/NFFT); 
            D_vec=[D_vec; D] ;
        end
        bb=bb+1;
        pos=(pos(end)+1:pos(end)+numBlocksInSegment(bb));
    end

        % de-range the data
    maxPanAngle = 45;
    maxDelay = 200e-6;
    GDMask = (abs(D_vec)<maxDelay*fs) & (G_vec<maxPanAngle*pi/180*2);
    sum(GDMask)
    if sum(GDMask)>10*3 % not sure about this threshhold
        % scale the gain vector to degrees.
        GD(:,1) = G_vec(GDMask)./pi*180-45;
        GD(:,2) = D_vec(GDMask);
    
    
        % Normalizing the range    
        X = GD; %normalize2d(GD);

        %% Clustering Using Gaussian Mixture Models

        % Covariance Structure Options
        [n,p] = size(X);

        %% Tune Gaussian Mixture Models - selecting in the range of 1-10 clusters.
        k = 1:10;
        nK = numel(k);
        Sigma = {'diagonal','full'};
        nSigma = numel(Sigma);
        SharedCovariance = {true,false};
        SCtext = {'true','false'};
        nSC = numel(SharedCovariance);
        RegularizationValue = 0.05;
        options = statset('MaxIter',1000);
        %
        % Fit the GMMs using the parameter combination of shared full covariance.  
        % Intialize with k-means clustering. Compute the BIC and for each fit.  
        % Track the terminal convergence status of each fit.

        % Preallocation
        gm = cell(nK,nSigma,nSC);         
        aic = zeros(nK,nSigma,nSC);
        bic = zeros(nK,nSigma,nSC);
        converged = false(nK,nSigma,nSC);

        % Fit all models
        for m = 1 %1:nSC
        for j = 2 %1:nSigma
            for i = 1:nK
                gm{i,j,m} = fitgmdist(X,k(i),...
                    'CovarianceType',Sigma{j},...
                    'SharedCovariance',SharedCovariance{m},...
                    'RegularizationValue',RegularizationValue,...
                    'Options',options,'Start',kmeans(X,i,'Distance','cityblock','Replicates',12));
                bic(i,j,m) = gm{i,j,m}.BIC;
                converged(i,j,m) = gm{i,j,m}.Converged;
            end
        end
        end
        allConverge = (sum(converged(:)) == nK*nSigma*nSC);

        %% differentiate the BIC
        dfbc = [0; diff(bic(:,2,1))];
        %dfbc = dfbc.*(1-(0:length(dfbc)-1).*0.05)' % Regularization of 5%

        [~, bestBIC] = min(dfbc);
        bestBICVal = bic(bestBIC,2,1); % why this model ??½

        % Cluster the training data using the best fitting model.  
        gmBest = gm{bestBIC,2,1};
        clusterX = cluster(gmBest,X);
        kGMM = gmBest.NumComponents;
        d = 500;
        x1 = linspace(min(X(:,1)) - 2,max(X(:,1)) + 2,d);
        x2 = linspace(min(X(:,2)) - 2,max(X(:,2)) + 2,d);
        [x1grid,x2grid] = meshgrid(x1,x2);
        X0 = [x1grid(:) x2grid(:)];
        mahalDist = mahal(gmBest,X0);
        threshold = sqrt(chi2inv(0.99,1));

        mu1 = gmBest.mu(:,1);
        mu2 = gmBest.mu(:,2);
        
        figure;
            h1 = gscatter(X(:,1),X(:,2),clusterX);
            hold on;
            for j = 1:kGMM
                idx = mahalDist(:,j)<=threshold;
                Color = h1(j).Color*0.75 + -0.5*(h1(j).Color - 1);
                h2 = plot(X0(idx,1),X0(idx,2),'.','Color',Color,'MarkerSize',1);
                uistack(h2,'bottom');
            end
            h3 = plot(gmBest.mu(:,1),gmBest.mu(:,2),'kx','LineWidth',2,'MarkerSize',10);
            title('Panning Parameter Distributions');
            xlabel('Amplitude Angle Estimate');
            ylabel('Delay Estimate');
            legend(h1,'Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5','Cluster 6','Cluster 7','Cluster 8','Cluster 9','Location','NorthWest');
            hold off;
        
    else
        bestBICVal=nan;
        kGMM=0;
    end
    