%% Run GMM by EM, with k=20 Gaussians.
% Initalize with k=20 labels
function Xred = smc_reduce_data_by_gmm_em_ND(X, numGaussians, plotFlag)
	if nargin < 2
		k = 30;
	else
		k = numGaussians;
	end
	if nargin < 3,
		plotFlag=0;
	end
% 	if size(X,1)>size(X,2)
% 		X=X';%abs(X)';
% 		transposed = 1;
% 	else
% 		transposed = 0;
% 	end

%	n = size(X,2);
%	d = size(X,1);
	init = kmeanspp(X',k);
	[labels,model,llh] = EMGMM_ND(X,init);
	if plotFlag,
	    plotClass(X,labels); title(sprintf('%1.0f Gaussians on panning parameter estimates',k))
	    xlabel('Angle Estimate [deg]'); ylabel('Delay Estimate [samples]');% axis([-40 40 -10 10]);
	end
	
	%% Compute variance for each Gaussian Mode
	for cc=1:length(unique(labels))
	    vari(cc) = trace(model.Sigma(:,:,cc));
	end

	%% Remove all class estimates with variance. above mean variance. 
    model
	MASKaboveSigmaMean = vari>mean(vari);
	model.mu(:,MASKaboveSigmaMean)=[];
	model.Sigma(:,MASKaboveSigmaMean)=[];
	model.pi_k(:,MASKaboveSigmaMean)=[];

	% find all data points lower than the mean var.
	classlabels = find(MASKaboveSigmaMean==0)
	Xred=[];
	for cl=1:length(classlabels)
	    Xred = [Xred; X(find(labels==classlabels(cl)),:)];
	end
% 	if transposed ~=0
% 		Xred = Xred'; 
% 	end
end