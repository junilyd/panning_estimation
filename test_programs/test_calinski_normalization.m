%%
clear all;
close all;

cd ~/panning_project/test_programs/;
load panning_data;
X=panning_data;
maxNumClusters = 20;

figure; scatter(X(:,1),X(:,2));

myfunc = @(X,maxNumClusters)(kmeans(X,maxNumClusters,'Distance','cityblock','Replicates',12));
eva = evalclusters(X,myfunc,'CalinskiHarabasz','KList',[1:maxNumClusters]);%'CalinskiHarabasz','KList',[1:maxNumClusters]);
optimalK = eva.OptimalK;
critVal = eva.CriterionValues;
optimalVal = critVal(optimalK);

clf(1)
[~,centers] = kmeans(X,optimalK,'Distance','cityblock','Replicates',12);

figure(1);
subplot(211); scatter(X(:,1),X(:,2),'.'); hold on;
              plot(centers(:,1),centers(:,2),'k+','linewidth',2);
              xlabel('Amplitude Angle [deg.]'); xlim([-40 40]);
              ylabel('Delay [samples]'); 
subplot(212); plot(eva);grid on;


c1=critVal;
c2=critVal/length(X)
c1n=c1/max(c1);
c2n=c2/max(c2);

%%
Y = datasample(X,floor(length(X)/5),1,'Replace',false);
figure; scatter(Y(:,1),Y(:,2));

myfunc = @(Y,maxNumClusters)(kmeans(Y,maxNumClusters,'Distance','cityblock','Replicates',12));
eva_y = evalclusters(Y,myfunc,'CalinskiHarabasz','KList',[1:maxNumClusters]);%'CalinskiHarabasz','KList',[1:maxNumClusters]);
optimalK_y = eva_y.OptimalK;
critVal_y = eva_y.CriterionValues;
optimalVal_y = critVal_y(optimalK);

[~,centers] = kmeans(Y,optimalK_y,'Distance','cityblock','Replicates',12);

figure(2);
subplot(211); scatter(Y(:,1),Y(:,2),'.'); hold on;
              plot(centers(:,1),centers(:,2),'k+','linewidth',2);
              xlabel('Amplitude Angle [deg.]'); xlim([-40 40]);
              ylabel('Delay [samples]'); 
subplot(212); plot(eva_y);grid on;

c1_y=critVal_y;
c2_y=critVal_y/length(Y)
c1n_y=c1_y/max(c1_y);
c2n_y=c2_y/max(c2_y);