% knee point detection
for ii=1:200
tmp = muWeightedSaved{ii};
graph=tmp(:,21);
% intialize
cv = graph(1);
pv = graph(1);
av = graph(1);
% begin
for m=1:length(graph)-1
    cv = graph(m);
    av = graph(m+1);
    diffFunc(m) = pv+av-8*cv;
    pv = cv;
end
clear localMax a;
[a,localMax]=findpeaks(diffFunc);
aSorted  = sort(a,'descend');
 for n=2:length(localMax)-1
     a(n) = atan(abs(aSorted(n+1)-aSorted(n))) + atan(abs(aSorted(n)-aSorted(n-1)));
 end
localMax
plot(a)
pause
end