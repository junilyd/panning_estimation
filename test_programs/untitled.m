[k,alpha,mu,C] = gmm_overfitted(X',kmax,kmax,regVal,thrStop,covType,plotFlag);
for kk=1:kmax, detC(kk)=det(C(:,:,kk)); end
GMM_MODEL = gmdistribution(mu',C,alpha);%,'CovType','diagonal'); % generate a GMM
degree_of_membership = posterior(GMM_MODEL,X); % calculate the posterior probability of observations

%%
% Punique=sum(degree_of_membership==0)/sum(sum(degree_of_membership==0));
% removeNdx=find(Punique==0);
% detC(removeNdx)=[];alpha(removeNdx)=[];mu(:,removeNdx)=[];degree_of_membership(:,removeNdx)=[];C(:,:,removeNdx)=[];
% removeNdx=[];

metric = detC;
metric=metric/sum(metric);
[metric,sortNdx]=sort(metric);
mu=mu(:,sortNdx);C=C(:,:,sortNdx); detC=detC(sortNdx);  alpha=alpha(sortNdx);

testMu=(mu.*[maxPanAngle maxDelaySamples]')';
[ removeNdx ] = removeRedundantMeans(testMu, 0.5); 
detC(removeNdx)=[];alpha(removeNdx)=[];mu(:,removeNdx)=[];degree_of_membership(:,removeNdx)=[];C(:,:,removeNdx)=[];

GMM_MODEL = gmdistribution(mu',C,alpha);
degree_of_membership = posterior(GMM_MODEL,X); %posterior mixing probability

kk=size(mu,2);
while kk>1
    outlierIndex = [];
    for nn=1:N
        %if 
            sum(degree_of_membership(nn,:)~=0)>1
        end
            ndx=find(degree_of_membership(nn,1:kk)~=0);
            if ndx(end)==kk
                GMM_MODEL = gmdistribution(mu(:,1:kk-1)',C(:,:,1:kk-1),alpha(1:kk-1));
                degree_of_membership = posterior(GMM_MODEL,X);
                break
            end
        end
        %[~,minNdx] = min(metric(ndx));
        %if length(ndx)>1
        %    ndx(minNdx) = [];
        %    outlierIndex = [outlierIndex ndx];
        %end
    end 
    kk=kk-1
end
% for kk=size(mu,2):-1:2
%     outlierIndex = [];
%     for nn=1:N
%         ndx=find(degree_of_membership(nn,:)~=0);
%         [~,minNdx] = min(metric(ndx));
%         if length(ndx)>1
%             ndx(minNdx) = [];
%             outlierIndex = [outlierIndex ndx];
%         end
%     end
% %     if sum(unique(outlierIndex)==size(mu,2)+1-kk)==1
% %         
% end
mu(:,unique(outlierIndex)) = [];
trueParams
mu.*[maxPanAngle maxDelaySamples]'