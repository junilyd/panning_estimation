%%  AM3D_convert_to_WEKA.m
%   This is an example of how to format your data on order to 
%   convert to data to feature files for WEKA and PRTools.
%   version 1.0 Jacob M�ller
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%  Define the "featureNames" vector.
featureNames = {'Amplitude', 'Delay'}
fileName = 'panning';
filePath = '/home/jacob/panning_project/test_programs';
dim = length(featureNames);
% for ii=1:dim
%     data(:,ii) = eval(featureNames{ii});
% end
labelVector = clusterX;
data = X;
classNames = {'1','2','3','4','5','6','7'};

AM3D_convert_data_to_arff(data, labelVector, featureNames, fileName, filePath, classNames )

%AM3D_convert_data_to_arff(data, labelVector, featureNames, fileName, filePath)