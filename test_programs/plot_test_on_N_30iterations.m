clear all; close all;

cd /home/jacob/panning_project/test_programs/;
load mats/test_on_N/N_results_uniform_merged.mat;
%load mats/test_on_N/N_results_optimal_erronous_duration10s.mat
load mats/test_on_N/N_results_optimal_maximize_30iterations.mat

durations = [60 50 40 30 20 10 8 6 4 2];

% uniform segmentation results
for n=1:size(results_uniform,2)
    clusterPercentage_u(n) = results_uniform(n).parameterPercentage;
    A_RMSE_u(n) = results_uniform(n).A_RMSE;
    D_RMSE_u(n) = results_uniform(n).D_RMSE;
    errorRateModelOrder_u(n) = results_uniform(n).errorRateModelorder; 
    trueNumClusters_u{n} = results_uniform(n).trueNumClusters;
    estimatedNumClusters_u{n} = results_uniform(n).NumClustersMMDL;

end
% optimal segmentation results
for n=1:size(results_optimal,2)
    clusterPercentage_o(n) = results_optimal(n).parameterPercentage;
    A_RMSE_o(n) = results_optimal(n).A_RMSE;
    D_RMSE_o(n) = results_optimal(n).D_RMSE;
    errorRateModelOrder_o(n) = results_optimal(n).errorRateModelorder; 
    durations_o(n) = results_optimal(n).duration;
    estimatedNumClusters_o{n} = results_optimal(n).NumClustersMMDL;
end
% D_RMSE_u(10)=D_RMSE_u(10)+0.01; % window size is half of the other tests
% A_RMSE_u(10)=A_RMSE_u(10)+0.01;
% %errorRateModelOrder_u(n)
% clusterPercentage_u(10) = clusterPercentage_u(n)-0.15;
% clusterPercentage_u(5:8) = clusterPercentage_u(5:8)-0.008
% errorRateModelOrder_u(10) = errorRateModelOrder_u(10)+15
% 
% errorRateModelOrder_o(3:6) = errorRateModelOrder_o(3:6)-5
% errorRateModelOrder_o(3:4) = errorRateModelOrder_o(3:4)-5
% clusterPercentage_o(3:4) = clusterPercentage_o(3:4) + 0.02;
% clusterPercentage_o(6) = clusterPercentage_o(6)+0.015;



figure(1)
    plot(durations, clusterPercentage_u,'-o'); hold on ;
    plot(durations_o, clusterPercentage_o,'-o'); hold on ;
    xlabel('Signal duration [sec.]');
    ylabel('Correct Cluster Estimates [%]');
    grid on
    
figure(2)
    plot(durations, A_RMSE_u,'-o'); hold on ;
    plot(durations_o, A_RMSE_o,'-o'); hold on ;
    xlabel('Signal duration [sec.]');
    ylabel('RMSE - Amplitude Panning Angle [deg.]');
    grid on;
figure(3)
    plot(durations, D_RMSE_u,'-o'); hold on ;
    plot(durations_o, D_RMSE_o,'-o'); hold on ;
    xlabel('Signal duration [sec.]');
    ylabel('RMSE - Delay Estimate [samples]');
    grid on;
    
figure(4)
    plot(durations, errorRateModelOrder_u,'-o'); hold on ;
    plot(durations_o, errorRateModelOrder_o,'-o'); hold on ;
    xlabel('Signal duration [sec.]');
    ylabel('Model Order Error Rate [%]');
    grid on;
    
    
% extract the errors as a function of model order
fignum=100;
for n=1:size(results_uniform,2)
    [t,ndx]=sort(trueNumClusters_u{n});
    e=estimatedNumClusters_u{n};
    e=e(ndx)
    figure(fignum+n);
    
    stem(t); hold on;
    stem(e,'x');
    title(sprintf('Duration %1.0f sec',durations(n)));
    xlabel('Iteration Index');
    ylabel('Number of Parameters');
    legend('True Order','Estimated Order');       
end
eu=[]; eo=[];
for n=1:size(results_optimal,2)-1
    r=results_uniform(1).trueNumClusters;
    [t,ndx]=sort( r(1:30) );
    
    
    eu=estimatedNumClusters_u{n};
    eu=eu(ndx)
    eo=estimatedNumClusters_o{n};
    eo=eo(ndx)
    figure(fignum+n+100);
    
    stem(t,'Markersize',10); hold on;
    stem(eo,'x','Markersize',10);
    stem(eu,'c*');
    title(sprintf('Duration %1.0f sec',results_optimal(n).duration));
    xlabel('Iteration Index');
    ylabel('Number of Parameters');
    legend('True Order','Uniform Estimated Order','Optimal Estimated Order');       
end

% 
% % comput for the first 30 iterations
% for iterationNumber = 1:5
%     iter = 30;
%     [errorAngle, errorDelay,trueNumClusters,NumClustersMMDL,errorRateModelOrder] = evaluate_synthetic(results_optimal(iterationNumber).finalMu, results_optimal(iterationNumber).true, iter, 5);
% 
%     correctAmplitudes = errorAngle(errorAngle<0.95);
%     A_RMSE=sqrt(mean(correctAmplitudes.^2))
%     numCorrectParameters = length(correctAmplitudes)
%     totalParameterEst = sum(trueNumClusters)
%     parameterPercentage = numCorrectParameters/totalParameterEst
% 
%     correctDelay = errorDelay(errorAngle<0.95);
%     D_RMSE=sqrt(mean(correctDelay.^2))
% 
%     results_optimal(iterationNumber).errorAngle = errorAngle;
%     results_optimal(iterationNumber).errorDelay = errorDelay;
%     results_optimal(iterationNumber).trueNumClusters = trueNumClusters;
%     results_optimal(iterationNumber).NumClustersMMDL = NumClustersMMDL;
%     results_optimal(iterationNumber).errorRateModelorder = errorRateModelOrder;
%     %results_optimal(iterationNumber).iter = iter;
%     %results_optimal(iterationNumber).true = true;
%     %results_optimal(iterationNumber).finalMu = finalMu;
%     results_optimal(iterationNumber).D_RMSE = D_RMSE;
%     results_optimal(iterationNumber).totalParameterrEst = totalParameterEst;
%     results_optimal(iterationNumber).NumCorrectParameters = numCorrectParameters;
%     results_optimal(iterationNumber).parameterPercentage = parameterPercentage;
%     results_optimal(iterationNumber).A_RMSE = A_RMSE;
%     %results_optimal(iterationNumber).savedX = savedX;
%     %results_optimal(iterationNumber).duration = duration;
%     %results_optimal(iterationNumber).savedNumBlocksInSegment = savedNumBlocksInSegment;
% end
%  save mats/test_on_N/N_results_optimal_minimize_30iterations.mat