function [mindl, bestk, bestmu, bestcov] = smc_GMM_MMDL_stft(Xl, Xr, fs, NFFT)

%%
    D_vec=[];
    G_vec=[];
   
%    for ii=1:size(Xl,2)
        X1 = Xl(2:NFFT/2-1,:); % remove DC.
        X2 = Xr(2:NFFT/2-1,:);

        thrl = min(min(20*mean(abs(X1)),0.4*max(abs(X1)) ));

        fl=find(abs(X1)>thrl); % thr
        
        ratio = X2(fl)./X1(fl);
        
        G=acot(abs(ratio));  
        G_vec=[G_vec; G];

        D = -imag(log(ratio))./(2*pi*(fl-1)/NFFT); 
        D_vec=[D_vec; D];
%    end

        % de-range the data
    maxPanAngle = 45;
    maxDelay = 200e-6;
    GDMask = (abs(D_vec)<maxDelay*fs) & (G_vec<maxPanAngle*pi/180*2);
    if sum(GDMask)>10*5 % not sure about this threshhold
        % scale the gain vector to degrees.
        GD(:,1) = G_vec(GDMask)./pi*180-45;
        GD(:,2) = D_vec(GDMask);
    else
        mindl = 0; 
        bestk = 0
        bestmu = 0; 
        return;
    end
    
    % Normalizing the range    
    X = GD;

%% Clustering Using Gaussian Mixture Models
covType = 0;
if (covType==1 || covType == 2)
    regVal = 1e-2;
else
    regVal = 0;
end
%[bestk,bestpp,bestmu,bestcov,dl,countf,mindl] = mixtures4(y,2,25,0,1e-4,0);

[bestk,~,bestmu,bestcov,dl,countf,mindl] = mixtures4(GD',2,15,1e-2,1e-4,covType,0);

