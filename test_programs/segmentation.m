%%
clear all;
M=4;
sti = '/Users/home/Documents/P9/panning_project/test_programs/SQAM_FLAC/';
files={strcat(sti,'13.flac'),strcat(sti,'24.flac'),strcat(sti,'08.flac'),strcat(sti,'21.flac'),strcat(sti,'08.flac'),strcat(sti,'21.flac'),strcat(sti,'04.flac'),strcat(sti,'17.flac'),strcat(sti,'16.flac')};
theta=(    [-30  -10     10     30    ]+45)/180*pi;
delaySec = [200e-6 100e-6 -100e-6 -200e-6]/1.4;

for m=1:M,
    [source{m},fs]=audioread(files{m});
    len(m)=length(source{m});
end
len=min(len);
x=zeros(len,2);
delay = floor(abs(delaySec*fs));

for m=1:M,
    source{m}=source{m}(:,1);    
    source{m}=source{m}(1:len);
    source{m}=source{m}/var(source{m});    
end

for m=1:M;
  g(1,m)=sin(theta(m));
  g(2,m)=cos(theta(m));
  if delaySec(m) > 0 
    x(:,1)=x(:,1)+g(1,m)*source{m};
    x(:,2)=x(:,2)+g(2,m)*[zeros(delay(m),1); source{m}(1:end-delay(m))];
  else
    x(:,2)=x(:,2)+g(2,m)*source{m};
    x(:,1)=x(:,1)+g(1,m)*[zeros(delay(m),1); source{m}(1:end-delay(m))];
  end
end
    
    

%%
%clear all; close all;
% loop over 30-120 ms
% evaluate for each size 
% plot std
%[x,fs] = audioread('/Users/home/dropbox/Mixing_Parameter_Estimation/trumpet_horn_mixture.wav');
%[x,fs] = audioread('/Users/home/Music/Logic/sinusoidal_mix/Bounces/cello_mix_panned_28.wav');
%[x,fs] = audioread('/Users/home/Music/iTunes/iTunes Media/Music/Fears Unfolding/Unknown Album/Angel In Red Rough.mp3');
%x=x(fs*1:end,:);
% 
% fs2=8e3;
% K=gcd(fs,fs2);
% P=fs2/K;
% Q=fs/K;
% x=resample(x,P,Q);
% fs=fs2;

testDuration = 1;
N=testDuration*fs;


B = floor(length(x)/N)-1;
mu1 = cell(B,1);
mu2 = cell(B,1);
for bb=0:B-1
    xBlock=x(N*bb+1 : N*bb+1 + N,:);
    vector = floor([50:2:120].*1e-3*fs);
    
    ndx = 1;
    for ii = vector
        [J(ndx), numClusters(ndx)] = smc_kmeans_std(xBlock, ii, fs);
        ndx=ndx+1;
    end

    [~, optSize(bb+1)] = min(smooth(J));
    fprintf('opt size is: %1.0f',vector(optSize(bb+1))/fs*1e3)

    %figure; plot(vector/fs*1e3, J,vector(optSize)/fs*1e3,J(optSize),'x');

    [s(bb+1),M(bb+1),mu1{bb+1},mu2{bb+1}] = smc_kmeans_std(xBlock, vector(optSize(bb+1)), fs, 1);
end

mu2 = mu2(s<mean(s));
meanVec=[];
for ii = 1:size(mu2,1)
    meanVec = [meanVec; mu2{ii,:}]
end
figure; scatterhist(meanVec(:,1), meanVec(:,2),100)
%meanVec2=meanVec(s<mean(s))
[ndx,KMeansMean] = kmeans(meanVec,round(mean(M)),'Distance','cityblock','Replicates',12);
KMeansMean

% testDuration = 0.5;
% N=testDuration*fs;
% 
% NMin  = floor(10e-3*fs);
% NMax = N;
% 
% KMax = floor(0.2*fs);%floor(NMax/NMin);
% m=1 %KMax
% k_opt=[];
% while (m*NMin) < (length(x))
%     cost=[];
%     J=[];
%     K = min(m, KMax);
%     for k=1:K
%         fprintf('\nm-k+1 = %1.0f \n m = %1.0f',m-k+1,m)
%         xBlock = x(1:N,:);%x(m-k+1:m);
%         % do clustering here
%         [J, numClusters] = smc_kmeans_std(xBlock, length(m-k+1:m)*NMin,fs)
%         if (m-k) > 0
%             cost(k) = J + smc_kmeans_std(xBlock,length(1:m*NMin-k*NMin),fs) 
%         else
%             cost(k) = J;
%         end
% 
%         %plot(xtmp,fs);% pause; clf;
%     end
%     [~, k_opt(m)] = min(cost)
%     m=m+1;
% end

