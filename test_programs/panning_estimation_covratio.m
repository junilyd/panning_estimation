% panning_estimation_covratio
 %% Apply panning parameters to M audio recordings
 clear all;
 load test_covration_100iter_notoptimal16april.mat
 for iter=1:300
     if iter<101
         X = savedX{iter};
         [~, ~, ~, ~,~, ~, ~, ~, bestmuCovRatio{iter}, meanthr{iter}] = smc_GMM_MMDL_normdetC(X, fs, 5);
     else
 close all;
 
    fs = 44100;
    maxNumSources=5; 
    maxDelaySec= 150e-6;
    maxDelaySamples = floor(maxDelaySec*fs);
    wlen     = floor(600e-3*fs);
    NFFT  = 2^nextpow2(floor(wlen*2));


    numSources = randi(maxNumSources-1)+1;
    [theta, delaySec, true(iter).Points] = smc_random_panning_parameters(numSources,fs, maxDelaySec);

    startSec = 2;
    durationSec = 18;

%% Apply panning parameters to M audio recordings
    [x,fileNumber] = smc_apply_panning_to_sqam_max_duration(theta, delaySec, numSources, fs);
    
    X=smc_estimate_panning_space_from_optimal_segments(x, wlen*ones(1000,1), fs, 4, NFFT, 6);
    
     [~, ~, ~, ~,~, ~, ~, ~, bestmuCovRatio{iter}, meanthr{iter}] = smc_GMM_MMDL_normdetC(X, fs, size(true(iter).Points,1));
    
    savedX{iter,1} = X;
    savedx{iter,1} = x;
    savedFilenumber{iter,1} = fileNumber;
    
    bestmuCovRatio{iter}
    true(iter).Points
     end
 end
 save test_covration_100iter_notoptimal.mat
 close all;
 for ii=1:iter
    
    m=[];
    t = true(ii).Points;
    m = bestmuCovRatio{ii};
    
    figure(length(true(ii).Points));
    hold on
%     if ~isempty(m)
        for nn=1:size(m,1)
            text(m(nn,1), m(nn,2), sprintf('%1.0f',ii),'Color','blue','horizontalalignment','right');
        end
        plot(t(:,1), t(:,2),'k+','markersize',12);
        for nn=1:length(t)
            text(t(nn,1), t(nn,2), sprintf('%1.0f',ii));
        end
        plot(m(:,1), m(:,2), 'bx','markersize',18);

    
    title(sprintf('Estimates of panning parameters (%1.0f clusters)',length(true(ii).Points)))
    xlabel('Pan Angle Estimate');
    ylabel('Delay Estimate');

end
% for ii=2:maxNumClusters
%     figure(ii)
% %     legend('True Mean','MMDL covRatio','MMDL smoothed','MMDL Picked');
% end

for ii=1:iter
    fprintf('iter %3.0f, true: %1.0f, MMDL: %1.0f\n',ii, length(true(ii).Points),size(bestmuCovRatio{ii},1))
    trueNumClusters(ii) = length(true(ii).Points);
    NumClustersMMDL(ii) = size(bestmuCovRatio{ii},1);
end
trueNumClusters
NumClustersMMDL
errorRateClusters = 100-100*(sum(NumClustersMMDL==trueNumClusters)/iter)