function [mindl, bestk, initbestmu, initbestcov,initbestpp,safemu, safemuMean, bestcovCovRatio, bestmuCovRatio, bestppCovRatio,X2,X3] = smc_GMM_MMDL_normdetC(X, fs, trueNumClusters)
if nargin <3
    trueNumClusters = 2;
end
%% Clustering Using Gaussian Mixture Models
covType = 0;
regVal =  0.5e-4; % FOR old criteria 0.2e-3;
PlotFlag=0;
level=2;
bestk=[];
maxPanAngle = 45;
maxDelay = 150e-6;

X2=[];
for iter=1:12
bestmu=[]; bestcov=[]; ndx=[]; ratioVari=[]; CratioSort=[]; Cratio=[]; bestpp=[]; detC=[];insideIndicator=[];
xAxisC=[]; xAxisRatio=[]; sortedxAxisC = [];tmp=[];matchIndicator=[]; stickyClusters=[]; sortndx=[]; sortbackndx=[];
[bestk(iter),bestpp,bestmu,bestcov,dl,countf,mindl,safemu, safemuMean, safemuSelected] = mixtures4_remove_outliers(X',2,25,regVal,1e-4,covType,PlotFlag,fs); % regVal was 1e-4

initbestpp = bestpp;
initbestmu = bestmu;
initbestcov = bestcov;
% move these plots merge with 2D.
% figure; 
%     smc_plotGMM(X,initbestmu,initbestcov,initbestpp);
%     title('MMDL Initial Output');
    % choose from cov ratio
    for ii=1:bestk(iter)
       % OLD CRITERIA 
       % Cration(ii) = det(bestcov(:,:,ii));
       
       %% NEW CRITERIA
        [uu,ei,vv]=svd(bestcov(:,:,ii));
        a = sqrt(ei(1,1)*level*level);
        b = sqrt(ei(2,2)*level*level);
        % %% define distnce from y center cluster
        d = X'-bestmu(:,ii);

        % Determin theta rotation angle for the ellipse
        t = atan2(uu(2,:),uu(1,:)); % maybe acos(u(1,1)/sqrt(2))
        t = t(1);
        XX = (d(1,:))*cos(t)+(d(2,:))*sin(t);
        YY = -d(1,:)*sin(t)+d(2,:)*cos(t); 
        % area of ellipse
        detC(ii) = det(bestcov(:,:,ii));
        % projection of ellipse on x-axis
        xAxisC(ii) = a*cos(t)+b*sin(t);
        bestmu(:,ii);
        % locate points inside each elliptic covariance 
        insideIndicator(:,ii,iter) = (XX.^2/a^2+YY.^2/b^2<1); 
              
    end
    % remove every point of an embedding covariance
    % (a covariance that is containing another covariance)
    embeddingClusters = zeros(size(bestk(iter)));
    for  ii=1:bestk(iter)
        % locate the embedded points
        matchIndicator = sum(insideIndicator(:,ii,iter))==sum(insideIndicator(:,ii,iter)&insideIndicator(:,:,iter));
        if sum(matchIndicator)>1
%            fprintf('ii=%1.0f is with mean: \n',ii);
%            bestmu(:,ii)
            % remove index of ii
            matchIndicator(ii) = 0;
            % remove it from clusters it is embedded in
            insideIndicator(:,matchIndicator,iter) = insideIndicator(:,matchIndicator,iter)-insideIndicator(:,ii,iter);
            % remo
            embeddingClusters = embeddingClusters|matchIndicator;
        end
    end

    % credit only the smallest covariance, by removing points that overlap
    % from the bigger covariances
%     [~,sortndx] = sort(detC);
%     [~,sortbackndx] = sort(sortndx);    
%     tmp = insideIndicator(:,sortndx,iter);
%     for ii=1:bestk(iter)-1
%         tmp(:,ii+1:end) = tmp(:,ii+1:end)-(tmp(:,ii)&tmp(:,ii+1:end));   
%         insideIndicator(:,:,iter) = tmp(:,sortbackndx);
%     end
    % (or) just kill every cluster that touches a smaller cluster
    [~,sortndx] = sort(detC);
    [~,sortbackndx] = sort(sortndx);    
    tmp = insideIndicator(:,sortndx,iter);
    stickyClusters = zeros(size(bestk(iter)));
    for ii=1:bestk(iter)-1
        % silent all the sticky points in large covariance
        matchIndicator = sum( tmp(:,ii) & [zeros(size(tmp,1),ii) tmp(:,ii+1:end)] )>1;
        matchIndicator(ii) = 0;
        stickyClusters = stickyClusters|matchIndicator;
    end    
    stickyClusters = stickyClusters(sortbackndx);
       
    indicator05 =  ones(size(bestk(iter)));
    collector05 = zeros(size(bestk(iter)));
    for ii=1:bestk(iter)-1
        tmp = 45*abs(bestmu(1,:)-bestmu(1,ii))<0.51;
        %pause
        if sum(tmp)>1
            %% NO PRINT fprintf('clinch between:\n')
            bestmu(:,tmp==1);
            xAxisC(tmp);
            indicator05 = sum(insideIndicator(:,:,iter)==1)./xAxisC;
            indicator05 = indicator05.*tmp;
            indicator05(indicator05==max(indicator05)) = 0;
            indicator05 = indicator05~=0;
            collector05 = indicator05|collector05;
            %pause
        end
    end

    % set zeros in all sticky cluster points
    insideIndicator(:,:,iter) =insideIndicator(:,:,iter) .*(stickyClusters==0);
    
    % set zeros in all embedding cluster points
    insideIndicator(:,:,iter) =insideIndicator(:,:,iter) .*(embeddingClusters==0);
    
    % set zeros in all embedding cluster points
    insideIndicator(:,:,iter) =insideIndicator(:,:,iter) .*(collector05==0);
    
    
    
    % plot point assignments
            %tmpbestmu = [bestmu(1,:)*45, bestmu(2,:)*6];
    %figure; 
    if PlotFlag == 2
        hold on;
        smc_plotGMM(X,initbestmu,initbestcov,initbestpp);
    end
        %title('MMDL Initial Output');
    for ii=1:bestk(iter)
        X2=[X2; [X(insideIndicator(:,ii,iter)==1,1), X(insideIndicator(:,ii,iter)==1,2)]];       
        if PlotFlag == 2
            hold on;
            plot(X(insideIndicator(:,ii,iter)==1,1)*maxPanAngle,X(insideIndicator(:,ii,iter)==1,2)*floor(maxDelay*fs),'.','color',[0.8500    0.3250    0.0980]);
        end
            %pause
        pointPercentage(ii) = (eps+sum(insideIndicator(:,ii,iter)))/(eps+sum(sum(insideIndicator(:,:,iter))));
        
        %% NO PRINT fprintf('[%5.1f,%5.1f],contains: %5.0f, detC: %10.2f, xAxis: %5.2f p/xa: %5.0f, d p/xa: %7.1f \n',bestmu(1,ii)*45,bestmu(2,ii)*6,sum(insideIndicator(:,ii,iter)==1),detC(ii)*1000000,xAxisC(ii)/sum(xAxisC),sum(insideIndicator(:,ii,iter)==1)/xAxisC(ii),eps+pointPercentage(ii)/(xAxisC(ii)*detC(ii)))
       % pause 
    end
    %clf(1000);
    
    for  ii=1:bestk(iter)
        pointPercentage(ii) = (realmin+sum(insideIndicator(:,ii,iter)))/(eps+sum(sum(insideIndicator(:,:,iter))));
        
        Cratio(ii) = eps+pointPercentage(ii)/(xAxisC(ii)*detC(ii));
        pa(ii)=sum(insideIndicator(:,ii,iter)==1)/xAxisC(ii);

        % only for plotting
        XForplot{ii,1} = [X(insideIndicator(:,ii,iter)==1,1) X(insideIndicator(:,ii,iter)==1,2) ]; 
    end


%    % 1) Sort Cratios and compare to lowest val. 
    [~, ndx] = sort(-Cratio);
    CratioSort=Cratio(ndx);
    CratioOverCratio1 = CratioSort/CratioSort(1);
    pointPercentage(ndx);
    pa=pa(ndx);

   for ii=1:bestk(iter)-1
       ratioVari(ii+1) = CratioSort(ii+1)/mean(CratioSort(1:ii));
   end
    
   
   % collect data for discrimination
   %trueNumClusters;
   %bestpp=bestpp(ndx);
   %CratioSort;
   %df = [0 diff(CratioSort)];
   %CratioOverCratio1;
   %ratioVari*100;

if trueNumClusters < length(CratioOverCratio1)
    trueval(iter) = CratioOverCratio1(trueNumClusters);
    truevalp1(iter) = CratioOverCratio1(trueNumClusters+1);
elseif trueNumClusters == length(CratioOverCratio1)
     trueval(iter) = CratioOverCratio1(trueNumClusters);
     truevalp1(iter) = 0;
end
%trueval;
%truevalp1;

   oldFig=gcf;
% Plot the ratio function
if PlotFlag==2
   figure(35);
        plot(CratioOverCratio1); 
            xlabel('$k$','interpreter','latex'); 
            ylabel('$$\frac{m_k}{m_1}$$','interpreter','latex')
            
            hold on;    title('Ratio function');
                        plot(trueNumClusters, CratioOverCratio1(trueNumClusters),'kx');
                        plot(trueNumClusters+1, CratioOverCratio1(trueNumClusters+1),'kx');
            text(trueNumClusters+0.2, CratioOverCratio1(trueNumClusters),sprintf('%1.0e',CratioOverCratio1(trueNumClusters)))
            text((trueNumClusters+1)+0.2, CratioOverCratio1(trueNumClusters+1),sprintf('%1.0e',CratioOverCratio1(trueNumClusters+1)))
            xlim([trueNumClusters-1 trueNumClusters+3]);
            ylim([-0.1e-4 CratioOverCratio1(trueNumClusters)*1.2])
            hold off;
            
   figure(oldFig.Number); hold on;
end
   while (CratioSort(end)/CratioSort(1) <1e-7 && CratioSort(end) < 1/2000 && bestk(iter) > 1) | (pa(end) < 350  && bestk(iter) > 1)% CratioSort(end)<1 && bestk(iter) > 1%  variSort(end)>20*variSort(1)
   CCCEND = CratioSort(end);
   CratioSort;
    CratioSort(end)/CratioSort(1);
       % 2) compare to the one above
       CratioSort = CratioSort(1:end-1);
       bestk(iter)=bestk(iter)-1;
       pa = pa(1:end-1);
   end
   
   % we mean every cluster which represents same model order.
   % remove this if, is algorithm becomes unprecise.
   if ~exist('bestmuIter{bestk{iter}')
        bestmuIter{bestk(iter)} = bestmu(:,ndx(1:bestk(iter)));
        bestppIter{bestk(iter)} = bestpp(ndx(1:bestk(iter)));
        bestcovIter{bestk(iter)} = bestcov(:,:,ndx(1:bestk(iter)));
   else
        bestmuIter{bestk(iter)}=(bestmuIter{bestk(iter)} + bestmu(:,ndx(1:bestk(iter))))/2;
        bestppIter{bestk(iter)}=(bestppIter{bestk(iter)} + bestpp(ndx(1:bestk(iter))))/2;
        bestcovIter{bestk(iter)}=(bestcovIter{bestk(iter)} + bestcov(:,:,ndx(1:bestk(iter))))/2;    
   end
end

bestk = round(mean(bestk));
%traverse through bestmuIter to find one that is large enough.
tndx=1;
while size(bestmuIter{tndx},2)<bestk
    size(bestmuIter{tndx},2);
    tndx=tndx+1;
end
% some weighting could be applied to repeated estimates here.
bestmuCovRatio = bestmuIter{:,tndx}';
bestcovCovRatio = bestcovIter{tndx};
bestppCovRatio = bestppIter{tndx};
bestmuCovRatio(bestk+1:end,:) = [];
bestcovCovRatio(:,:,bestk+1:end) = [];
bestppCovRatio(bestk+1:end) = [];
% only for plotting final estimate
X3=[];
for ii=1:bestk
    X3=[X3; [X(insideIndicator(:,ndx(ii),iter)==1,1), X(insideIndicator(:,ndx(ii),iter)==1,2)]];
end


% remove same sign clusters (pan right is: Gain(+) --> Delay(-) )
signMask = sign(bestmuCovRatio(:,1))~=sign(bestmuCovRatio(:,2));      
bestmuCovRatio = bestmuCovRatio(signMask,:); 
bestppCovRatio = bestppCovRatio(signMask);
% remove too close clusters
bestmuCovRatio = bestmuCovRatio.*[maxPanAngle floor(maxDelay*fs)];
[val,ndx] = unique(round(bestmuCovRatio*2)/2,'rows');
bestmuCovRatio = bestmuCovRatio(ndx,:);
   
       
% debugging / training
if exist('trueval')
    trueval = trueval(trueval~=0);
    truevalp1 = truevalp1(truevalp1~=0);
end
%meanthr = [mean(trueval) mean(truevalp1) max(trueval) min(truevalp1) median(trueval) median(truevalp1) ] ;% mean(truevalp1./trueval) 

% if (length(trueval) == length(truevalp1) )
%     figure; kde(trueval);
%     hold on; kde(truevalp1);
% end

%close all
% old stuff
bestmu = [maxPanAngle floor(maxDelay*fs)].*bestmu';
%[val,ndx] = unique(round(bestmu*2)/2,'rows');
%finalMu = bestmu(ndx,:);
