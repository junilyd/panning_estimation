% plot figure of SQAM estimates
dur = [2:2:8]
uniform = [ .74 .904 .93  .935]-.02;
MMDL    = [.849 .930  .935 .94]-.02;
%CH      = [.859 .915 .935 .945];

figure(7);
        h1=plot(dur,MMDL,'k-*', ...
                dur,uniform,'b-o');%, ...
                %dur,CH,'k:v');
        grid on;
        ylim([.739-0.02 .929])
        xticks(dur); yticks([0.74:0.04:0.92]);
        %set(gca,'XTickLabel',['2';' ';'4';' ';'6';' ';'8';])
        xlabel('signal duration [sec.]');
        ylabel('precision');
        l1=legend('MMDL seg.','Uniform seg.');%,'CH');
        set(l1,'color','w','edgecolor','w');