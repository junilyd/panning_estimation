
% gem tendenserne og ikke normalfordelte distributioner 
%
 clear all; clf;
% ITD ILD Dan ellis 
clear all;% clf;
% duration = 0.1;
N = 16001; % fixed at the moment !!! 800 samples is good with blackamnharris window
fs  = 44100;
duration = 16;
f0  = [200 128 659 173 234 470 150 280];
pan = [-40 -30 -20 -10 0 15 25 35]';

%[x,fs] = wavread('/Users/home/dropbox/Mixing Parameter Estimation/trumpet_horn_mixture.wav');
%[x,fs] = audioread('/Users/home/Music/iTunes/iTunes Media/Music/Fears Unfolding/Unknown Album/Angel In Red Rough.mp3');
%[x,fs] = wavread('/Users/home/Music/iTunes/iTunes Media/Music/Unknown Artist/Unknown Album/Intro til lydmand.wav');
%[x,fs] = audioread('/Users/home/Music/iTunes/iTunes Media/Music/Frank Ocean/Channel ORANGE/10 Pyramids.m4a');
%[x,fs] = audioread('/Users/home/Music/iTunes/iTunes Media/Music/The Beatles/Anthology Box Set/1-01 Free As a Bird.m4a');

%[x, fs] = audioread('/Users/home/Music/Logic/sinusoidal_mix/Bounces/trumpet_mix_no_extra_pan.wav');
%[x,fs] = audioread('/Users/home/Music/Logic/sinusoidal_mix/Bounces/panned_guitar43.wav');
%[x,fs] = audioread('/Users/home/Music/Logic/sinusoidal_mix/Bounces/panned_guitar_switch_sides.wav');
%[x,fs] = audioread('/Users/home/Music/Logic/sinusoidal_mix/Bounces/4_sources.wav');
%[x,fs] = audioread('/Users/home/Music/Logic/sinusoidal_mix/Bounces/2_sources.wav');
%[x,fs] = audioread('/Users/home/Music/Logic/sinusoidal_mix/Bounces/3_sources.wav');
%[x,fs] = audioread('/Users/home/Music/Logic/sinusoidal_mix/Bounces/cello_mix.wav');
%[x,fs] = audioread('/Users/home/Music/Logic/sinusoidal_mix/Bounces/cello_mix_switch_sides.wav');
%[x,fs] = audioread('/Users/home/Music/Logic/sinusoidal_mix/Bounces/cello_mix_center_out.wav');
%[x,fs] = audioread('/Users/home/Music/Logic/sinusoidal_mix/Bounces/cello_1_center_out.wav');
%[x,fs] = audioread('/Users/home/Music/Logic/sinusoidal_mix/Bounces/cello_2_center_out.wav');



startSec = 1/fs;

%x = x(startSec*fs:startSec*fs+floor(duration*fs),:);
x = ones(duration*44100,2);
runs = floor(length(x)/N)

% compute mean of the full cost matrix
buffer = (1:N)';
for ii = 1:runs
        xTest = x(buffer,:);
        [xTest] = smc_panned_sinusoids(f0.*rand, pan, N/fs, fs);
        x(buffer,:) = xTest;
        [C(:,:,ii), panSpace] = smc_panning_ratio(xTest,fs);      
        buffer = buffer + N;
        cost(:,ii) = sum(C,3);
        cost(:,ii) = cost(:,ii)./sum(cost(:,ii));
end
meanCost = (mean(cost,2))';


buffer = (1:N)';
for ii = 1:runs
    xTest = x(buffer,:);
    %[h(buffer,:)] = smc_panned_sinusoids(f0.*rand, pan, duration, fs);
    %[C, panSpace] = smc_panning_ratio(xTest,fs);
    buffer = buffer + N;
%end
cost(:,ii) = sum(C(:,:,ii),3);%./(meanCost.^(1)+eps); %fix kanterne p� 1/meanCost eller brug den og inds�t derefter i den oprindelige cost med peakv�rderne 
cost(:,ii) = cost(:,ii)./sum(cost(:,ii));
diffCost = [0; diff(cost(:,ii))];
end
% % % find peaks above +- panThresh degrees.
% panThresh = 0;
% [peaks] = smc_peaks(diffCost,12);
% positivePeaksNdx = peaks.ndx(peaks.sign==1);
% positivePeaksVal = peaks.val(peaks.sign==1);
% positivePeaksVal(abs(panSpace(positivePeaksNdx))<panThresh) = [];
% positivePeaksNdx(abs(panSpace(positivePeaksNdx))<panThresh) = [];
% 
% % extract mainlobes around peaks
% for pp=1:length(positivePeaksNdx)    
%     peakNdx = positivePeaksNdx(pp);
%     % go forward and check sign
%     sign = -1;
%     posWidth = 0;
%     while sign <= 0 % go forward
%         posWidth = posWidth+1;
%         if peakNdx+posWidth <= length(panSpace)
%             sign = diffCost(peakNdx+posWidth);
%         else
%             posWidth = posWidth - 2; % should be 1
%             sign = 1;
%         end
%     end
%     sign = 1;
%     negWidth = 0;
%     while sign >= 0 % go backward
%         negWidth = negWidth-1;
%         if peakNdx+negWidth > 1
%          sign = diffCost(peakNdx+negWidth);
%         else
%             sign = -1;
%         end
%         %sign = diffCost(peakNdx+negWidth);
%     end
%     if panSpace(peakNdx) < 0
%         posWidth = posWidth -1;
%     else 
%         posWidth = posWidth +1;
%     end
%     peterPan(:,pp) = zeros(size(panSpace));
% %    mainLobe(:,pp) = [panSpace(peakNdx+negWidth) panSpace(peakNdx+posWidth)]'; 
%     peterPan(peakNdx+negWidth:peakNdx+posWidth,pp) = cost(peakNdx+negWidth:peakNdx+posWidth)';
%     %peterPan(peakNdx:peakNdx,pp) = abs(cost(peakNdx:peakNdx))';
% end
% 
% meanPeterPan(:,ii) = mean(peterPan,2);
% 
% cost2test(:,ii) = cost(:,ii)./meanCost';
% end
 cost(isnan(cost)) = 0;
% 
% figure(1)
% subplot(411);
%     plot(panSpace, cost);
% subplot(412)
%    plot(panSpace,diffCost);
%    hold on; plot(panSpace(positivePeaksNdx),positivePeaksVal,'ko','linewidth',5); grid on
% subplot(413)
%    plot(panSpace,peterPan)    % 
% subplot(414)
%    plot(panSpace,mean(meanPeterPan,2))    % 
% 
% figure(2)
% subplot(211)
%     imagesc(panSpace,(1:buffer(end))./fs+startSec,meanPeterPan');
%     axis xy
%     caxis(max(caxis)+[-max(caxis)*0.8 0]);
%     title('"Peak Picked" Panogram'); ylabel('Time [sec]'); xlabel('Estimated Angle [deg]')
% subplot(212)
%     %threshC = cost>max(max(cost))*0.7;
%     %threshC = cost.*threshC;
%     plot(panSpace, sum(meanPeterPan,2))
%     xlim([panSpace(1), panSpace(end)])
%     ylabel('sum of energy'); xlabel('Estimated Angle [deg]')
% %     
figure(3)
subplot(211)
    imagesc(panSpace,(1:buffer(end))./fs+startSec,cost'.^1.0);
    axis xy
    caxis(max(caxis)+[-max(caxis)*0.995 0]);
    title('Panogram'); ylabel('Time [sec]'); xlabel('Estimated Angle [deg]')
subplot(212)
    threshC = cost<max(max(cost))*0.975;
    threshC = cost.*threshC;
    plot(panSpace, sum(threshC,2));
    plot(panSpace, sum(cost,2)); 
    xlim([panSpace(1), panSpace(end)])
    ylabel('sum of energy'); xlabel('Estimated Angle [deg]')
soundsc(x,fs)

%     
% figure(4)
% imagesc(cost2test)