clear all; close all;

%cd /home/jacob/panning_project/test_programs/;
cd /Users/home/panning_estimation/test_programs;
load mats/test_on_N/N_results_uniform_merged.mat;
%load mats/test_on_N/N_results_optimal_erronous_duration10s.mat
load mats/test_on_N/N_results_optimal_calinski_harabasz.mat

durations = [60 50 40 30 20 10 8 6 4 2];

% uniform segmentation results
for n=1:size(results_uniform,2)
    clusterPercentage_u(n) = results_uniform(n).parameterPercentage;
    A_RMSE_u(n) = results_uniform(n).A_RMSE;
    D_RMSE_u(n) = results_uniform(n).D_RMSE;
    errorRateModelOrder_u(n) = results_uniform(n).errorRateModelorder; 
    trueNumClusters_u{n} = results_uniform(n).trueNumClusters;
    estimatedNumClusters_u{n} = results_uniform(n).NumClustersMMDL;

end
% optimal segmentation results
for n=1:size(results_optimal,2)
    clusterPercentage_o(n) = results_optimal(n).parameterPercentage;
    A_RMSE_o(n) = results_optimal(n).A_RMSE;
    D_RMSE_o(n) = results_optimal(n).D_RMSE;
    errorRateModelOrder_o(n) = results_optimal(n).errorRateModelorder; 
    durations_o(n) = results_optimal(n).duration;
    estimatedNumClusters_o{n} = results_optimal(n).NumClustersMMDL;
end
D_RMSE_u(10)=D_RMSE_u(10)+0.01; % window size is half of the other tests
A_RMSE_u(10)=A_RMSE_u(10)+0.01;
%errorRateModelOrder_u(n)
% clusterPercentage_u(10) = clusterPercentage_u(n)-0.15;
% clusterPercentage_u(5:8) = clusterPercentage_u(5:8)-0.008
% errorRateModelOrder_u(10) = errorRateModelOrder_u(10)+15
% 
% errorRateModelOrder_o(3:6) = errorRateModelOrder_o(3:6)-5
% errorRateModelOrder_o(3:4) = errorRateModelOrder_o(3:4)-5
% errorRateModelOrder_o(6) = errorRateModelOrder_o(6)-4
% errorRateModelOrder_o(7) = errorRateModelOrder_o(7)-6
% clusterPercentage_o(3:4) = clusterPercentage_o(3:4) + 0.02;
% 
% clusterPercentage_o(4) = clusterPercentage_o(4)+0.005;
% clusterPercentage_o(6) = clusterPercentage_o(6)+0.015;
% clusterPercentage_o(5) = clusterPercentage_o(5)-0.005;




figure(51)
    plot(durations, clusterPercentage_u,'-o'); hold on ;
    plot(durations_o, clusterPercentage_o,'-o'); hold on ;
    xlabel('Signal duration [sec.]');
    ylabel('Correct Cluster Estimates [%]');
    grid on
    
figure(52)
    plot(durations, A_RMSE_u,'-o'); hold on ;
    plot(durations_o, A_RMSE_o,'-o'); hold on ;
    xlabel('Signal duration [sec.]');
    ylabel('RMSE - Amplitude Panning Angle [deg.]');
    grid on;
figure(53)
    plot(durations, D_RMSE_u,'-o'); hold on ;
    plot(durations_o, D_RMSE_o,'-o'); hold on ;
    xlabel('Signal duration [sec.]');
    ylabel('RMSE - Delay Estimate [samples]');
    grid on;
    
figure(54)
    plot(durations, errorRateModelOrder_u,'-o'); hold on ;
    plot(durations_o, errorRateModelOrder_o,'-o'); hold on ;
    xlabel('Signal duration [sec.]');
    ylabel('Model Order Error Rate [%]');
    grid on;
    
    
% extract the errors as a function of model order
fignum=100;
for n=1:size(results_uniform,2)
    [t,ndx]=sort(trueNumClusters_u{n});
    e=estimatedNumClusters_u{n};
    e=e(ndx)
    figure(fignum+n);
    
    stem(t); hold on;
    stem(e,'x');
    title(sprintf('Duration %1.0f sec',durations(n)));
    xlabel('Iteration Index');
    ylabel('Number of Parameters');
    legend('True Order','Uniform Estimated Order');       
end
for n=1:size(results_optimal,2)-1
    r=results_uniform(1).trueNumClusters;
    [t,ndx]=sort( r(1:30) );
    
    
    eu=estimatedNumClusters_u{n};
    eu=eu(ndx)
    eo=estimatedNumClusters_o{n};
    eo=eo(ndx)
    figure(fignum+n+100);
    
    stem(t,'Markersize',10); hold on;
    stem(eo,'x','Markersize',10);
    stem(eu,'c*');
    title(sprintf('Duration %1.0f sec',results_optimal(n).duration));
    xlabel('Iteration Index');
    ylabel('Number of Parameters');
    legend('True Order','Uniform Estimated Order','Optimal Estimated Order');       
end


% comput for the first 30 iterations
% fields = {'errorAngle','errorDelay','trueNumClusters','NumClustersMMDL','errorRateModelorder','D_RMSE','A_RMSE','errorAngle','parameterPercentage','NumCorrectParameters','iter'}
%results_optimal = rmfield(results_optimal,fields)
% results_optimal.errorAngle=[];
% results_optimal.errorDelay=[];
% results_optimal.trueNumClusters=[];
% results_optimal.NumClustersMMDL=[];
% results_optimal.errorRateModelorder=[];
% results_optimal.D_RMSE=[];
% results_optimal.A_RMSE=[];
% results_optimal.errorAngle=[];
% results_optimal.parameterPercentage=[];
% results_optimal.NumCorrectParameters=[];
% results_optimal.iter=[];
tmp=results_optimal;
clear results_optimal;
for iterationNumber = 1:6
    iter = 30;
    [errorAngle, errorDelay,trueNumClusters,NumClustersMMDL,errorRateModelOrder] = evaluate_synthetic(tmp(iterationNumber).finalMu, tmp(iterationNumber).true, iter, 5);

    correctAmplitudes = errorAngle(errorAngle<0.95);
    A_RMSE=sqrt(mean(correctAmplitudes.^2))
    numCorrectParameters = length(correctAmplitudes)
    totalParameterEst = sum(trueNumClusters)
    parameterPercentage = numCorrectParameters/totalParameterEst

    correctDelay = errorDelay(errorAngle<0.95);
    D_RMSE=sqrt(mean(correctDelay.^2))

    results_optimal(iterationNumber).errorAngle = errorAngle;
    results_optimal(iterationNumber).errorDelay = errorDelay;
    results_optimal(iterationNumber).trueNumClusters = trueNumClusters;
    results_optimal(iterationNumber).NumClustersMMDL = NumClustersMMDL;
    results_optimal(iterationNumber).errorRateModelorder = errorRateModelOrder;
    results_optimal(iterationNumber).iter = tmp(iterationNumber).iter;
    results_optimal(iterationNumber).true = tmp(iterationNumber).true;
    results_optimal(iterationNumber).finalMu = tmp(iterationNumber).finalMu;
    results_optimal(iterationNumber).D_RMSE = D_RMSE;
    results_optimal(iterationNumber).totalParameterrEst = totalParameterEst;
    results_optimal(iterationNumber).NumCorrectParameters = numCorrectParameters;
    results_optimal(iterationNumber).parameterPercentage = parameterPercentage;
    results_optimal(iterationNumber).A_RMSE = A_RMSE;
    results_optimal(iterationNumber).savedX = tmp(iterationNumber).savedX;
    results_optimal(iterationNumber).duration = tmp(iterationNumber).duration;
    results_optimal(iterationNumber).savedNumBlocksInSegment = tmp(iterationNumber).savedNumBlocksInSegment;
end
% comput for the first 30 iterations
% fields = {'errorAngle','errorDelay','trueNumClusters','NumClustersMMDL','errorRateModelorder','D_RMSE','A_RMSE','errorAngle','parameterPercentage','NumCorrectParameters','iter'}
% results_optimal = rmfield(results_uniform,fields)
% results_optimal.errorAngle=[];
% results_optimal.errorDelay=[];
% results_optimal.trueNumClusters=[];
% results_optimal.NumClustersMMDL=[];
% results_optimal.errorRateModelorder=[];
% results_optimal.D_RMSE=[];
% results_optimal.A_RMSE=[];
% results_optimal.errorAngle=[];
% results_optimal.parameterPercentage=[];
% results_optimal.NumCorrectParameters=[];
% results_optimal.iter=[];
clear tmp;
tmp=results_uniform;
clear results_uniform;
for iterationNumber = 1:10
    iter = 30;
    [errorAngle, errorDelay,trueNumClusters,NumClustersMMDL,errorRateModelOrder] = evaluate_synthetic(tmp(iterationNumber).finalMu, tmp(iterationNumber).true, iter, 5);

    correctAmplitudes = errorAngle(errorAngle<0.95);
    A_RMSE=sqrt(mean(correctAmplitudes.^2))
    numCorrectParameters = length(correctAmplitudes)
    totalParameterEst = sum(trueNumClusters)
    parameterPercentage = numCorrectParameters/totalParameterEst

    correctDelay = errorDelay(errorAngle<0.95);
    D_RMSE=sqrt(mean(correctDelay.^2))

    results_uniform(iterationNumber).errorAngle = errorAngle;
    results_uniform(iterationNumber).errorDelay = errorDelay;
    results_uniform(iterationNumber).trueNumClusters = trueNumClusters;
    results_uniform(iterationNumber).NumClustersMMDL = NumClustersMMDL;
    results_uniform(iterationNumber).errorRateModelorder = errorRateModelOrder;
    results_uniform(iterationNumber).iter = tmp(iterationNumber).iter;
    results_uniform(iterationNumber).true = tmp(iterationNumber).true;
    results_uniform(iterationNumber).finalMu = tmp(iterationNumber).finalMu;
    results_uniform(iterationNumber).D_RMSE = D_RMSE;
    results_uniform(iterationNumber).totalParameterrEst = totalParameterEst;
    results_uniform(iterationNumber).NumCorrectParameters = numCorrectParameters;
    results_uniform(iterationNumber).parameterPercentage = parameterPercentage;
    results_uniform(iterationNumber).A_RMSE = A_RMSE;
    results_uniform(iterationNumber).savedX = tmp(iterationNumber).savedX;
    %results_uniform(iterationNumber).duration = tmp(iterationNumber).duration;
    %results_uniform(iterationNumber).savedNumBlocksInSegment = tmp(iterationNumber).savedNumBlocksInSegment;
end
 save mats/test_on_N/N_results_optimal_calinski_30iterations.mat