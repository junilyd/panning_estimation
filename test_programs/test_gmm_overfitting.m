clear all; clf;
%% LOAD MEASUREMENT DISTRIBUTION (7 sources)
%cd /home/jacob/panning_project/test_programs/
%load panning_data;
%X=panning_data;
%clear all;
for iter=1:1%200
%%
elipses=[];
elipsesPruned=[];
% PARAMETERS FOR CLUSTERING
regVal  = 0.5e-4;
covType = 1;
plotFlag= 0;
thrStop = 1e-3; 
kmax=35;
%% create random sources from SQAM
fs = 44100;

maxNumSources=5; 
maxDelaySec= 150e-6;
maxDelaySamples = floor(maxDelaySec*fs);
maxPanAngle=44;

trueNumSources = randi(maxNumSources-1)+1;
[theta, delaySec, trueParams] = smc_random_panning_parameters(trueNumSources,fs, maxDelaySec, maxPanAngle);

[xFullband,fileNumber] = smc_apply_panning_to_sqam_max_duration(theta, delaySec, trueNumSources, fs);


%soundsc(x,fs)
% %% filter
% fc = 4e3; % cutoff freq.
% [b,a] = butter(6,fc/(fs/2));
% x = filter(b,a,xFullband);
x=xFullband;
% parameters for the fourier transform
%% estimate spatial distribution space
wlen     = floor(600e-3*fs);
NMin = floor(wlen/4); KMax = 10; % only if optimal segmentation is applied
NFFT  = 2^nextpow2(NMin*KMax);

[X] = smc_estimate_panning_space_from_optimal_segments(x, ones(1000,1)*wlen, fs, trueNumSources, NFFT, maxDelaySamples, maxPanAngle);

%% estimate overfitted statististics
%normVal = [norm(X(:,1)) norm(X(:,2))];
% overfitting
k=kmax;
[k,alpha,mu,C] = gmm_overfitted(X',kmax,kmax,regVal,thrStop,covType,plotFlag);

%% remove sticky clusters here...
level=5; % the level parameter is related the region size and affects available proximity.
[XPruned, labels, elipses, elipsesPruned,kPruned,alphaPruned,muPruned,CPruned] ...
    = remove_sticky_clusters(X, fs, trueNumSources,alpha,mu,C,level);

%% re-estimate clusters after pruning data model
kmax = size(elipsesPruned,3);
%[kPruned,alphaPruned,muPruned,CPruned] = gmm_mmdl(XPruned', 2, kmax, regVal, thrStop, covType, plotFlag);
[kPruned,alphaPruned,muPruned,CPruned] = gmm_overfitted(XPruned',kPruned,kPruned,regVal,thrStop,covType,plotFlag,muPruned);




%% plots and print
figure(1);
    subplot(221); scatter(X(:,1)*maxPanAngle,X(:,2)*maxDelaySamples,'.b'); axis([-45,45,-6.5,6.5])
        hold on; 
            for ii=1:size(elipses,3), ...
                plot(elipses(1,:,ii)*maxPanAngle,elipses(2,:,ii)*maxDelaySamples, 'k'); 
            end 
        hold off;
        hold on; 
            for ii=1:size(elipsesPruned,3), ...
                plot(elipsesPruned(1,:,ii)*maxPanAngle,elipsesPruned(2,:,ii)*maxDelaySamples, 'k--', 'linewidth',3); 
            end 
        hold off;
    subplot(222); scatter(XPruned(:,1)*maxPanAngle,XPruned(:,2)*maxDelaySamples','.b');axis([-45,45,-6.5,6.5])
    subplot(223); plot_GMM(X, alpha, mu, C, maxPanAngle, maxDelaySamples);
    subplot(224); plot_GMM(XPruned, alphaPruned, muPruned, CPruned, maxPanAngle, maxDelaySamples);
drawnow;
% print result
trueParams
muPruned=muPruned.*[maxPanAngle maxDelaySamples]'
kPruned = size(muPruned,2);
muAlphas=muPruned(:,alphaPruned>1/(kPruned));
alphaPruned
%% sort after detC size
% for ii=1:size(muPruned,2)
%     detC(ii) = det(CPruned(:,:,ii));
%     p(ii)=sum(labels==ii);
% end
% [detC,ndx]=sort(detC,'ascend');
% detC=detC/sum(detC);
% muPruned = muPruned(:,ndx)
% alphaPruned = alphaPruned(ndx)
% p=p/sum(p);
% 
% for ii=1:kPruned
%     beta(ii) = alphaPruned(ii)*p(ii)/detC(ii);
% end


saved.X{iter} = X; clear X;
saved.XPruned{iter} = XPruned; clear XPruned;
saved.alphaPruned{iter} = alphaPruned; clear alphaPruned;
%saved.detC{iter} = detC; clear detC;
saved.muPruned{iter} = muPruned; clear muPruned;
saved.mu{iter} = mu; clear mu;
saved.C{iter} = C; clear C;
saved.CPruned{iter} = CPruned; clear CPruned;
saved.alpha{iter} = alpha; clear alpha;
saved.k{iter} = k; clear k;
saved.theta{iter} = theta; clear theta;
saved.delaySec{iter} = delaySec; clear delaySec;
saved.kPruned{iter} = kPruned; clear kPruned;
%saved.p{iter} = p; clear p;
saved.x{iter} = x; clear x;
saved.xFullband{iter} = xFullband; clear xFullband;
saved.labels{iter} = labels; clear labels;
saved.fileNumber{iter} = fileNumber; clear fileNumber;
saved.trueParams{iter} = trueParams; clear trueParams;
saved.trueNumSource{iter} = trueNumSources; clear trueNumSources;
%saved.beta{iter} = beta; clear beta;
end
