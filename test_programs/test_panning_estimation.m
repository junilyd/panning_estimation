% Test program for estimation of source panning parameters and number of
% sources in each segment. (with uniform segmentation)
clear all
cd /home/jacob/panning_project/test_programs/;
% Parameters
fs = 44100;
maxNumSources=10; 
maxDelaySec= 150e-6;
maxDelaySamples = floor(maxDelaySec*fs);
maxPanAngle=44;
trueNumSources =  5; %randi(maxNumSources-1)+1;
wlen=floor(600e-3*fs);

% Apply panning parameters to MedleyDB database.
[theta, delaySec, trueParams] = smc_random_panning_parameters(trueNumSources,fs, maxDelaySec, maxPanAngle);
% sort to be able to plot results correctly
[theta,ndx]=sort(theta);
delaySec=delaySec(ndx);

[x,fileNumber, source]  = smc_apply_panning_to_medleyDB_max_duration(theta, delaySec, trueNumSources, fs);
xFullband=x;
% Estimate parameters across segments first.
[muEstimates,muDegSampl,alphaEstimates, CEstimates] = smc_estimate_source_panning_across_segments(xFullband,fs);
% Detect active sources in each uniform segment.
[muEstimatesInSegments] = smc_detect_active_sources_in_each_segment(xFullband,fs,muEstimates,muDegSampl,CEstimates,alphaEstimates);

%% plot figure that compares audio and panning estimates
segCount= size(muEstimatesInSegments,2);
figure(102);  clf
    % sort to plot ground truth panning.
    [~,ndx]=sort(muDegSampl(1,:));
    muDegSampl=muDegSampl(:,ndx);
    for m=1:size(muDegSampl,2)
        plot((0:length(source{m})-1)./fs, 3*source{m}/max(abs(source{m}))+muDegSampl(1,m)); hold on;
    end
    ylabel('Pan Angle [L= 45, R= -45]'); xlabel('Time [sec]');
    scnt=segCount;
    for segCount=1:scnt
        tmp=muEstimatesInSegments{segCount};
        for ii=1:size(tmp,2)
            plot( ((segCount-1:segCount)*wlen/fs), [tmp(1,ii)' tmp(1,ii)']  ,'k','linewidth',3); hold on
        end
    end
legend('source 1','source 2','source 3','active source indicator');   
% results across segments (all candidates for each segment) 
muDegSampl
trueParams