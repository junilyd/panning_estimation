%% Apply panning parameters to M audio recordings
%M=7; fs = 44100;
%theta=(    [-30    -20    -10    0 10      20      30    ]+45)/180*pi;
%delaySec = [200e-6 150e-6 100e-6 0 -100e-6 -150e-6 -200e-6]/1.4;


function [x,fs] = smc_panning_data(M, theta, delaySec, durationSec)

fs = 44100;
startSec = 2;
sti = '/home/jacob/audio/sqam/';
%sti = '/Users/home/Documents/P9/panning_project/test_programs/SQAM_FLAC/';
files={strcat(sti,'14.flac'),strcat(sti,'26.flac'),strcat(sti,'09.flac'),strcat(sti,'22.flac'),strcat(sti,'08.flac'),strcat(sti,'03.flac'),strcat(sti,'04.flac'),strcat(sti,'17.flac'),strcat(sti,'16.flac')};
truePoints = [theta(:)/pi*180-45 delaySec(:)*fs;];


for m=1:M,
    [source{m},fs]=audioread(files{m}, [startSec*fs (startSec+durationSec)*fs]);
    len(m)=length(source{m});
end
len=min(len);
x=zeros(len,2);
delay = floor(abs(delaySec*fs));

for m=1:M,
    source{m}=source{m}(:,1);    
    source{m}=source{m}(1:len);
    source{m}=source{m}/var(source{m});    
end

for m=1:M;
  g(1,m)=sin(theta(m));
  g(2,m)=cos(theta(m));
  if delaySec(m) > 0 
    x(:,1)=x(:,1)+g(1,m)*source{m};
    x(:,2)=x(:,2)+g(2,m)*[zeros(delay(m),1); source{m}(1:end-delay(m))];
  else
    x(:,2)=x(:,2)+g(2,m)*source{m};
    x(:,1)=x(:,1)+g(1,m)*[zeros(delay(m),1); source{m}(1:end-delay(m))];
  end
end