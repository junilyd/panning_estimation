% calinkski_harabasz evaluation
close all
% cd /home/jacob/panning_project/test_programs/
% load panning_data2;
% X = panning_data;
fs = 44100;

maxNumSources=5; 
maxDelaySec= 150e-6;
maxDelaySamples = floor(maxDelaySec*fs);
maxPanAngle=44;
trueNumSources = randi(maxNumSources-1)+1;

iter=1;
for numSources = 5:5
    %% Apply panning parameters to M audio recordings
    fs = 44100;

    %numSources = randi(maxNumSources-1)+1;
    %[theta, delaySec, true(iter).Points] = smc_random_panning_parameters(numSources,fs);
[theta, delaySec, trueParams] = smc_random_panning_parameters(trueNumSources,fs, maxDelaySec, maxPanAngle);
[xFullband,fileNumber] = smc_apply_panning_to_sqam_max_duration(theta, delaySec, trueNumSources, fs);
% filter
fc = 4e3; % cutoff freq.
[b,a] = butter(6,fc/(fs/2));
x = filter(b,a,xFullband);
    
    fs=44100;
    startSec = 2
    durationSec = 6;
    %x = smc_apply_panning_to_sqam(theta, delaySec, numSources, durationSec, fs);
    
    % implement the short time fourier transform
    wlen     = floor(200e-3*fs);
    NMin = floor(wlen/4);
    NFFT  = 2^nextpow2(wlen);%2^14;
    w = hann(wlen);
    Xl = smc_stft_complex(x(:,1), w, NMin, NFFT);
    Xr = smc_stft_complex(x(:,2), w, NMin, NFFT);
    
    X = smc_estimate_panning_space(Xl, Xr, NFFT,fs);
    [numBlocksInSegment, numClustersInSegment] = optimal_segmentation_panning_stft(Xl,Xr,NMin,fs, NFFT);
    [Xred] = smc_estimate_panning_space_from_optimal_segments(x, numBlocksInSegment*wlen, fs, true(iter).Points, NFFT);

    
%    X = smc_reduce_data_by_gmm_em(X);
%    X = smc_reduce_data_by_gmm_em(X);
     X = smc_reduce_data_by_gmm_em(Xred);

    % Calinski Harabasz Evaluation
    eva = evalclusters(X,'kmeans','CalinskiHarabasz','KList',[1:10]);


    optimalK(numSources) = eva.OptimalK;
    critVal = eva.CriterionValues;
    optimalVal(numSources) = critVal(optimalK(numSources));

    
    figure(100+numSources)
    subplot(211); scatter(X(:,1),X(:,2))
    subplot(212); plot(eva)
    showfigs
end