%[X1] = smc_estimate_panning_space_dft(x, ones(1000,1)*wlen, fs, true(iter).Points, NFFT, maxDelaySamples);
[X1] = smc_estimate_panning_space_from_optimal_segments(x, ones(1000,1)*wlen, fs, true(iter).Points, NFFT, maxDelaySamples);
[X] = smc_estimate_panning_space_from_optimal_segments(x, numBlocksInSegment*wlen, fs, true(iter).Points, NFFT, maxDelaySamples);
figure; 
plot(45* X1(:,1), 150e-6*fs*X1(:,2),'o'); hold on;
plot(45* X(:,1), 150e-6*fs*X(:,2),'k.'); hold on;
legend('without segmentation','with segmentation')
xlabel('Panning Angle [deg.]')
ylabel('Delay [samples]');
ylim([-6.5 6.5])
xlim([-45 45 ])