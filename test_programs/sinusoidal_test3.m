clear all; clf;
% ITD ILD Dan ellis 
duration = 0.5;
fs  = 8e3;
f0  = [63 140 216] ;
pan = [-40 35 10]';
panAngle = pan;
% Make a script to produce panned
%sinusoids with phases also.
[P,Pc] = smc_panned_sinusoids(f0, pan, duration, fs);
P(:,1)=P(:,1).*hann(length(P(:,1)));
P(:,2)=P(:,2).*hann(length(P(:,2)));
soundsc(P,fs)

% f versus theta
% integrate over all frequencies for all angles
% angles equals ratio of energy
% 1) pandir (theta) to gain ratios
baseAngle = 45;
%panAngle  = 30;
phi0 = baseAngle * pi/180; 
phi  = panAngle  * pi/180;
theta = phi + phi0;
g = [cos(theta) sin(theta)]';
%g=g/sum(g.^2);
L = [ [-cos(phi0) sin(phi0)]' ...
      [ cos(phi0) sin(phi0)]' ...
      ];
  
p = (g'*L)';

% 2) gain (energy) ratios to pandir (theta)
g2 = inv(L)'*p;
g2 =g2/sum(g2.^2);
theta2 = [acos(g2(1))];
phi2 = (theta-phi0);
panAngle2 = phi/pi*180;

% searchspace theta
    N = length(Pc);
    Z = smc_Z_dft(Pc,N);
    X = Z*Pc;

    for ii=1:N
    ratios(ii) = abs(X(ii,1))/abs(X(ii,2));
    end
    
    ratiosToLookFor = g(1,:)./g(2,:)
    
    for ii = 1:length(ratiosToLookFor)
        % if ratios(n) is above 1 it is left side
        searchValue = ratiosToLookFor(ii);
        if searchValue < 1
            searchValue = 1/searchValue
            
            
            Ratio = 1./ratios;
            side = 'right';
        else
            searchValue = searchValue
            subtractRatio = ratios;
            side = 'left';
        end
        scaleFactor = 1;
        % fit the ratio function to have max at closest to theta-related value       
        ratioFunction(:,ii) = abs(searchValue - subtractRatio);
        ratioFunction(:,ii) = (ratioFunction(:,ii) - max(ratioFunction(:,ii)))./searchValue.*(-scaleFactor);
        ratioFunction(:,ii) = ratioFunction(:,ii)-max(ratioFunction(:,ii))+1;
        %ratiofunction(:,ii) = (ratiofunction(:,ii)./searchValue)*10
        
    end
    figure(1)
        subplot(611)
        plot([1:N],abs(X(:,1)),[1:N],abs(X(:,2))); xlim([0 N/2]);
        subplot(612)
        plot([1:N],ratios); xlim([0 N/2]);%legend('values above 1 is the left side');      
        subplot(613)
        plot([1:N],1./ratios); xlim([0 N/2]); % %legend('values above 1 is the right side');
        subplot(614)
        plot([1:N], ratioFunction(:,1)); axis([0 N/2 -scaleFactor scaleFactor]); % legend('ratiofunction for specific theta');
        subplot(615)
        plot([1:N],ratioFunction(:,2)); axis([0 N/2 -scaleFactor scaleFactor]); % legend('ratiofunction for specific theta');
        subplot(616)
        plot([1:N],ratioFunction(:,3)); axis([0 N/2 -scaleFactor scaleFactor]); % legend('ratiofunction for specific theta');

    
    
%% search through part of the space
clear all; clf;
% ITD ILD Dan ellis 
duration = 0.1;
fs  = 8e3;
f0  = [200 333 700 73 234 370] ;
pan = [-34 -24 -5 17 30 40]';
panAngle = pan;
% Make a script to produce panned
%sinusoids with phases also.
[P,Pc] = smc_panned_sinusoids(f0, pan, duration, fs);
%P(:,1)=P(:,1).*blackmanharris(length(P(:,1)));
%P(:,2)=P(:,2).*blackmanharris(length(P(:,2)));
N = length(Pc);
%Z = smc_Z_dft(Pc,N);
%X = Z*Pc;

[X(:,1),pX(:,1),f] = smc_dft(Pc(:,1), 2^11, fs);
[X(:,2),pX(:,2),f] = smc_dft(Pc(:,2), 2^11, fs);
X = 10.^(X./20);

for ii=1:N
    ratios(ii) = abs(X(ii,1))/abs(X(ii,2));
end
    
panSpace = [-42:0.5:42]';
for ii = 1:length(panSpace)
    panAngle = panSpace(ii);
    % 1) pandir (theta) to gain ratios
    baseAngle = 45;
    phi0 = baseAngle * pi/180; 
    phi  = panAngle  * pi/180;
    theta = phi + phi0;
    g = [cos(theta) sin(theta)]';
    %g=g/sum(g.^2);
    % L = [ [-cos(phi0) sin(phi0)]' ...
    %       [ cos(phi0) sin(phi0)]' ...
    %       ];
    % p = (g'*L)';
    ratiosToLookFor(ii) = g(1)./g(2);
end

for ii = 1:length(ratiosToLookFor)
    %scaleFactor = 300;
    tmp = ratios./ratiosToLookFor(ii);
    tmp = (tmp-1);
    tmp = tmp.*(-1);
    tmp = abs(tmp).*(-1);
    tmp = tmp + 1;
    %tmp(tmp>0.99) = 0;
    tmp(tmp<0) = 0; %% overvej at detektere st�rrelsen p� ti peaks !!
    
    meanTmp(ii) = mean(tmp([1:N]));
    medianTmp(ii) = median(tmp([1:N]));
    
    %tmp = tmp-meanTmp(ii); 
    tmp(tmp<0.9) = 0; %% overvej at detektere st�rrelsen p� ti peaks !
    ratioFunction(:,ii) = tmp;
end
% plot with imagesc
    figure(1)
    subplot(411)
        imagesc(panSpace,[1:N],ratioFunction);
            axis xy
            xlabel('Pan Angle [deg.]'); ylabel('Frequency [samples]');
            ylim([0 N-1]);
            titleString = 'True \Theta = ';
            for ii = 1:length(pan)
                titleString =  [titleString sprintf('%1.0f ',pan(ii))];
            end
            title(titleString)    
    subplot(412)
        plot(panSpace, sum(ratioFunction(floor(N/4):floor(N/2),:)));
        %plot(panSpace, sum(ratioFunction(1:floor(N/2),:)));
            grid on;
            xlim([panSpace(1) panSpace(end)])
%        
% %     
    figure(2)
        
        for ii=1:length(ratiosToLookFor)
            subplot(413)
            plot(ratios./ratiosToLookFor(ii))
            subplot(414)
            plot(ratioFunction(:,ii)); title(sprintf('panAngle = %1.0f,  ratio = ratioToLookFor = %1.1f',panSpace(ii),ratiosToLookFor(ii)));pause
        end
    
    figure(2); 
    plot(panSpace,meanTmp,'r');
% %     
% %     
    
    
    
    
    
    
    
    