%% from test_ktrue_
kTrue=4;
for iter=598:5000

elipses=[];
elipsesPruned=[];
% PARAMETERS FOR CLUSTERING
regVal  = 0.5e-4;
covType = 1;
plotFlag= 0;
thrStop = 1e-3; 
kmax=35;
thrForCorrect=.5;
%% create random sources from IOWA
fs = 44100;

maxNumSources=5; 
maxDelaySec= 150e-6;
maxDelaySamples = floor(maxDelaySec*fs);
maxPanAngle=44;
trueNumSources =  randi(maxNumSources-1)+1;

[theta, delaySec, trueParams] = smc_random_panning_parameters(trueNumSources,fs, maxDelaySec, maxPanAngle);
[xFullband,fileNumber] = smc_apply_panning_to_iowa_max_duration(theta, delaySec, trueNumSources, fs);
% filter
fc = 4e3; % cutoff freq.
[b,a] = butter(6,fc/(fs/2));
x = filter(b,a,xFullband);

% parameters for the fourier transform
%% estimate spatial distribution space
wlen     = floor(600e-3*fs);
NMin = floor(wlen/4); KMax = 10; % only if optimal segmentation is applied
NFFT  = 2^nextpow2(NMin*KMax);

[X] = smc_estimate_panning_space_from_optimal_segments(x, ones(1000,1)*wlen, fs, trueNumSources, NFFT, maxDelaySamples, maxPanAngle);
%% estimate overfitted statististics

[k,alpha,mu,C] = gmm_overfitted(X',kmax,kmax,regVal,thrStop,covType,plotFlag);
for kk=1:size(C,3), detC(kk)=det(C(:,:,kk)); end
GMM_MODEL = gmdistribution(mu',C,alpha);%,'CovType','diagonal'); % generate a GMM
degree_of_membership = posterior(GMM_MODEL,X); % calculate the posterior probability of observations

%%
% Punique=sum(degree_of_membership==0)/sum(sum(degree_of_membership==0));
% removeNdx=find(Punique==0);
% detC(removeNdx)=[];alpha(removeNdx)=[];mu(:,removeNdx)=[];degree_of_membership(:,removeNdx)=[];C(:,:,removeNdx)=[];
% removeNdx=[];
metric = detC;
metric=metric/sum(metric);
[metric,sortNdx]=sort(metric);
mu=mu(:,sortNdx);C=C(:,:,sortNdx); detC=detC(sortNdx);  alpha=alpha(sortNdx);

testMu=(mu.*[maxPanAngle maxDelaySamples]')';
[ removeNdx ] = removeRedundantMeans(testMu, 0.5);
detC(removeNdx)=[];alpha(removeNdx)=[];mu(:,removeNdx)=[];degree_of_membership(:,removeNdx)=[];C(:,:,removeNdx)=[];

GMM_MODEL = gmdistribution(mu',C,alpha);
degree_of_membership = posterior(GMM_MODEL,X); %posterior mixing probability
N=length(X);

kk=1;
percentageOverlap=0.001;
while sum((degree_of_membership(:,1:kk)>percentageOverlap),2)<2 
    kk=kk+1, 
end
trueParams
mu(:,1:kk-1).*[maxPanAngle maxDelaySamples]'
muEstimates{iter} = mu(:,1:kk-1).*[maxPanAngle maxDelaySamples]';  


trueParameters{iter} = trueParams;
origC{iter} = C;
origMu{iter}= mu;
origPrior{iter}=alpha;

%% Partition Entropy
%% Evaluation of the clustering
ep = 0.01*eps; % add a very small number in order to prevent division by 0

for kk=1:size(degree_of_membership,2)
    df=[]; fm=[]; clear GMM_MODEL
    GMM_MODEL = gmdistribution(mu(:,1:kk)',C(:,:,1:kk),alpha(1:kk));%,'CovType','diagonal'); % generate a GMM
    df = posterior(GMM_MODEL,X); % calculate the posterior probability of observations
    df = (ep+df)/(1+ep*(size(df,2)));
    fm = (df).^2;
    PCoeff(kk) = 1/N*sum(sum(fm(:,1:kk))); % Partition Coefficient
    fm = df.*log10(df);
    PEntropy(kk) = -1/N*sum(sum(fm)); % Partition Entropy
end
%figure(749)
%subplot(211);plot(1:kk,PCoeff,'-ok'); grid minor; legend('part. coeff'); ylim([0.95 1]); drawnow;
%subplot(212);plot(1:kk,PEntropy,'-ob');grid minor; legend('part. entropy'); ylim([0 0.05]); drawnow;
PCDiff{iter}=[size(trueParams,1)/100 diff(PCoeff(1:6))]*1e2;
PEDiff{iter}=[size(trueParams,1)/100 diff(PEntropy(1:6))]*1e2;
PCDiff{iter}
PEDiff{iter}
PCSaved{iter}=PCoeff;
PESaved{iter}=PEntropy;
clear  mu alpha C degree_of_membership detC GMM_MODEL X PCoeff PEntropy
end
