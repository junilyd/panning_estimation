function [firstErrorNdx] = evaluate_overfitting_output_first_error(saved, thrForCorrect, iter)
if nargin <3
    iter = size(saved.X,2);
end
% 
% for hh=1:iter
%     trueMinusEstimated(hh) = abs(saved.trueNumSource{hh}-saved.kPruned{hh});
%     trueNumClusters(hh) = saved.trueNumSource{hh};
%     estimatedNumClusters(hh) = saved.kPruned{hh};
% end
% errorRateModelOrder = 100-100*(sum(estimatedNumClusters==trueNumClusters)/iter);

%errorAngle=[];
%errorDelay=[];
for hh=1:iter
    tp = saved.trueParams{hh};    
    ep = saved.muPruned{hh}';
    totalEstimatedParamsPruned{hh} = ep;
    
    qq=1;
    ndx=1;
    while ~isempty(ndx)
        [ndx] = find( abs(ep(qq,1)-tp(:,1))<thrForCorrect & abs(ep(qq,2)-tp(:,2))<thrForCorrect,2);
        if isempty(ndx), firstErrorNdx(hh)=qq; break; end
        tp(ndx,:)=[nan nan];
        qq=qq+1;
    end    

%     errorAngle=[errorAngle tmpErrorAngle];
%     errorDelay=[errorDelay tmpErrorDelay];
%     tmpErrorAngle=[];
%     tmpErrorDelay=[];
end

end
