function [bestBICVal, kGMM, finalMu,out] = smc_GMM_BIC(y, numSamplesInSegment, fs, truePoints, NFFT, MaxDelaySamples)

    D_vec=[];
    G_vec=[];
    %NFFT=2^12;
    M=(7+1);
    
    bb=1;
    win = hann(numSamplesInSegment(bb));
    pos=1:numSamplesInSegment(bb);
    while pos(end)<length(y)
%         Xl=y(pos,1);
%         Xr=y(pos,2);
%         pwl = abs(fft(blackmanharris(length(Xl)).*smc_prewhiten(Xl),NFFT));
%         pwr = abs(fft(blackmanharris(length(Xr)).*smc_prewhiten(Xr),NFFT));
%         %X1 = smc_dft_complex(Xl,hann(length(Xl)),NFFT,fs); %Xl(1:NFFT/2-1,ii);
%         %X2 = smc_dft_complex(Xr,hann(length(Xr)),NFFT,fs); %Xr(1:NFFT/2-1,ii);
%         X1 = fft(blackmanharris(length(Xl)).*Xl,NFFT);
%         X2 = fft(blackmanharris(length(Xr)).*Xr,NFFT);
%         
%         wdpiLimit = round(NFFT/2/MaxDelaySamples/2)-1;
%         
%         X1(wdpiLimit:end) = [];
%         X2(wdpiLimit:end) = [];
%         pwl(wdpiLimit:end) = [];
%         pwr(wdpiLimit:end) = [];
%         
%         minBin=2;
%         X1(1:minBin-1) = [];
%         X2(1:minBin-1) = [];
%         pwl(1:minBin-1) = [];
%         pwr(1:minBin-1) = [];
%         
%         oldthrl = min(20*mean(abs(X1)),0.4*max(abs(X1)) );
%         
%         absX1 = abs(X1);
%         absX2 = abs(X2);
%         O = absX1.*absX2;
%         
% %         diffFunction = -1*[0; diff([0;diff(abs(X2-X1))])];
% %         meandf = mean(diffFunction);
% %         
% %         thrl = mean(pwl.*pwr);
% %         fl=find(pwl.*pwr>thrl & diffFunction<meandf);
% %         while length(fl) > NFFT/100/MaxDelaySamples%800, 
% %             length(fl);
% %             thrl = thrl + thrl*0.2;
% %             fl=find(pwl.*pwr>thrl & diffFunction<meandf);
% %         end
%          thrl = mean(pwl.*pwr)*4;
%         %fl=find(Xl>thrl);
%         diffFunction = -1*[0; diff([0;diff(abs(X2-X1))])];
%         meandf = mean(diffFunction);
%         
%         likelidf= abs([0;diff(absX1)]-[0;diff(absX2)])*NFFT;
%         meanlidf = mean(likelidf); 
%         %thrl = mean(O)
%         fl=find(pwl.*pwr>thrl);% & likelidf>meanlidf);

        [fl, Xl, Xr] = smc_compute_threshold_for_segmentation(y(pos,:),MaxDelaySamples,NFFT);
        G=acot(abs((X2(fl)./X1(fl))));  
        G_vec=[G_vec; G];

        D = -imag(log(X2(fl)./X1(fl)))./(2*pi*(fl-1)/NFFT); 
        D_vec=[D_vec; D];
       
        bb=bb+1;

        pos=(pos(end)+1:pos(end)+numSamplesInSegment(bb));
        %pos=pos+numSamplesInSegment(bb);
    end

    % de-range the data
    maxPanAngle = 45;
    maxDelay = 200e-6;
    GDMask = (abs(D_vec)<maxDelay*fs) & (G_vec<maxPanAngle*pi/180*2);
    out = [D_vec G_vec];
    if sum(GDMask)>M*3
        % scale the gain vector to degrees.
        GD(:,1) = G_vec(GDMask)./pi*180-45;
        GD(:,2) = D_vec(GDMask);
    X=GD;
%     
%     X = smc_reduce_data_by_gmm_em(X);
%     X = smc_reduce_data_by_gmm_em(X);
%     X = smc_reduce_data_by_gmm_em(X);
%     
%% Clustering Using Gaussian Mixture Models

% Covariance Structure Options
[n,p] = size(X');

%% Tune Gaussian Mixture Models - selecting in the range of 1-10 clusters.
k = 1:10;
nK = numel(k);
Sigma = {'diagonal','full'};
nSigma = numel(Sigma);
SharedCovariance = {true,false};
SCtext = {'true','false'};
nSC = numel(SharedCovariance);
RegularizationValue = 0.5e-4;
options = statset('MaxIter',1000);
%
% Fit the GMMs using the parameter combination of shared full covariance.  
% Intialize with k-means clustering. Compute the BIC and for each fit.  
% Track the terminal convergence status of each fit.

% Preallocation
gm = cell(nK,nSigma,nSC);         
aic = zeros(nK,nSigma,nSC);
bic = zeros(nK,nSigma,nSC);
converged = false(nK,nSigma,nSC);

% Fit all models
for m = 1 %1:nSC
    for j = 2 %1:nSigma
        for i = 1:nK
            gm{i,j,m} = fitgmdist(X,k(i),...
                'CovarianceType',Sigma{j},...
                'SharedCovariance',SharedCovariance{m},...
                'RegularizationValue',RegularizationValue,...
                'Options',options, ...
                'Start',kmeans(X,i,'Distance','cityblock','Replicates',12));
            bic(i,j,m) = gm{i,j,m}.BIC;
            converged(i,j,m) = gm{i,j,m}.Converged;
        end
    end
end
allConverge = (sum(converged(:)) == nK*nSigma*nSC);

%% differentiate the BIC
dfbc = [0; diff(bic(:,2,1))];
%dfbc = dfbc.*(1-(0:length(dfbc)-1).*0.05)'; % Regularization of 5%

[~, bestBIC] = min(dfbc);
bestBICVal = bic(bestBIC,2,1); % why this model ??½

% Cluster the training data using the best fitting model.  
gmBest = gm{bestBIC,2,1}; 
clusterX = cluster(gmBest,X);
kGMM = gmBest.NumComponents;
d = 500;
x1 = linspace(min(X(:,1)) - 2,max(X(:,1)) + 2,d);
x2 = linspace(min(X(:,2)) - 2,max(X(:,2)) + 2,d);
[x1grid,x2grid] = meshgrid(x1,x2);
X0 = [x1grid(:) x2grid(:)];
mahalDist = mahal(gmBest,X0);
threshold = sqrt(chi2inv(0.99,1));

mu1(:,2) = gmBest.mu(:,2);
mu1(:,1) = gmBest.mu(:,1);

figure;
h1 = gscatter(X(:,1),X(:,2),clusterX);
hold on;
for j = 1:kGMM
    idx = mahalDist(:,j)<=threshold;
    Color = h1(j).Color*0.75 + -0.5*(h1(j).Color - 1);
    h2 = plot(X0(idx,1),X0(idx,2),'.','Color',Color,'MarkerSize',1);
    uistack(h2,'bottom');
end

h3 = plot(gmBest.mu(:,1),gmBest.mu(:,2),'kx','LineWidth',2,'MarkerSize',10);
title('Panning Parameter Distributions');
xlabel('Amplitude Angle Estimate');
ylabel('Delay Estimate');
legend(h1,'Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5','Cluster 6','Cluster 7','Cluster 8','Cluster 9','Location','NorthWest');
hold off;

bestmu = mu1;
maxPanAngle = 45;
maxDelay = 200e-6;
bestmu = bestmu.*[maxPanAngle maxDelay*fs];
%finalMu = unique(round(bestmu,1),'rows');
[val,ndx] = unique(round(bestmu,1),'rows');
finalMu = bestmu(ndx,:);
%     else 
%         bestBICVal = nan;
%         kGMM =0;
end
