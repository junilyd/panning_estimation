function y = normalize2d(x)
    range1 = max(x(:,1))-min(x(:,1));
    range2 = max(x(:,2))-min(x(:,2));
    ranges = [range1, range2];
    [largestRangeVal, maxndx] = max(ranges);
    [~, minndx] = min(ranges);
    
    ratio = ranges(maxndx)/ranges(minndx);
    
    x(:,minndx) = x(:,minndx)*ratio;
    
    y=x;
    
    
    
end