clear all;
cd /home/jacob/panning_project/test_programs/
%load('mats/sep2017/test_on_trueK4_savedMuWeighted_3025_GV_iter.mat');

nanNdx=[];

% read test
load('mats/sep2017/test_on_trueK2_5_pruning_by_posterior_weight_2564_iter.mat');

%% compute first error ndx.

for hh=1:iter
    tp = trueParameters{hh}    
    ep = muEstimates{hh}'
    totalEstimatedParamsPruned{hh} = ep;
    qq=1;
    ndx=1;
    while ~isempty(ndx)
        % locate the cluster estimate in tp
        [ndx] = find( abs(ep(qq,1)-tp(:,1))<thrForCorrect & abs(ep(qq,2)-tp(:,2))<thrForCorrect,2); 
        % ERROR - not recalling all, but found a wrong estimate
        if isempty(ndx), 
            firstErrorNdx(hh)=qq; 
            truePositive(hh)=qq-1; 
            falsePositive(hh)=size(ep,1)-truePositive(hh); 
            break; 
        end 
        % set the recent estimated cluster to 'nan'
        tp(ndx,:)=[nan nan]; 
        % ERROR - all clusters have been found and there are more estimates.
        if isnan(tp), 
            %collectRecallOverestimate(hh)= size(ep,1)-size(tp,1); 
            truePositive(hh)=size(tp,1); 
            falsePositive(hh)=size(ep,1)-size(tp,1); 
        break;
        end 
        
        qq=qq+1;
        % ERROR - there are less estimates than clusters
        if qq>size(ep,1), 
            firstErrorNdx(hh)=size(ep,1)-size(tp,1); 
            truePositive(hh) = qq-1;
            break;
        end
    end    
    
end


%% check difference between true and estimated.
for hh=1:iter
    sizeTp(hh)=size(trueParameters{hh},1);
    sizeEp(hh)=size(muEstimates{hh}',1);
end

 
%fe=firstErrorNdx(hh), 

tp=truePositive; 
fp=falsePositive; 

truePositives=sum(truePositive);
falsePositives=sum(falsePositive);
%oe=collectRecallOverestimate(hh)
    relevantElements = sum(sizeTp);
    % Precision Recall
    truePositives = sum(truePositives);
    %falsePositives = sum(estimatedNumClusters)-truePositives(k);
    precision =  truePositives/(truePositives+falsePositives);
    recall = truePositives/relevantElements;
%%


% methodNdx =  [19;27]; %[19;27]; 
% for methodNumber = 1:length(methodNdx)
% 
% 
% % test for true number of clusters
% kTrueRange=[4:4];
% for kTrue = kTrueRange
% thrForCorrect = 0.5; % both for angle(degrees) and samples(delay)
% 
% % define true number of clusters
% for ii=1:size(muSorted,2)
%     trueNumClusters(ii) = size(trueParameters{ii},1);
% end
% % extract all data corresponding to the true number of clusters
% clusterMask = (trueNumClusters==kTrue);
% indexOfDataToExtract = find(clusterMask);
% for ii=1:sum(clusterMask)
%     trueParametersByTrueClusters{ii} = trueParameters{indexOfDataToExtract(ii)};
%     %muSelectedByTrueClusters{ii} = muSelected{indexOfDataToExtract(ii)};
%     muSortedByTrueClusters{ii} = [[muSorted{indexOfDataToExtract(ii)}]; [zeros(kmax-size(muSorted{indexOfDataToExtract(ii)},1), size(muSorted{indexOfDataToExtract(ii)},2))]];
% end
% 
% 
% kVec=1:kmax-15;
% for k=kVec
%     saved.trueParams = trueParametersByTrueClusters;
%     saved.X=trueParametersByTrueClusters; 
%     for hh=1:size(muSortedByTrueClusters,2)
%         ms = muSortedByTrueClusters{hh}; 
%         saved.muPruned{hh} = ms(1:k,methodNdx(methodNumber):methodNdx(methodNumber)+1)'; % ms(:,13:14)': ICD based, ms(:,2:3)': prior based, ms(:,7:8)', determinant based
%         saved.trueNumSource{hh} = size(trueParametersByTrueClusters{hh},1);
%         saved.kPruned{hh} = size(saved.muPruned{hh},2);
%     end
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     % evaluation
%     [errorAngle, errorDelay,trueNumClusters,estimatedNumClusters,errorRateModelOrder,totalEstimatedParamsPruned, trueMinusEstimated] ...
%         = evaluate_overfitting_precision_recall(saved,thrForCorrect);
% 
%     correctMask=(errorAngle<thrForCorrect) & (errorDelay<thrForCorrect);
%     correctAmplitudes = errorAngle(correctMask);
%     correctDelay = errorDelay(correctMask);
% 
%     relevantElements(k) = sum(trueNumClusters);
% 
%     A_RMSE(k,methodNumber, kTrue)=sqrt(mean(correctAmplitudes.^2));
%     D_RMSE(k, methodNumber, kTrue)=sqrt(mean(correctDelay.^2));
% 
%     % Precision Recall
%     truePositives(k) = sum(correctMask);
%     falsePositives(k) = sum(estimatedNumClusters)-truePositives(k);
%     precision(k) =  truePositives(k)/(truePositives(k)+falsePositives(k));
%     recall(k) = truePositives(k)/relevantElements(k);
%     
%     clear estimatedNumClusters correctMask correctAmplitudes correctDelay errorAngle errorDelay ms trueNumClusters trueNumClusters 
% end
% 
% end
% prec(:,methodNumber,kTrue) = precision';
% recll(:,methodNumber,kTrue) = recall';
% clear precision recall trueParametersByTrueClusters %muSortedByTrueClusters
% end
% 
% %% plot precision and recall results
% figure(3);
% for kTrue=kTrueRange
% subplot(2,2,kTrue-1); hold on;
% title(sprintf('Performance for true k=%1.0f',kTrue))
%         h1=plot(kVec,recll(:,1,kTrue),'k-*',kVec,prec(:,1,kTrue),'k--*', ...
%                 kVec,recll(:,2,kTrue),'b-o',kVec,prec(:,2,kTrue),'b--o');
%         xlabel('k'); ylabel('prob.'); ylim([0.1 1.007]); grid minor; xlim([1, kVec(end)])
% columnlegend(3,{'\beta: Recall'      ,'\beta: Precision', ...
%                 '\alpha: Recall','\alpha: Precision'} ...
%                 ,'location','southwest');
% end
% 
% % plot RMSE errors
% figure(4);
% for kTrue=kTrueRange
% subplot(2,2,kTrue-1); hold on;
% title(sprintf('RMSE for true k=%1.0f',kTrue))
%         h2=plot(kVec,A_RMSE(:,1,kTrue),'k-*',kVec,D_RMSE(:,1,kTrue),'k--*', ...
%                 kVec,A_RMSE(:,2,kTrue),'b-o',kVec,D_RMSE(:,2,kTrue),'b--o');
%         xlabel('k'); ylabel('Hz or Samples'); ylim([0 .11]);  grid minor;
% columnlegend(3,{'Determinant: A_{RMSE}'      ,'Determinant: D_{RMSE}', ...
%                 'Prior: A_{RMSE}','Prior: D_{RMSE}'} ...
%                 ,'location','northwest');
% end
% 
% figure(3);
% kTrue=4
% 
% %% plot for article
% figure;
%         h1=plot(kVec,recll(:,1,kTrue),'k-*',kVec,prec(:,1,kTrue),'k--*', ...
%                 kVec,recll(:,2,kTrue),'b-o',kVec,prec(:,2,kTrue),'b--o', ...
%                 [4 4],[-0.5 1],'-.k');
%         xlabel('$$k$$', 'interpreter','latex'); ylabel('probability'); ylim([0.2 1.007]); grid on; 
%         xlim([kVec(1), kVec(end)]);
%         leg1=legend('$$\beta$$: Recall'      ,'$$\beta$$: Precision', ...
%                 '$$\alpha$$: Recall','$$\alpha$$: Precision', 'True $$K$$', 'interpreter','latex'); 
%        %legend boxoff;
%         set(leg1,'color','w','edgecolor','w');
% 
%         xticks([1:20]); yticks([0.2:0.1:1]);
%         set(gca,'XTickLabel',[' 1';'  ';'  ';' 4';'  ';'  ';'  ';' 8';'  ';'  ';'  ';'12';'  ';'  ';'  ';'16';'  ';'  ';'  ';'20';]);
%         set(gca,'YTickLabel',['0.2';'   ';'0.4';'   ';'0.6';'   ';'0.8';'   ';' 1 ';]);
% 
% % plot F-measure
% %figure; 
%         F = (2*prec(:,1:2,kTrue).*recll(:,1:2,kTrue)) ./ (prec(:,1:2,kTrue)+recll(:,1:2,kTrue));
%         hold on;
%         plot(kVec,F(:,1),'*-k',kVec,F(:,2),'o-b')
%         xticks([1:20]); yticks([0.2:0.1:1]);
%         set(gca,'XTickLabel',[' 1';'  ';'  ';' 4';'  ';'  ';'  ';' 8';'  ';'  ';'  ';'12';'  ';'  ';'  ';'16';'  ';'  ';'  ';'20';]);
%         set(gca,'YTickLabel',['0.2';'   ';'0.4';'   ';'0.6';'   ';'0.8';'   ';' 1 ';]);
% 
% 
% %% ML Ratios
% [firstErrorNdx] = evaluate_overfitting_output_first_error(saved, thrForCorrect);
% for methodNumber = 1:length(methodNdx)
% 
%     for n=1:hh
%         ms = muWeightedSaved{n};%    muSortedByTrueClusters{n};% saved.muPruned{hh} = ms(1:kVec(end),methodNdx(methodNumber):methodNdx(methodNumber)+1)';
%         ML1(n,methodNumber) = ms(1, methodNdx(methodNumber)+2);
%         ML2(n,methodNumber) = ms(2, methodNdx(methodNumber)+2);
%         if firstErrorNdx(n)==1
%             MLLastCorrect(n,methodNumber)=nan;    
%         else
%             MLLastCorrect(n,methodNumber) = ms(firstErrorNdx(n)-1, methodNdx(methodNumber)+2);
%         end
%         MLFirstError(n,methodNumber) = ms(firstErrorNdx(n), methodNdx(methodNumber)+2);
% 
%     end
% end
%         % remove nans
%         nanNdx=[nanNdx find(isnan(MLLastCorrect(:,methodNumber)))];
%         MLLastCorrect(nanNdx,:) = [];ML1(nanNdx,:) = [];ML2(nanNdx,:) = [];MLFirstError(nanNdx,:) = [];
%         figure(45); 
%         h1=histfit(MLFirstError(:,1),500); hold on; h2=histfit(MLLastCorrect(:,1),500); 
%             set(h1(1),'facecolor','y'); set(h1(2),'color','b');set(h2(1),'facecolor','g'); set(h2(2),'color','r')
%         figure; h1=histfit(MLFirstError(:,2),500); hold on; h2=histfit(MLLastCorrect(:,2),500);
%             set(h1(1),'facecolor','y'); set(h1(2),'color','b');set(h2(1),'facecolor','g'); set(h2(2),'color','r')
% 
% %export_fig -pdf -transparent /home/jacob/panning_project/test_programs/testfigure
% %% choose threshold
% %choose_precision_recall_threshold;
% %% GLRT
% GLRT