%% Compare discrete fourier transforms by different methods
% 1) dft
% 2) fft
% 3) Z matrix
[x,xc]=smc_panned_sinusoids(200,0,0.2,8000);
x=x(:,1);
xc=xc(:,1);
N=length(x);

% 1) dft
%[X1]=dft(xc,length(x)); % -j og Zx

% 2) fft
[X2] = fft(xc); % reference j

% 3) Z matrix
[Z] = vandermonde(2*pi/N.*[0:1:N-1],N); % +j og Zx (-)
X3 = Z*xc;

% 4) smc Z matrix
[Z4,X4] = smc_Z_dft(xc,N); % +j og Zx (-)
X4 = Z4*xc;

figure(1)
%    subplot(411); plot(abs(X1));
    subplot(412); plot((X2));
    subplot(413); plot((X3));
    subplot(414); plot((X4));
    
% tjek sign of j and order of multiplication