
%% Estimate parameters across segments first.
fs = 44100;
maxNumSources=10; 
maxDelaySec= 150e-6;
maxDelaySamples = floor(maxDelaySec*fs);
maxPanAngle=44;
trueNumSources =  randi(maxNumSources-1)+1;

[theta, delaySec, trueParams] = smc_random_panning_parameters(trueNumSources,fs, maxDelaySec, maxPanAngle);
[xFullband,fileNumber] = smc_apply_panning_to_iowa_max_duration(theta, delaySec, trueNumSources, fs);
[muEstimates,muDegSampl,alphaEstimates, CEstimates] = smc_estimate_source_panning(xFullband,fs);
muDegSampl
trueParams

% find all possible combinations of clusters
allCombos=[];
numClust=size(muDegSampl,2);
for iii=1:numClust, allCombos{iii} = nchoosek([1:numClust],iii), end

%% compute likelihood of each component separately.
% filter
fc = 4e3; % cutoff freq.
[b,a] = butter(6,fc/(fs/2));
x = filter(b,a,xFullband);
% 
% % parameters for the fourier transform
%% estimate spatial distribution space
wlen     = floor(200e-3*fs);
NMin = floor(wlen/4); KMax = 10; % only if optimal segmentation is applied
NFFT  = 2^nextpow2(NMin*KMax);

NLOGL=[];
for nn=1:wlen:length(x)-wlen
    xWindowed=x(nn:nn+wlen,:);
    % maybe this should have changed threshold in the DFT.
    [X] = smc_estimate_panning_space_from_short_segments(xWindowed, ones(1000,1)*wlen, fs, NFFT, maxDelaySamples, maxPanAngle, fc);
    figure(1); scatter(X(:,1)*maxPanAngle,X(:,2)*maxDelaySamples,'.');
    trueParams;
    muDegSampl;

    % try all combinations
    cnt=1;
    cmb=1;
    while cnt<=numClust
        combMat = allCombos{cnt};
        
        for cntcmb=1:size(combMat,1)
            GMM_MODEL = gmdistribution(muEstimates(:,[combMat(cntcmb,:)])',CEstimates(:,:,[combMat(cntcmb,:)]),alphaEstimates([combMat(cntcmb,:)]));%,'CovType','diagonal'); % generate a GMM
            [degree_of_membership,NLOGL(cmb)] = posterior(GMM_MODEL,X); % calculate the likelihood 
            combinations{cmb}=combMat(cntcmb,:);
            cmb=cmb+1;
        end
        cnt=cnt+1;
    end
    [minLL,b]=min(NLOGL);
    muInSegment = muDegSampl(:,combinations{b});
    [~,ndx]=sort(muInSegment(1,:));
    muInSegment=muInSegment(:,ndx)
    soundsc(xWindowed,fs);
    pause
    
    
end
