% test af EM ND vs. EM DN
%
%
% k = 30;
% 
% cd /home/jacob/panning_project/test_programs/
% load panning_data;
% X=panning_data';
% [labelDN, modelDN, llhDN, BICDN] = EMGMM(X,k);
% figure(1); plotClass(X,labelDN); title('DN plot');

cd /home/jacob/panning_project/test_programs/
load panning_data;
X=panning_data;

normVal = [norm(X(:,1)) norm(X(:,2))];
% overfitting
k=30;
X=X./normVal;
maxVec = max(abs(X));
X=X./maxVec;

[init,~] = kmeans(X,k,'Distance','cityblock','Replicates',12);

[labelND, modelND, llhND, BICND] = EMGMM_ND(X,init');

% sort by determinant of cov and plot smallest on top.
S=modelND.Sigma;

for kk=1:k, detC(kk)=det(S(:,:,kk)), end

[~, ndx] = sort(detC);
X=X(ndx,:);
labelND=labelND(ndx);

figure(2); plotClass(X'.*[45 6]',labelND); title('ND plot');