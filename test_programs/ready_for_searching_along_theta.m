clear all; clf;
% ITD ILD Dan ellis 
duration = 0.5;
fs  = 8e3;
f0  = [63 140 216] ;
pan = [-40 35 10]';
panAngle = pan;
% Make a script to produce panned
%sinusoids with phases also.
[P,Pc] = smc_panned_sinusoids(f0, pan, duration, fs);
P(:,1)=P(:,1).*hann(length(P(:,1)));
P(:,2)=P(:,2).*hann(length(P(:,2)));
soundsc(P,fs)

% f versus theta
% integrate over all frequencies for all angles
% angles equals ratio of energy
% 1) pandir (theta) to gain ratios
baseAngle = 45;
%panAngle  = 30;
phi0 = baseAngle * pi/180; 
phi  = panAngle  * pi/180;
theta = phi + phi0;
g = [cos(theta) sin(theta)]';
%g=g/sum(g.^2);
L = [ [-cos(phi0) sin(phi0)]' ...
      [ cos(phi0) sin(phi0)]' ...
      ];
  
p = (g'*L)';

% 2) gain (energy) ratios to pandir (theta)
g2 = inv(L)'*p;
g2 =g2/sum(g2.^2);
theta2 = [acos(g2(1))];
phi2 = (theta-phi0);
panAngle2 = phi/pi*180;

% searchspace theta
    N = length(Pc);
    Z = smc_Z_dft(Pc,N);
    X = Z*Pc;

    for ii=1:N
    ratios(ii) = abs(X(ii,1))/abs(X(ii,2));
    end
    
    ratiosToLookFor = g(1,:)./g(2,:)
    
    for ii = 1:length(ratiosToLookFor)
        % if ratios(n) is above 1 it is left side
        searchValue = ratiosToLookFor(ii);
        if searchValue < 1
            searchValue = 1/searchValue
            subtractRatio = 1./ratios;
            side = 'right';
        else
            searchValue = searchValue
            subtractRatio = ratios;
            side = 'left';
        end
        scaleFactor = 1;
        % fit the ratio function to have max at closest to theta-related value       
        ratiofunction(:,ii) = abs(searchValue - subtractRatio);
        ratiofunction(:,ii) = (ratiofunction(:,ii) - max(ratiofunction(:,ii)))./searchValue.*(-scaleFactor);
        ratiofunction(:,ii) = ratiofunction(:,ii)-max(ratiofunction(:,ii))+1;
        %ratiofunction(:,ii) = (ratiofunction(:,ii)./searchValue)*10
        
    end
    figure(1)
        subplot(611)
        plot([1:N],abs(X(:,1)),[1:N],abs(X(:,2))); xlim([0 N/2]);
        subplot(612)
        plot([1:N],ratios); xlim([0 N/2]);%legend('values above 1 is the left side');      
        subplot(613)
        plot([1:N],1./ratios); xlim([0 N/2]); % %legend('values above 1 is the right side');
        subplot(614)
        plot([1:N], ratiofunction(:,1)); axis([0 N/2 -scaleFactor scaleFactor]); % legend('ratiofunction for specific theta');
        subplot(615)
        plot([1:N],ratiofunction(:,2)); axis([0 N/2 -scaleFactor scaleFactor]); % legend('ratiofunction for specific theta');
        subplot(616)
        plot([1:N],ratiofunction(:,3)); axis([0 N/2 -scaleFactor scaleFactor]); % legend('ratiofunction for specific theta');

    % plot with imagesc
% search through left side from -45 to 0


