[errorAngle, errorDelay,trueNumClusters,NumClustersMMDL,errorRateModelOrder] = evaluate_synthetic(finalMu, true, iter, 5);

correctAmplitudes = errorAngle(errorAngle<0.51);
A_RMSE=sqrt(mean(correctAmplitudes.^2))
numCorrectParameters = length(correctAmplitudes)
totalParameterEst = sum(trueNumClusters)
parameterPercentage = numCorrectParameters/totalParameterEst

correctDelay = errorDelay(errorAngle<0.6);
D_RMSE=sqrt(mean(correctDelay.^2))

N2000ms.savedX = savedX;
N2000ms.errorAngle = errorAngle;
N2000ms.errorDelay = errorDelay;
N2000ms.trueNumClusters = trueNumClusters;
N2000ms.NumClustersMMDL = NumClustersMMDL;
N2000ms.errorRateModelorder = errorRateModelOrder;
N2000ms.iter = iter;
N2000ms.true = true;
N2000ms.finalMu = finalMu;
N2000ms.D_RMSE = D_RMSE;
N2000ms.parameterPercentage = parameterPercentage;
N2000ms.A_RMSE = A_RMSE;


save('mats/test_on_N/N_2000ms_uniform.mat','N2000ms')