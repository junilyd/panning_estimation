function [XPruned, labelsX, elipses, elipsesPruned,kPruned,alphaPruned,muPruned,CPruned] = remove_sticky_clusters(X, fs, trueNumClusters,alpha,mu,C,level)
    if nargin < 7
        level=2; % figure out why 2 here ...
    end

    k = size(mu,2);
    XPruned=[];

        for ii=1:k
            [uu,ei,vv]=svd(C(:,:,ii));
            a = sqrt(ei(1,1)*level*level);
            b = sqrt(ei(2,2)*level*level);
            % %% define distnce from y center cluster
            d = X'-mu(:,ii);
            % Determin theta rotation angle for the ellipse
            t = atan2(uu(2,:),uu(1,:)); % maybe acos(u(1,1)/sqrt(2))
            t = t(1);
            XX = (d(1,:))*cos(t)+(d(2,:))*sin(t);
            YY = -d(1,:)*sin(t)+d(2,:)*cos(t); 
            % area of ellipse
            detC(ii) = det(C(:,:,ii));
            % locate points inside each elliptic covariance 
            insideIndicator(:,ii) = (XX.^2/a^2+YY.^2/b^2<1); 
            
            % for plotting
            phi = [0:0.01:2*pi];
            xx = a*cos(phi);
            yy = b*sin(phi);
            cord = [xx' yy']';
            cord = uu*cord;
            elipses(:,:,ii) = [cord(1,:)+mu(1,ii); cord(2,:)+mu(2,ii)];
        end 

        % (or) just kill every cluster that touches a smaller cluster
        [~,sortndx] = sort(detC);
        [~,sortbackndx] = sort(sortndx);    
        tmp = insideIndicator(:,sortndx);
        stickyClusters = zeros(size(k));
        for ii=1:k-1
            % silent all the sticky points in large covariance
            matchIndicator = sum( tmp(:,ii) & [zeros(size(tmp,1),ii) tmp(:,ii+1:end)] )>1;
            matchIndicator(ii) = 0;
            stickyClusters = stickyClusters|matchIndicator;
        end    
        stickyClusters = stickyClusters(sortbackndx);

        % set zeros in all sticky cluster points
        insideIndicator = insideIndicator .*(stickyClusters==0);

%          for ii=1:k
%             XPruned=[XPruned; [X(insideIndicator(:,ii)==1,1), X(insideIndicator(:,ii)==1,2)]];
%          end
         
         labelsX = zeros(size(XPruned,1),1);
         labelCnt= 0;
         for ii=1:k
            % Prune X, labels, statistics and ellipses            
            if sum(insideIndicator(:,ii))>1 && sign(mu(1,ii))==sign(mu(2,ii))       
                labelCnt=labelCnt+1;
                XPruned=[XPruned; [X(insideIndicator(:,ii)==1,1), X(insideIndicator(:,ii)==1,2)]];
                labelsX(insideIndicator(:,ii)==1,1) = labelCnt;             
                elipsesPruned(:,:,labelCnt) = elipses(:,:,ii);
                muPruned(:,labelCnt) = mu(:,ii);
                CPruned(:,:,labelCnt) = C(:,:,ii);
                alphaPruned(labelCnt) = alpha(ii);                
            end
         end
        kPruned=labelCnt;
end