function [mindl, bestk, bestmu] = smc_GMM_MMDL_dft(Xl, Xr, fs, NFFT,MaxDelaySamples)

%%
    D_vec=[];
    G_vec=[];
   
        pwl = abs(fft(blackmanharris(length(Xl)).*smc_prewhiten(Xl),NFFT));
        pwr = abs(fft(blackmanharris(length(Xr)).*smc_prewhiten(Xr),NFFT));
        X1 = fft(blackmanharris(length(Xl)).*Xl,NFFT);
        X2 = fft(blackmanharris(length(Xr)).*Xr,NFFT);
        
        wdpiLimit = round(NFFT/2/MaxDelaySamples/2)-1;
        
        X1(wdpiLimit:end) = [];
        X2(wdpiLimit:end) = [];
        pwl(wdpiLimit:end) = [];
        pwr(wdpiLimit:end) = [];
        
        minBin=2;
        X1(1:minBin-1) = [];
        X2(1:minBin-1) = [];
        pwl(1:minBin-1) = [];
        pwr(1:minBin-1) = [];
        
        oldthrl = min(20*mean(abs(X1)),0.4*max(abs(X1)) );
        
        absX1 = abs(X1);
        absX2 = abs(X2);
        O = absX1.*absX2;
        
        thrl = mean(pwl.*pwr);
        fl=find(pwl.*pwr>thrl);
        
        ratio = X2(fl)./X1(fl);
        
        G=acot(abs(ratio));  
        G_vec=[G_vec; G];

        D = -imag(log(ratio))./(2*pi*(fl-1)/NFFT); 
        D_vec=[D_vec; D];
        
        % de-range the data
    maxPanAngle = 45;
    maxDelay = 150e-6;
    GDMask = (abs(D_vec)<maxDelay*fs) & (G_vec<maxPanAngle*pi/180*2);
    if sum(GDMask)>2 
        % scale the gain vector to degrees.
        GD(:,1) = G_vec(GDMask)./pi*180-45;
        GD(:,2) = D_vec(GDMask);
    else
        mindl = -1e-6;
        bestk = 0;
        bestmu = 0;
        return;
    end
    
    % Normalizing the range
    X = GD.*[1/45 1/6];

    %% Clustering Using Gaussian Mixture Models
covType = 0;
regVal = 0.5e-4; % this was 1e-2 before.

    if size(X,1)>10*2
        [bestk,~,bestmu,~,~,~,mindl] = mixtures4(X',2,19,regVal,1e-4,covType,0) ;
        mindl;
    elseif size(X,1)>10
        [bestk,~,bestmu,~,~,~,mindl] = mixtures4(X',2,9,regVal,1e-4,covType,0) ;
    elseif size(X,1)>4 
        [bestk,~,bestmu,~,~,~,mindl] = mixtures4(X',1,3,regVal,1e-4,covType,0) ;
    else
        mindl = -1e6
        bestk = 0;
        bestmu = 0;
        return;
    end
