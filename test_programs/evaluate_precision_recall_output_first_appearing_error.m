% evaluate for duration 18 sec.
clear all;% clf(3); clf(4) 
cd /home/jacob/panning_project/test_programs/
% GOD --> load test_MMDL_40iter_4April_dur_18sec.mat
%load test_MMDL_40iter_findfejl_27March_dur_18sec.mat
%load mats/sep2017/test_SQAM_diagonal_morenoisy_k25.mat
%load mats/sep2017/test_SQAM_diagonal_noisy_k25_250x12.mat
%load mats/sep2017/test_IOWA_diagonal_250samples_k25.mat
%load mats/sep2017/test_SQAM_diagonal_250samples_k35_non-contra.mat
%load mats/sep2017/test_IOWA_diagonal_250samples_k35_non-contra.mat

%%%%%%%%%%%%%%% ONLY For QDC method %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%load('mats/sep2017/test_IOWA_QDCdiagonal_250samples_k35_110iter.mat');

% ms(:,2:3)': prior based, 
% ms(:,7:8)', determinant / 1sigma counts
% ms(:,13:14)': ICD based, 
% ms(:,19:20)': determinant over alpha, 
%load('mats/sep2017/test_IOWA_QDCdiagonal_250samples_k35_200iter.mat'); % Lab presentation
%load('mats/sep2017/test_IOWA_QDCdiagonal_250samples_k35_111iter_noRedundant.mat'); % test with 2D pruning
%load('mats/sep2017/test_IOWA_QDCdiagonal_250samples_k35_200iter_noRedundant_GMM.mat'); %  GMM 200 iterations
%load('mats/sep2017/test_IOWA_QDCdiagonal_250samples_k35_189iter_noRedundant_W.mat');% QDC latest
%load('mats/sep2017/test_IOWA_QDCdiagonal_250samples_k35_1258iter_noRedundant_GMM.mat'); % GMM 
%load('mats/sep2017/test_IOWA_QDCdiagonal_250samples_k35_845iter_noRedundant_GMM_withdm.mat'); % GMM
load('mats/sep2017/test_on_trueK4_savedMuWeighted_1258_sumdm_iter.mat'); % with GV
%load('mats/sep2017/test_on_trueK4_savedMuWeighted_37_GV_iter.mat');
% ONLY SQAM
%load('mats/sep2017/test_SQAM_GMMdiagonal_250samples_k35_iter995_noRedundant.mat'); % 
%load('mats/sep2017/test_SQAM_GMMdiagonal_250samples_k35_iter295_noRedundant.mat');
%load('mats/sep2017/test_on_trueK4_savedMuWeighted_4893iter.mat');


methodNdx =  [19;23]; %[2;7;13];%
for methodNumber = 1:length(methodNdx)


% test for true number of clusters
kTrueRange=[4:4];
for kTrue = kTrueRange
thrForCorrect = 0.5; % both for angle(degrees) and samples(delay)

% define true number of clusters
for ii=1:size(muSorted,2)
    trueNumClusters(ii) = size(trueParameters{ii},1);
end
% extract all data corresponding to the true number of clusters
clusterMask = (trueNumClusters==kTrue);
indexOfDataToExtract = find(clusterMask);
for ii=1:sum(clusterMask)
    trueParametersByTrueClusters{ii} = trueParameters{indexOfDataToExtract(ii)};
    %muSelectedByTrueClusters{ii} = muSelected{indexOfDataToExtract(ii)};
    muSortedByTrueClusters{ii} = [[muSorted{indexOfDataToExtract(ii)}]; [zeros(kmax-size(muSorted{indexOfDataToExtract(ii)},1), size(muSorted{indexOfDataToExtract(ii)},2))]];
end


kVec=1:kmax-15;
for k=kVec
    saved.trueParams = trueParametersByTrueClusters;
    saved.X=trueParametersByTrueClusters; 
    for hh=1:size(muSortedByTrueClusters,2)
        ms = muSortedByTrueClusters{hh}; 
        saved.muPruned{hh} = ms(1:k,methodNdx(methodNumber):methodNdx(methodNumber)+1)'; % ms(:,13:14)': ICD based, ms(:,2:3)': prior based, ms(:,7:8)', determinant based
        saved.trueNumSource{hh} = size(trueParametersByTrueClusters{hh},1);
        saved.kPruned{hh} = size(saved.muPruned{hh},2);
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % evaluation
    [errorAngle, errorDelay,trueNumClusters,estimatedNumClusters,errorRateModelOrder,totalEstimatedParamsPruned, trueMinusEstimated] ...
        = evaluate_overfitting_precision_recall(saved,thrForCorrect);

    [firstErrorNdx] = evaluate_overfitting_output_first_error(saved, thrForCorrect);
    
    correctMask=(errorAngle<thrForCorrect) & (errorDelay<thrForCorrect);
    correctAmplitudes = errorAngle(correctMask);
    correctDelay = errorDelay(correctMask);

    relevantElements(k) = sum(trueNumClusters);

    A_RMSE(k,methodNumber, kTrue)=sqrt(mean(correctAmplitudes.^2));
    D_RMSE(k, methodNumber, kTrue)=sqrt(mean(correctDelay.^2));

    % Precision Recall
    truePositives(k) = sum(correctMask);
    falsePositives(k) = sum(estimatedNumClusters)-truePositives(k);
    precision(k) =  truePositives(k)/(truePositives(k)+falsePositives(k));
    recall(k) = truePositives(k)/relevantElements(k);
    
    clear estimatedNumClusters correctMask correctAmplitudes correctDelay errorAngle errorDelay ms trueNumClusters trueNumClusters 
end
prec(:,methodNumber,kTrue) = precision';
recll(:,methodNumber,kTrue) = recall';
clear precision recall trueParametersByTrueClusters muSortedByTrueClusters
end
end

% plot precision and recall results
figure(3);
for kTrue=kTrueRange
subplot(2,2,kTrue-1); hold on;
title(sprintf('Performance for true k=%1.0f',kTrue))
        h1=plot(kVec,recll(:,1,kTrue),'k-*',kVec,prec(:,1,kTrue),'k--*', ...
                kVec,recll(:,2,kTrue),'b-o',kVec,prec(:,2,kTrue),'b--o');
        xlabel('k'); ylabel('prob.'); ylim([0.1 1.007]); grid minor; xlim([1, kVec(end)])
columnlegend(3,{'\beta: Recall'      ,'\beta: Precision', ...
                '\alpha: Recall','\alpha: Precision'} ...
                ,'location','southwest');
end

% plot RMSE errors
figure(4);
for kTrue=kTrueRange
subplot(2,2,kTrue-1); hold on;
title(sprintf('RMSE for true k=%1.0f',kTrue))
        h2=plot(kVec,A_RMSE(:,1,kTrue),'k-*',kVec,D_RMSE(:,1,kTrue),'k--*', ...
                kVec,A_RMSE(:,2,kTrue),'b-o',kVec,D_RMSE(:,2,kTrue),'b--o');
        xlabel('k'); ylabel('Hz or Samples'); ylim([0 .11]);  grid minor;
columnlegend(3,{'Determinant: A_{RMSE}'      ,'Determinant: D_{RMSE}', ...
                'Prior: A_{RMSE}','Prior: D_{RMSE}'} ...
                ,'location','northwest');
end

figure(3);
kTrue=4

%% plot for article
figure;
        h1=plot(kVec,recll(:,1,kTrue),'k-*',kVec,prec(:,1,kTrue),'k--*', ...
                kVec,recll(:,2,kTrue),'b-o',kVec,prec(:,2,kTrue),'b--o', ...
                [4 4],[-0.5 1],'-.k');
        xlabel('$$k$$', 'interpreter','latex'); ylabel('probability'); ylim([0.2 1.007]); grid on; 
        xlim([kVec(1), kVec(end)]);
        leg1=legend('$$\beta$$: Recall'      ,'$$\beta$$: Precision', ...
                '$$\alpha$$: Recall','$$\alpha$$: Precision', 'True $$K$$', 'interpreter','latex'); 
       %legend boxoff;
        set(leg1,'color','w','edgecolor','w');

        xticks([1:20]); yticks([0.2:0.1:1]);
        set(gca,'XTickLabel',[' 1';'  ';'  ';' 4';'  ';'  ';'  ';' 8';'  ';'  ';'  ';'12';'  ';'  ';'  ';'16';'  ';'  ';'  ';'20';]);
        set(gca,'YTickLabel',['0.2';'   ';'0.4';'   ';'0.6';'   ';'0.8';'   ';' 1 ';]);

% plot F-measure
%figure; 
        F = (2*prec(:,1:2,kTrue).*recll(:,1:2,kTrue)) ./ (prec(:,1:2,kTrue)+recll(:,1:2,kTrue));
        hold on;
        plot(kVec,F(:,1),'*-k',kVec,F(:,2),'o-b')
        xticks([1:20]); yticks([0.2:0.1:1]);
        set(gca,'XTickLabel',[' 1';'  ';'  ';' 4';'  ';'  ';'  ';' 8';'  ';'  ';'  ';'12';'  ';'  ';'  ';'16';'  ';'  ';'  ';'20';]);
        set(gca,'YTickLabel',['0.2';'   ';'0.4';'   ';'0.6';'   ';'0.8';'   ';' 1 ';]);

        
%export_fig -pdf -transparent /home/jacob/panning_project/test_programs/testfigure