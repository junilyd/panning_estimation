phi0 = 45 * pi/180;
panAngle = 30
phi = panAngle * pi/180;
theta = phi + phi0;
g1 = cos(theta);
g2 = sin(theta);
g = [g1 g2]';
C = 1;
gScaled = sqrt(C)*g/sum(g.^2)
l1 = [-cos(phi0) sin(phi0)]';
l2 = [ cos(phi0) sin(phi0)]';
L = [l1 l2];
pT = gScaled'*L
estimatedAngle = atan(pT(1)/pT(2))*180/pi

%%
clear all;
trueAngle1 = 23.45
trueAngle2 = -32.46
fs = 44100;
duration = 0.04;

load('~/Documents/MATLAB/code/test_programs/source_separation/betaMean_Martin_40ms.mat');
load('~/Documents/MATLAB/code/test_programs/source_separation/a_hat_file_used_in_test_on_L.mat');
s500 = smc_sum_of_sines(smc_beta_model(500,0,18), abs(aHat(:,1,3)), 18, duration, fs );
s1500 = smc_sum_of_sines(smc_beta_model(700,0,7), abs(aHat(:,1,3)), 7, duration, fs );

[g1] = VBAP2(trueAngle1);
[g2] = VBAP2(trueAngle2);
s1 = s500*g1(1) + s1500*g2(1);
s2 = s500*g1(2) + s1500*g2(2);
s = [s1 s2];
figure;
subplot(211); smc_spectrogram(s1,fs); xlim([0.02 0.04]); ylim([0 6e3]);
subplot(212); smc_spectrogram(s2,fs); xlim([0.02 0.04]); ylim([0 6e3]);

x1 = analytic(s1); fs2=fs/2;
x2 = analytic(s2); fs2=fs/2;

[w0,L] = joint_anls(x1, [1e-3 0.25],2^12);
pitchEstimateJointANLS = w0*fs/4/pi;

[f, X1] = smc_fft(s1,fs,2^22);
[pitchEstimateL CL]= smc_ANLS(X1, pitchEstimateJointANLS*[0.97 1.03], L, fs);
[f, X2] = smc_fft(s2,fs,2^22);
[pitchEstimateR CR] = smc_ANLS(X2, pitchEstimateJointANLS*[0.93 1.03], L, fs);
if CR>CL
    pitchEstimate = pitchEstimateR
else
    pitchEstimate = pitchEstimateL
end

Z = smc_Z(pitchEstimate, length(x1), fs2, L);
aLeft = inv(Z'*Z)*Z'*x1;
aRight = inv(Z'*Z)*Z'*x2;

g1 = sum(abs(aLeft));
g2 = sum(abs(aRight));
phi0 = 45 * pi/180;
l1 = [-cos(phi0) sin(phi0)]';
l2 = [ cos(phi0) sin(phi0)]';
L = [l1 l2];
g = [g1 g2]'; C = 1;
gScaled = sqrt(C)*g/sqrt(g1^2+g2^2);
pT = gScaled'*L;
estimatedAngle1 = atan(pT(1)/pT(2))*180/pi;

x1Hat = Z*aLeft;
x2Hat = Z*aRight;
sL = ianalytic(x1-x1Hat);
sR = ianalytic(x2-x2Hat);
%
figure;
subplot(211); 
    smc_spectrogram(sL,fs);xlim([0.02 0.04]); ylim([0 6e3]); 
    title('')
subplot(212); smc_spectrogram(sR,fs);xlim([0.02 0.04]); ylim([0 6e3]);




