function tfMat = smc_stft_complex(x,w,timestep,NFFT)

x = x(:); 
w = w(:);
N=length(x);
wLen = length(w);
%calc size and init output t-f matrix
numTime = ceil((N-wLen+1)/timestep);
tfMat = zeros(NFFT,numTime+1);

for i = 1:numTime
    startNdx = ((i-1)*timestep)+1; % current start index
    tfMat(:,i) = fft(x(startNdx:(startNdx+wLen-1)).*w, NFFT);
end
i = i+1;
startNdx = ((i-1)*timestep)+1;
lastStart = min(startNdx, N);
lastEnd = min((startNdx + wLen - 1), N);
tfMat(:,end)=fft([x(lastStart:lastEnd);
                    zeros(wLen-(lastEnd-lastStart+1),1)].*w, NFFT);
tfMat(1,:) = []; % remove DC