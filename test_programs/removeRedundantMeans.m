function [ ndx ] = removeRedundantMeans(testMu, thr)%, maxPanAngle, maxDelaySamples)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% instead of sorting we should remove clusters closer that are
% closer than threshold used for detecting correct clusters.
% Then recall<=1

%testMu = [maxPanAngle maxDelaySamples].*testMu'

for ii=1:size(testMu,1)
    ndx=find( (abs(testMu(ii,1)-testMu(:,1))<thr)&(abs(testMu(ii,2)-testMu(:,2))<thr) );
    testMu(ndx(2:end),:)=0; 
end
ndx = find( (testMu(:,1)==0)&(testMu(:,2)==0));
ndx=[ndx; find(sign(testMu(:,1))~=sign(testMu(:,2)))];
ndx=unique(ndx);
end

