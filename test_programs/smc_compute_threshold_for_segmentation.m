function [fl, Xl, Xr] = smc_compute_threshold_for_segmentation(x,MaxDelaySamples,NFFT)
	    xl=x(:,1);
        xr=x(:,2);
        pwl = abs(fft(blackmanharris(length(xl)).*smc_prewhiten(xl),NFFT));
        pwr = abs(fft(blackmanharris(length(xr)).*smc_prewhiten(xr),NFFT));
        Xl = fft(blackmanharris(length(xl)).*xl,NFFT);
        Xr = fft(blackmanharris(length(xr)).*xr,NFFT);
        
        wdpiLimit = round(NFFT/2/MaxDelaySamples/2)-1;
        
        Xl(wdpiLimit:end) = [];
        Xr(wdpiLimit:end) = [];
        pwl(wdpiLimit:end) = [];
        pwr(wdpiLimit:end) = [];
        
        minBin=2;
        Xl(1:minBin-1) = [];
        Xr(1:minBin-1) = [];
        pwl(1:minBin-1) = [];
        pwr(1:minBin-1) = [];
                
        absXl = abs(Xl);
        absXr = abs(Xr);
        O = absXl.*absXr;
        
        thrl = mean(pwl.*pwr);        
        fl=find(pwl.*pwr>thrl);	
end