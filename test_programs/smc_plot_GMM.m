% clear all; close all;
% cd /home/jacob/panning_project/test_programs
load mats/test_MMDL_82iter_18April.mat
% X = savedX{43};
% X=X';

function plot_GMM(X, alpha, mu, C, maxPanAngle, maxDelaySamples, res)
   if nargin < 7
    res=100;
   end
%     regVal=0.5e-4;
%     covType=0;
%     plotFlag=2;
%     fs=44100;
%     if nargin < 2
%         [bestk,bestpp,bestmu,bestcov,dl,countf,mindl,safemu, safemuMean, safemuSelected] = mixtures4_remove_outliers(X,2,25,regVal,1e-4,covType,plotFlag,fs); % regVal was 1e-4
%     end
    
    xx=[];
    yy=[];
    semi_indic=[];
    indic=[];
    ii=1;
    for x=linspace(-1,1,res)
        xx(ii:ii+res-1)=x;
        yy(ii:ii+res-1) = linspace(-1,1,res);
        ii=ii+res;
    end

    for i=1:size(C,3)
        semi_indic(i,:) = multinorm([xx; yy],mu(:,i),C(:,:,i));
        indic(i,:) = semi_indic(i,:)*alpha(i);
    end

    A = sum(indic);
    A = reshape(A,res,res);
    x=linspace(-1,1,res);
    
    surfc(x*maxPanAngle,x*maxDelaySamples,log(A/sum(sum(A))));
    xlabel('Amplitude Panning Angle [deg.]');
    ylabel('Delay [samples]');
    zlabel('log-likelihood');


end