function [theta, delaySec, truePoints] = smc_random_panning_parameters(maxNumParams,fs, maxDelaySec,maxPanAngle)
%
% This function computes stereophonic panning parameters. 
%
% INPUT: 
%       maxNumParams:   Input stereo audio data (preferably long duration) 
%       fs       :      Sampling rate of xFullband
%       maxDelaySec:    maximum delay in seconds
%       maxPanAngle:    maximum amplitude panning angle in degrees.
%
% OUTPUT:
%       theta:          gain cofficients.
%       delaySec:       delay in seconds 
%       truePoints:     the true panning parameters (for comparison)
%
%
% This method is used in the paper by J. M. Hjerrild and M. G. Christensen 
% "ESTIMATION OF SOURCE PANNING PARAMETERS AND SEGMENTATION OF STEREOPHONIC MIXTURES."
%
% Usage:
% [muEstimates,muDegSampl,alphaEstimates, CEstimates] = smc_random_panning_parameters(maxNumParams,fs, maxDelaySec,maxPanAngle)
%
% Implemented by Jacob Møller Hjerrild at the Audio Analysis Lab, Aalborg
% University.
%------------------------------------------------------------
	if nargin < 2
		fs = 44100;
	end

	M = maxNumParams;
	maxDelay = floor(maxDelaySec*fs)-1; % (-1) has been input to avoid boundary.   
	%maxPanAngle=45;
    
    if mod(M,2)
        M = ceil(M/2);
        mirror=M-1;
    else
        M = M/2;
        mirror=M;
    end
    
    thetaAngle = datasample(2.5:2.5:maxPanAngle,M,'Replace',false);
    ndxMirrored = datasample(1:M,mirror,'Replace',false);
    thetaAngle(end+1:end+mirror) = -thetaAngle(ndxMirrored);
    
    theta = (thetaAngle+45)/180*pi;
    

	for ii=1:maxNumParams
	    delaySamples(ii) = sign(thetaAngle(ii))* randi(maxDelay,1);
	end

	truePoints = [theta(:)/pi*180-45 delaySamples(:)];
	delaySec = delaySamples/fs;
end