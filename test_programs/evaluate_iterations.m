%iter= 50;
%clf(1);clf(2);clf(3);
for ii=1:iter
    t = true(ii).Points
    m = finalMu(ii).MMDL
    
    figure(length(true(ii).Points));
    hold on
    for nn=1:length(m)
        text(m(nn,1), m(nn,2), sprintf('%1.0f',ii),'Color','blue','horizontalalignment','right');
    end
    plot(t(:,1), t(:,2),'k+','markersize',12);
    for nn=1:length(t)
        text(t(nn,1), t(nn,2), sprintf('%1.0f',ii));
    end
    plot(m(:,1), m(:,2), 'bx','markersize',18)
    m = finalMu(ii).MMDL
    plot(m(:,1), m(:,2), 'r.')

    m = finalMu(ii).BIC
    plot(m(:,1), m(:,2), 'g.')
    
    title(sprintf('Estimates of panning parameters (%1.0f clusters)',length(true(ii).Points)))
    xlabel('Pan Angle Estimate');
    ylabel('Delay Estimate');

end
maxNumClusters = 4;
for ii=2:maxNumClusters
    figure(ii)
    legend('True Mean','MMDL','BIC','Kmeans');
end

for ii=iter
    trueNumClusters(ii) = length(true(ii).Points);
    NumClustersMMDL(ii) = length(finalMu(ii).MMDL);
end
errorRateClusters = 100*(1-sum(NumClustersMMDL==trueNumClusters)/iter)
% sort results somehow - by rounding fx