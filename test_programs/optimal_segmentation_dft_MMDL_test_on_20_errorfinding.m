
clear all; close all;

for iter = 1:40
    %% Apply panning parameters to M audio recordings
    maxNumSources=5; 
    maxDelaySec= 150e-6;
    fs = 44100;

    numSources = randi(maxNumSources-1)+1;
    [theta, delaySec, true(iter).Points] = smc_random_panning_parameters(numSources,fs, maxDelaySec);

    startSec = 2;
    durationSec = 18;

%% Apply panning parameters to M audio recordings
% M=7; fs = 44100;
% sti = '/home/jacob/audio/sqam/'
% %sti = '/Users/home/Documents/P9/panning_project/test_programs/SQAM_FLAC/';
% files={strcat(sti,'14.flac'),strcat(sti,'26.flac'),strcat(sti,'9.flac'),strcat(sti,'22.flac'),strcat(sti,'8.flac'),strcat(sti,'3.flac'),strcat(sti,'4.flac'),strcat(sti,'17.flac'),strcat(sti,'16.flac')};
% theta=(    [-30    -20    -10    0 10      20      30    ]+45)/180*pi;
% delaySec = [200e-6 150e-6 100e-6 0 -100e-6 -150e-6 -200e-6]/1.4;
% true(iter).Points = [theta(:)/pi*180-45 delaySec(:)*fs;];

durationSec =18;
    x = smc_apply_panning_to_sqam(theta, delaySec, numSources, durationSec, fs);

    % parameters for the fourier transform
    wlen     = floor(200e-3*fs);
    NMin = floor(wlen/4);
    w = hann(wlen);

    %%   Apply optimal segmentation in dft-domain
    %       - set min and max segment sizes
    KMax = 8;
    NMax = NMin * KMax;
    % resize x to be multiple of Nmin samples.
    M = floor(size(x,1)/NMin) ;
    x=x(1:M*NMin,:);
    NFFT  = 2^nextpow2(NMin*KMax);

    k_opt=[];
    numClusters=[];
    mu1=[];
    mu2=[];
    m=1;
    while (m<=M)
        cost=[];
        J=[];
        K = min(m, KMax);
        for k=1:K
            N  = ((m-k)*NMin+1:m*NMin);
            N2 = (1:(m-k)*NMin);
            [J, numClusters(m)] = smc_GMM_MMDL_dft( x(N, 1), x(N, 2), fs, NFFT);
            if (m>k)
                cost(k) = J + smc_GMM_MMDL_dft( x(N2, 1), x(N2, 2), fs, NFFT); 
            else
                cost(k) = J;
            end
        end
        [~, k_opt(m)] = min(cost);
        m=m+1;
    end
    m = M; % is defined above the loop.
    bb=1;
    while (m > 0)
        numBlocksInSegment(bb) = k_opt(m);
        numClustersInSegment(bb) = numClusters(m);
        m=m-k_opt(m);
        bb=bb+1;
    end
    numBlocksInSegment = numBlocksInSegment(end:-1:1);
    numClustersInSegment = numClustersInSegment(end:-1:1);

    %% Evaluate the given optimal segments 
    [X] = smc_estimate_panning_space_from_optimal_segments(x, numBlocksInSegment*wlen, fs, true(iter).Points, NFFT);

    %Xred = smc_reduce_data_by_gmm_em(X);
    
    savedX{iter,1} = X;
   % savedXred{iter} = Xred;
    
    [~,bestk(iter),~,~,finalMu(iter).MMDL,safemu(iter).first,safemu(iter).Mean, safemu(iter).Selected, safemu(iter).covRatio] = smc_GMM_MMDL(X, fs, size(true(iter).Points,1));
   % [~,bestkRed(iter),~,~,finalMu(iter).RedMMDL,safemu(iter).Redfirst,safemu(iter).RedMean, safemu(iter).RedSelected,safemu(iter).redcovRatio] = smc_GMM_MMDL(Xred, fs, size(true(iter).Points,1));

    %[~, ~, finalMu(iter).BIC] = smc_GMM_BIC(X, fs);
    %[~, ~, finalMu(iter).kMeans] = smc_kmeans(X, fs, 12);
    
    
    % Short time fourier transform
    % wlen     = floor(200e-3*fs);
    NMin = floor(wlen/4);
    NFFT  = 2^nextpow2(wlen);%2^14;
    w = hann(wlen);
    Xl = smc_stft_complex(x(:,1), w, NMin, NFFT);
    Xr = smc_stft_complex(x(:,2), w, NMin, NFFT);
    
    % plot segementations
    plotSegments = 0;
    if plotSegments
        figure;
        smc_plot(x,fs);
        tmp=0;
        for ii=1:length(numBlocksInSegment) 
            subplot(211); hold on; 
                plot([tmp +numBlocksInSegment(ii)*NMin/fs tmp+numBlocksInSegment(ii)*NMin/fs],[1.5*min(x(:,1)) 1.5*max(x(:,1))],'k')
                tmp = tmp +numBlocksInSegment(ii)*NMin/fs;
        end
        figure;
            imagesc([1:(size(Xl,2))]*NMin/fs-NMin/fs/2, linspace(0,1,(NFFT/2-1))*fs/2, 20*log10(abs(Xl(1:NFFT/2-1,:))));
            axis xy;
            caxis(max(caxis)+[-60 0]);
            xlabel('Time [sec]'); ylabel('Frequency [Hz]');
            title(sprintf('Spectrogram of track'));
            ylim([0 fs/2]);

        tmp=0; % to fit the above
        for ii=1:length(numBlocksInSegment) 
            hold on; plot([tmp+numBlocksInSegment(ii)*NMin/fs tmp+numBlocksInSegment(ii)*NMin/fs],[0 fs/2],'w')
            tmp=tmp+numBlocksInSegment(ii)*NMin/fs;
        end
    end
    true(iter).Points
    safemu(iter).covRatio
clear X Xl Xr x numBlocksInSegment numClustersInSegment numClusters k_opt cost 
showfigs
close all
end
evaluate_iterations_covariance_function(safemu, true, iter, maxNumSources);