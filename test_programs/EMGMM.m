function [label, model, llh, BIC] = EMGMM_ND(x, init)
% EM algorithm for fitting a Gaussian mixture model.
% The algorithm is implemented from the Bishop book.
%
% Input: 
%           x:      N x D data matrix
%           init:   k (1 x 1) number of components 
%                   or label (n x 1, 1 <= label(i) <= k) 
%                   or model structure.
% Output:
%           label:  n x 1 cluster label
%           model:  trained model
%           llh:    loglikelihood
%
%   Written by Jacob Møller (junilyd@gmail.com).
%
%% 
fprintf('EM GMM is running ... \n');
tol = 1e-6;
maxiter = 500;
llh = -inf(maxiter,1);
gamma_znk = initialization(x, init);
for iter = 2:maxiter
    [~,label(:,1)] = max(gamma_znk,[],2);
    gamma_znk = gamma_znk(:,unique(label));   % remove empty clusters
    model     = maximization(x, gamma_znk);
    [gamma_znk, llh(iter),BIC] = expectation(x,model);
    if abs(llh(iter)-llh(iter-1)) < tol*abs(llh(iter)); break; end;
end
llh = llh(2:iter);

function gamma_znk = initialization(x, init)
N = size(x,1);
if isstruct(init)  % init with a model
    gamma_znk  = expectation(x,init);
elseif numel(init) == 1  % random init k
    k = init;
    label = ceil(k*rand(1,N));
    gamma_znk = full(sparse(1:N,label,1,N,k,N));
elseif all(size(init)==[1,N])  % init with labels
    label = init;
    k = max(label);
    gamma_znk = full(sparse(1:N,label,1,N,k,N));
else
    error('ERROR: init is not valid.');
end

function [gamma_znk, llh, BIC] = expectation(x, model)
mu = model.mu;
Sigma = model.Sigma;
pi_k = model.pi_k;

N = size(x,1);
k = size(mu,2);
gamma_znk = zeros(N,k);
for i = 1:k
    gamma_znk(:,i) = loggausspdf(x, mu(:,i), Sigma(:,:,i));
end
gamma_znk = bsxfun(@plus,gamma_znk, log(pi_k)); % gamma = gamma + log(pi_k)
T   = logsumexp(gamma_znk,2); % T = log(sum(exp(x)))
llh = sum(T)/N; 
BIC = 2*llh + k*log(N);%+3/2*log(n);
gamma_znk = exp(bsxfun(@minus,gamma_znk,T));

function model = maximization(x, gamma_znk)
[N,D] = size(x);
k = size(gamma_znk,2);
Nk = sum(gamma_znk,1);
pi_kNew = Nk/N; %(9.22)

mu = bsxfun(@times, x'*gamma_znk, 1./Nk);

Sigma = zeros(D,D,k);
r = sqrt(gamma_znk);
for i = 1:k
    Xo = bsxfun(@minus,x', mu(:,i)); % Xo = (X-mu)*sqrt(gamma)
    Xo = bsxfun(@times,Xo,r(:,i)');
    Sigma(:,:,i) = Xo*Xo'/Nk(i)+eye(D)*(1e-6); % 1e-6=tol? % sqrt(gamma)* (X-mu)(X-mu)' + lambda
end

model.mu = mu;
model.Sigma = Sigma;
model.pi_k = pi_kNew;

function y = loggausspdf(x, mu, Sigma)
D = size(x,2);
x = bsxfun(@minus,x',mu); % x-mu
[U,p]= chol(Sigma);
if p ~= 0
    error('ERROR: Sigma is not PD.');
end
Q = U'\x;
q = dot(Q,Q,1);  % quadratic term (M distance)
c = D*log(2*pi)+2*sum(log(diag(U)));   % normalization constant
y = -(c+q)/2;