clear all; close all;
%cd /Users/home/panning_estimation/test_programs;
cd ~/panning_project/test_programs;
load mats/test_on_N/guitar_experiment.mat;
    iter=1;
    
            X = savedX{iter}; x = savedx{iter}; f0 = savedf0{iter};numBlocksInSegment = savedNumBlocksInSegment{iter};
                plotNMin = floor(wlen/4);
            NFFT  = 2^nextpow2(wlen*2);%2^14;
            w = hann(wlen/4);
            Xl = smc_stft_complex(x(:,1), w, plotNMin, NFFT);
            Xr = smc_stft_complex(x(:,2), w, plotNMin, NFFT);

            figure;
            (imagesc([1:(size(Xl,2))]*NMin/fs-NMin/fs/2, linspace(0,1,(NFFT/2-1))*fs/2, 20*log10(abs(Xl(1:NFFT/2-1,:)))));
            axis xy;
            caxis(max(caxis)+[-60 0]);
            xlabel('Time [sec]'); ylabel('Frequency [Hz]');
            ylim([0 fs/14]);
            hold on; (plot(tAXis,[x(:,2) x(:,1)]*500+2500))

        tmp=0; % to fit the above
        for ii=1:length(numBlocksInSegment) 
            hold on; plot([tmp+numBlocksInSegment(ii)*NMin/fs tmp+numBlocksInSegment(ii)*NMin/fs],[0 fs/2],'w')
            tmp=tmp+numBlocksInSegment(ii)*NMin/fs;
        end
        
        
            fs=44100;
            L=30;
            SNR=80;
            fig=0;
            [~, x300_1, f0(1,1), betaCoeffReturnValue, aHatReturnVector, fBetaCoeff, xVar] = smc_synthetic_guitar_with_white_noise_inharmonic(fs,300e-3,L,SNR,fig);
            [~, x600_1, f0(2,1), betaCoeffReturnValue, aHatReturnVector, fBetaCoeff, xVar] = smc_synthetic_guitar_with_white_noise_inharmonic(fs,600e-3,L,SNR,fig);
            [~, x900_1, f0(3,1), betaCoeffReturnValue, aHatReturnVector, fBetaCoeff, xVar] = smc_synthetic_guitar_with_white_noise_inharmonic(fs,900e-3,L,SNR,fig);
            [~, x1200_1, f0(4,1), betaCoeffReturnValue, aHatReturnVector, fBetaCoeff, xVar] = smc_synthetic_guitar_with_white_noise_inharmonic(fs,1200e-3,L,SNR,fig);
            [~, x1800_1, f0(5,1), betaCoeffReturnValue, aHatReturnVector, fBetaCoeff, xVar] = smc_synthetic_guitar_with_white_noise_inharmonic(fs,1800e-3,L,SNR,fig);
            [~, x3600_1, f0(5,1), betaCoeffReturnValue, aHatReturnVector, fBetaCoeff, xVar] = smc_synthetic_guitar_with_white_noise_inharmonic(fs,3600e-3,L,SNR,fig);
        
        % source definitions in energy
        o3=ones(size(x300_1))';
        o6=ones(size(x600_1))';
        o9=ones(size(x900_1))';
        o12=ones(size(x1200_1))';
        o18=ones(size(x1800_1))';
        o36=ones(size(x3600_1))';
        z3=zeros(size(x300_1))';
        z6=zeros(size(x600_1))';
        z9=zeros(size(x900_1))';
        z12=zeros(size(x1200_1))';

        s1 = [o9 z9 o9 z9 z9 o9 z3 o6 z6 o12 z9 o3 z3 o3 z6 o6 z6 o36]';
        x1 = [x900_1' z900'   x900_1' z900'   z900' x900_1' z300' x600_1' z600'    x1200_1' z900'                 x300_1' z300' x300_1' z600' x600_1' z600' x3600_1']';

        s2 = [z9 o9 z9 o9 z9 o9 z9 o12 z12 o3 z3 o3 z3 o6 z6 o6 z12 z3 o3 o6 z3 o6 z3]';

%         figure;
%         plot(tAXis, s2.*x(:,2)-1,tAXis, s1.*x(:,1)+1); hold on;
%         plot(tAXis, s1+1,'k');
%         plot(tAXis, s2-1,'k');
       
        
        figure;
        %%
        tmp=0;
        %figure;
        hf = subplot(212)
        subplot(212)
            (imagesc([1:(size(Xr,2))]*NMin/fs-NMin/fs/2, linspace(0,1,(NFFT/2-1))*fs/2, 20*log10(abs(Xr(1:NFFT/2-1,:)))));
            axis xy;
            caxis(max(caxis)+[-60 0]);
            xlabel('Time [sec]'); ylabel('Frequency [Hz]');
            %title(sprintf('Spectrogram of track'));
            ylim([0 fs/14]);       
            hold on; 
        ht = subplot(211)
            plot(tAXis,[x(:,2) x(:,1)]);%*700+2500)
        
            colors={'k','w'};
        for pl=1:2
            tmp=0; % to fit the above
        subplot(2,1,pl); hold on;
            for ii=1:length(numBlocksInSegment) 
                hold on; plot([tmp+numBlocksInSegment(ii)*NMin/fs tmp+numBlocksInSegment(ii)*NMin/fs],[-1 fs/2],colors{pl})
                tmp=tmp+numBlocksInSegment(ii)*NMin/fs;
                xlim([0 15])
            end
        end
        subplot(211)
            ylim([-1 1]);

        
        (plot(tAXis, s1*2-1.5,'k.'));%(s1-0.9)*24000,'k.'));
        (plot(tAXis, s2-1.5,'k.'));%((s2)-0.9)*25500,'k.')); 
        set(gca,'xtick',[])
        l=legend('left signal', 'right signal','active source indicator')
        
        l.Orientation = 'horizontal';
%          yyaxis right;
        ylabel('Amplitude');
        axf = get(hf,'Position');
        axf(2)=axf(2)+0; %or wathever
        axf(4)=axf(4)+0.25
        set(hf,'Position',axf);
        
         axt = get(ht,'Position');         
         axt(2)=axt(2)+0.13
         axt(4)=axt(4)-0.12;
         set(ht,'Position',axt);
                

        