% read csv data and create pr-tools data set.
clear all;
prwaitbar off;

a = csvread('panning_for_PR.arff');
fl = {'Amplitude Ratio', 'Delay Ratio'};



   

x = prdataset(a(:,1:end-1), a(:,end));
x = setfeatlab(x, fl);
x = setname(x,'Panning Parameter Estimates');
x = setuser(x,'Jacob');

% split tot 80% train and 20% test
%xTrain = seldat(x, 0.2)
classsize = classsizes(x);
[xTrain, xTest] = gendat(x,[classsize*0.8]);
% analyze data
W = gaussm(x,1); % one gaussian per class.
figure(1)
    scatterd(x);
    plotm(W,4);
% 
% figure(2)
%     scatterd(xTest)
%     dendr = hclust(distm(xTest),'s');
% figure(3)
%     plotdg(dendr)

figure(4);
z = prdataset(xTest.data);
z = setlabtype(z,'soft');
    [lab,w]=emclust(z,qdc,7);
    scatterd(z); plotm(w,3);
