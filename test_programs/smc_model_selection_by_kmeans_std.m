% This function does no plotting
function [M, stdOutput, mu1] = smc_model_selection_by_kmeans_std(X, fs, maxClusters)
    if size(X,1)<size(X,2)
        X=X';
    end
    if nargin < 3
        M = 10;
    else
        M=maxClusters;
    end
    
    % de-range the data
    % maxPanAngle = 45;
    % maxDelay = 200e-6;
    % GDMask = (abs(X(:,2))<maxDelay*fs) & (X(:,1)<maxPanAngle*pi/180*2);
    % if sum(GDMask)>10*3
        % scale the gain vector to degrees.
        GD = X;%(GDMask,:);

        % K-means Clustering
        [ndx,muInit] = kmeans(GD,M,'Distance','cityblock','Replicates',12);
        % delete every sample over std*factor
        STDFACTOR = 0.4;
        %PERCENTPROXIMITY = 0.02;
        CENTERPERCENTAGE = 0.2;

        criteria = 1;
        while criteria ~=0
            %figure(ff);clf;ff=ff+1;
            for cc=1:M
                clusterG = GD(ndx==cc,1); clusterD = GD(ndx==cc,2);
                stdClusterG(cc) = std(clusterG); stdClusterD(cc) = std(clusterD);
                % mask out everything STDFACTOR away from mean in x and y direction
                stdMask{cc} = (abs(clusterG-muInit(cc,1)) > STDFACTOR*stdClusterG(cc)) | (abs(clusterD-muInit(cc,2)) > STDFACTOR*stdClusterD(cc));   

                % mask out everything STDFACTOR below from mean in x and y direction
                stdMaskInside{cc} = (abs(clusterG-muInit(cc,1)) < STDFACTOR*stdClusterG(cc)) & (abs(clusterD-muInit(cc,2)) < STDFACTOR*stdClusterD(cc));   
                Mask2STD{cc} = (abs(clusterG-muInit(cc,1)) < 2*stdClusterG(cc)) & (abs(clusterD-muInit(cc,2)) < 2*stdClusterD(cc));   

                centerDensity(cc) = sum( stdMaskInside{cc} )/length(stdMaskInside{cc});
            end 
            % compute number of classes based on center proximity
            sortedMeanDensity = centerDensity(centerDensity>CENTERPERCENTAGE);
            if min(centerDensity) < mean(sortedMeanDensity)
                M=M-1
                criteria = 1;
                clear centerDensity stdClusterD stdClusterG;
                [ndx,mu1] = kmeans(GD,M,'Distance','cityblock','Replicates',10);
            else 
                criteria = 0;
            end
        end

        % GD21=[];
        % GD22=[];
        % for cc=1:M
        %     clusterG = GD(ndx==cc,1);
        %     clusterD = GD(ndx==cc,2);      
        %     tmpG = clusterG(Mask2STD{cc});
        %     tmpD = clusterD(Mask2STD{cc});
        %     GD21 = [GD21; tmpG];
        %     GD22 = [GD22; tmpD];            
        % end
        % GD2 = [GD21 GD22];
        % % try the rest with GD2
        % [ndx,mu2] = kmeans(GD2,M,'Distance','cityblock');
        % GD=GD2;

        %figure(ff);clf;ff=ff+1;
            for cc=1:M
                %plot(GD(ndx==cc,1),GD(ndx==cc,2),C{cc},'MarkerSize',12); hold on;
                stdClusterD2(cc) = std(GD(ndx==cc,1)); 
                stdClusterG2(cc) = std(GD(ndx==cc,2)); 
            end  
        stdOutput =  mean((stdClusterD2+stdClusterG2)/2);

            hold on ;
            plot(mu1(:,1),mu1(:,2),'kx','MarkerSize',15,'LineWidth',3);
            %legend('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster5', ...
            %       'Cluster 6','Cluster 7','Cluster 8','Cluster 9','Cluster10', 'Centroids','Location','NW');
            %title 'K-Means Cluster Assignments and Centroids'
            hold off; 
    % else 
    %     stdOutput = nan;
    %     M=0;
    %     mu1=0;
    %     mu2=0;
    % end
end