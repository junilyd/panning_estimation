clear all
cd /home/jacob/panning_project/test_programs/
% GOD --> load test_MMDL_40iter_4April_dur_18sec.mat
%test_MMDL_40iter_findfejl_27March_dur_18sec.mat

load mats/test_MMDL_82iter_18April.mat;
for ii=55:55
    X = savedX{ii};
    if ii<41
        X=X.*[1 150/200]; % normalization was different in the beginning.
    end
    %X=X(sign(X(:,1))~=sign(X(:,2)),:);
    ii
    [~, ~, ~, ~,~,~, ~, ~, safemu(ii).covRatio, tmp,X2] = smc_GMM_MMDL_normdetC(X, fs, size(true(ii).Points,1));
    meanVal(ii,1) = tmp(1);
    meanVal(ii,2) = tmp(2);
    minmax(ii,1) = tmp(3);
    minmax(ii,2) = tmp(4);
    minmax(ii,3) = tmp(5);
    minmax(ii,4) = tmp(6);
    %meanratio(ii) = tmp(7)
    true(ii).Points
    %pause
    %close all
end
% errorVector =[57 48 45 29] ;%[45 46 48 49 50 53 54 55 57 73 75 75];
% for ii =errorVector
%     x = savedx{ii};
%     [X] = smc_estimate_panning_space_from_optimal_segments(savedx{ii}, ones(1000,1)*wlen, fs, true(ii).Points, NFFT, maxDelaySamples);
%     true(ii).Points
%     [~, ~, ~, ~,~,~, ~, ~, safemu(ii).covRatio, tmp,X2] = smc_GMM_MMDL_normdetC(X, fs, size(true(ii).Points,1));
%     true(ii).Points
%     safemu(ii).covRatio
%     %savedX{iter}=X;
%     pause
% end
 [errorAngle, errorDelay,trueNumClusters,NumClustersMMDL,errorRateModelOrder]  = evaluate_iterations_covariance_function(safemu, true, iter, maxNumSources);


sc = 100/85;
% due to error in iter=58
NumClustersMMDL=numClusters-1
correctAmplitudes = errorAngle(errorAngle<0.6);
A_RMSE=sqrt(mean(correctAmplitudes.^2))
numCorrectParameters = length(correctAmplitudes) * sc
totalParameterEst = sum(trueNumClusters) * sc
parameterPercentage = numCorrectParameters/totalParameterEst
errorRateModelOrder

correctDelay = errorDelay(errorAngle<0.6);
D_RMSE=sqrt(mean(correctDelay.^2))