 function [safemu, safek, safeNdx] = smc_mixture4_print_outliers(estmu, estcov, k)
    % Compute variance for each Gaussian Mode
    vari=[];
    MASKaboveSigmaMean=[];
    for cc=1:length(estmu)
        vari(cc) = trace(estcov(:,:,cc)); % 1,1: variance in amp. direction
    end
    % mask all class estimates with variance. above mean variance. 
    MASKaboveSigmaMean = vari>mean(vari)*(2/k);
    % find all data points lower than the mean var.
    safeNdx = find(MASKaboveSigmaMean==0);
    ksAboveMean = (k-1)-length(safeNdx);
    safemu = round(estmu(:,safeNdx)'.*[45 10]*10)/10;
    safek = length(unique(safemu(:,1)));



 end