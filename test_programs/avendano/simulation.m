clear all; close all;
a = [0:1/50:1];

psi = (2*(a-a.^2)./(a.^2+(1-a).^2));

% if a<0.5 --> deltaHAt = -1
% if a>0.5 --> deltaHAt = 1
% if a==0.5 --> deltaHAt = 0
deltaHAt = (a-0.5)*2
Psi = (1-psi).*deltaHAt

figure(1);
plot(a,psi); ylabel('\psi'); xlabel('\alpha'); grid on;
title('Similarity Function vs. alpha');
figure(2); 
plot(a,Psi); ylabel('\psi'); xlabel('\alpha'); grid on;
title('panning index vs. alpha')