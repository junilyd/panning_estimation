clear all; close all;
cd /home/jacob/panning_project/test_programs/
%% Apply panning parameters to M audio recordings
M=7; fs = 44100;
sti = '/home/jacob/panning_project/test_programs/SQAM_FLAC/';
%sti = '/Users/home/Documents/P9/panning_project/test_programs/SQAM_FLAC/';
files={strcat(sti,'14.flac'),strcat(sti,'26.flac'),strcat(sti,'09.flac'),strcat(sti,'22.flac'),strcat(sti,'08.flac'),strcat(sti,'03.flac'),strcat(sti,'04.flac'),strcat(sti,'17.flac'),strcat(sti,'16.flac')};
theta=(   [-30    -20    -10    0 10      20      30    ]+45)/180*pi;
delaySec =[200e-6 150e-6 100e-6 0 -100e-6 -150e-6 -200e-6]/1.4;
truePoints = [theta(:)/pi*180-45 delaySec(:)*fs;];
fs=44100;
for m=1:M
    [source{m},fs]=audioread(files{m}, [2*fs 20*fs]);
    len(m)=length(source{m});
end
len=min(len);
x=zeros(len,2);
delay = floor(abs(delaySec*fs));

for m=1:M
    source{m}=source{m}(:,1);    
    source{m}=source{m}(1:len);
    source{m}=source{m}/var(source{m});    
end

for m=1:M
  g(1,m)=sin(theta(m));
  g(2,m)=cos(theta(m));
  if delaySec(m) > 0 
    x(:,1)=x(:,1)+g(1,m)*source{m};
    x(:,2)=x(:,2)+g(2,m)*[zeros(delay(m),1); source{m}(1:end-delay(m))];
  else
    x(:,2)=x(:,2)+g(2,m)*source{m};
    x(:,1)=x(:,1)+g(1,m)*[zeros(delay(m),1); source{m}(1:end-delay(m))];
  end
end
    
%%

% implement the short time fourier transform
wlen     = floor(200e-3*fs);
NMin = floor(wlen/4);
NFFT  = 2^nextpow2(wlen);%2^14;
w = hann(wlen);
Xl = smc_stft_complex(x(:,1), w, NMin, NFFT);
Xr = smc_stft_complex(x(:,2), w, NMin, NFFT);

%%
    D_vec=[];
    G_vec=[];
    %F=2^12;
    %M=(M+2);
    
    %win = hann(bufferLen);
    %pos=1:bufferLen;
    %while pos(end)<size(Xl,2),%len,
    for ii=1:size(Xl,2)
        X1 = Xl(1:NFFT/2-1,ii);
        X2 = Xr(1:NFFT/2-1,ii);

        thrl = min(20*mean(abs(X1)),0.4*max(abs(X1)) );

        fl=find(abs(X1)>thrl); % thr
        
        ratio = X2(fl)./X1(fl);
        
        G=acot(abs(ratio));  
        size(G)
        G_vec=[G_vec; G];

        D = -imag(log(ratio))./(2*pi*(fl-1)/NFFT); 
        D_vec=[D_vec; D];
        %scatterhist(G,D,100); pause;
    end

        % de-range the data
    maxPanAngle = 45;
    maxDelay = 200e-6;
    GDMask = (abs(D_vec)<maxDelay*fs) & (G_vec<maxPanAngle*pi/180*2);
    if sum(GDMask)>M*3
        % scale the gain vector to degrees.
        GD(:,1) = G_vec(GDMask)./pi*180-45;
        GD(:,2) = D_vec(GDMask);
    end
        % K-means Clustering
        % [ndx,mu1] = kmeans(GD,M,'Distance','cityblock','Replicates',12);
X = (GD);

%% Clustering Using Gaussian Mixture Models
%% How Gaussian Mixture Models Cluster Data
% Gaussian mixture models (GMM) are often used for data clustering.
% Usually, fitted GMMs cluster by assigning query data points to the
% multivariate normal components that maximize the component posterior
% probability given the data. That is, given a fitted GMM,
% <docid:stats_ug.brx2uny-1 cluster> assigns query data to the component
% yielding the highest posterior probability. This method of assigning a
% data point to exactly one cluster is called _hard_ clustering. For an
% example showing how to fit a GMM to data, cluster using the fitted model,
% and estimate component posterior probabilities, see
% <docid:stats_ug.bra9fvn Cluster Data from Mixture of Gaussian
% Distributions>.
%%
% However, GMM clustering is more flexible because you can view it as a
% _fuzzy_ or _soft clustering_ method. Soft clustering methods assign a
% score to a data point for each cluster. The value of the score indicates
% the association strength of the data point to the cluster. As opposed to
% hard clustering methods, soft clustering methods are flexible in that
% they can assign a data point to more than one cluster.  When clustering
% with GMMs, the score is the posterior probability.  For an example of
% soft clustering using GMM, see <docid:stats_ug.buqq83f Cluster Gaussian
% Mixture Data Using Soft Clustering>.
%%
% Moreover, GMM clustering can accommodate clusters that have different
% sizes and correlation structures within them. Because of this, GMM
% clustering can be more appropriate to use than, e.g, _k_-means
% clustering.
%%
% Like most clustering methods, you must specify the number of desired
% clusters before fitting the model. The number of clusters specifies the
% number of components in the GMM. For GMMs, it is best practice to also
% consider the: 
%
% * Component covariance structure.  You can specify diagonal or full
% covariance matrices, or whether all components have the same covariance
% matrix.
% * Initial conditions. The Expectation-Maximization (EM) algorithm fits
% the GMM. Like the _k_-means clustering algorithm, EM is sensitive to
% initial conditions and might converge to a local optimum.  You can
% specify your own starting values for the parameters, specify initial
% cluster assignments for data points or let them be randomly chosen, or
% specify to use the _k_-means ++ algorithm.
% * Regularization parameter. If, for example, you have more predictors
% than data points, then you can regularize for estimation stability.
%
%% Covariance Structure Options
% Load Fisher's iris data set.  Consider clustering the
% sepal measurements.
%load fisheriris;
%X = meas(:,1:2); 
[n,p] = size(X);
rng(3); % For reproducibility

figure;
plot(X(:,1),X(:,2),'.','MarkerSize',15); hold on;
plot(truePoints(:,1), truePoints(:,2),'w+','MarkerSize',15,'LineWidth',2)
title('Panning Parameter Distributions');
xlabel('Amplitude Angle Estimate');
ylabel('Delay Estimate');
%% Tune Gaussian Mixture Models
% This example shows how to determine the best Gaussian mixture model (GMM)
% fit by adjusting the number of components and the component covariance
% matrix structure.

%%
% Suppose _k_ is the number of desired components or clusters, and $\Sigma$
% is the covariance structure for all components.  Follow these steps to
% tune a GMM.
%
% # Choose a (_k_, $\Sigma$) pair, and then fit a GMM using the chosen
% parameter specification and the entire data set.
% # Estimate the AIC and BIC.
% # Repeat steps 1 and 2 until you exhaust all (_k_, $\Sigma$) pairs of
% interest.
% # Choose the fitted GMM that balances low AIC with simplicity.
%
%%
% For this example, choose a grid of values for _k_ that include 2 and 3,
% and some surrounding numbers.  Specify all available choices for
% covariance structure.  If _k_ is too high for the data set, then the
% estimated component covariances can be badly conditioned.  Specify to use
% regularization to avoid badly conditioned covariance matrices. Increase
% the number of EM algorithm iterations to 10000.
k = 1:10;
nK = numel(k);
Sigma = {'diagonal','full'};
nSigma = numel(Sigma);
SharedCovariance = {true,false};
SCtext = {'true','false'};
nSC = numel(SharedCovariance);
RegularizationValue = 0.05; % Does this parameter work correctly ? 
options = statset('MaxIter',1000);
%
% Fit the GMMs using all parameter combination.  Compute the AIC and BIC
% for each fit.  Track the terminal convergence status of each fit.

% Preallocation
gm = cell(nK,nSigma,nSC);         
aic = zeros(nK,nSigma,nSC);
bic = zeros(nK,nSigma,nSC);
converged = false(nK,nSigma,nSC);

% Fit all models
for m = 1:nSC
    for j = 1:nSigma
        for i = 1:nK
            gm{i,j,m} = smc_fitgmdist(X,k(i),...
                'CovarianceType',Sigma{j},...
                'SharedCovariance',SharedCovariance{m},...
                'RegularizationValue',RegularizationValue,...
                'Options',options,'Start',kmeans(X,i,'Distance','cityblock','Replicates',12));
            bic(i,j,m) = gm{i,j,m}.BIC;
            converged(i,j,m) = gm{i,j,m}.Converged;
        end
    end
end

allConverge = (sum(converged(:)) == nK*nSigma*nSC);
%%
% |gm| is a cell array containing all of the fitted |gmdistribution| model
% objects. All of the fitting instances converged. 
%%
% Plot separate bar charts to compare the AIC and BIC among all fits. Group
% the bars by _k_.
% figure;
% bar(reshape(aic,nK,nSigma*nSC));
% title('AIC For Various $k$ and $\Sigma$ Choices','Interpreter','latex');
% xlabel('$k$','Interpreter','Latex');
% ylabel('AIC');
% legend({'Diagonal-shared','Full-shared','Diagonal-unshared',...
%     'Full-unshared'});

figure;
bar(reshape(bic,nK,nSigma*nSC));
title('BIC For Various $k$ and $\Sigma$ Choices','Interpreter','latex');
xlabel('$c$','Interpreter','Latex');
ylabel('BIC');
legend({'Diagonal-shared','Full-shared','Diagonal-unshared',...
    'Full-unshared'});

% figure;
% plot(k,aic(:,:,1),k,aic(:,:,2));
% title('AIC For Various $k$ and $\Sigma$ Choices','Interpreter','latex');
% xlabel('$c$','Interpreter','Latex');
% ylabel('AIC');
% legend({'Diagonal-shared','Full-shared','Diagonal-unshared',...
%     'Full-unshared'});

figure;
plot(k,bic(:,:,1),k,bic(:,:,2));
title('BIC For Various $k$ and $\Sigma$ Choices','Interpreter','latex');
xlabel('$c$','Interpreter','Latex');
ylabel('BIC');
legend({'Diagonal-shared','Full-shared','Diagonal-unshared',...
    'Full-unshared'});

%% differentiate the BIC
%dfbc(:,1) = [0; diff(bic(:,1,1))]
dfbc = [0; diff(bic(:,2,1))];

figure;
plot(dfbc,'k'); hold on;  title('Differentited BIC for full-shared Covariance');
dfbc = dfbc.*(1-(0:length(dfbc)-1).*0.05)';
hold on; plot(dfbc);
[bestBICdiffVal, bestBIC] = min(dfbc);
bistBICVal = bic(bestBIC,2,1);

% 
% 
% df2bc(:,1) = [0; diff(dfbc(:,1))]
% df2bc(:,2) = [0; diff(dfbc(:,2))]
% figure;
% plot(df2bc)

%%
% According to the AIC and BIC values, the best model ...
%%
% Cluster the training data using the best fitting model.  Plot the
% clustered data and the component ellipses.
gmBest = gm{bestBIC,1,1}; % find automatically
clusterX = smc_cluster(gmBest,X);
kGMM = gmBest.NumComponents;
d = 500;
x1 = linspace(min(X(:,1)) - 2,max(X(:,1)) + 2,d);
x2 = linspace(min(X(:,2)) - 2,max(X(:,2)) + 2,d);
[x1grid,x2grid] = meshgrid(x1,x2);
X0 = [x1grid(:) x2grid(:)];
mahalDist = mahal(gmBest,X0);
threshold = sqrt(chi2inv(0.99,1));

figure;
h1 = gscatter(X(:,1),X(:,2),clusterX);
hold on;
for j = 1:kGMM
    idx = mahalDist(:,j)<=threshold;
    Color = h1(j).Color*0.75 + -0.5*(h1(j).Color - 1);
    h2 = plot(X0(idx,1),X0(idx,2),'.','Color',Color,'MarkerSize',1);
    uistack(h2,'bottom');
end
h3 = plot(gmBest.mu(:,1),gmBest.mu(:,2),'kx','LineWidth',2,'MarkerSize',10); hold on;
plot(truePoints(:,1), truePoints(:,2),'w+','MarkerSize',15,'LineWidth',2)
title('Panning Parameter Distributions');
xlabel('Amplitude Angle Estimate [$^{\circ}$]','interpreter','latex');
ylabel('Delay Estimate [samples]');
legend(h1,'Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5','Cluster 6','Cluster 7','Cluster 8','Cluster 9','Location','NorthWest');
hold off;
%%
% This data set includes labels.  Determine how well |gmBest| clusters the
% data by comparing each prediction to the true labels.
% species = categorical(species);
% Y = zeros(n,1);
% Y(species == 'versicolor') = 1;
% Y(species == 'virginica') = 2;
% Y(species == 'setosa') = 3;
% 
% miscluster = Y ~= clusterX;
% clusterError = sum(miscluster)/n
%%
% The best fitting GMM groups 8% of the observations into the wrong
% cluster. 
%%
% |cluster| does not always preserve cluster order.  That is, if
% you cluster several fitted |gmdistribution| models, |cluster| might
% assign different cluster labels for similar components.
