function [muEstimatesInSegments] = smc_detect_active_sources(xFullband,fs,muEstimates,muDegSampl, CEstimates,alphaEstimates)
% check if these exists outside function first
if ~exist('maxNumSources') & ~exist('maxDelaySec')'
    maxNumSources=5; 
    maxDelaySec= 150e-6;
    maxDelaySamples = floor(maxDelaySec*fs);
    maxPanAngle=44;
   % trueNumSources =  randi(maxNumSources-1)+1;
end
% find all possible combinations of clusters
allCombos=[];
numClust=size(muEstimates,2);
for iii=1:numClust, allCombos{iii} = nchoosek([1:numClust],iii); end
%% compute likelihood of each component separately.
% filter
fc = 4e3; % cutoff freq.
[b,a] = butter(6,fc/(fs/2));
x = filter(b,a,xFullband);
% 
% % parameters for the fourier transform
%% estimate spatial distribution space
winDur=600e-3;
wlen     = floor(winDur*fs);
NMin = floor(wlen/4); KMax = 10; % only if optimal segmentation is applied
NFFT  = 2^nextpow2(NMin*KMax);
segCount=0;
NLOGL=[];
for nn=1:wlen:length(x)-wlen
    xWindowed=x(nn:nn+wlen,:);
    % maybe this should have changed threshold in the DFT.
    [X] = smc_estimate_panning_space_from_short_segments(xWindowed, ones(1000,1)*wlen, fs, NFFT, maxDelaySamples, maxPanAngle, fc);
    % try all combinations
    cnt=1;
    cmb=1;
    while cnt<=numClust
        combMat = allCombos{cnt};
        
        for cntcmb=1:size(combMat,1)
            GMM_MODEL = gmdistribution(muEstimates(:,[combMat(cntcmb,:)])',CEstimates(:,:,[combMat(cntcmb,:)]),alphaEstimates([combMat(cntcmb,:)]));%,'CovType','diagonal'); % generate a GMM
            [degree_of_membership,NLOGL(cmb)] = posterior(GMM_MODEL,X); % calculate the likelihood 
            combinations{cmb}=combMat(cntcmb,:);
            cmb=cmb+1;
        end
        cnt=cnt+1;
    end
    [minLL,b]=min(NLOGL);
    muInSegment = muDegSampl(:,combinations{b});
    [~,ndx]=sort(muInSegment(1,:));
    % estimate no sources when silence
    thr=0.0004;
    if rms(xWindowed)>thr
        muInSegment=muInSegment(:,ndx);
    else
        muInSegment=nan;
    end
    segCount=segCount+1;
    muEstimatesInSegments{segCount} = muInSegment;   
end
end