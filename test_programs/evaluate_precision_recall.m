% evaluate for duration 18 sec.
clear all; close all;
cd /home/jacob/panning_project/test_programs/
% GOD --> load test_MMDL_40iter_4April_dur_18sec.mat
%load test_MMDL_40iter_findfejl_27March_dur_18sec.mat
%load mats/sep2017/test_SQAM_diagonal_morenoisy_k25.mat
%load mats/sep2017/test_SQAM_diagonal_noisy_k25_250x12.mat
%load mats/sep2017/test_IOWA_diagonal_250samples_k25.mat
%load mats/sep2017/test_SQAM_diagonal_250samples_k35_non-contra.mat
%load mats/sep2017/test_IOWA_diagonal_250samples_k35_non-contra.mat

%%%%%%%%%%%%%%% ONLY For QDC method %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%load('mats/sep2017/test_IOWA_QDCdiagonal_250samples_k35_110iter.mat');
load('mats/sep2017/test_IOWA_QDCdiagonal_250samples_k35_200iter.mat');

saved.trueParams = trueParameters;

saved.X=trueParameters; 
for hh=1:size(muSelected,2)
    saved.muPruned{hh} = muSelected{hh}';
    saved.trueNumSource{hh} = size(trueParameters{hh},1)
    saved.kPruned{hh} = size(muSelected{hh},1)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[errorAngle, errorDelay,trueNumClusters,estimatedNumClusters,errorRateModelOrder,totalEstimatedParamsPruned, trueMinusEstimated] ...
    = evaluate_overfitting_and_pruning(saved);

thrForCorrect = 0.5; % both for angle(degrees) and samples(delay)
correctMask=(errorAngle<thrForCorrect) & (errorDelay<thrForCorrect);
correctAmplitudes = errorAngle(correctMask);
correctDelay = errorDelay(errorAngle<thrForCorrect);

relevantElements = sum(trueNumClusters)
%numCorrectParamEstimates = length(correctAmplitudes)
%parameterPercentage = numCorrectParamEstimates/relevantElements;

A_RMSE=sqrt(mean(correctAmplitudes.^2))
D_RMSE=sqrt(mean(correctDelay.^2))


% Precision Recall
truePositiveClusters  = correctAmplitudes;
truePositives = sum(correctMask);
falsePositives = estimatedNumClusters-truePositives;

falseNegatives = trueNumClusters;

precision =  truePositives/sum(estimatedNumClusters)
recall = truePositives/relevantElements