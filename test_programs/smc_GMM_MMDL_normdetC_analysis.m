function [mindl, bestk, bestmu, bestcov,safemu, safemuMean, safemuSelected, bestmuCovRatio,X2,bestpp] = smc_GMM_MMDL_normdetC_analysis(X, fs, trueNumClusters)

%% Clustering Using Gaussian Mixture Models
covType = 0;
regVal =  0.5e-4 % FOR old criteria 0.2e-3;
PlotFlag=2;
level=2;
bestk=[];

X2=[];
for iter=1:1
bestmu=[]; bestcov=[]; ndx=[]; ratioVari=[]; CratioSort=[]; Cratio=[]; bestpp=[]; detC=[];insideIndicator=[];
xAxisC=[]; xAxisRatio=[]; sortedxAxisC = [];tmp=[];matchIndicator=[]; stickyClusters=[]; sortndx=[]; sortbackndx=[];
[bestk(iter),bestpp,bestmu,bestcov,dl,countf,mindl,safemu, safemuMean, safemuSelected] = mixtures4_remove_outliers(X',2,25,regVal,1e-4,covType,PlotFlag); % regVal was 1e-4

    % choose from cov ratio
    for ii=1:bestk(iter)
       % OLD CRITERIA 
       % Cration(ii) = det(bestcov(:,:,ii));
       
       %% NEW CRITERIA
        [uu,ei,vv]=svd(bestcov(:,:,ii));
        a = sqrt(ei(1,1)*level*level);
        b = sqrt(ei(2,2)*level*level);
        % %% define distnce from y center cluster
        d = X'-bestmu(:,ii);

        % Determin theta rotation angle for the ellipse
        t = atan2(uu(2,:),uu(1,:));
        xAngle(ii) = t(1);
        XX = (d(1,:))*cos(xAngle(ii))+(d(2,:))*sin(xAngle(ii));
        YY = -d(1,:)*sin(xAngle(ii))+d(2,:)*cos(xAngle(ii)); 
        % area of ellipse
        detC(ii) = det(bestcov(:,:,ii));
        % projection of ellipse on x-axis
        xAxisC(ii) = abs(a*cos(xAngle(ii)))+abs(b*sin(xAngle(ii)));
        bestmu(:,ii);
        % locate points inside each elliptic covariance 
        insideIndicator(:,ii,iter) = (XX.^2/a^2+YY.^2/b^2<1); 
              
    end
    % remove every point of an embedding covariance
    % (a covariance that is containing another covariance)
    embeddingClusters = zeros(size(bestk(iter)));
    for  ii=1:bestk(iter)
        % locate the embedded points
        matchIndicator = sum(insideIndicator(:,ii,iter))==sum(insideIndicator(:,ii,iter)&insideIndicator(:,:,iter));
        if sum(matchIndicator)>1
%            fprintf('ii=%1.0f is with mean: \n',ii);
%            bestmu(:,ii)
            % remove index of ii
            matchIndicator(ii) = 0;
            % remove it from clusters it is embedded in
            insideIndicator(:,matchIndicator,iter) = insideIndicator(:,matchIndicator,iter)-insideIndicator(:,ii,iter);
            % remo
            embeddingClusters = embeddingClusters|matchIndicator;
        end
    end


    % kill every cluster that touches a smaller cluster
    [~,sortndx] = sort(detC);
    [~,sortbackndx] = sort(sortndx);    
    tmp = insideIndicator(:,sortndx,iter);
    stickyClusters = zeros(size(bestk(iter)));
    for ii=1:bestk(iter)-1
        % silent all the sticky points in large covariance
        matchIndicator = sum( tmp(:,ii) & [zeros(size(tmp,1),ii) tmp(:,ii+1:end)] )>1;
        matchIndicator(ii) = 0;
        stickyClusters = stickyClusters|matchIndicator;
    end    
    stickyClusters = stickyClusters(sortbackndx);
       
    indicator05 =  ones(size(bestk(iter)));
    collector05 = zeros(size(bestk(iter)));
    for ii=1:bestk(iter)-1
        tmp = 45*abs(bestmu(1,:)-bestmu(1,ii))<0.51
        %pause
        if sum(tmp)>1
            fprintf('clinch between:\n')
            bestmu(:,tmp==1)
            xAxisC(tmp)
            indicator05 = sum(insideIndicator(:,:,iter)==1)./xAxisC
            indicator05 = indicator05.*tmp
            indicator05(indicator05==max(indicator05)) = 0;
            indicator05 = indicator05~=0
            collector05 = indicator05|collector05
            %pause
        end
    end

    % set zeros in all sticky cluster points
    insideIndicator(:,:,iter) =insideIndicator(:,:,iter) .*(stickyClusters==0);
    
    % set zeros in all embedding cluster points
    insideIndicator(:,:,iter) =insideIndicator(:,:,iter) .*(embeddingClusters==0);
    
    % set zeros in all embedding cluster points
    insideIndicator(:,:,iter) =insideIndicator(:,:,iter) .*(collector05==0);
    
    
    
    % plot point assignments
    for ii=1:bestk(iter)
        X2=[X2; [X(insideIndicator(:,ii,iter)==1,1), X(insideIndicator(:,ii,iter)==1,2)]];         
        hold on;
        plot(X(insideIndicator(:,ii,iter)==1,1),X(insideIndicator(:,ii,iter)==1,2),'.');
        pointPercentage(ii) = (eps+sum(insideIndicator(:,ii,iter)))/(eps+sum(sum(insideIndicator(:,:,iter))));
        fprintf('[%5.1f,%5.1f],contains: %5.0f, detC: %10.2f, xAngle: %4.2f: xAxis: %5.2f p/xa: %5.0f, d p/xa: %7.1f \n',bestmu(1,ii)*45,bestmu(2,ii)*6,sum(insideIndicator(:,ii,iter)==1),detC(ii)*1000000,1-abs(1-abs((xAngle(ii)/(pi/2)))),xAxisC(ii)/sum(xAxisC),sum(insideIndicator(:,ii,iter)==1)/xAxisC(ii),eps+pointPercentage(ii)/(xAxisC(ii)*detC(ii)))
    end
    
    for  ii=1:bestk(iter)
        pointPercentage(ii) = (eps+sum(insideIndicator(:,ii,iter)))/(eps+sum(sum(insideIndicator(:,:,iter))));
        
        Cratio(ii) = eps+pointPercentage(ii)/(xAxisC(ii)*detC(ii));
        pa(ii)=sum(insideIndicator(:,ii,iter)==1)/xAxisC(ii);
        % only for plotting
        XForplot{ii,1} = [X(insideIndicator(:,ii,iter)==1,1) X(insideIndicator(:,ii,iter)==1,2) ]; 
    end


%    % 1) Sort Cratios and compare to lowest val. 
    [~, ndx] = sort(-Cratio);
    CratioSort=Cratio(ndx);
    CratioOverCratio1 = CratioSort/CratioSort(1);
    pointPercentage(ndx);

   
   while (CratioSort(end)/CratioSort(1) <1e-7 && CratioSort(end) < 1/2000 && bestk(iter) > 1) | (pa(end) < 350  && bestk(iter) > 1)% CratioSort(end)<1 && bestk(iter) > 1%  variSort(end)>20*variSort(1)
   CCCEND = CratioSort(end);
    CratioSort(end)/CratioSort(1);
       % 2) compare to the one above
       CratioSort = CratioSort(1:end-1);
       bestk(iter)=bestk(iter)-1;
       pa = pa(1:end-1);
   end
   
   bestmuIter{bestk(iter)} = bestmu(:,ndx(1:bestk(iter)));
   bestppIter{bestk(iter)} = bestpp(ndx(1:bestk(iter)));
   bestcovIter{bestk(iter)} = bestcov(:,:,ndx(1:bestk(iter)))

end

bestk = round(mean(bestk));
bestmuCovRatio = bestmuIter{:,bestk}'
bestpp = bestppIter{:,bestk}
bestcov = bestcovIter{:,bestk}



% remove same sign clusters (pan right is: Gain(+) --> Delay(-) )
signMask = sign(bestmuCovRatio(:,1))~=sign(bestmuCovRatio(:,2));      
bestmuCovRatio = bestmuCovRatio(signMask,:); 
bestpp = bestpp(signMask);
% remove too close clusters
maxPanAngle = 45;
maxDelay = 150e-6;
bestmuCovRatio = bestmuCovRatio.*[maxPanAngle maxDelay*fs];
[val,ndx] = unique(round(bestmuCovRatio*2)/2,'rows');
bestmuCovRatio = bestmuCovRatio(ndx,:)

pause% old stuff
bestmu = [maxPanAngle maxDelay*fs].*bestmu';
[val,ndx] = unique(round(bestmu*2)/2,'rows');
