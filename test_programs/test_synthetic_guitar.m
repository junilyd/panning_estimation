clear all;
close all;
dur = [300e-3 600e-3 900e-3 1200e-3 1500e-3 1800e-3 3000e-3];
for iter=1:30
cd /home/jacob/Documents/MATLAB/code/test_programs/RMSE;
fs=44100;
L=30;
SNR=80;
fig=0;

duration = 60;

numSources = randi(4)+1;
sec30=duration*fs;

sig=[];
for nn=1:numSources
    x=[];
    while length(x) < sec30+3*fs
        if rand>0.7
            x = [x; zeros(randi(6),1);];
        else
            x = [x; smc_synth(fs,dur(randi(7)),L,SNR,fig);];
        end
    end
    sig(:,nn) = x(1:sec30);
end


cd /home/jacob/panning_project/test_programs/

    maxDelaySec= 150e-6;
    maxDelaySamples = floor(maxDelaySec*fs);

    [theta, delaySec, true(iter).Points] = smc_random_panning_parameters(numSources,fs, maxDelaySec);

    [x] = smc_apply_panning_to_synthetic(sig,theta, delaySec, numSources, fs);
    %soundsc(x,fs)
    % parameters for the fourier transform
    %% Segmentation Part
    wlen     = floor(600e-3*fs);
    NMin = floor(wlen/2);
    w = hann(wlen);

    %%   Apply optimal segmentation in dft-domain
    %       - set min and max segment sizes
    KMax = 12;
    NMax = NMin * KMax;
    % resize x to be multiple of Nmin samples.
    M = floor(size(x,1)/NMin) ;
    x=x(1:M*NMin,:);
    NFFT  = 2^nextpow2(NMin*KMax);

    k_opt=[];
    numClusters=[];
    mu1=[];
    mu2=[];
    m=1;
    while (m<=M)
        cost=[];
        J=[];
        K = min(m, KMax);
        for k=1:K
            N  = ((m-k)*NMin+1:m*NMin);
            N2 = (1:(m-k)*NMin);
            [J, numClusters(m)] = smc_GMM_MMDL_dft( x(N, 1), x(N, 2), fs, NFFT,maxDelaySamples);
            if (m>k)
                cost(k) = J + smc_GMM_MMDL_dft( x(N2, 1), x(N2, 2), fs, NFFT,maxDelaySamples); 
            else
                cost(k) = J;
            end
        end
        [~, k_opt(m)] = max(cost);
        m=m+1;
    end
    m = M; % is defined above the loop.
    bb=1;
    while (m > 0)
        numBlocksInSegment(bb) = k_opt(m);
        numClustersInSegment(bb) = numClusters(m);
        m=m-k_opt(m);
        bb=bb+1;
    end
    numBlocksInSegment = numBlocksInSegment(end:-1:1);
    numClustersInSegment = numClustersInSegment(end:-1:1);

    %% Evaluate the given optimal segments 
    [X] = smc_estimate_panning_space_from_optimal_segments(x, numBlocksInSegment*NMin, fs, true(iter).Points, NFFT, maxDelaySamples);
    
    savedX{iter,1} = X;
    savedx{iter,1} = x;
    %savedf0{iter,1} = f0;
    savedNumBlocksInSegment{iter,1} = numBlocksInSegment;
    
    %[~, bestk(iter),~,~,finalMu(iter).MMDL,safemu(iter).first,safemu(iter).Mean, safemu(iter).Selected, safemu(iter).covRatio] = smc_GMM_MMDL_normdetC(X, fs, size(true(iter).Points,1));
    [~, bestk(iter),~,~,~,~,~, ~, finalMu(iter).MMDL, ~,~,~] = smc_GMM_MMDL_normdetC(X, fs,numSources);
       %% plot segementations
 
    % Short time fourier transform
    wlen     = floor(600e-3*fs);
    %wlen=wlen/2
    plotNMin = floor(wlen/2);
    NFFT  = 2^nextpow2(wlen*2);%2^14;
    w = hann(wlen/4);
    Xl = smc_stft_complex(x(:,1), w, plotNMin, NFFT);
    Xr = smc_stft_complex(x(:,2), w, plotNMin, NFFT);
    
    plotSegments = 1;
    if plotSegments
         figure;
        [figureNumber,tAXis] = smc_plot(x,fs);
        tmp=0;
%         for ii=1:length(numBlocksInSegment) 
%             subplot(211); hold on; 
%                 plot([tmp+numBlocksInSegment(ii)*NMin/fs tmp+numBlocksInSegment(ii)*NMin/fs],[1.5*min(x(:,1)) 1.5*max(x(:,1))], 'k')
%                 tmp = tmp+numBlocksInSegment(ii)*NMin/fs;
%         end
        figure;
            reduce_plot(imagesc([1:(size(Xr,2))]*plotNMin/fs-plotNMin/fs/2, linspace(0,1,(NFFT/2-1))*fs/2, 20*log10(abs(Xr(1:NFFT/2-1,:)))));
            axis xy;
            caxis(max(caxis)+[-60 0]);
            xlabel('Time [sec]'); ylabel('Frequency [Hz]');
            %title(sprintf('Spectrogram of track'));
            ylim([0 fs/14]);
            hold on; reduce_plot(tAXis,[x(:,2) x(:,1)]*500+2500)

        tmp=0; % to fit the above
        for ii=1:length(numBlocksInSegment) 
            hold on; plot([tmp+numBlocksInSegment(ii)*plotNMin/fs tmp+numBlocksInSegment(ii)*plotNMin/fs],[0 fs/2],'w')
            tmp=tmp+numBlocksInSegment(ii)*plotNMin/fs;
        end
        yyaxis right;
        ylabel('Amplitude');
        set(gca, 'ytick',[0.62 0.8 0.98]-0.02);
        set(gca, 'YTickLabel',[-1 0 1],'YColor','black')
        
    end
    true(iter).Points
    finalMu(iter).MMDL 
    %safemu(iter).covRatio
    %%
    close all;
end