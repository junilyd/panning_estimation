clear all;% close all

% locate error in delay direction
maxNumSources=6; 
fs = 44100;
wlen     = floor(200e-3*fs);
NMin = floor(wlen/4);
w = blackman(wlen);
MaxDelaySamples=6;

numSources = randi(maxNumSources-1)+1;
[theta, delaySec, true.Points] = smc_random_panning_parameters(numSources,fs, 140e-6);

startSec = 1;
durationSec = 18;

%theta=(    [2.5 -2.5]+45)/180*pi
%delaySec = [150e-6 -150e-6]/1.4*100;

x = smc_apply_panning_to_sqam(theta, delaySec, numSources, durationSec, fs);

soundsc(x(1:2*fs,:),fs)

    %   Apply optimal segmentation in dft-domain
%       - set min and max segment sizes
KMax = 8;
NMax = NMin * KMax;
% resize x to be multiple of Nmin samples.
M = floor(size(x,1)/NMin) ;
x=x(1:M*NMin,:);
NFFT  = 2^nextpow2(NMin*KMax)/2;


X = smc_estimate_panning_space_from_optimal_segments(x,ones(1000,1)*wlen,fs,true.Points,NFFT, MaxDelaySamples);

figure(1); scatter(X(:,1), X(:,2))

true.Points

[mindl, bestk, bestmu, bestcov,finalMu,safemu, safemuMean, safemuSelected, bestmuCovRatio, meanthr,X2] = smc_GMM_MMDL_normdetC(X, fs,size(true.Points,1));

bestmuCovRatio