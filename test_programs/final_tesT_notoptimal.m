clear all;

cd /home/jacob/panning_project/test_programs/
%load test_covration_100iter_notoptimal16april.mat
load mats/final_experiment_not_optimal.mat;
for dd =51:51 % 51:100
    x = savedx{dd};
    [X] = smc_estimate_panning_space_from_optimal_segments(savedx{dd}, ones(1000,1)*wlen, fs, true(dd).Points, NFFT, maxDelaySamples);
    true(dd).Points
    [~, ~, ~, ~,~,~, ~, ~, safemu(dd).covRatio, tmp,X2] = smc_GMM_MMDL_normdetC(X, fs, size(true(dd).Points,1));
    true(dd).Points
    safemu(dd).covRatio    
end

 [errorAngle, errorDelay,trueNumClusters,NumClustersMMDL,errorRateModelOrder]  = evaluate_iterations_covariance_function(safemu, true, dd, 5);

sc = 100/86;
% due to error in iter=58
NumClustersMMDL=NumClustersMMDL
correctAmplitudes = errorAngle(errorAngle<0.6);
A_RMSE=sqrt(mean(correctAmplitudes.^2))
numCorrectParameters = length(correctAmplitudes) * sc
totalParameterEst = sum(trueNumClusters) * sc
parameterPercentage = numCorrectParameters/totalParameterEst
errorRateModelOrder

correctDelay = errorDelay(errorAngle<0.6);
D_RMSE=sqrt(mean(correctDelay.^2))