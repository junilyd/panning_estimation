% Genralized Likelihood Ratio Test
% choose metric from list. (methodNdx)
m=methodNdx(2)+2;
% compute Likelihood Ratio
% plot T(k), k
clear T
figure;
for ii=1:100%iter-1
   clear det tmp;
   %tmp=ones(35,size(muWeightedSaved{ii},1));
   tmp(1:size(muWeightedSaved{ii},1),1:size(muWeightedSaved{ii},2)) = muWeightedSaved{ii};
   
   
   L0_detC=ones(1,35); L1_detC=ones(1,35);
   L0_detC(1:size(tmp,1)) = tmp(:,m);
   for k = 1:35%size(muWeightedSaved{ii},1) % maybe choose common min. length
      if k>size(tmp,1)-1
         L1_detC(k)=0;
      else
       L1_detC(k) = sum(tmp(k+1:end,m));
      end
       T(k,ii) = L1_detC(k)/L0_detC(k)/k;%(size(muWeightedSaved{ii},1));
   end
   k;
   plot(1:k,T(:,ii),'-o'); hold on; plot(1:k,T(:,ii),'kx')
end
ylim([0 3000])
grid minor;
mean(T,2)
