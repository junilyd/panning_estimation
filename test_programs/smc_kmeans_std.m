function [stdOutput, M, mu1, mu2] = smc_kmeans_std(y, bufferLen, fs)

    D_vec=[];
    G_vec=[];
    F=2^12;
    M=(5+1);
    
    %N=bufferLen; %round(0.08*fs);
    win = hann(bufferLen);
    pos=1:bufferLen;
    while pos(end)<length(y),%len,
        xl=y(pos,1);
        xr=y(pos,2);

        Xl=(fft(win.*xl,F));
        Xr=(fft(win.*xr,F));  
        
        thrl = min(40*mean(abs(Xl)),0.6*max(abs(Xl)) );
        thrr = min(40*mean(abs(Xr)),0.6*max(abs(Xr)) );
        fl=find(abs(Xl(1:F/2-1))>thrl); % thr
        fr=find(abs(Xr(1:F/2-1))>thrl);  % changed from thrr

        G=acot(abs((Xr(fl)./Xl(fl))));  
        G_vec=[G_vec; G];

        D = -imag(log(Xr(fl)./Xl(fl)))./(2*pi*(fl-1)/F); 
        D_vec=[D_vec; D];

        pos=pos+bufferLen;
    end

    % de-range the data
    maxPanAngle = 45;
    maxDelay = 200e-6;
    GDMask = (abs(D_vec)<maxDelay*fs) & (G_vec<maxPanAngle*pi/180*2);
    if sum(GDMask)>M*3
        % scale the gain vector to degrees.
        GD(:,1) = G_vec(GDMask)./pi*180-45;
        GD(:,2) = D_vec(GDMask);

        % K-means Clustering
        [ndx,mu1] = kmeans(GD,M,'Distance','cityblock','Replicates',12);

        % delete every sample over std*factor
        STDFACTOR = 0.4;
        PERCENTPROXIMITY = 0.02;
        CENTERPERCENTAGE = 0.2;

        criteria = 1;
        while criteria ~=0
            %figure(ff);clf;ff=ff+1;
            for cc=1:M
                clusterG = GD(ndx==cc,1); clusterD = GD(ndx==cc,2);
                stdClusterG(cc) = std(clusterG); stdClusterD(cc) = std(clusterD);
                % mask out everything STDFACTOR away from mean in x and y direction
                stdMask{cc} = (abs(clusterG-mu1(cc,1)) > STDFACTOR*stdClusterG(cc)) | (abs(clusterD-mu1(cc,2)) > STDFACTOR*stdClusterD(cc));   

                % mask out everything STDFACTOR below from mean in x and y direction
                stdMaskInside{cc} = (abs(clusterG-mu1(cc,1)) < STDFACTOR*stdClusterG(cc)) & (abs(clusterD-mu1(cc,2)) < STDFACTOR*stdClusterD(cc));   
                Mask2STD{cc} = (abs(clusterG-mu1(cc,1)) < 2*stdClusterG(cc)) & (abs(clusterD-mu1(cc,2)) < 2*stdClusterD(cc));   

                % comput how many percentage of samples are close to the mean
                % greatest distance from farthest sample to mean
                percentDistanceG = PERCENTPROXIMITY*abs(max(clusterG-mu1(cc,1)));
                percentDistanceD = PERCENTPROXIMITY*abs(max(clusterD-mu1(cc,2)));

                % count samples below one std
                stdMuDistanceG = abs(0.1*stdClusterG(cc)-mu1(cc,1));
                stdMuDistanceD = abs(0.1*stdClusterD(cc)-mu1(cc,2));

                meanMask{cc} = (abs(clusterG-mu1(cc,1)) < percentDistanceG) | (abs(clusterD-mu1(cc,2)) < percentDistanceD);
                stdMuMask{cc} = (abs(clusterG-mu1(cc,1)) < stdMuDistanceG) | (abs(clusterD-mu1(cc,2)) < stdMuDistanceD);

                %meanDensity(cc) = sum( meanMask{cc} )/length(clusterG);
                centerDensity(cc) = sum( stdMaskInside{cc} )/length(stdMaskInside{cc});
            end 
            % compute number of classes based on center proximity
            sortedMeanDensity = centerDensity(centerDensity>CENTERPERCENTAGE);
            if min(centerDensity) < mean(sortedMeanDensity)/2
                M=M-1;
                criteria = 1;
                clear centerDensity stdClusterD stdClusterG;
                [ndx,mu1] = kmeans(GD,M,'Distance','cityblock','Replicates',10);
            else 
                criteria = 0;
            end
        end

        GD21=[];
        GD22=[];
        for cc=1:M
            clusterG = GD(ndx==cc,1);
            clusterD = GD(ndx==cc,2);      
            tmpG = clusterG(Mask2STD{cc});
            tmpD = clusterD(Mask2STD{cc});
            GD21 = [GD21; tmpG];
            GD22 = [GD22; tmpD];            
        end
        GD2 = [GD21 GD22];
        % try the rest with GD2
        if length(GD2)>M
        [ndx,mu2] = kmeans(GD2,M,'Distance','cityblock');
        GD=GD2;
        end
        %figure(ff);clf;ff=ff+1;
            for cc=1:M
                %plot(GD(ndx==cc,1),GD(ndx==cc,2),C{cc},'MarkerSize',12); hold on;
                stdClusterD2(cc) = std(GD(ndx==cc,1)); 
                stdClusterG2(cc) = std(GD(ndx==cc,2)); 
            end  
        stdOutput =  mean((stdClusterD2+stdClusterG2)/2);
        %     plot(mu2(:,1),mu2(:,2),'kx','MarkerSize',15,'LineWidth',3);
        %     legend('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster5', ...
        %            'Cluster 6','Cluster 7','Cluster 8','Cluster 9','Cluster10', 'Centroids','Location','NW');
        %     title 'K-Means Cluster Assignments and Centroids'
        %     hold off; 
    else 
        stdOutput = nan;
        M=0;
        mu1=0;
        mu2=0;
    end
end