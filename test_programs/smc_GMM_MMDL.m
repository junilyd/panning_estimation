function [mindl, bestk, bestmu, bestcov,finalMu,safemu, safemuMean, safemuSelected, bestmuCovRatio, meanthr,xc] = smc_GMM_MMDL(X, fs, trueNumClusters)

%% Clustering Using Gaussian Mixture Models
covType = 0;
regVal =  0.5e-4 % FOR old criteria 0.2e-3;
PlotFlag=2;
level=2;
bestk=[]; 
for iter=1:12
bestmu=[]; bestcov=[]; ndx=[]; ratioVari=[]; CratioSort=[]; Cratio=[]; bestpp=[]; detC=[];insideIndicator=[];
[bestk(iter),bestpp,bestmu,bestcov,dl,countf,mindl,safemu, safemuMean, safemuSelected] = mixtures4_remove_outliers(X',2,25,regVal,1e-4,covType,PlotFlag); % regVal was 1e-4

    % choose from cov ratio
    for ii=1:bestk(iter)
       % OLD CRITERIA 
       % Cration(ii) = det(bestcov(:,:,ii));
       
       %% NEW CRITERIA
        [uu,ei,vv]=svd(bestcov(:,:,ii));
        a = sqrt(ei(1,1)*level*level)
        b = sqrt(ei(2,2)*level*level)
        % %% define distnce from y center cluster
        d = X'-bestmu(:,ii);
        % % Determin theta angle from azimuth and rotation angle for the ellipse

        % vEig=eig(cov);
        t = atan2(uu(2,:),uu(1,:));
        t = t(1)
        XX = (d(1,:))*cos(t)+(d(2,:))*sin(t);
        YY = -d(1,:)*sin(t)+d(2,:)*cos(t); 
        
        % projection of sllipse on x-axis
        xAxisC(ii) = abs(a*cos(t)+b*sin(t));
        bestmu(:,ii);
        % find number of points inside elliptic covariance 
        insideIndicator(:,ii) = (XX.^2/a^2+YY.^2/b^2<1); 
        sumOfInsideIndicator(ii) = sum(insideIndicator(:,ii));
        detC(ii) = det(bestcov(:,:,ii));
        Cratio(ii) = detC(ii)/sumOfInsideIndicator(ii);
       
        % only for plotting
       XForplot{ii,1} = [X(insideIndicator(:,ii)==1,1) X(insideIndicator(:,ii)==1,2) ];        
    end
            
        
%    % 1) Sort Cratios and compare to lowest val. 
    [CratioSort, ndx] = sort(Cratio);
    CratioOverCratio1 = CratioSort/CratioSort(1);
%     
    xAxisC = sort(xAxisC);
    xAxisRatio= xAxisC/xAxisC(1)
%     % minimize each Cratio %%%%%%%%%%%%%%%%
%     % sort covariances by Cratios size
%     sortedDetC = detC(ndx);
%     sortedC = bestcov(:,:,ndx);
%     sortedBestmu = bestmu(:,ndx);
%     sortedSumOfInsideIndicator = sumOfInsideIndicator(ndx);
%     sortedInsideIndicator = insideIndicator(:,ndx);
%     Xtest = X;%(sortedInsideIndicator(:,1)==0,:);
%     %insideIndicator =  insideIndicator(sortedInsideIndicator(:,1)==0,:)
%     % minimize Cratio from second to last
%      scaleFac=1;
%      for m=2:bestk(iter)
%          sortedSumOfInsideIndicator(m)*scaleFac
%          while  sortedDetC(m) > sortedDetC(1) && sortedSumOfInsideIndicator(m)*scaleFac
%             % shrink C
%             sortedC(:,:,m) = sortedC(:,:,m)*scaleFac;
%             sortedDetC(m) = det(sortedC(:,:,m));
%             
%             % compute new Cratio here
%             [uu,ei,~]=svd(sortedC(:,:,m));
%             a = sqrt(ei(1,1)*level*level);
%             b = sqrt(ei(2,2)*level*level);
%             d = Xtest'-sortedBestmu(:,m);
%             t = atan2(uu(2,:),uu(1,:));
%             t = t(1);
%             XX = (d(1,:))*cos(t)+(d(2,:))*sin(t);
%             YY = -d(1,:)*sin(t)+d(2,:)*cos(t); 
%             insideIndicator(:,m) = (XX.^2/a^2+YY.^2/b^2<1);
%             tmp_detC = det(sortedC(:,:,m));
%             newCratio = tmp_detC/sum(insideIndicator(:,m));
% 
%             if newCratio<CratioSort(m)
%                 CratioSort(m) = newCratio;
%                 theta = [0:0.01:2*pi];
%                 xx = a*cos(theta);
%                 yy = b*sin(theta);
%                 cord = [xx' yy']';
%                 cord = uu*cord;
% 
%                 figure(345); 
%                 plot((cord(1,:)+sortedBestmu(1,m)), (cord(2,:)+sortedBestmu(2,m)),'k','LineWidth',2); hold on;
%             end
%             scaleFac=scaleFac-0.02;
%          end
%      end
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        

   
   %%%%%%%%%% plot biggest first   
%    for ii=1:bestk(iter)
%        Xplot = XForplot{ndx(end+1-ii)};
%        figure(345);
%        scatter(Xplot(:,1), Xplot(:,2),'.'); hold on ;
%    end
%    hold off;
   %%%%%%%%%
   %CratioSort = sort(CratioSort);
   for ii=1:bestk(iter)-1
       ratioVari(ii+1) = CratioSort(ii+1)/mean(CratioSort(1:ii));
   end
     

   
   
   
   % collect data for discrimination
   trueNumClusters
   CratioOverCratio1
   ratioVari

if trueNumClusters < length(CratioOverCratio1)
    trueval(iter) = CratioOverCratio1(trueNumClusters);
    truevalp1(iter) = CratioOverCratio1(trueNumClusters+1);
elseif trueNumClusters == length(CratioOverCratio1)
     trueval(iter) = CratioOverCratio1(trueNumClusters);
     truevalp1(iter) = 0;
end
trueval;
truevalp1;


bestk(iter)
   while CratioSort(end)>95*CratioSort(1) && bestk(iter) > 1%  variSort(end)>20*variSort(1)
       % 2) compare to the one above
       CratioSort = CratioSort(1:end-1);
       bestk(iter)=bestk(iter)-1;
   end
   
   bestmuIter{bestk(iter)} = bestmu(:,ndx(1:bestk(iter)));
   bestppIter{bestk(iter)} = bestpp(ndx(1:bestk(iter)));

end

bestk = round(mean(bestk));
bestmuCovRatio = bestmuIter{:,bestk}'
% bestpp = bestppIter{bestk};



% remove same sign clusters (pan right is: Gain(+) --> Delay(-) )
signMask = sign(bestmuCovRatio(:,1))~=sign(bestmuCovRatio(:,2));      
bestmuCovRatio = bestmuCovRatio(signMask,:); 
bestpp = bestpp(signMask);
% remove too close clusters
maxPanAngle = 45;
maxDelay = 150e-6;
bestmuCovRatio = bestmuCovRatio.*[maxPanAngle maxDelay*fs];
[val,ndx] = unique(round(bestmuCovRatio*2)/2,'rows');
bestmuCovRatio = bestmuCovRatio(ndx,:)
       
% debugging / training
trueval = trueval(trueval~=0)
truevalp1 = truevalp1(truevalp1~=0)

meanthr = [mean(trueval) mean(truevalp1) max(trueval) min(truevalp1) median(trueval) median(truevalp1) ] ;% mean(truevalp1./trueval) 

if (length(trueval) == length(truevalp1) )
    figure; kde(trueval);
    hold on; kde(truevalp1)
end
% script_plot_gaussians(trueval,truevalp1);
%script_plot_pdf(trueval,truevalp1)
%script_plot_roc(trueval,truevalp1)

% old stuff
bestmu = [maxPanAngle maxDelay*fs].*bestmu';
[val,ndx] = unique(round(bestmu*2)/2,'rows');
finalMu = bestmu(ndx,:);



% figure; scatterhist(safemu(:,1),safemu(:,2),400); title('mu clusters after MMDL w. removed outliers')
% 
% figure; scatterhist(safemuMean(:,1),safemuMean(:,2),400); title('mu after further smoothing by low proximity');
% 
% figure; scatterhist(safemuSelected(:,1),safemuSelected(:,2),400); title('selected directly in MMDL and low covariances');