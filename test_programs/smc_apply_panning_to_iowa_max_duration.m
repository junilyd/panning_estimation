function [x,smplNdx] = smc_apply_panning_to_iowa_max_duration(theta, delaySec, numSources, fs);
sti = '~/iowa/min60sec_files_from_IOWA/';

p = pwd;
cd(sti);
files=dir;
smplNdx = randsample(length(files)-2,numSources)+2;

    for m=1:numSources,
     %x    strcat(sti,sprintf('%1.0f',smplNdx(m)),'.flac')
    %[source{m},fs]=audioread(files{m}, [startSec*fs (startSec+durationSec)*fs]);
     [tmp,fs]=audioread(strcat( sti , sprintf(files(smplNdx(m)).name)));%, [startSec*fs (startSec+durationSec)*fs]);
     source{m} = tmp(:,1);
     len(m)=length(source{m});
    end
    len=min(len);
    x=zeros(len,2);
    
    delay = floor(abs(delaySec*fs));

    for m=1:numSources
        source{m}=source{m}(:,1)/max(abs( source{m}(:,1) ));    
        source{m}=source{m}(1:len);
        source{m}=source{m}/max(abs(source{m}));%/var(source{m});    
    end

    for m=1:numSources
      g(1,m)=sin(theta(m));
      g(2,m)=cos(theta(m));
      if delaySec(m) > 0 
        x(:,1)=x(:,1)+g(1,m)*source{m};
        x(:,2)=x(:,2)+g(2,m)*[zeros(delay(m),1); source{m}(1:end-delay(m))];
      else
        x(:,2)=x(:,2)+g(2,m)*source{m};
        x(:,1)=x(:,1)+g(1,m)*[zeros(delay(m),1); source{m}(1:end-delay(m))];
      end
    end
    x=x./max(max(abs(x)));

cd(p);
end