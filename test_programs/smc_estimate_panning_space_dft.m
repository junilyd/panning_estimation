function [X, out] = smc_estimate_panning_space_dft(y, numSamplesInSegment, fs, truePoints, NFFT, MaxDelaySamples)
%                                                              (x, numBlocksInSegment*wlen, fs, true(iter).Points, NFFT)

    D_vec=[];
    G_vec=[];
    
    bb=1;
    win = blackman(numSamplesInSegment(bb));
    pos=1:numSamplesInSegment(bb);
    size(y);
    size(win);
    pos(end);
    while (pos(end)<=length(y) && length(numSamplesInSegment)>=bb)
        xl=y(pos,1);
        xr=y(pos,2);
        

        Xl=(fft(blackmanharris(length(xl)).*xl,NFFT));
        Xr=(fft(blackmanharris(length(xr)).*xr,NFFT));  
        
        pwl = abs(fft(blackmanharris(length(xl)).*smc_prewhiten(xl),NFFT));
        pwr = abs(fft(blackmanharris(length(xr)).*smc_prewhiten(xr),NFFT));

        wdpiLimit = round(NFFT/2/MaxDelaySamples/2)-1;
        
        Xl(wdpiLimit:end) = [];
        Xr(wdpiLimit:end) = [];
        pwl(wdpiLimit:end) = [];
        pwr(wdpiLimit:end) = [];

        minBin=2;
        Xl(1:minBin-1) = [];
        Xr(1:minBin-1) = [];
        pwl(1:minBin-1) = [];         
        pwr(1:minBin-1) = [];         
        
        oldthrl = min(40*mean(abs(Xl)),0.5*max(abs(Xl)) );
        
%         absXl = abs(Xl);
%         absXr = abs(Xr);
%         O = absXl.*absXr;
        thrl = mean(pwl.*pwr);
        %fl=find(Xl>thrl);
%         diffFunction = -1*[0; diff([0;diff(abs(Xr-Xl))])];
%         meandf = mean(diffFunction);
%         likelidf= abs([0;diff(absXl)]-[0;diff(absXr)])*NFFT;
%         meanlidf = mean(likelidf); 
        %thrl = mean(O)
        fl=find(pwl.*pwr>thrl);% & likelidf>meanlidf);
        while length(fl) > 250 %NFFT/2/MaxDelaySamples/2/20, 
            thrl = thrl + thrl*0.08;
            fl=find(pwl.*pwr>thrl);% & likelidf>meanlidf);
        end
        
        
        
    fAxis = fs/NFFT*(minBin:round(wdpiLimit)-1)';
        ratio = Xr(fl)./Xl(fl);
        
        G=acot(abs(ratio));  
        G_vec=[G_vec; G];
        D = -imag(log(ratio))./(2*pi*(fl-1)/NFFT); 
        D_vec=[D_vec; D];
       
        bb=bb+1;
        pos=(pos(end)+1:pos(end)+numSamplesInSegment(bb));
    end
    out = [(D_vec) G_vec];
    % de-range the data
    maxPanAngle = 45;
    maxDelay = 150e-6;
    GDMask = (abs(D_vec)<maxDelay*fs) & (G_vec<maxPanAngle*pi/180*2);
    % scale the gain vector to degrees.
    GD(:,1) = (G_vec(GDMask)./pi*180-45)/maxPanAngle;
    GD(:,2) = (D_vec(GDMask))/(maxDelay*fs);
    X=GD;
    

    %X = smc_reduce_data_by_gmm_em(X);
    %X = smc_reduce_data_by_gmm_em(X);
    %X = smc_reduce_data_by_gmm_em(X);
end