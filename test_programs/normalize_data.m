%normalize_data.m
close all;
fs=44100;
cd /home/jacob/panning_project/test_programs/;
load panning_data2;

zset = panning_data(:,1);
xset = panning_data(:,2);


figure(1);
scatter(zset,xset)
[ndx,mu1] = kmeans(X,7,'Distance','cityblock','Replicates',12);
hold on;
plot(mu1(:,1).*maxPanAngle,mu1(:,2).*maxDelay,'k+','linewidth',2);


% max expectations
maxPanAngle = 45;
maxDelay = 200e-6*fs;

zset = zset./maxPanAngle;
xset = xset./(maxDelay);

figure(2);
scatter(zset.*maxPanAngle,xset.*maxDelay);

X=[zset,xset];

[ndx,mu1] = kmeans(X,7,'Distance','cityblock','Replicates',12);
hold on;
plot(mu1(:,1).*maxPanAngle,mu1(:,2).*maxDelay,'k+','linewidth',2);
