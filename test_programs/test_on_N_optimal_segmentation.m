
% dur = [300e-3 600e-3 900e-3 1200e-3 1500e-3 1800e-3 3000e-3];
% cd /home/jacob/panning_project/test_programs/
% load mats/synthetic_guitar_clustering_and_pruning.mat;
%startIter = size(savedx,1)-1;
%numSources = randi(4)+1;
%cd /home/jacob/Documents/MATLAB/code/test_programs/RMSE;

clear all;
close all;%
cd /home/jacob/panning_project/test_programs/
load mats/test_on_N/N_results_optimal_maximize.mat;
load mats/test_on_N/savedx_2min.mat;
finalMu=results_optimal.finalMu;


iterationNumber=1; % representing num. of durations
durations = [2 4 6 8 10 15 20];%[60 50 40 30 20 10 8 6 4 2];
numIterations = 20; % representing num. of files to test.

for duration = durations
    load mats/test_on_N/savedx_2min.mat;

    fs=44100;
    durSamples=duration*fs;

    % clear some memory
    for iter=1:31+numIterations
        tmp = savedx{iter};
        savedx{iter} = tmp(1:durSamples,:);
    end

    for iter=31:31+numIterations
     fs=44100;

    tmp = savedx{iter};
    x = tmp(1:durSamples,:);

    cd /home/jacob/panning_project/test_programs/

        maxDelaySec= 150e-6;
        maxDelaySamples = floor(maxDelaySec*fs);

        % parameters for the fourier transform
        %% Segmentation Part
        wlen     = floor(200e-3*fs);
        NMin = floor(wlen/2);
        w = hann(wlen);

        %%   Apply optimal segmentation in dft-domain
        %       - set min and max segment sizes
        KMax = 20;
        NMax = NMin * KMax;
        % resize x to be multiple of Nmin samples.
        M = floor(size(x,1)/NMin) ;
        x=x(1:M*NMin,:);
        NFFT  = 2^nextpow2(NMin*KMax);

        k_opt=[];
        numClusters=[];
        mu1=[];
        mu2=[];
        m=1;
        while (m<=M)
            cost=[];
            J=[];
            K = min(m, KMax);
            for k=1:K
                N  = ((m-k)*NMin+1:m*NMin);
                N2 = (1:(m-k)*NMin);
                [J, numClusters(m)] = smc_GMM_MMDL_dft( x(N, 1), x(N, 2), fs, NFFT,maxDelaySamples);
                if (m>k)
                    cost(k) = J + smc_GMM_MMDL_dft( x(N2, 1), x(N2, 2), fs, NFFT,maxDelaySamples); 
                else
                    cost(k) = J;
                end
            end
            [~, k_opt(m)] = max(cost);
            m=m+1;
        end
        m = M; % is defined above the loop.
        bb=1;
        while (m > 0)
            numBlocksInSegment(bb) = k_opt(m);
            numClustersInSegment(bb) = numClusters(m);
            m=m-k_opt(m);
            bb=bb+1;
        end
        numBlocksInSegment = numBlocksInSegment(end:-1:1);
        numClustersInSegment = numClustersInSegment(end:-1:1);

        %% Evaluate the given optimal segments 
        [X] = smc_estimate_panning_space_from_optimal_segments(x, numBlocksInSegment*NMin, fs, true(iter).Points, NFFT, maxDelaySamples);

        savedX{iter,1} = X;
        savedNumBlocksInSegment{iter,1} = numBlocksInSegment;

        [~, bestk(iter),~,~,~,~,~, ~, finalMu(iter).MMDL, ~,~,~] = smc_GMM_MMDL_normdetC(X, fs,size(true(iter).Points,1));
        
        %% plot segementations
        % Short time fourier transform
        %wlen     = floor(600e-3*fs);
        plotNMin = floor(wlen/2);
        NFFT  = 2^nextpow2(wlen*2);%2^14;
        w = hann(wlen/4);
        Xl = smc_stft_complex(x(:,1), w, plotNMin, NFFT);
        Xr = smc_stft_complex(x(:,2), w, plotNMin, NFFT);

        plotSegments = 0;
        if plotSegments
             figure;
            [figureNumber,tAXis] = smc_plot(x,fs);
            tmp=0;
            figure;
                reduce_plot(imagesc([1:(size(Xr,2))]*plotNMin/fs-plotNMin/fs/2, linspace(0,1,(NFFT/2-1))*fs/2, 20*log10(abs(Xr(1:NFFT/2-1,:)))));
                axis xy;
                caxis(max(caxis)+[-60 0]);
                xlabel('Time [sec]'); ylabel('Frequency [Hz]');
                %title(sprintf('Spectrogram of track'));
                ylim([0 fs/14]);
                hold on; reduce_plot(tAXis,[x(:,2) x(:,1)]*500+2500)

            tmp=0; % to fit the above
            for ii=1:length(numBlocksInSegment) 
                hold on; plot([tmp+numBlocksInSegment(ii)*plotNMin/fs tmp+numBlocksInSegment(ii)*plotNMin/fs],[0 fs/2],'w')
                tmp=tmp+numBlocksInSegment(ii)*plotNMin/fs;
            end
            yyaxis right;
            ylabel('Amplitude');
            set(gca, 'ytick',[0.62 0.8 0.98]-0.02);
            set(gca, 'YTickLabel',[-1 0 1],'YColor','black')

        end
        true(iter).Points
        finalMu(iter).MMDL 
        close all;
    end

    [errorAngle, errorDelay,trueNumClusters,NumClustersMMDL,errorRateModelOrder] = evaluate_synthetic(finalMu, true, iter, 5);

    correctAmplitudes = errorAngle(errorAngle<0.95);
    A_RMSE=sqrt(mean(correctAmplitudes.^2))
    numCorrectParameters = length(correctAmplitudes)
    totalParameterEst = sum(trueNumClusters)
    parameterPercentage = numCorrectParameters/totalParameterEst

    correctDelay = errorDelay(errorAngle<0.95);
    D_RMSE=sqrt(mean(correctDelay.^2))

    results_optimal(iterationNumber).errorAngle = errorAngle;
    results_optimal(iterationNumber).errorDelay = errorDelay;
    results_optimal(iterationNumber).trueNumClusters = trueNumClusters;
    results_optimal(iterationNumber).NumClustersMMDL = NumClustersMMDL;
    results_optimal(iterationNumber).errorRateModelorder = errorRateModelOrder;
    results_optimal(iterationNumber).iter = iter;
    results_optimal(iterationNumber).true = true;
    results_optimal(iterationNumber).finalMu = finalMu;
    results_optimal(iterationNumber).D_RMSE = D_RMSE;
    results_optimal(iterationNumber).totalParameterrEst = totalParameterEst;
    results_optimal(iterationNumber).NumCorrectParameters = numCorrectParameters;
    results_optimal(iterationNumber).parameterPercentage = parameterPercentage;
    results_optimal(iterationNumber).A_RMSE = A_RMSE;
    results_optimal(iterationNumber).savedX = savedX;
    results_optimal(iterationNumber).duration = duration;
    results_optimal(iterationNumber).savedNumBlocksInSegment = savedNumBlocksInSegment;
    
    save('mats/test_on_N/N_results_optimal_maximize_50.mat','results_optimal','-v7.3');
    clear savedX finalMu errorAngle errorDelay NumClustersMMDL errorRateModelOrder D_RMSE totalParameterEst numCorrectParameters parameterPercentage A_RMSE;
    iterationNumber = iterationNumber+1;
end