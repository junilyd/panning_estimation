%  seven sources mixture
% cd /home/jacob/panning_project/test_programs/;
% load mats/panning_data2.mat;
% fs=44100;
% X = panning_data.*[1/45 1/(200e-6*fs)];
% trueNumClusters=7;
% figure;

clear all; close  all;
cd /home/jacob/panning_project/test_programs/;
load mats/test_MMDL_82iter_18April.mat;

index=82;%randi(82-41)+41;
X=savedX{index};

trueNumClusters = size(true(index).Points,1);

% plot 3D Gaussian mixture
[mindl, bestk, bestmu, bestcov,bestpp,safemu, safemuMean, bestcovCovRatio, bestmuCovRatio, bestppCovRatio,X2,X3] = smc_GMM_MMDL_normdetC(X, fs,trueNumClusters);
view(-26,14);
% plot after pruning step 1. 
figure;
    plot(X(:,1)*45,X(:,2)*150e-6*fs,'.'); xlim([-45 45]), ylim([-8 8]); % 'Color',[0.5 0.5 0.5]);
    hold on;
    plot(X2(:,1)*45,X2(:,2)*150e-6*fs,'.')
for comp=1:length(bestpp)
    hold on;
    elipsnorm_interior_points(bestmu([1,2],comp),bestcov([1,2],[1,2],comp),2,1, X', fs)
end
xlabel('Amplitude Panning Angle [deg.]');
ylabel('Delay [samples]');
legend('All Param. estimates','Param. estimates after pruning step 1', 'Contour ellipsoids')
% plot after pruning step 2. 
figure;
    plot(X(:,1)*45,X(:,2)*150e-6*fs,'.'); xlim([-45 45]), ylim([-8 8]); % 'Color',[0.5 0.5 0.5]);
    hold on;
    plot(X3(:,1)*45,X3(:,2)*150e-6*fs,'.')
for comp=1:length(bestpp)
    hold on;
    elipsnorm_interior_points(bestmu([1,2],comp),bestcov([1,2],[1,2],comp),2,1, X', fs)
end
xlabel('Amplitude Panning Angle [deg.]');
ylabel('Delay [samples]');
legend('All Param. estimates','Param. estimates after pruning step 2', 'Contour ellipsoids')


% Uniform Segmentation
clear X2 X3;
x=savedx{index};
[XX] = smc_estimate_panning_space_from_optimal_segments(x, ones(1000,1)*wlen, fs, true(iter).Points, NFFT, maxDelaySamples);
[mindl, bestk, bestmu, bestcov,bestpp,safemu, safemuMean, bestcovCovRatio, bestmuCovRatio, bestppCovRatio,X2,X3] = smc_GMM_MMDL_normdetC(XX, fs,trueNumClusters);
view(-26,14);
title('Uniform Segmentation');

% plot after pruning step 1. 
figure;
    plot(XX(:,1)*45,XX(:,2)*150e-6*fs,'.'); xlim([-45 45]), ylim([-8 8]); % 'Color',[0.5 0.5 0.5]);
    hold on;
    plot(X2(:,1)*45,X2(:,2)*150e-6*fs,'.')
for comp=1:length(bestpp)
    hold on;
    elipsnorm_interior_points(bestmu([1,2],comp),bestcov([1,2],[1,2],comp),2,1, X', fs)
end
title('Uniform Segmentation');
xlabel('Amplitude Panning Angle [deg.]');
ylabel('Delay [samples]');
legend('All Param. estimates','Param. estimates after pruning step 1', 'Contour ellipsoids')
% plot after pruning step 2. 
figure;
    plot(XX(:,1)*45,XX(:,2)*150e-6*fs,'.'); xlim([-45 45]), ylim([-8 8]); % 'Color',[0.5 0.5 0.5]);
    hold on;
    plot(X3(:,1)*45,X3(:,2)*150e-6*fs,'.')
for comp=1:length(bestpp)
    hold on;
    elipsnorm_interior_points(bestmu([1,2],comp),bestcov([1,2],[1,2],comp),2,1, X', fs)
end
title('Uniform Segmentation');
xlabel('Amplitude Panning Angle [deg.]');
ylabel('Delay [samples]');
legend('All Param. estimates','Param. estimates after pruning step 2', 'Contour ellipsoids')
