for ii=1:500
C = origC{ii};

for nn=1:kmax, detC(nn) = det(C(:,:,nn)); end

[~, ndx] = sort(detC);
C=C(:,:,ndx);

for nn=1:kmax, C1(nn) = C(1,1,nn); end
for nn=1:kmax, C2(nn) = C(2,2,nn); end
C3=C1.*C2;

clf; 
plot(log(C1)); hold on; plot(log(C2)); plot(log(C1.*C2))
plot(1:35,ones(1,35)*log(C1(4)),'b');
plot(1:35,ones(1,35)*log(C2(4)),'r');
plot(1:35,ones(1,35)*log(C3(4)),'k');
legend('gain','delay','det')
trueParams = trueParameters{ii}
ms=muSorted{ii};
ms(1:5,35:36)
sign(diff(C1))-sign(diff(C2))
pause;
end