function GD = smc_estimate_panning_space(Xl, Xr, NFFT,fs)

    D_vec=[];
    G_vec=[];
 
    for ii=1:size(Xl,2)
        X1 = Xl(1:NFFT/2-1,ii);
        X2 = Xr(1:NFFT/2-1,ii);

        thrl = 20;%min(20*mean(abs(X1)),0.4*max(abs(X1)) );

        fl=find(abs(X1)>thrl); % thr
        
        ratio = X2(fl)./X1(fl);
        
        G=acot(abs(ratio));  
        G_vec=[G_vec; G];

        D = -imag(log(ratio))./(2*pi*(fl-1)/NFFT); 
        D_vec=[D_vec; D];
    end

    % de-range the data
    maxPanAngle = 45;
    maxDelay = 200e-6;
    GDMask = (abs(D_vec)<maxDelay*fs) & (G_vec<maxPanAngle*pi/180*2);

    GD(:,1) = G_vec(GDMask)./pi*180-45;
    GD(:,2) = D_vec(GDMask);
end