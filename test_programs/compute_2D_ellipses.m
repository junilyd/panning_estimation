function [ elipses, detC, insideElipseIndicator,percentageInside] = compute_2D_ellipses( X,mu,C,stdFactor,classLabels)
    if nargin < 4
        stdFactor=2;
    end

    k = size(mu,2);
    XPruned=[];
    for ii=1:k
        [U,S,~] = svd(C(:,:,ii));       
        lambda0 =  S(1,1);
        lambda1 =  S(2,2);        
        sqrtLambda0 = sqrt(lambda0)*stdFactor;
        sqrtLambda1 = sqrt(lambda1)*stdFactor;
        
        % subtract expected value from each cluster samples
        labelMask = find(classLabels==ii);
        X0 = X(labelMask,:)'-mu(:,ii);
        % Determin angle of principal axis and x-axis
        Theta = atan2(U(2,:),U(1,:)); % maybe acos(u(1,1)/sqrt(2))
        Theta = Theta(1);
        XX = (X0(1,:))*cos(Theta)+(X0(2,:))*sin(Theta);
        YY = -X0(1,:)*sin(Theta)+X0(2,:)*cos(Theta); 
        % area of ellipse
        detC(ii,1) = det(C(:,:,ii));
        % locate points inside each elliptic covariance contour
        %insideElipseIndicator(:,ii) = (XX.^2/sqrtLambda0^2+YY.^2/sqrtLambda1^2<1);
        insideElipseIndicator(ii,1) = sum((XX.^2/sqrtLambda0^2+YY.^2/sqrtLambda1^2<1));
        percentageInside(ii,1) = insideElipseIndicator(ii,1)/length(labelMask); 
        % for plotting
        phi = [0:0.01:2*pi];
        xx = sqrtLambda0*cos(phi);
        yy = sqrtLambda1*sin(phi);
        cord = [xx' yy']';
        cord = U*cord;
        elipses(:,:,ii) = [cord(1,:)+mu(1,ii); cord(2,:)+mu(2,ii)];
        clear X0 XX YY;
    end 

end

