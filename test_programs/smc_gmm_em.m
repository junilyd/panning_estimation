%% Run GMM by EM, with k=20 Gaussians.
% Initalize with k=20 labels
function Xred = smc_reduce_data_by_gmm_em(X, numGaussians)
	if nargin < 2
		k = 30;
	else
		k = numGaussians;
	end
	if size(X,1)>size(X,2)
		X=abs(X)';
		transposed = 1;
	end

	n = size(X,2);
	d = size(X,1);
	init = kmeanspp(X,k);
	[labels,model,llh] = mixGaussEm(X,init);
	    %plotClass(X,labels); title(sprintf('%1.0f Gaussians on panning parameter estimates',k))
	    %xlabel('Angle Estimate [deg]'); ylabel('Delay Estimate [samples]'); axis([-40 40 -10 10]);

	%% Compute variance for each Gaussian Mode
	for cc=1:length(unique(labels))
	    vari(cc) = trace(model.Sigma(:,:,cc));
	end

	%% Remove all class estimates with variance. above mean variance. 
	MASKaboveSigmaMean = vari>mean(vari)/2;
	model.mu(:,MASKaboveSigmaMean)=[];
	model.Sigma(:,MASKaboveSigmaMean)=[];
	model.w(:,MASKaboveSigmaMean)=[];

	% find all data points lower than the mean var.
	classlabels = find(MASKaboveSigmaMean==0);
	Xred=[];
	for cl=1:length(classlabels)
	    Xred = [Xred X(:,find(labels==classlabels(cl)))];
	end
	if transposed, Xred = Xred'; end
end