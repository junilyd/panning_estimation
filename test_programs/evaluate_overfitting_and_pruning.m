function [errorAngle, errorDelay,trueNumClusters,estimatedNumClusters,errorRateModelOrder, totalEstimatedParamsPruned, trueMinusEstimated] = evaluate_overfitting_and_pruning(saved, plotFlag)
iter = size(saved.X,2);

if nargin == 1
    plotFlag = 0;
end

for hh=1:iter   
    m=[];
    t = saved.trueParams{hh};
    m = saved.muPruned{hh}';
    
    if plotFlag == 1
        figure(saved.trueNumSource{hh}); hold on; 
            for qq=1:size(m,1)
                text(m(qq,1), m(qq,2), sprintf('%1.0f',hh),'Color','blue','horizontalalignment','right');
            end
            plot(t(:,1), t(:,2),'k+','markersize',12);
            for qq=1:length(t)
                text(t(qq,1), t(qq,2), sprintf('%1.0f',hh));
            end
            plot(m(:,1), m(:,2), 'bx','markersize',18);


        title(sprintf('Estimates of panning parameters (%1.0f clusters)',saved.trueNumSource{hh}));
        xlabel('Pan Angle Estimate');
        ylabel('Delay Estimate');
    end
end

for hh=1:iter
    %fprintf('ii: %1.0f, true: %1.0f, MMDL: %1.0f\n',hh, saved.trueNumSource{hh},saved.kPruned{hh});
    trueMinusEstimated(hh) = saved.trueNumSource{hh}-saved.kPruned{hh};
    trueNumClusters(hh) = saved.trueNumSource{hh};
    estimatedNumClusters(hh) = saved.kPruned{hh};
end
trueNumClusters;
estimatedNumClusters;
errorRateModelOrder = 100-100*(sum(estimatedNumClusters==trueNumClusters)/iter)

errorAngle=[];
errorDelay=[];
for hh=1:iter
    tp = saved.trueParams{hh};
    [~,ndx] = sort(tp(:,1));
    tp = tp(ndx,:);
    
    ep = saved.muPruned{hh}';
    totalEstimatedParamsPruned{hh} = ep;
    [~,ndx] = sort(ep(:,1));
    ep = ep(ndx,:);

    tp;
    ep;
    % detect the clusters that has been found
    min(length(tp),size(ep,1));
    for qq=1:min(length(tp),size(ep,1))
        if length(ep)>=length(tp)
            qq;
            [tmpErrorAngle(qq),minndx] = min(abs(tp(qq,1)-ep(:,1)));
            tmpErrorDelay(qq) = abs(ep(minndx,2)-tp(qq,2));
        else
            qq;
            [tmpErrorAngle(qq),minndx] = min(abs(tp(:,1)-ep(qq,1)));        
            tmpErrorDelay(qq) = abs(ep(qq,2)-tp(minndx,2));
        end
    end

    errorAngle=[errorAngle tmpErrorAngle];
    errorDelay=[errorDelay tmpErrorDelay];
    tmpErrorAngle=[];
    tmpErrorDelay=[];
end


