clear all;
%close all;

M=4;
sti = '/Users/home/Documents/P9/panning_project/test_programs/SQAM_FLAC/';
files={strcat(sti,'13.flac'),strcat(sti,'25.flac'),strcat(sti,'08.flac'),strcat(sti,'21.flac')};
theta=([-30 20 0 30 ]+45)/180*pi;
delaySec = [-200e-6 -100e-6 100e-6 300e-6];

for m=1:M,
    [source{m},fs]=audioread(files{m});
    len(m)=length(source{m});
end
len=min(len);
y=zeros(len,2);
delay = floor(abs(delaySec*fs));

for m=1:M,
    source{m}=source{m}(:,1);    
    source{m}=source{m}(1:len);
    source{m}=source{m}/var(source{m});    
end

for m=1:M;
  g(1,m)=sin(theta(m));
  g(2,m)=cos(theta(m));
  if delaySec(m) > 0 
    y(:,1)=y(:,1)+g(1,m)*source{m};
    y(:,2)=y(:,2)+g(2,m)*[zeros(delay(m),1); source{m}(1:end-delay(m))];
  else
    y(:,2)=y(:,2)+g(2,m)*source{m};
    y(:,1)=y(:,1)+g(1,m)*[zeros(delay(m),1); source{m}(1:end-delay(m))];
  end
end
theta(1:M)
delaySec(1:M)*fs
D_vec=[];
G_vec=[];
F=8192;
thr=1e3;

N=round(0.05*fs);
win=hanning(N);
pos=1:N;
while pos(end)<len,
   xl=y(pos,1);
   xr=y(pos,2);
   
   Xl=(fft(win.*xl,F));
   Xr=(fft(win.*xr,F));  
   
   fl=find(abs(Xl(1:F/2-1))>thr);
   fr=find(abs(Xr(1:F/2-1))>thr);

   % Use both fl and fr?
   G=atan(abs((Xr(fl)./Xl(fl))));  
   G_vec=[G_vec; G];
   
   D = -imag(log(Xr(fl)./Xl(fl)))./(2*pi*(fl-1)/F); 
   %D=(angle(Xl(fl))-angle(Xr(fl)))./(2*pi*(fl-1)/F); 
   D_vec=[D_vec; D];
   
   pos=pos+N;
end

figure(1);clf;hist(G_vec,100);
figure(2);clf;hist(D_vec,10000); xlim([-60 60])
