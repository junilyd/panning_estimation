
% gem tendenserne og ikke normalfordelte distributioner 
%
 clear all; clf;
% ITD ILD Dan ellis 
clear all;% clf;
% duration = 0.1;
N = 401; % fixed at the moment !!! 800 samples is good with blackamnharris window
fs  = 8e3;
duration = N/fs;
f0  = [200 128 659 173 234 470 150 280];
pan = [-40 -30 -20 -10 0 15 25 35]';

%[x,fs] = wavread('/Users/home/dropbox/Mixing Parameter Estimation/trumpet_horn_mixture.wav');
[x,fs] = audioread('/Users/home/Music/iTunes/iTunes Media/Music/Fears Unfolding/Unknown Album/Angel In Red Rough.mp3');
%[x,fs] = wavread('/Users/home/Music/iTunes/iTunes Media/Music/Unknown Artist/Unknown Album/Intro til lydmand.wav');
%[x,fs] = audioread('/Users/home/Music/iTunes/iTunes Media/Music/Frank Ocean/Channel ORANGE/10 Pyramids.m4a');
%[x,fs] = audioread('/Users/home/Music/iTunes/iTunes Media/Music/The Beatles/Anthology Box Set/1-01 Free As a Bird.m4a');
startSec = 1;
numRuns = 1;
x = x(startSec*fs:startSec*fs+N*numRuns,:);
soundsc(x,fs)

runs = floor(length(x)/N)
buffer = [1:N];
for ii = 1:runs
    xTest = x(buffer,:);
    %[h(buffer,:)] = smc_panned_sinusoids(f0.*rand, pan, duration, fs);
    [C(:,:,ii), panSpace] = smc_panning_ratio(xTest,fs);
    buffer = buffer + N;
end
cost = sum(C,3);
cost = cost./sum(cost);
diffCost = [0 diff(cost)];

% find peaks above +- panThresh degrees.
panThresh = 4;
[peaks] = smc_peaks(diffCost,10);
positivePeaksNdx = peaks.ndx(peaks.sign==1);
positivePeaksVal = peaks.val(peaks.sign==1);
positivePeaksVal(abs(panSpace(positivePeaksNdx))<panThresh) = [];
positivePeaksNdx(abs(panSpace(positivePeaksNdx))<panThresh) = [];

% extract mainlobes around peaks
for pp=1:length(positivePeaksNdx)
    peakNdx = positivePeaksNdx(pp);
    % go forward and check sign
    sign = -1;
    posWidth = 0;
    while sign <= 0 % go forward
        posWidth = posWidth+1;
        sign = diffCost(peakNdx+posWidth);
    end
    sign = 1;
    negWidth = 0;
    while sign >= 0 % go backward
        negWidth = negWidth-1;
        sign = diffCost(peakNdx+negWidth);
    end
    if panSpace(peakNdx) < 0
        posWidth = posWidth -1;
    else 
        posWidth = posWidth +1;
    end
    peterPan(:,pp) = zeros(size(panSpace));
    mainLobe(:,pp) = [panSpace(peakNdx+negWidth) panSpace(peakNdx+posWidth)]'; 
    peterPan(peakNdx+negWidth:peakNdx+posWidth,pp) = cost(peakNdx+negWidth:peakNdx+posWidth)'
end


    

figure(1)
subplot(411);
    plot(panSpace, cost);
subplot(412)
    plot(panSpace,diffCost);
    hold on; plot(panSpace(positivePeaksNdx),positivePeaksVal,'ko','linewidth',5); grid on
subplot(413)
    plot(panSpace,peterPan)    % 
subplot(414)
    plot(panSpace,sum(peterPan,2))    % 

%     figure(1)
%     subplot(211)
%         imagesc(panSpace,1:N,sum(C));
%             axis xy
%             xlabel('Estimated Pan Angle [deg.]'); ylabel('Frequency [Hz]');
%             titleString = 'True \Theta = ';
%             for ii = 1:length(pan)
%                 titleString =  [titleString sprintf('%1.0f ',pan(ii))];
%             end
%             title(titleString)    
%     subplot(212)
%         %plot(panSpace, sum(ratioFunction(floor(N/4):floor(N/2),:)));
%         plot(panSpace, sum(ratioFunction(1:N,:)));
%             grid on;
%             xlim([panSpace(1) panSpace(end)])
%             title('Integration over the frequency range');
%             xlabel('Estimated Pan Angle [deg.]');