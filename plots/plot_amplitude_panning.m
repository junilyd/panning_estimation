%%
clear all; 
clf
hold on

theta0 = pi/2;
theta = -pi/10;

plot_arc(-pi/4+theta0,pi/4+theta0,0,0,1,'k--'); 
axis([-1.1,1.8,-0.1,1.1]);

speakerR.x = cos(-pi/4 +theta0);
speakerR.y = sin(-pi/4 +theta0);
plot_arc(pi/4+theta0, theta0, 0,0,0.17,'k');
text(0.05,.425,'$$\phi$$','interpreter','latex')

speakerL.x = cos(pi/4 +theta0)
speakerL.y = sin(pi/4 +theta0)
plot_arc(-pi/4+theta0, theta0, 0,0,0.2,'k');
text(-0.12,.19,'$$\phi_0$$','interpreter','latex')

plot([0 speakerL.x],[0 speakerL.y],'k');
plot([0 speakerR.x],[0 speakerR.y],'k');

pannedSpeaker.x = cos(theta +theta0)
pannedSpeaker.y = sin(theta +theta0)
plot_arc(theta+theta0, theta0, 0,0,0.4,'k');
text(0.05,.2,'$$-\phi_0$$','interpreter','latex')

plot([0 pannedSpeaker.x],[0 pannedSpeaker.y],'k');

plot_rectangle(-pi/4 +theta0, 1,-0.075,0.1,0.15,'k')
plot_rectangle(pi/4 +theta0, 1,-0.075,0.1,0.15,'k')
plot_rectangle(theta +theta0, 1,-0.075,0.1,0.15,'k--');

plot_arrow( 0,0,0,1.25,'headwidth',0.02,'headheight',0.03);
text(-0.65,0.058,'$$y$$','interpreter','latex')
plot_arrow( 0.3,0,-0.707,0,'headwidth',0.03,'headheight',0.04);
text(0.03,1.2,'$$x$$','interpreter','latex')

text(0.8,.9,'Right');
text(0.8,0.83,'Speaker');
text(-0.97,0.94,'Left');
text(-0.97,0.87,'Speaker');
text(cos(theta+theta0)-0.05,sin(theta+theta0)+0.24,'Virtual')
text(cos(theta+theta0)-0.05,sin(theta+theta0)+0.17,'Source')

axis off
legend off

 %fig2tikz(gcf,'7','Stereophonic Configuration','Speaker Setup','~/panning_project/diary')
