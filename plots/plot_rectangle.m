function P = plot_arc(alpha,x,y,w,h,plotType)
    %w=0.1; %width
    %h=0.15; %height
    %x=-0;y=0; %corner position
    xv=[x x+w x+w x x];yv=[y y y+h y+h y];
    %figure(1), plot(xv,yv);axis equal;

    %rotate angle alpha
    R(1,:)=xv;R(2,:)=yv;
    %alpha=30*2*pi/360;
    XY=[cos(alpha) -sin(alpha);sin(alpha) cos(alpha)]*R;
    hold on;plot(XY(1,:),XY(2,:),plotType);
    %axis([-2 4 0 5])
end