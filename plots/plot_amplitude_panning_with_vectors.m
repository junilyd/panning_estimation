%%
clear all; 
clf
hold on

phi0 = pi/2;
phi = -pi/10;
theta = phi+phi0;

plot_arc(-pi/4+phi0,pi/4+phi0,0,0,1,'k--'); 
axis([-1.1,1.8,-0.1,1.1]);

speakerR.x = cos(-pi/4 +phi0);
speakerR.y = sin(-pi/4 +phi0);
%plot_arc(pi/4+theta0, theta0, 0,0,0.17,'k');
%text(0.05,.425,'$$\phi$$','interpreter','latex')

speakerL.x = cos(pi/4 +phi0)
speakerL.y = sin(pi/4 +phi0)
%plot_arc(-pi/4+theta0, theta0, 0,0,0.2,'k');
%text(-0.12,.19,'$$\phi_0$$','interpreter','latex')



pannedSpeaker.x = cos(phi +phi0)
pannedSpeaker.y = sin(phi +phi0)
%plot_arc(theta+theta0, theta0, 0,0,0.4,'k');
%text(0.05,.2,'$$-\phi_0$$','interpreter','latex')

% L
%plot([0 speakerL.x],[0 speakerL.y],'k');
%plot([0 speakerR.x],[0 speakerR.y],'k');
scale=0.889;
plot_arrow( 0,0,scale*speakerR.x,scale*speakerR.y,'headwidth',0.02,'headheight',0.03);
text(0.6,0.55,'$$g_2\textbf{l}_2$$','interpreter','latex'); % g2l2
plot_arrow( 0,0,speakerR.x,speakerR.y,'headwidth',0.02,'headheight',0.03);
text(0.7,0.65,'$$\textbf{l}_2$$','interpreter','latex'); % l2

scale2=sqrt(1-scale^2)
plot_arrow( 0,0,scale2*speakerL.x,scale2*speakerL.y,'headwidth',0.04,'headheight',0.05);
text(-0.45,0.3,'$$g_1\textbf{l}_1$$','interpreter','latex'); % g1l1
plot_arrow( 0,0,speakerL.x,speakerL.y,'headwidth',0.02,'headheight',0.03);
text(-0.72,0.65,'$$\textbf{l}_1$$','interpreter','latex'); % l1

plot_arrow(scale2*speakerL.x,scale2*speakerL.y,scale2*speakerL.x + scale*speakerR.x,scale2*speakerL.y + scale*speakerR.y,'headwidth',0.02,'headheight',0.03);
plot_arrow(scale*speakerR.x,scale*speakerR.y,scale*speakerR.x + scale2*speakerL.x,scale2*speakerR.y + scale*speakerL.y,'headwidth',0.02,'headheight',0.03);
plot_arrow( 0,0,pannedSpeaker.x,pannedSpeaker.y,'headwidth',0.02,'headheight',0.03);
text(0.3,0.85,'$$\textbf{p}$$','interpreter','latex'); % p


% p
%plot([0 pannedSpeaker.x],[0 pannedSpeaker.y],'k');

plot_rectangle(-pi/4 +phi0, 1,-0.075,0.1,0.15,'k')
plot_rectangle(pi/4 +phi0, 1,-0.075,0.1,0.15,'k')
plot_rectangle(phi +phi0, 1,-0.075,0.1,0.15,'k--');

plot_arrow( 0,0,0,1.25,'headwidth',0.02,'headheight',0.03);
text(-0.65,0.058,'$$y$$','interpreter','latex')
plot_arrow( 0.3,0,-0.707,0,'headwidth',0.03,'headheight',0.04);
text(0.03,1.2,'$$x$$','interpreter','latex')

text(0.8,.9,'Right');
text(0.8,0.83,'Speaker');
text(-0.97,0.94,'Left');
text(-0.97,0.87,'Speaker');
text(cos(phi+phi0)-0.05,sin(phi+phi0)+0.24,'Virtual')
text(cos(phi+phi0)-0.05,sin(phi+phi0)+0.17,'Source')

axis off
legend off

 fig2tikz(gcf,'7','Stereophonic configuration with vector formulation','Speaker Setup Vectors','~/panning_project/diary')
