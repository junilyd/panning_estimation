%function fig2tikz(gcf,type,caption,filename,svn_path) 
%
% INPUTS: 
%       gcf: figure handle 
%       type: '1' for wide layout and '2' for square layou and '3' for subplot horizontal layout
%       caption: Figure caption for the latex document.
%       filename: Name of the two files which will be created as *.tikz and *.tex
%       svn_path: the path to svn dir
% OUTPUTS:
%       one *.tikz file as specified inside the file.
%       one *.tex file as specified inside the file.
%       Print to screen of where these files are being created
%
% DEPENDENCIES:
%       Dependent on matlab2tikz.m 
%
% EXAMPLE:
% fig2tikz(gcf,'wide','Graph Plot',figure_filename,'/Path/to/svn/root_repo');
%
function fig2tikz(gcf,type,caption,filename,svn_path) 

    filename_tex = filename; filename = strrep(filename,' ','_');
    filetikz = sprintf('%s/img/tikz/%s.tikz',svn_path,filename);
    filetex = sprintf('%s/img/%s.tex',svn_path,filename);
    filePdf = sprintf('%s/img/%s',svn_path,filename);
    system(sprintf('touch %s && touch %s && touch %s.pdf',filetex,filetikz, filePdf)); 
    system(sprintf('sudo rm %s && rm %s && rm %s.pdf',filetex,filetikz,filePdf)); 

    if type == '1' 
        matlab2tikz(sprintf('%s',filetikz),'nosize',true);
    end
    if type == '2'
         leg = legend('location', 'Northwest');
         % set(leg, 'location','northwest');
         legend boxoff;
         matlab2tikz(sprintf('%s/img/tikz/%s.tikz',svn_path,filename),'width','7cm','height','7cm');
    end
    if type == '3'
         matlab2tikz(sprintf('%s/img/tikz/%s.tikz',svn_path,filename),'width','7cm','height','4cm');
    end
    if type == '4'
         matlab2tikz(sprintf('%s/img/tikz/%s.tikz',svn_path,filename),'width','11cm','height','4cm');
    end
    if type == '5'
         matlab2tikz(sprintf('%s/img/tikz/%s.tikz',svn_path,filename),'width','16cm','height','4cm');
    end
    system(sprintf('printf "\\n \\\\\\begin{figure}[!t]\\n    \\centering\\n    \\input img/tikz/%s.tikz\\n    \\caption{%s}\\n    \\label{fig:%s}\\n \\\\\\end{figure}" >> %s',filename,caption,filename,filetex));
    fprintf(sprintf('\n\nTwo files have been written:\n(image)\n    %s\nand\n(wrapper)\n    %s\n\nThis means that you can do:\n    input img/%s.tex in your document.\n\n',filetikz,filetex,filename));
    if type == '7'
         matlab2tikz(sprintf('%s/img/tikz/%s.tikz',svn_path,filename),'width','16cm','height','7cm');
    end
    
    export_fig(filePdf,'-pdf', '-transparent')
end