\section{Signal Model}
\label{sec:signal_model}
In the following the signal model and assumptions are introduced.
The mixture ${y}_m(n)$ in channel $m$ at time instance $n$ is modelled as a sum of $K$ attenuated and delayed sources ${s}_k(n)$
\begin{equation}\label{eq:mixture}
 {y}_m(n) = \sum_{k=1}^K \gamma_{m,k} {s}_k(n-\delta_{m,k}).% + {e}_{k}(n). 
\end{equation}
The $k\textup{th}$ source signal $s_k(n)$ mixed in channel $m$, is attenuated by gain coefficient $\gamma_{m,k}$ and delayed by $\delta_{m,k}$ samples. By expressing the $k$th source on the frequency grid $\omega$ as $S_{m,k}(\omega) = \sum_{n=0}^{N-1} s_k(n)  \gamma_{m,k} e^{-j\omega (n + \delta_{m,k})}$, the assumption of W-disjoint orthogonality~\cite{rickardBSS2} is expressed as	
%
\begin{equation}\label{eq:orth}
    S_{1,k}(\omega)S_{1,i}(\omega) \approx 0 \quad \forall \omega, k\neq i.
\end{equation}
Since this assumption is violated even for speech signals~\cite{rickardwdisjoint}, we propose an MAP-optimal adaptive time segmentation to support this underlying assumption of frequency sparsity, as described in Section~\ref{sec:signal_segmentation}. In typical music productions, the gain coefficients are based on the tangent law~\cite{bernfeld1973attempts}, consequently inducing a constant perceived distance between listener and virtual source position. The gains $\gamma_m$ for channel $m$ are expressed as
\begin{equation}
	\gamma_m = 
	\begin{cases}
    	\cos\Phi_k,    & \text{for } m=1\\
    	\sin\Phi_k,    & \text{for } m=2
	\end{cases}
\end{equation}
where $\Phi_k = \phi_k+\phi_0$ is a sum of the perceived angle $\phi_k$ of the $k$th source and the speaker base angles $\pm\phi_0 = 45^\circ$. The conditions $0^\circ<\phi_0<90^\circ$, $-\phi_0\leq\phi\leq\phi_0$ and $\gamma_1, \gamma_2 \in [0,1] $ are met~\cite{pulkki2}.
Delays below $600 \mu s$ between stereo channels makes the virtual source position migrate toward the earlier speaker~\cite{blauert}. It is ensured that the maximum frequency $\omega_{\textup{max}}$ and delay $\delta_{\textup{max}}$ are restricted to $|\omega_{\textup{max}}\delta_{\textup{max}}|<\pi$.
A two channel signal $y_m(n) \in {\rm I\!R}^N$ is defined from $N$ consecutive samples, defined for discrete time indeces going forward from $n$ to $n+N-1$, where we propose that $N$ is the adaptive segment size. The data in the $m\text{th}$ channel is represented as 
    \begin{equation}\label{eq:datavector}
        y_m(n) = [y_m(n)\quad y_m(n+1)\quad \dots \quad y_m(n+N-1)]^T.
    \end{equation}
For each segment, we estimate a frequency domain measurement vector $\symvec{x}$ as a function channel ratios
\begin{equation}\label{eq:clusters}
    \symvec{x} = 
    f\Big(\hat{\gamma}(\omega), {\hat{\delta}(\omega)}\Big) = 
    f \bigg( \left|\frac{Y_{2}(\omega)}{Y_{1}(\omega)}\right|, 
     \frac{1}{\omega} \angle \frac{Y_{1}(\omega)}{Y_{2}(\omega)} 
    \bigg), 
\end{equation}
where $Y_m(\omega)$ is the DFT of $y_m(n)$ and $\angle$ denotes phase. As modelled in~\cite{rickardwdisjoint}, the marginal distributions are non-correlated and the non-W-disjoint error does not produce parameter dependency. Under the given assumptions $\symvec{x}$ will cluster around the true $K$ source parameters. We model $\symmat{x}$ as a $K$-component Gaussian mixture with diagonal covariance matrices expressed as
\begin{equation}
  p(\symvec{x}|\symvec{\theta}) = \sum_{k=1}^K\alpha_k
  {\frac{ \exp \big\{-\frac{1}{2}(\symvec{x}-\symvec{\mu}_k)^T \symmat{C}^{-1}_k (\symvec{x}-\symvec{\mu}_k)\big\}}{\sqrt{(2\pi)^d \det{(\symmat{C}_k)}}{}}  },
\end{equation}
where \{$\alpha_k,\symvec{\mu}_k, \symmat{C}_k\}$ is the mixing probability, mean and covariance of the $k$th Gaussian. Thus, $\symvec{\mu}_k$ is the source parameter expressing the virtual positioning of the $k\text{th}$ source and $\symmat{\theta}\defeqn{\{{\alpha}_1,\dots,{\alpha}_K, \symvec{\mu}_1,\dots,\symvec{\mu}_K, \symmat{C}_1,\dots,\symmat{C}_K\}}$ specifies the full mixture as the complete set of parameters. Generally, $\alpha_k \geq 0, \quad \sum_{k=1}^K \alpha_k =1, \text{ for} \quad k=1,\dots,K$. 