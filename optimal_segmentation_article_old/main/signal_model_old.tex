\section{Signal Model} % (fold)
\label{sec:signal_model}
In the following the signal model and assumptions are introduced. Consider an M-channel music mixture consisting of $K$ unknown sources embedded in noise at time instant $n$. The data in the $m^{\textup{th}}$ channel is represented as $\symvec{x}_m(n) \in {\rm I\!R}^N$,  
\begin{equation}
	\symvec{x}_m(n) = [x_m(n)\quad x_m(n+1)\quad \cdots \quad x_m(n+N-1)]^T
\end{equation}
for $m=1,\cdots,M$. The signal mixture is modelled as a linear superposition of $K$ attenuated and delayed sources $\symvec{s}_k$ embedded in noise $\symvec{e}_{m,k}$. 
The signals captured by channel $m$, relating to the $k^{\textup{th}}$ source are attenuated by gain coefficient $g_{m,k}$ and delayed by $\tau_{m,k}$ depending on their perceptional virtual positioning, given by the panning parameters. The signal mixture is,
 \begin{equation}
 	\symvec{x}_m(n) = \sum_{k=1}^K g_{m,k} \symvec{s}_k(n-f_s\tau_{m,k}) + \symvec{e}_{m,k}(n) 
 \end{equation}
where $g_{m,k}$ and $\tau_{m,k}$ are the attenuation and delay applied to the signal, respectively and $f_s$ is the sampling frequency.
Considering stereophonic mixtures with $M=2$ in a stereo loudspeaker setup, the tangent law [Bernfeld,VBAP] describes an amplitude panning angle applied to the $K$ sources, with a linear relation to the gain coefficients $g_1$ and $g_2$. The trigonometric functions are used for the panning attenuation because they induce a constant perceived distance between listener and the virtual source, described by $1 = \cos^2+\sin^2$. The gains are
\begin{equation}
	g_m = 
	\begin{cases}
    	\cos\theta_k,    & \text{for } m=1\\
    	\sin\theta_k,    & \text{for } m=2
	\end{cases}
\end{equation}
where $\theta = \phi+\phi_0$ is a sum of the perceived angle $\phi$ and the speaker base angles $\pm\phi_0 = 45^\circ$. Under the conditions $0^\circ<\phi_0<90^\circ$, $-\phi_0\leq\phi\leq\phi_0$ and $g_1, g_2 \in [\ 0,1]\ $ the gains can be expressed as,
\begin{equation}
     \symvec{g}_k = \symvec{p}_k \symmat{L}^{-1}  
\end{equation}
where the unit-vector $\symvec{p}$ points towards the virtual source with $\symvec{L}$ as a unitary loudspeaker base matrix. 
% The amplitude panning angle $\hat{\theta}_k$ of the $k^\textup{th}$ source is found as, 
% \begin{equation}
%  	\hat{\theta}_k = \arctan{\frac{p_k(1)}{p_k(2)}}
% \end{equation} 
%
It is relevant to note here that panning parameters are applied in the post-processing of a music production, and delays can be added to enhance the spatial perception [Blauert]. Since all mixing parameters are applied in the post-processing procedure, only the direct path is investigated. It simplifies notation if the attenuation and delay panning parameters are modelled as ratios between the two channels. The two channel non-reverberant mixtures are then expressed as, 
\begin{equation}\label{eq:sk}
	 	\symvec{x}_1(n) = \sum_{k=1}^K \symvec{s}_k(n)% + \symvec{e}_{k}(n)\\
\end{equation}
\begin{equation}\label{eq:sk2}
 	\symvec{x}_2(n) = \sum_{k=1}^K \gamma_{k} \symvec{s}_k(n-\delta_{k})% + \symvec{e}_{k}(n)  
\end{equation}
where $\delta_{k}=f_s\tau_{k}$ is the relative delay of source $k$ between the channels and $\gamma_{k}$ is the relative attenuation factor corresponding to the ratio of attenuation of source $k$ between the channels and% $\symvec{e}_k(n)$ is independent white Gaussian noise. 
We can express the mixtures of eq.~\ref{eq:sk} and~\ref{eq:sk2} in frequency domain as,
\begin{equation}
 	S_1(\omega) = \sum_{n=1}^N \sum_{k=1}^K \symvec{s}_k(n)e^{-j\omega l n}, 
 \end{equation}
\begin{equation}
 	S_2(\omega) = \sum_{n=1}^N \sum_{k=1}^K \gamma_k \symvec{s}_k(n)e^{-j\omega l n \delta_k}, 
 \end{equation}
with $\omega=\frac{2\pi}{N}$ and $l = {0,\cdots,N-1}$. 
All panning parameters $\gamma_k$ and $\delta_k$ that are associated with each frequency point can be computed as,
\begin{equation}
 	(\hat{\boldsymbol{\gamma}}, \hat{\boldsymbol{\delta}}) = \left(\left|\frac{S_2(\omega)}{S_1(\omega)}\right|, \frac{1}{\omega} \angle \frac{S_2(\omega)}{S_1(\omega)}\right) 
 \end{equation} 
to avoid phase ambiguity we must ensure that $|\omega_{\textup{max}}\delta_{\textup{max}}|<\pi$. 
The ratio of the frequency representations of the mixtures does not depend on the source components, but only on the panning parameters
associated with the active source component, as long as only one source is active at each frequency point $\omega$ that we extract. The so called approximate disjoint orthogonality is expressed as [rickard],
\begin{equation}
	\symvec{s}_i(\omega)\symvec{s}_j(\omega)\approx0 \quad \forall \omega, i\neq j
\end{equation}
The assumption that the sources are approximately disjoint orthogonal is satisfied by segmentation of the mixture $\symmat{x}$ into segments that each gives optimal separation of the clustered ratios. The optimal segmentation is described in Section~\ref{sec:segmentation}.  
% where $X_i(\omega)$ is the discrete Fourier Transform (DFT) of the $i^{\textup{th}}$ source $s_i(n)$ defined as
% \begin{equation}
%  	X_i(\omega) = \sum_{n=1}^N W(n) s_i(n)e^{-j\frac{2\pi}{N}k}, \quad \quad k = {0,\cdots,N-1}
%  \end{equation} 
% Where $W(n)$ is the windowing function. 
Music productions have a long duration often several minutes. Furthermore, it reasonable to assume that they have stationary panning parameters. It is desired to collect a greater amount of data and select only parts of this data that carries relevant information. Therefore we define an indicator function $b(\omega)$, that removes great part of the noise in the spectrum and lowers computional complexity,
\begin{equation}
	b(\omega)  	
	\begin{cases}
    	1,    & |X_1(\omega)||X_2(\omega)|>|\symvec{X}_1|^T|\symvec{X}_2|/N)\\
    	0,    & \text{otherwise}
	\end{cases}	
\end{equation}  
It is possible to pick a specific amount of samples by increasing the threshhold and improve on complexity.

%Based on this assumption we exploit the duration by clustering of spectral features which reflect the panning parameters of the mixtures. In order extract useful information, an optimal segmentation scheme is applied to the mixtures based on the MMDL criteria by [Mario].   


% section signal_model (end)
