function out = GenerateStereoSignal(in,angle,delay)
% Function used to generate a stereo signal from a mono signal, 
% by applying amplitude and delay panning. 
%
% Input: 
% in    Input signal (assumed real)
% angle Angle (degrees) used to calculate gains for amplitude panning
% delay Vector swith delays (in samples) for each channel
% 
% Output:
% out   Stereophonic output signal
% 
% Example: out = GenerateStereoSignal(in,30,[0 80])
% 
% Martin Weiss Hansen 2016

if size(in,2) > size(in,1)
    in = in.';
end

inputDelayed = [[zeros(delay(1),1); in(1:end-delay(1))] [zeros(delay(2),1); in(1:end-delay(2))]];

out = inputDelayed*diag([cosd(angle) sind(angle)]);
